﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parsanje
{
    class BitiInMaske
    {
        public int Biti { set; get; }
        public int Maska { set; get; }
        public int Stevilo1 { set; get; }
        public int Stevilo2 { set; get; }

        public BitiInMaske(int Biti, int Maska, int Stevilo1, int Stevilo2)
        {
            this.Biti = Biti;
            this.Maska = Maska;
            this.Stevilo1 = Stevilo1;
            this.Stevilo2 = Stevilo2;
        }

        //metoda ki preveri ali je koeficient AC v razponu števil ki ga določajo biti
        public bool isInRange(int stevilo)
        {
            if (stevilo >= Stevilo1 && stevilo <= Stevilo2)
            {
                return true;
            }
            return false;
        }
    }
}