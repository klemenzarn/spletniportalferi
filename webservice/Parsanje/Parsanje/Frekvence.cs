﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parsanje
{
    class Frekvence
    {
        public double Red { get; set; }
        public double Green { set; get; }
        public double Blue { set; get; }
        public int X { set; get; }
        public int Y { set; get; }

        public Frekvence()
        {
            this.Red = 0;
            this.Green = 0;
            this.Blue = 0;
            this.X = 0;
            this.Y = 0;
        }

        public Frekvence(double red, double green, double blue)
        {
            this.Red = red;
            this.Blue = blue;
            this.Green = green;
            this.X = 0;
            this.Y = 0;
        }

        public Frekvence(int x, int y, double red, double green, double blue)
        {
            this.Red = red;
            this.Blue = blue;
            this.Green = green;
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return "X: " + this.X + ", Y: " + this.Y + ", Red: " + this.Red + ", Green: " + this.Green + ", Blue: " + this.Blue;
        }
    }
}