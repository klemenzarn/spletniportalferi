﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using HtmlAgilityPack;
using System.Text;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Xml;
using LemmaSharp;
using System.Drawing;
using System.IO;
using System.Threading;
using System.IO.Compression;
using System.Collections;

namespace Parsanje
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        private String zamenjajSumnike(String s)
        {
            return s.Replace("&#x161;", "š").Replace("&#269;", "č").Replace("&#382;", "ž").Replace("&#x17D;", "Ž");
        }
        private String strcon = "Server=localhost;Database=feri;Uid=keko;Pwd=keko;";
        private String oglasi_con = "Server=localhost;Database=ppj;Uid=keko;Pwd=keko;";


        [WebMethod]
        public String ShraniSliko(String user_name, String base64){
            String replaced_base64 = base64.Replace("data:image/png;base64,", "");
            byte[] imageBytes = Convert.FromBase64String(replaced_base64);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap)Image.FromStream(ms);
            //če je kak piksel transparenten mu moramo nastaviti color na white, drugače v formatu
            //jpeg se transparenten piksel obarva črno...
            /*for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    Color temp = bitmap.GetPixel(i, j);
                    if (temp.A != 255)
                    {
                        bitmap.SetPixel(i, j, Color.White);
                    }
                }
            }*/
            long ime_datoteke = DateTime.Now.Ticks;
            if (!Directory.Exists(@"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\"))
            {
                DirectoryInfo di = Directory.CreateDirectory(@"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\");
            }
            string uploadPath = @"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\" + ime_datoteke.ToString() + ".png";
            ms.Close();

            // convert to image first and store it to disk
            using (MemoryStream mOutput = new MemoryStream())
            {
                bitmap.Save(mOutput, System.Drawing.Imaging.ImageFormat.Png);
                using (FileStream fs = File.Create(uploadPath))
                using (BinaryWriter bw = new BinaryWriter(fs))
                bw.Write(mOutput.ToArray());
            }
            DCT(bitmap, user_name, ime_datoteke);
            return "OK";
        }

        [WebMethod]
        public List<String> getVseSlike(String user_name)
        {
            izracunajKonstante();
            napolniBiteInMaske();
            List<String> output = new List<String>();
            string[] imenaDatotek = null;
            try
            {
                imenaDatotek = Directory.GetFiles(@"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct", "*.bin");
            } catch(Exception ex){
                return output;
            }
            foreach(string ime in imenaDatotek){
                f1 = new int[(N + 1) * (N + 1)];
                f2 = new int[(N + 1) * (N + 1)];
                f3 = new int[(N + 1) * (N + 1)];
                dekodiranje = new List<int>();
                String pot = ime;
                FileInfo fi = new FileInfo(pot);

                using (FileStream fileStream = new FileStream(pot, FileMode.Open))
                {
                    using (BinaryReader binaryreader = new BinaryReader(new GZipStream(fileStream, CompressionMode.Decompress)))
                    {
                        //napolnemo list bitov iz binarne datoteke
                        while (true)
                        {
                            try
                            {
                                BitArray b = new BitArray(new byte[] { binaryreader.ReadByte() });
                                int[] biti = new int[8];
                                for (int i = 7; i >= 0; i--)
                                {
                                    if (b[i] == true)
                                    {
                                        biti[i] = 1;
                                    }
                                    else
                                    {
                                        biti[i] = 0;
                                    }
                                }
                                Array.Reverse(biti);
                                dekodiranje.AddRange(biti);
                            }
                            catch (Exception ex)
                            {
                                break;
                            }
                        }
                    }
                }
                dekodiranjeBinarneSlike();
                output.Add(IDCT());
            }
            return output;
        }

        private String IDCT()
        {
            if (frekvence != null)
            {
                int prva = dolzina_slike;
                int druga = visina_slike;
                Bitmap nova_slika = new Bitmap(prva, druga);
                double dolzina = (double)prva / 8;
                double visina = (double)druga / 8;
                int razlika1 = (int)Math.Ceiling(dolzina);
                int razlika2 = (int)Math.Ceiling(visina);
                double red, green, blue;
                for (int i = 0; i < razlika1; i++)
                {
                    for (int j = 0; j < razlika2; j++)
                    {
                        //izračunamo po formuli
                        for (int x = 0; x < 8; x++)
                        {
                            for (int y = 0; y < 8; y++)
                            {
                                if (i * 8 + x < prva && j * 8 + y < druga)
                                {
                                    red = 0;
                                    green = 0;
                                    blue = 0;
                                    //seštejemo use pixle v bloku (barve)
                                    for (int u = 0; u < 8; u++)
                                    {
                                        for (int v = 0; v < 8; v++)
                                        {
                                            red += frekvence[i * 8 + u, j * 8 + v].Red * Cos[x, u] * Cos[y, v] * C[u] * C[v];
                                            green += frekvence[i * 8 + u, j * 8 + v].Green * Cos[x, u] * Cos[y, v] * C[u] * C[v];
                                            blue += frekvence[i * 8 + u, j * 8 + v].Blue * Cos[x, u] * Cos[y, v] * C[u] * C[v];
                                        }
                                    }
                                    //izračunamo po formuli
                                    red = Math.Ceiling(red * 0.25 + 128);
                                    green = Math.Ceiling(green * 0.25 + 128);
                                    blue = Math.Ceiling(blue * 0.25 + 128);
                                    if (red > 255)
                                    {
                                        red = 255;
                                    }
                                    if (green > 255)
                                    {
                                        green = 255;
                                    }
                                    if (blue > 255)
                                    {
                                        blue = 255;
                                    }
                                    if (red < 0)
                                    {
                                        red = 0;
                                    }
                                    if (green < 0)
                                    {
                                        green = 0;
                                    }
                                    if (blue < 0)
                                    {
                                        blue = 0;
                                    }
                                    nova_slika.SetPixel(i * 8 + x, j * 8 + y, Color.FromArgb(Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue)));
                                    //    izpisi(x+", "+y+": " + red[x, y]+", "+green[x,y]+", "+blue[x,y]);
                                    //frekvence[i * 8 + x, j * 8 + y] = new Frekvence(i * 8 + x, j * 8 + y, red[x, y], green[x, y], blue[x, y]);
                                }
                            }
                        }
                    }
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    nova_slika.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] imageBytes = ms.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    return "data:image/png;base64," + base64String;
                }
            }
            else
            {
                //izpisi("Najprej opravi kodiranje slike!");
                return null;
            }
        }

        //dekodira binarno sliko
        private void dekodiranjeBinarneSlike()
        {
            offset = 0;
            R = new int[64];
            G = new int[64];
            B = new int[64];

            int[] temp_d = new int[16];
            int index = 0;

            //dobimo visino in sirino od prvih štirih zlogov v binarni datoteki
            for (int i = 0; i < 16; i++)
            {
                temp_d[index] = dekodiranje[i];
                index++;
            }
            dolzina_slike = convertBinToDec(temp_d);
            index = 0;
            for (int i = 16; i < 32; i++)
            {
                temp_d[index] = dekodiranje[i];
                index++;
            }
            visina_slike = convertBinToDec(temp_d);

            double dol_del = (double)dolzina_slike / 8;
            double vis_del = (double)visina_slike / 8;
            int sirina_stolpci = (int)Math.Ceiling(dol_del);
            int visina_stolpci = (int)Math.Ceiling(vis_del);
            frekvence = new Frekvence[sirina_stolpci * 8, visina_stolpci * 8];
            offset = 32;

            //vsak blok po sebej dekodiramo
            for (int b = 0; b < sirina_stolpci; b++)
            {
                for (int c = 0; c < visina_stolpci; c++)
                {
                    //in seveda dekodiramo usak kanal posebaj
                    R = dekodirajBlok();
                    G = dekodirajBlok();
                    B = dekodirajBlok();

                    //naredimo inverzni cik cak
                    invCikCak(R, G, B);

                    //nato pa zapišemo vse v naš list ki vsebuje primerke razreda Frekvence
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            frekvence[b * 8 + i, c * 8 + j] = new Frekvence(b * 8 + i, c * 8 + j, rdeca[i, j], zelena[i, j], modra[i, j]);
                        }
                    }
                }
            }
        }

        //inverzni cik cak. Polje enodemenzionalne dolžine napolni v dvodemenzionalno polje po principu cik cak
        private void invCikCak(int[] R, int[] G, int[] B)
        {
            int OVERFLOW = 2033;
            int N = 7;
            double[,] temp = new double[8, 8];
            int x = 0, y = 0;
            int index = 0;
            do
            {
                rdeca[y, x] = R[index];
                zelena[y, x] = G[index];
                modra[y, x] = B[index];
                temp[y, x] = OVERFLOW;
                if ((x > 0) && (y < N) && ((int)temp[y + 1, x - 1] < OVERFLOW)) // lahko gre levo dol
                {
                    x--;
                    y++;
                }
                else
                {
                    if ((x < N) && (y > 0) && ((int)temp[y - 1, x + 1] < OVERFLOW)) // lahko gre desno gor
                    {
                        x++;
                        y--;
                    }
                    else if ((x > 0) && (x < N)) // lahko gre desno in ni v 1. stolpcu
                        x++;
                    else if ((y > 0) && (y < N)) // lahko gre dol in ni v 1. vrstici
                        y++;
                    else if (x < N) // lahko gre desno (in je v 1. stolpcu)
                        x++;
                }
                index++;
            } while (index < 64);
        }

        //metoda, ki pretvori polje bitov v decimalno število
        private int convertBinToDec(int[] bits)
        {
            int vrednost = 0;
            if (bits[0] == 1) //če je negativno število
            {
                for (int i = 0; i < bits.Length; i++)
                {
                    if (bits[i] == 1) bits[i] = 0;
                    else bits[i] = 1;
                }
                int rezultat = convertBinToDec(bits) + 1;
                return -rezultat;
            }
            else
            {
                int index = 0;
                for (int i = bits.Length - 1; i > 0; i--)
                {
                    if (bits[i] == 1)
                    {
                        vrednost += (int)Math.Pow(2, index);
                    }
                    index++;
                }
            }
            return vrednost;
        }

        //dekodira blok po blok
        private int[] dekodirajBlok()
        {
            int index = 0;
            int[] output = new int[64];
            int DC = 0;
            int index_barva = 0;
            index = 0;
            int vsota_koef = 0;
            int[] DC_bits = new int[11];
            for (int i = offset; i < offset + 11; i++)
            {
                DC_bits[index] = dekodiranje[i];
                index++;
            }
            DC = convertBinToDec(DC_bits);
            output[index_barva] = DC;
            index_barva++;
            offset += 11;
            vsota_koef++;
            //MessageBox.Show(DC.ToString());

            while (vsota_koef < 64)
            {
                int prvi_bit = dekodiranje[offset];
                offset++;
                if (prvi_bit == 1)   //tip C
                {
                    index = 0;
                    int dolzina = 0;
                    int[] dolzina_bits = new int[5];
                    dolzina_bits[0] = 0;
                    for (int i = offset; i < offset + 4; i++)
                    {
                        dolzina_bits[index + 1] = dekodiranje[i];
                        index++;
                    }
                    dolzina = convertBinToDec(dolzina_bits);
                    offset += 4;
                    int vrednost = 0;
                    index = 0;
                    int[] vrednost_biti = new int[dolzina];
                    for (int i = offset; i < offset + dolzina; i++)
                    {
                        vrednost_biti[index] = dekodiranje[i];
                        index++;
                    }
                    vrednost = convertBinToDec(vrednost_biti);
                    output[index_barva] = vrednost;
                    index_barva++;
                    offset += dolzina;
                    vsota_koef++;
                    //MessageBox.Show("tip c");
                }
                else  //tip A ali B
                {
                    int stevilo_nicel = 0;
                    index = 0;
                    int[] nicle_bits = new int[7];
                    nicle_bits[0] = 0;
                    for (int i = offset; i < offset + 6; i++)
                    {
                        nicle_bits[index + 1] = dekodiranje[i];
                        index++;
                    }
                    offset += 6;
                    stevilo_nicel = convertBinToDec(nicle_bits);


                    if (stevilo_nicel == 64 - vsota_koef)
                    {
                        //tip B
                        for (int i = 0; i < stevilo_nicel; i++)
                        {
                            output[index_barva] = 0;
                            index_barva++;
                        }
                        vsota_koef += stevilo_nicel;
                        // MessageBox.Show("tip a ali b");
                    }
                    else
                    {
                        //tip A
                        for (int i = 0; i < stevilo_nicel; i++)
                        {
                            output[index_barva] = 0;
                            index_barva++;
                        }
                        int dolzina = 0;
                        int[] dolzina_bits = new int[5];
                        dolzina_bits[0] = 0;
                        index = 0;
                        for (int i = offset; i < offset + 4; i++)
                        {
                            dolzina_bits[index + 1] = dekodiranje[i];
                            index++;
                        }
                        dolzina = convertBinToDec(dolzina_bits);
                        offset += 4;
                        int vrednost = 0;
                        index = 0;
                        int[] vrednost_biti = new int[dolzina];
                        for (int i = offset; i < offset + dolzina; i++)
                        {
                            vrednost_biti[index] = dekodiranje[i];
                            index++;
                        }
                        vrednost = convertBinToDec(vrednost_biti);
                        output[index_barva] = vrednost;
                        index_barva++;
                        offset += dolzina;
                        vsota_koef += stevilo_nicel + 1;
                    }


                    //MessageBox.Show("tip a ali b");

                }
            }
            return output;
        }

        private double[] C = new double[8];
        private double[,] Cos = new double[8, 8];
        int[] f1, f2, f3;
        int dolzina_slike = 0, visina_slike = 0;
        double[,] red = new double[8, 8];
        double[,] green = new double[8, 8];
        double[,] blue = new double[8, 8];
        double[,] rdeca = new double[8, 8];
        double[,] modra = new double[8, 8];
        double[,] zelena = new double[8, 8];
        int offset = 0;
        Frekvence[,] frekvence;
        int N = 7;
        List<int> kodiranje, dekodiranje;
        List<Frekvence> vse = new List<Frekvence>();
        int[] R, G, B;
        List<BitiInMaske> biti_maske = new List<BitiInMaske>();
        Frekvence[,] dekodirano = new Frekvence[8, 8];

        //predhodni izračun konstant kot so Cosinus in C 
        private void izracunajKonstante()
        {
            //C je konstanta... za C[0] = 1/koren2 drugače je 1
            C[0] = 1.0 / Math.Sqrt(2);
            for (int i = 1; i < 8; i++)
            {
                C[i] = 1;
            }
            //tudi naše kosinuse lahko izračunamo prej
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Cos[i, j] = Math.Cos(((2 * i + 1) * j * Math.PI) / 16);
                }
            }
        }

        //napolnemo bite in maske, da bomo lahko kasneje računali koliko bitov zasede Koeficient AC
        private void napolniBiteInMaske()
        {
            biti_maske.Add(new BitiInMaske(2, 3, -2, 1));
            biti_maske.Add(new BitiInMaske(3, 7, -4, 3));
            biti_maske.Add(new BitiInMaske(4, 15, -8, 7));
            biti_maske.Add(new BitiInMaske(5, 31, -16, 15));
            biti_maske.Add(new BitiInMaske(6, 63, -32, 31));
            biti_maske.Add(new BitiInMaske(7, 127, -64, 63));
            biti_maske.Add(new BitiInMaske(8, 255, -128, 127));
            biti_maske.Add(new BitiInMaske(9, 511, -256, 255));
            biti_maske.Add(new BitiInMaske(10, 1023, -512, 511));
            biti_maske.Add(new BitiInMaske(11, 2047, -1024, 1023));
            biti_maske.Add(new BitiInMaske(12, 4095, -2048, 2047));
        }

        public void DCT(Image slika1, String user_name, long ime_datoteke)
        {
            izracunajKonstante();
            f1 = new int[(N + 1) * (N + 1)];
            f2 = new int[(N + 1) * (N + 1)];
            f3 = new int[(N + 1) * (N + 1)];
            napolniBiteInMaske();

            f1 = new int[(N + 1) * (N + 1)];
            f2 = new int[(N + 1) * (N + 1)];
            f3 = new int[(N + 1) * (N + 1)];
            kodiranje = new List<int>();
            string temp = Convert.ToString(slika1.Width, 2);
            int[] bits = temp.PadLeft(16, '0').Select(c => int.Parse(c.ToString())).ToArray();
            kodiranje.AddRange(bits);
            string temp1 = Convert.ToString(slika1.Height, 2);
            int[] bits1 = temp1.PadLeft(16, '0').Select(c => int.Parse(c.ToString())).ToArray();
            kodiranje.AddRange(bits1);
            double dolzina = (double)slika1.Width / 8;
            double visina = (double)slika1.Height / 8;
            int razlika1 = (int)Math.Ceiling(dolzina);
            int razlika2 = (int)Math.Ceiling(visina);
            int stevilo_blokov = razlika1 * razlika2;
            FDCT(razlika1, razlika2, slika1);
            if (!Directory.Exists(@"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\"))
            {
                DirectoryInfo di = Directory.CreateDirectory(@"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\");
            }
            String fileName = @"C:\xampp\htdocs\feri\app\webroot\img\Server\" + user_name + "\\dct\\" + ime_datoteke.ToString() + ".bin";
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create)) // destiny file directory.
            {
                using (BinaryWriter binaryWriter = new BinaryWriter(new GZipStream(fileStream, CompressionMode.Compress)))
                {
                    int dolzina1 = kodiranje.Count;
                    if (dolzina % 8 != 0)
                    {
                        int sprememba = izracunDelitelja(dolzina1);
                        for (int i = 0; i < sprememba; i++)
                        {
                            kodiranje.Add(0);
                        }
                    }
                    int stevec_bajtov = 0;
                    for (int i = 0; i < kodiranje.Count; i = i + 8)
                    {
                        int[] temp_array = { kodiranje[i], kodiranje[i + 1], kodiranje[i + 2], kodiranje[i + 3], kodiranje[i + 4], kodiranje[i + 5], kodiranje[i + 6], kodiranje[i + 7] };
                        byte temp12 = convertBitsToByte(temp_array);
                        binaryWriter.Write(temp12);
                        stevec_bajtov++;
                    }
                    //MessageBox.Show(stevec_bajtov.ToString());
                    binaryWriter.Close();
                }
            }
        }

        private void FDCT(int st_vrstic, int st_stolpcev, Image slika1)
        {
            int amplituda = 128;
            Bitmap bitmap = (Bitmap)slika1;
            Color[,] barve = new Color[st_vrstic * 8, st_stolpcev * 8];
            frekvence = new Frekvence[st_vrstic * 8, st_stolpcev * 8];

            //napolnemo barve že prej, tako da je algoritem hitrejši...
            for (int i = 0; i < st_vrstic * 8; i++)
            {
                for (int j = 0; j < st_stolpcev * 8; j++)
                {
                    if (i >= slika1.Width || j >= slika1.Height)
                    {
                        barve[i, j] = Color.White;
                    }
                    else
                    {
                        barve[i, j] = bitmap.GetPixel(i, j);
                    }
                }
            }

            //računamo
            for (int i = 0; i < st_vrstic; i++)
            {
                for (int j = 0; j < st_stolpcev; j++)
                {
                    //izračunamo po formuli
                    for (int u = 0; u < 8; u++)
                    {
                        for (int v = 0; v < 8; v++)
                        {
                            red[u, v] = 0;
                            green[u, v] = 0;
                            blue[u, v] = 0;
                            //seštejemo use pixle v bloku (barve) in upoštevamo faktor stiskanja
                            if (12 < 15 - v - u)
                            {
                                for (int x = 0; x < 8; x++)
                                {
                                    for (int y = 0; y < 8; y++)
                                    {
                                        red[u, v] += (barve[i * 8 + x, j * 8 + y].R - amplituda) * Cos[x, u] * Cos[y, v];
                                        green[u, v] += (barve[i * 8 + x, j * 8 + y].G - amplituda) * Cos[x, u] * Cos[y, v];
                                        blue[u, v] += (barve[i * 8 + x, j * 8 + y].B - amplituda) * Cos[x, u] * Cos[y, v];
                                    }
                                }
                                //izračunamo po formuli
                                red[u, v] *= C[u] * C[v] * 0.25;
                                green[u, v] *= C[u] * C[v] * 0.25;
                                blue[u, v] *= C[u] * C[v] * 0.25;
                            }
                            frekvence[i * 8 + u, j * 8 + v] = new Frekvence(i * 8 + u, j * 8 + v, red[u, v], green[u, v], blue[u, v]);
                        }
                    }
                    cikCak();
                    kodirajBlok(f1);
                    kodirajBlok(f2);
                    kodirajBlok(f3);
                }
            }
        }

        private void cikCak()
        {
            int OVERFLOW = 2033;
            int N = 7;
            bool konec = false;
            int x = 0, y = 0;
            int index = 0;
            do
            {
                f1[index] = (int)red[y, x];
                f2[index] = (int)green[y, x];
                f3[index] = (int)blue[y, x];
                red[y, x] = OVERFLOW;
                green[y, x] = OVERFLOW;
                blue[y, x] = OVERFLOW;
                if ((x > 0) && (y < N) && ((int)red[y + 1, x - 1] < OVERFLOW)) // lahko gre levo dol
                {
                    x--;
                    y++;
                }
                else
                {
                    if ((x < N) && (y > 0) && ((int)red[y - 1, x + 1] < OVERFLOW)) // lahko gre desno gor
                    {
                        x++;
                        y--;
                    }
                    else if ((x > 0) && (x < N)) // lahko gre desno in ni v 1. stolpcu
                        x++;
                    else if ((y > 0) && (y < N)) // lahko gre dol in ni v 1. vrstici
                        y++;
                    else if (x < N) // lahko gre desno (in je v 1. stolpcu)
                        x++;
                    else konec = true;
                }
                index++;
            } while (konec == false);
        }

        //pretvorimo bajte v podatkovni tip byte
        public byte convertBitsToByte(int[] bits)
        {
            byte vrednost = 0;
            int index = 0;
            for (int i = 7; i >= 0; i--)
            {
                if (bits[i] == 1)
                {
                    vrednost += (byte)Math.Pow(2, index);
                }
                index++;
            }
            return vrednost;
        }

        //funkcija pretvori signed int v bite, odvisno od vhodnih parametrov, kot so število bitov in maska...
        private int[] convertIntegerToBitsArray(int number, int bit_num, int mask)
        {
            string temp = Convert.ToString(number & mask, 2);
            int[] bits = temp.PadLeft(bit_num, '0').Select(c => int.Parse(c.ToString())).ToArray();
            return bits;
        }

        //izračunamo s koliko biti lahko zakodiramo AC koeficient...
        private int izracunDolzineBitov(int ac)
        {
            int index_bitov = 0;
            //izračunamo v katerem razponu bitov je koeficient AC. od 1 - 12 napirmer -71 potrebuje 8 bitov in ne 12 zato to zračunamo
            for (int i = 0; i < biti_maske.Count; i++)
            {
                if (biti_maske[i].isInRange(ac))
                {
                    index_bitov = i;
                    break;
                }
            }
            return index_bitov;
        }

        //kodiramo pozamezen blok po cik caku
        private void kodirajBlok(int[] freq)
        {
            int DC = freq[0];
            int[] bits = convertIntegerToBitsArray(DC, 11, 2047);
            kodiranje.AddRange(bits);

            int index = 1;
            int ac = 0;
            int stevilo_nicel = 0;
            while (index < 64)
            {
                if (freq[index] != 0) //če je koeficient različen od nič gre za tip c
                {
                    ac = freq[index];
                    //MessageBox.Show(ac.ToString());
                    kodiranje.Add(1); //zapišemo da gre za neničelni koeficient
                    int index_bitov = izracunDolzineBitov(ac);
                    int[] dolzina = convertIntegerToBitsArray(biti_maske[index_bitov].Biti, 4, 15); //pretvorimo dolžino AC-ja (4 biti)
                    //MessageBox.Show(biti_maske[index_bitov].Biti.ToString());
                    kodiranje.AddRange(dolzina);
                    int[] ac_bits = convertIntegerToBitsArray(ac, biti_maske[index_bitov].Biti, biti_maske[index_bitov].Maska); //pretvorimo AC koeficient v bite
                    kodiranje.AddRange(ac_bits);

                    //MessageBox.Show(ac.ToString() + " je v " + (index_bitov +2));
                }
                else //drugače pa kodiramo po tipu a ali b
                {
                    stevilo_nicel++;
                    //če smo na koncu polja potem gre za tip b
                    if (index == 63)
                    {
                        kodiranje.Add(0); //zapišemo da gre za ničelni koeficient torej kodiramo po tipu b
                        int[] tek_dol = convertIntegerToBitsArray(stevilo_nicel, 6, 63);
                        //MessageBox.Show(stevilo_nicel.ToString());
                        kodiranje.AddRange(tek_dol);
                    }
                    else
                    {
                        //preverimo ali je naslednji element v polju nula
                        if (freq[index + 1] != 0)
                        {
                            //tip a
                            kodiranje.Add(0); //zapišemo da gre za ničelni koeficient torej kodiramo po tipu a
                            int[] tekoca_dolzina = convertIntegerToBitsArray(stevilo_nicel, 6, 63);
                            kodiranje.AddRange(tekoca_dolzina);
                            int ac_next = freq[index + 1];

                            int index_bitov = izracunDolzineBitov(ac_next);
                            int[] dolzina = convertIntegerToBitsArray(biti_maske[index_bitov].Biti, 4, 15); //pretvorimo dolžino AC-ja (4 biti)
                            kodiranje.AddRange(dolzina);
                            int[] ac_bits = convertIntegerToBitsArray(ac_next, biti_maske[index_bitov].Biti, biti_maske[index_bitov].Maska); //pretvorimo AC koeficient v bite
                            kodiranje.AddRange(ac_bits);
                            stevilo_nicel = 0;
                            index++;
                            //stevilo_nicel = 0;
                            /*MessageBox.Show(ac_next.ToString() + " je v " + (index_bitov + 2));
                            MessageBox.Show(ac_next.ToString() + " je v " + (index_bitov + 2));*/
                        }
                    }
                    //MessageBox.Show(ac.ToString());
                }
                index++;
            }
        }

        private int izracunDelitelja(int dolzina)
        {
            int output = 1;
            while (true)
            {
                if ((dolzina + output) % 8 == 0)
                {
                    break;
                }
                else
                {
                    output++;
                }
            }
            return output;
        }

        [WebMethod]
        public List<String> isci(String vir, String niz)
        {
            List<String> output = new List<String>();
            string connect_s = strcon;
            MySqlConnection conn = new MySqlConnection(connect_s);
            conn.Open();
            string sql_stavek = "";
            if (vir.Equals("vse"))
            {
                if (niz.Equals("vse"))
                    sql_stavek = "SELECT id, naslov, link, vsebina, vir FROM drugenovice";
                else
                    sql_stavek = "SELECT id, naslov, link, vsebina, vir FROM drugenovice WHERE naslov LIKE \"%" + niz + "%\" OR vsebina LIKE \"%" + niz + "%\"";
            }
            else
            {
                if(niz.Equals("vse"))
                    sql_stavek = "SELECT id, naslov, link, vsebina, vir FROM drugenovice WHERE vir = \""+vir+"\"";
                else
                    sql_stavek = "SELECT id, naslov, link, vsebina, vir FROM drugenovice WHERE (naslov LIKE \"%" + niz + "%\" OR vsebina LIKE \"%" + niz + "%\") AND vir = \"" + vir + "\"";
            }
            MySqlCommand mysql_com = conn.CreateCommand();
            mysql_com.CommandText = sql_stavek;
            MySqlDataReader reader = mysql_com.ExecuteReader();
            while (reader.Read())
            {
                output.Add(reader["naslov"].ToString());
                output.Add(reader["link"].ToString());
                output.Add(reader["vsebina"].ToString());
                output.Add(reader["vir"].ToString());
            }
            conn.Close();
            return output;
        }


        [WebMethod]
        public List<String> parse()
        {
            String rac_nov = "http://www.racunalniske-novice.com/novice/";
            String slo_tech = "https://slo-tech.com/novice/arhiv/";
            String ur_24 = "http://www.24ur.com/novice/it/";
            List<String> output = new List<String>();
            int stevilo_novic = 6;

            var web = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8,
            };
            HtmlDocument rac_novice_doc = web.Load(rac_nov);


            /*--------------------------------------------------------------------
              RAČUNALNIŠKE NOVICE
             -------------------------------------------------------------------*/
            //racunalniske-novice naslovi
            HtmlNodeCollection rac_nov_naslovi = rac_novice_doc.DocumentNode.SelectNodes("//*[@id=\"newsList\"]/div[3]/div/div[2]/div[1]/div/div/a[1]");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add(rac_nov_naslovi[i].InnerText.Replace("\"", " "));
            }
            //racunalniske-novice linki
            HtmlNodeCollection rac_nov_link = rac_novice_doc.DocumentNode.SelectNodes("//*[@id=\"newsList\"]/div[3]/div/div[2]/div[1]/div/div/a[@href]");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add(rac_nov_link[i].Attributes["href"].Value);
            }
            //racunalniske-novice vsebina
            HtmlNodeCollection rac_nov_vsebina = rac_novice_doc.DocumentNode.SelectNodes("//*[@id=\"newsList\"]/div[3]/div/div[2]/div[1]/div/div/div/a");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add(rac_nov_vsebina[i].InnerText.Replace("\"", " ").Replace("...več", "..."));
            }

            /*--------------------------------------------------------------------
              SLO-TECH
             -------------------------------------------------------------------*/
            HtmlDocument slo_tech_doc = web.Load(slo_tech);

            //slo-tech naslovi
            HtmlNodeCollection slo_tech_naslovi = slo_tech_doc.DocumentNode.SelectNodes("//*[@id=\"content\"]/div/article/header/h3/a");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add(zamenjajSumnike(slo_tech_naslovi[i].InnerText.Replace("\"", " ")));
            }
            //slo-tech linki
            HtmlNodeCollection slo_tech_link = slo_tech_doc.DocumentNode.SelectNodes("//*[@id=\"content\"]/div/article/header/h3/a[@href]");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add("https://slo-tech.com" + slo_tech_link[i].Attributes["href"].Value.Replace("#crta", ""));
            }
            //slo-tech vsebina
            HtmlNodeCollection slo_tech_vsebina = slo_tech_doc.DocumentNode.SelectNodes("//*[@id=\"content\"]/div/article/div[2]");
            for (int i = 0; i < stevilo_novic; i++)
            {
                output.Add(zamenjajSumnike(slo_tech_vsebina[i].InnerText.Replace("\"", " ")).Substring(0, 300) + "...");
            }

            /*--------------------------------------------------------------------
              24-ur IT novice
             -------------------------------------------------------------------*/
            HtmlDocument ur_24_doc = web.Load(ur_24);

            //24-ur naslov prve novice
            HtmlNode prva_novica = ur_24_doc.DocumentNode.SelectSingleNode("//*[@id=\"left\"]/div[1]/div[1]/div/div[1]/h2/a");
            String prva_novica_naslov = prva_novica.InnerText.Replace("\"", " ").Trim();
            output.Add(prva_novica_naslov);

            //24-ur naslovi
            HtmlNodeCollection ur_24_novice = ur_24_doc.DocumentNode.SelectNodes("//*[@id=\"smallnews\"]/div/div/div/div[1]/div[2]/h3/a");
            for (int i = 0; i < stevilo_novic - 1; i++)
            {
                output.Add(ur_24_novice[i].InnerText.Replace("\"", " "));
            }

            //24-ur link prve novice
            HtmlNode prva_novica_link = ur_24_doc.DocumentNode.SelectSingleNode("//*[@id=\"left\"]/div[1]/div[1]/div/div[1]/h2/a[@href]");
            output.Add("http://www.24ur.com" + prva_novica_link.Attributes["href"].Value);

            //24-ur linki novic
            HtmlNodeCollection ur_24_linki = ur_24_doc.DocumentNode.SelectNodes("//*[@id=\"smallnews\"]/div/div/div/div[1]/div[2]/h3/a[@href]");
            for (int i = 0; i < stevilo_novic - 1; i++)
            {
                output.Add("http://www.24ur.com" + ur_24_linki[i].Attributes["href"].Value);
            }

            //24-ur prva vsebina                                                //*[@id=\"left\"]/div[1]/div[1]/div/div[1]
            HtmlNode prva_novica_vsebina = ur_24_doc.DocumentNode.SelectSingleNode("//*[@id=\"left\"]/div[1]/div[1]/div/div[1]");
            string prva_novica_vseb = prva_novica_vsebina.InnerText.Replace("\"", " ").Trim().Substring(prva_novica_naslov.Length+1);
            int indexes = prva_novica_vseb.Trim().IndexOf(" ");
            output.Add(prva_novica_vseb.Substring(indexes));
 
           // output.Add(prva_novica_vseb.Substring(prva_novica_vseb.IndexOf(prva_crka)));

            //24-ur vsebina novic 
            HtmlNodeCollection ur_24_vsebina = ur_24_doc.DocumentNode.SelectNodes("//*[@id=\"smallnews\"]/div/div/div/div[2]/a");
            for (int i = 0; i < stevilo_novic - 1; i++)
            {
                output.Add(ur_24_vsebina[i].InnerText.Replace("\"", " "));
            }

            for (int i = 0; i < output.Count; i++)
            {
                output[i] = output[i].Trim();
            }

            return output;


        }

        //rogač
        [WebMethod]
        public void Posodobi()
        {
            List<string> regije = new List<string>();
            List<string> podrocja = new List<string>();
            List<string> int_regije = new List<string>(); //values
            List<string> int_podrocja = new List<string>(); //values
            List<string> datumi = new List<string>();
            List<string> delodajalec = new List<string>();
            List<string> naslov = new List<string>();
            List<string> opis = new List<string>();
            List<string> podrobno = new List<string>();
            List<string> kraji_dela = new List<string>();
            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();


            HtmlNode.ElementsFlags.Remove("option");
            document = new HtmlWeb().Load("http://www.mojazaposlitev.si/");

            foreach (HtmlNode node in document.DocumentNode.SelectNodes("//select[@id='regije-dela']//option"))
            {
                regije.Add(node.InnerText);
                int_regije.Add(node.Attributes["value"].Value);
            }
            foreach (HtmlNode node in document.DocumentNode.SelectNodes("//select[@id='podrocja']//option"))
            {


                if (node.Attributes["value"].Value == "1" || node.Attributes["value"].Value == "4" || node.Attributes["value"].Value == "7" || node.Attributes["value"].Value == "21" || node.Attributes["value"].Value == "23" || node.Attributes["value"].Value == "28")
                {
                    podrocja.Add(node.InnerText);
                    int_podrocja.Add(node.Attributes["value"].Value);
                }
            }


            string[] numbers;
            string[] kraji;
            string[] datum;
            string izobrazba;
            string placilo;
            string naloge;
            MySqlConnection connection = new MySqlConnection(oglasi_con);
            connection.Open();

            string strSql = "TRUNCATE TABLE oglasi";
            MySqlCommand cmd1 = new MySqlCommand(strSql, connection);
            cmd1.ExecuteNonQuery();

            for (int i = 1; i < regije.Count; i++)
            {
                for (int j = 0; j < podrocja.Count; j++)
                {
                    datumi.Clear();
                    delodajalec.Clear();
                    naslov.Clear();
                    opis.Clear();
                    podrobno.Clear();
                    kraji_dela.Clear();

                    document = new HtmlWeb().Load("http://www.mojazaposlitev.si/prosta-delovna-mesta?_action=submit&keywords=Ključne+besede&podrocja=" + int_podrocja[j] + "&regije=" + int_regije[i] + "&_action=IŠČI");
                    HtmlNode node = document.DocumentNode.SelectSingleNode("//span[@class='jobAdCount']");
                    numbers = Regex.Split(node.InnerText, @"\D");
                    if (Int32.Parse(numbers[1]) > 10)
                    {
                        for (int k = 0; k < Int32.Parse(numbers[1]); k = k + 10)
                        {
                            document = new HtmlWeb().Load("http://www.mojazaposlitev.si/prosta-delovna-mesta/?1=1&o=" + k + "&keywords=Ključne+besede&podrocja=" + int_podrocja[j] + "&regije=" + int_regije[i] + "&_action=IŠČI");
                            foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//p[@class='jobTitle']//a"))
                            {
                                naslov.Add(node1.Attributes["title"].Value);
                                podrobno.Add(node1.Attributes["href"].Value);
                            }
                            foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                            {
                                delodajalec.Add(node1.InnerText);
                                kraji = node1.NextSibling.InnerText.Split(',');
                                kraji_dela.Add(kraji[1]); //, kraj dela
                            }
                            foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//span[@class='date']"))
                                datumi.Add(node1.InnerText);
                            foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                                opis.Add(node1.NextSibling.NextSibling.InnerText.Replace("več>>", ""));
                        }
                        for (int k = 0; k < Int32.Parse(numbers[1]); k++)
                        {
                            document = new HtmlWeb().Load("http://www.mojazaposlitev.si/" + podrobno[k]);
                            HtmlNode node2 = document.DocumentNode.SelectSingleNode("//strong[.='Zahtevana izobrazba:']");
                            try
                            {
                                izobrazba = node2.SelectSingleNode("following-sibling::p[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                izobrazba = "";
                            }
                            HtmlNode node3 = document.DocumentNode.SelectSingleNode("//strong[.='Mesečno plačilo:']");
                            try
                            {
                                placilo = node3.SelectSingleNode("following-sibling::p[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                placilo = "";
                            }

                            HtmlNode node4 = document.DocumentNode.SelectSingleNode("//strong[.='Naloge in odgovornosti:']");
                            try
                            {
                                naloge = node4.SelectSingleNode("following-sibling::pre[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                naloge = "";
                            }

                            datum = datumi[k].Split(':');
                            DateTime dt = new DateTime();
                            bool error = false;
                            try
                            {
                                dt = Convert.ToDateTime(datum[1]);
                            }
                            catch
                            {
                                error = true;
                            }
                            string sql = "INSERT INTO oglasi(regija,int_regija,podrocje,int_podrocje,naslov,datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa,stran)VALUES (@regija,@int_regija,@podrocje,@int_podrocje,@naslov,@datum_objave,@kraj_dela,@opis,@zahtevana_izobrazba,@naloge,@placilo,@delodajalec,@povezava,@stran)";
                            MySqlCommand cmd = new MySqlCommand(sql, connection);
                            cmd.Parameters.AddWithValue("@regija", regije[i]);
                            cmd.Parameters.AddWithValue("@int_regija", Int32.Parse(int_regije[i]));
                            cmd.Parameters.AddWithValue("@podrocje", podrocja[j]);
                            cmd.Parameters.AddWithValue("@int_podrocje", Int32.Parse(int_podrocja[j]));
                            cmd.Parameters.AddWithValue("@naslov", naslov[k]);
                            if (error == false)
                                cmd.Parameters.AddWithValue("@datum_objave", dt.ToString("yyyyMMdd"));
                            else
                                cmd.Parameters.AddWithValue("@datum_objave", "0000-00-00");
                            cmd.Parameters.AddWithValue("@kraj_dela", kraji_dela[k]);
                            cmd.Parameters.AddWithValue("@opis", opis[k]);
                            cmd.Parameters.AddWithValue("@zahtevana_izobrazba", izobrazba);
                            cmd.Parameters.AddWithValue("@naloge", naloge);
                            cmd.Parameters.AddWithValue("@placilo", placilo);
                            cmd.Parameters.AddWithValue("@delodajalec", delodajalec[k]);
                            cmd.Parameters.AddWithValue("@povezava", "http://www.mojazaposlitev.si/" + podrobno[k]);
                            cmd.Parameters.AddWithValue("@stran", "http://www.mojazaposlitev.si");
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception s)
                            {

                            }
                        }
                    }
                    else if (Int32.Parse(numbers[1]) > 0 && Int32.Parse(numbers[1]) <= 10)
                    {
                        foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//p[@class='jobTitle']//a"))
                        {
                            naslov.Add(node1.Attributes["title"].Value);
                            podrobno.Add(node1.Attributes["href"].Value);
                        }
                        foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                        {
                            delodajalec.Add(node1.InnerText);
                            kraji = node1.NextSibling.InnerText.Split(',');
                            kraji_dela.Add(kraji[1]); //, kraj_dela
                        }
                        foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//span[@class='date']"))
                            datumi.Add(node1.InnerText);
                        foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                            opis.Add(node1.NextSibling.NextSibling.InnerText.Replace("več>>", ""));

                        for (int k = 0; k < Int32.Parse(numbers[1]); k++)
                        {

                            document = new HtmlWeb().Load("http://www.mojazaposlitev.si/" + podrobno[k]);
                            //še bolj podrobne podatke
                            HtmlNode node2 = document.DocumentNode.SelectSingleNode("//strong[.='Zahtevana izobrazba:']");
                            try
                            {
                                izobrazba = node2.SelectSingleNode("following-sibling::p[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                izobrazba = "";
                            }
                            HtmlNode node3 = document.DocumentNode.SelectSingleNode("//strong[.='Mesečno plačilo:']");
                            try
                            {
                                placilo = node3.SelectSingleNode("following-sibling::p[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                placilo = "";
                            }
                            HtmlNode node4 = document.DocumentNode.SelectSingleNode("//strong[.='Naloge in odgovornosti:']");
                            try
                            {
                                naloge = node4.SelectSingleNode("following-sibling::pre[1]").InnerText;
                            }
                            catch (NullReferenceException)
                            {
                                naloge = "";
                            }

                            datum = datumi[k].Split(':');
                            DateTime dt = new DateTime();
                            bool error = false;
                            try
                            {
                                dt = Convert.ToDateTime(datum[1]);
                            }
                            catch
                            {
                                error = true;
                            }
                            string sql = "INSERT INTO oglasi(regija,int_regija,podrocje,int_podrocje,naslov,datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa,stran)VALUES (@regija,@int_regija,@podrocje,@int_podrocje,@naslov,@datum_objave,@kraj_dela,@opis,@zahtevana_izobrazba,@naloge,@placilo,@delodajalec,@povezava,@stran)";
                            MySqlCommand cmd = new MySqlCommand(sql, connection);
                            cmd.Parameters.AddWithValue("@regija", regije[i]);
                            cmd.Parameters.AddWithValue("@int_regija", Int32.Parse(int_regije[i]));
                            cmd.Parameters.AddWithValue("@podrocje", podrocja[j]);
                            cmd.Parameters.AddWithValue("@int_podrocje", Int32.Parse(int_podrocja[j]));
                            cmd.Parameters.AddWithValue("@naslov", naslov[k]);
                            if (error == false)
                                cmd.Parameters.AddWithValue("@datum_objave", dt.ToString("yyyyMMdd"));
                            else
                                cmd.Parameters.AddWithValue("@datum_objave", "0000-00-00");
                            cmd.Parameters.AddWithValue("@kraj_dela", kraji_dela[k]);
                            cmd.Parameters.AddWithValue("@opis", opis[k]);
                            cmd.Parameters.AddWithValue("@zahtevana_izobrazba", izobrazba);
                            cmd.Parameters.AddWithValue("@naloge", naloge);
                            cmd.Parameters.AddWithValue("@placilo", placilo);
                            cmd.Parameters.AddWithValue("@delodajalec", delodajalec[k]);
                            cmd.Parameters.AddWithValue("@povezava", "http://www.mojazaposlitev.si/" + podrobno[k]);
                            cmd.Parameters.AddWithValue("@stran", "http://www.mojazaposlitev.si");
                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception s)
                            {

                            }
                        }
                    }
                }
            }
        }

        [WebMethod]
        public List<string> DobiZaPodrocjeRegijo(string regija, string podrocje, string zaht_izobrazba)
        {
            List<string> oglasi = new List<string>();
            List<string> datumi = new List<string>();
            List<string> delodajalec = new List<string>();
            List<string> naslov = new List<string>();
            List<string> opis = new List<string>();
            List<string> podrobno = new List<string>();
            List<string> kraji_dela = new List<string>();
            List<string> izobrazba = new List<string>();
            List<string> placilo = new List<string>();
            List<string> naloge = new List<string>();

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            string[] kraj;
            string[] numbers;
            string[] kraji;
            string[] datum;

            document = new HtmlWeb().Load("http://www.mojazaposlitev.si/prosta-delovna-mesta?_action=submit&keywords=Ključne+besede&podrocja=" + podrocje + "&regije=" + regija + "&_action=IŠČI&izobrazba=" + zaht_izobrazba);
            HtmlNode node = document.DocumentNode.SelectSingleNode("//span[@class='jobAdCount']");
            numbers = Regex.Split(node.InnerText, @"\D");
            if (Int32.Parse(numbers[1]) > 10)
            {
                for (int k = 0; k < Int32.Parse(numbers[1]); k = k + 10)
                {
                    document = new HtmlWeb().Load("http://www.mojazaposlitev.si/prosta-delovna-mesta/?1=1&o=" + k + "&keywords=Ključne+besede&podrocja=" + podrocje + "&regije=" + regija + "&_action=IŠČI&izobrazba=" + zaht_izobrazba);
                    foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//p[@class='jobTitle']//a"))
                    {
                        naslov.Add(node1.Attributes["title"].Value);
                        podrobno.Add(node1.Attributes["href"].Value);
                    }
                    foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                    {
                        delodajalec.Add(node1.InnerText);
                        kraj = node1.NextSibling.InnerText.Split(',');
                        kraji = kraj[1].Split(':');
                        kraji_dela.Add(kraji[1]); //, kraj dela
                    }
                    foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//span[@class='date']"))
                    {
                        datum = node1.InnerText.Split(':');
                        datumi.Add(datum[1]);
                    }
                    foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                        opis.Add(node1.NextSibling.NextSibling.InnerText.Replace("več>>", ""));
                }
                for (int k = 0; k < Int32.Parse(numbers[1]); k++)
                {
                    document = new HtmlWeb().Load("http://www.mojazaposlitev.si/" + podrobno[k]);
                    HtmlNode node2 = document.DocumentNode.SelectSingleNode("//strong[.='Zahtevana izobrazba:']");
                    try
                    {
                        izobrazba.Add(node2.SelectSingleNode("following-sibling::p[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        izobrazba.Add("");
                    }
                    HtmlNode node3 = document.DocumentNode.SelectSingleNode("//strong[.='Mesečno plačilo:']");
                    try
                    {
                        placilo.Add(node3.SelectSingleNode("following-sibling::p[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        placilo.Add("");
                    }

                    HtmlNode node4 = document.DocumentNode.SelectSingleNode("//strong[.='Naloge in odgovornosti:']");
                    try
                    {
                        naloge.Add(node4.SelectSingleNode("following-sibling::pre[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        naloge.Add("");
                    }
                }
            }
            else if (Int32.Parse(numbers[1]) > 0 && Int32.Parse(numbers[1]) <= 10)
            {
                foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//p[@class='jobTitle']//a"))
                {
                    naslov.Add(node1.Attributes["title"].Value);
                    podrobno.Add(node1.Attributes["href"].Value);
                }
                foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                {
                    delodajalec.Add(node1.InnerText);
                    kraj = node1.NextSibling.InnerText.Split(',');
                    kraji = kraj[1].Split(':');
                    kraji_dela.Add(kraji[1]); //, kraj dela
                }
                foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//span[@class='date']"))
                {
                    datum = node1.InnerText.Split(':');
                    datumi.Add(datum[1]);
                }
                foreach (HtmlNode node1 in document.DocumentNode.SelectNodes("//strong"))
                    opis.Add(node1.NextSibling.NextSibling.InnerText.Replace("več>>", ""));

                for (int k = 0; k < Int32.Parse(numbers[1]); k++)
                {

                    document = new HtmlWeb().Load("http://www.mojazaposlitev.si/" + podrobno[k]);
                    //še bolj podrobne podatke
                    HtmlNode node2 = document.DocumentNode.SelectSingleNode("//strong[.='Zahtevana izobrazba:']");
                    try
                    {
                        izobrazba.Add(node2.SelectSingleNode("following-sibling::p[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        izobrazba.Add("");
                    }
                    HtmlNode node3 = document.DocumentNode.SelectSingleNode("//strong[.='Mesečno plačilo:']");
                    try
                    {
                        placilo.Add(node3.SelectSingleNode("following-sibling::p[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        placilo.Add("");
                    }

                    HtmlNode node4 = document.DocumentNode.SelectSingleNode("//strong[.='Naloge in odgovornosti:']");
                    try
                    {
                        naloge.Add(node4.SelectSingleNode("following-sibling::pre[1]").InnerText);
                    }
                    catch (Exception s)
                    {
                        naloge.Add("");
                    }
                }
            }

            for (int i = 0; i < naslov.Count; i++)
            {
                /*
                    & # 2 6 8 ; = Č
                    & # 2 6 9 ; = č
                    & # 3 5 2 ; = Š
                    & # 3 5 3 ; = š
                    & # 3 8 1 ; = Ž
                    & # 3 8 2 ; = ž
                 */

                string neki = naslov[i] + "*" + datumi[i] + "*" + opis[i] + "*" + kraji_dela[i] + "*" + delodajalec[i] + "*" + izobrazba[i] + "*" + naloge[i] + "*" + placilo[i];
                string neki1 = neki.Replace('Č'.ToString(), "&#268");
                string neki2 = neki1.Replace('č'.ToString(), "&#269");
                string neki3 = neki2.Replace('Š'.ToString(), "&#352");
                string neki4 = neki3.Replace('š'.ToString(), "&#353");
                string neki5 = neki4.Replace('Ž'.ToString(), "&#381");
                string neki6 = neki5.Replace('ž'.ToString(), "&#382");
                string neki7 = neki6.Replace('●'.ToString(), "&#9679");
                oglasi.Add(neki7 + "*" + "http://www.mojazaposlitev.si/" + podrobno[i]);
            }
            return oglasi;
        }

        [WebMethod]
        public List<string> DobiZaPodrocjeRegijoIzobrazbo(string regija, string podrocje, string zaht_izobrazba)
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "I.");
            dictionary.Add(2, "II.");
            dictionary.Add(3, "III.");
            dictionary.Add(4, "IV.");
            dictionary.Add(5, "V.");
            dictionary.Add(6, "VI/1.");
            dictionary.Add(7, "VI/2.");
            dictionary.Add(8, "VII.");
            dictionary.Add(9, "VIII/1.");
            dictionary.Add(10, "VIII/2.");

            List<string> oglasi = new List<string>();
            List<string> datumi = new List<string>();
            List<string> delodajalec = new List<string>();
            List<string> naslov = new List<string>();
            List<string> opis = new List<string>();
            List<string> podrobno = new List<string>();
            List<string> kraji_dela = new List<string>();
            List<string> izobrazba = new List<string>();
            List<string> placilo = new List<string>();
            List<string> naloge = new List<string>();

            MySqlConnection connection = new MySqlConnection(oglasi_con);
            connection.Open();

            if (regija == "" && podrocje == "" && zaht_izobrazba == "")
            {
                MySqlCommand cmd = new MySqlCommand("SELECT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi", connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }
            if (regija == "" && podrocje != "" && zaht_izobrazba == "")
            {
                MySqlCommand cmd = new MySqlCommand("SELECT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_podrocje=@podrocje", connection);
                cmd.Parameters.AddWithValue("@podrocje", podrocje);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }

            if (regija != "" && podrocje == "" && zaht_izobrazba == "")
            {
                MySqlCommand cmd = new MySqlCommand("SELECT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_regija=@regija", connection);
                cmd.Parameters.AddWithValue("@regija", regija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }

            if (regija != "" && podrocje != "" && zaht_izobrazba == "")
            {
                MySqlCommand cmd = new MySqlCommand("SELECT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_regija=@regija AND int_podrocje=@podrocje", connection);
                cmd.Parameters.AddWithValue("@regija", regija);
                cmd.Parameters.AddWithValue("@podrocje", podrocje);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }

            if (regija == "" && podrocje == "" && zaht_izobrazba != "")
            {
                string vrednost = dictionary[Int32.Parse(zaht_izobrazba)];
                MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE zahtevana_izobrazba LIKE '" + vrednost + "%'", connection);
                //cmd.Parameters.AddWithValue("@izobrazba", vrednost);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }

            if (regija != "" && podrocje == "" && zaht_izobrazba != "")
            {
                string vrednost = dictionary[Int32.Parse(zaht_izobrazba)];
                MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_regija=@regija AND zahtevana_izobrazba LIKE '" + vrednost + "%'", connection);
                cmd.Parameters.AddWithValue("@regija", regija);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }

            if (regija == "" && podrocje != "" && zaht_izobrazba != "")
            {
                string vrednost = dictionary[Int32.Parse(zaht_izobrazba)];
                MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_podrocje=@podrocje AND zahtevana_izobrazba LIKE '" + vrednost + "%'", connection);
                cmd.Parameters.AddWithValue("@podrocje", podrocje);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }
            if (regija != "" && podrocje != "" && zaht_izobrazba != "")
            {
                string vrednost = dictionary[Int32.Parse(zaht_izobrazba)];
                MySqlCommand cmd = new MySqlCommand("SELECT DISTINCT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi WHERE int_regija=@regija AND int_podrocje=@podrocje AND zahtevana_izobrazba LIKE '" + vrednost + "%'", connection);
                cmd.Parameters.AddWithValue("@regija", regija);
                cmd.Parameters.AddWithValue("@podrocje", podrocje);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    naslov.Add(reader.GetString(0));
                    try
                    {
                        datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                    }
                    catch
                    {
                        datumi.Add("0000-00-00");
                    }
                    kraji_dela.Add(reader.GetString(2));
                    opis.Add(reader.GetString(3));
                    izobrazba.Add(reader.GetString(4));
                    naloge.Add(reader.GetString(5));
                    placilo.Add(reader.GetString(6));
                    delodajalec.Add(reader.GetString(7));
                    podrobno.Add(reader.GetString(8));
                }
            }
            connection.Close();
            for (int i = 0; i < naslov.Count; i++)
            {
                string neki = naslov[i] + "*" + datumi[i] + "*" + opis[i] + "*" + kraji_dela[i] + "*" + delodajalec[i] + "*" + izobrazba[i] + "*" + naloge[i] + "*" + placilo[i] + "*" + podrobno[i];
                oglasi.Add(neki);
            }
            return oglasi;
        }

        [WebMethod]
        public List<string> DobiZadnjeTri()
        {
            List<string> oglasi = new List<string>();
            List<string> datumi = new List<string>();
            List<string> delodajalec = new List<string>();
            List<string> naslov = new List<string>();
            List<string> opis = new List<string>();
            List<string> podrobno = new List<string>();
            List<string> kraji_dela = new List<string>();
            List<string> izobrazba = new List<string>();
            List<string> placilo = new List<string>();
            List<string> naloge = new List<string>();

            MySqlConnection connection = new MySqlConnection(oglasi_con);
            connection.Open();

            MySqlCommand cmd = new MySqlCommand("SELECT naslov, datum_objave,kraj_dela,opis,zahtevana_izobrazba,naloge,placilo,delodajalec,povezava_do_oglasa FROM oglasi ORDER BY datum_objave DESC LIMIT 3", connection);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                naslov.Add(reader.GetString(0));
                try
                {
                    datumi.Add(reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                }
                catch
                {
                    datumi.Add("0000-00-00");
                }
                kraji_dela.Add(reader.GetString(2));
                opis.Add(reader.GetString(3));
                izobrazba.Add(reader.GetString(4));
                naloge.Add(reader.GetString(5));
                placilo.Add(reader.GetString(6));
                delodajalec.Add(reader.GetString(7));
                podrobno.Add(reader.GetString(8));
            }
            for (int i = 0; i < naslov.Count; i++)
            {
                string neki = naslov[i] + "*" + datumi[i] + "*" + opis[i] + "*" + kraji_dela[i] + "*" + delodajalec[i] + "*" + izobrazba[i] + "*" + naloge[i] + "*" + placilo[i] + "*" + podrobno[i];
                oglasi.Add(neki);
            }
            return oglasi;
        }

        //plut
        [WebMethod]
        public List<String> parametrizacija(string vir, string niz)
        {
            MySqlConnection connection = new MySqlConnection(strcon);
            connection.Open();
            string sql = "";
            if (vir != "" && niz == "")
            {
                sql = "SELECT naslov, datum, kraj, izvajalec, vsebina, povezava FROM dogodki WHERE vir='" + vir + "'";
            }
            else if (niz != "" && vir == "")
            {
                sql = "SELECT naslov, datum, kraj, izvajalec, vsebina, povezava FROM dogodki WHERE vsebina LIKE '%" + niz + "%' OR naslov LIKE '%" + niz + "%'";
            }
            else if (vir != "" && niz != "")
            {
                sql = "SELECT naslov, datum, kraj, izvajalec, vsebina, povezava FROM dogodki WHERE vir='" + vir + "' AND (vsebina LIKE '%" + niz + "%' OR naslov LIKE '%" + niz + "%')da";
            }
            else if (vir == "" && niz == "")
            {
                sql = "SELECT naslov, datum, kraj, izvajalec, vsebina, povezava, vir FROM dogodki ORDER BY vir";
            }
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            /*    cmd.Parameters.AddWithValue("@lema", odstranjeni_duplikati[i]);
                cmd.Parameters.AddWithValue("@st_pojavitev", stevec);
                cmd.Parameters.AddWithValue("@odkument", datoteka);*/
            cmd.ExecuteNonQuery();
            MySqlDataReader reader = cmd.ExecuteReader();
            List<String> polje = new List<String>();
            while (reader.Read())
            {
                polje.Add(reader["naslov"].ToString());
                polje.Add(reader["datum"].ToString());
                polje.Add(reader["kraj"].ToString());
                polje.Add(reader["vsebina"].ToString());
                polje.Add(reader["izvajalec"].ToString());
                polje.Add(reader["povezava"].ToString());
            }
            reader.Close();
            connection.Close();
            return polje;
        }
        private String sumniki(String s)
        {
            return s.Replace("&scaron;", "š").Replace("&#268;", "Č").Replace("&#269;", "č").Replace("&#382;", "ž").Replace("&nbsp;", string.Empty);
        }

        [WebMethod]
        public List<String> kc()
        {
            List<String> izhod = new List<String>();
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();

            //ZA KARIRERNI CENTER
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://www.kc.uni-mb.si/dogodki/aktualni");

            List<string> dogodki = new List<string>();
            List<string> naslovi = new List<string>();
            List<string> datum_dogodka = new List<string>();
            List<string> vsebina_dogodka = new List<string>();
            List<string> kraj_dogodka = new List<string>();
            List<string> linki = new List<string>();
            List<string> izvajalec = new List<string>();
            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='maincol']/ol/li"))
            {
                dogodki.Add(li.InnerText);
            }
            for (int i = 1; i <= dogodki.Count; i++)
            {

                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("  //*[@id='maincol']/ol/li[" + i.ToString() + "]/div/a"))
                {

                    linki.Add(li.Attributes["href"].Value.ToString());
                }
            }
            for (int i = 0; i < linki.Count; i++)
            {
                doc = web.Load(linki[i]);
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='maincol']/div/div[4]"))
                {
                    vsebina_dogodka.Add(li.InnerText);
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='maincol']/div/h1"))
                {
                    naslovi.Add(li.InnerText);
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='maincol']/div/p[1]/span[2]"))
                {
                    datum_dogodka.Add(li.InnerText);
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='maincol']/div/p[2]/span[2]"))
                {
                    kraj_dogodka.Add(li.InnerText);
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='maincol']/div/div[2]/span[2]/p"))
                {
                    izvajalec.Add(li.InnerText);
                }


            }
            string nezazeljeno = "Informacije: .(JavaScript mora biti omogočen, če si želite ogledati ta naslov)";
            for (int i = 0; i < linki.Count; i++)
            {
                izhod.Add(naslovi[i] + System.Environment.NewLine);
                izhod.Add(datum_dogodka[i] + System.Environment.NewLine);
                izhod.Add(kraj_dogodka[i] + System.Environment.NewLine);
                izhod.Add(izvajalec[i] + System.Environment.NewLine + System.Environment.NewLine);
                if (vsebina_dogodka[i].Contains(nezazeljeno))
                {
                    izhod.Add(vsebina_dogodka[i].Remove(vsebina_dogodka[i].IndexOf(nezazeljeno)));
                }
                else
                {
                    izhod.Add(vsebina_dogodka[i] + System.Environment.NewLine + System.Environment.NewLine);
                }
                izhod.Add(linki[i]);
            }
            for (int i = 0; i < izhod.Count; i++)
            {
                izhod[i] = izhod[i].Trim();
            }
            return izhod;
        }

        [WebMethod]
        public List<String> ssum()
        {
            List<String> izhod = new List<String>();
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();
            //ZA SSUM
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://ssum.um.si/dogodki/");
            List<string> meseci = new List<string>();
            List<string> naslovi = new List<string>();
            List<string> vsebina = new List<string>();
            List<string> linki = new List<string>();
            List<string> datumi = new List<string>();
            List<string> kraj_dogajanja = new List<string>();

            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//div[1]/div/div[2]/div/div/a"))
            {
                linki.Add(li.Attributes["href"].Value.ToString());
            }
            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("/html/body/div[1]/div/div/div/div/a/h1"))
            {
                naslovi.Add(li.InnerText);
            }
            for (int i = 0; i < linki.Count; i++)
            {
                doc = web.Load(linki[i]);
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='post_content']/div[1]/div[2]/div[2]"))
                {
                    vsebina.Add(li.InnerText);
                }
            }

            // MessageBox.Show(meseci.Count.ToString());
            for (int i = 0; i < linki.Count; i++)
            {
                izhod.Add(naslovi[i] + System.Environment.NewLine);
                izhod.Add(vsebina[i] + System.Environment.NewLine + System.Environment.NewLine);
                izhod.Add(linki[i] + System.Environment.NewLine + System.Environment.NewLine);
            }
            for (int i = 0; i < izhod.Count; i++)
            {
                izhod[i] = izhod[i].Trim();
            }
            return izhod;
        }

        [WebMethod]
        public List<String> epk()
        {
            List<String> izhod = new List<String>();
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();
            //ZA SSUM
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://www.maribor2012.eu/nc/koledar-dogodkov/");
            List<string> vstopnina = new List<string>();
            List<string> naslovi = new List<string>();
            List<string> vsebina = new List<string>();
            List<string> linki = new List<string>();
            List<string> datumi = new List<string>();
            List<string> kraj_dogajanja = new List<string>();
            // /html/body/div[1]/div/div/h2  //*[@id='page_left']/div[1]/div[3]/div[1]/div[2]/h4/a
            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='page_left']/div[1]/div[3]/div/div[2]/h4/a"))
            {
                naslovi.Add(li.InnerText);
                linki.Add(li.Attributes["href"].Value.ToString());
            }

            for (int i = 0; i < naslovi.Count; i++)
            {
                doc = web.Load("http://www.maribor2012.eu/" + linki[i]);
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='page_left']/div/div[1]/p"))
                {
                    vsebina.Add(sumniki(li.InnerText));
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='page_left']/div/div[2]/p"))
                {
                    vstopnina.Add(sumniki(li.InnerText));
                }

                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='page_left']/div/div/h3[text()='Čas dogajanja']/following-sibling::text()"))
                {
                    datumi.Add(sumniki(li.InnerText));
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='page_left']/div/div[2]/h3[text()='Prizorišče']/following-sibling::p"))
                {
                    kraj_dogajanja.Add(sumniki(li.InnerText));
                }
            }
            for (int i = 0; i < linki.Count; i++)
            {
                izhod.Add(naslovi[i]);
                izhod.Add(datumi[i]);
                izhod.Add(kraj_dogajanja[i]);
                izhod.Add(vstopnina[i]);
                izhod.Add(vsebina[i] + System.Environment.NewLine);
                izhod.Add("http://www.maribor2012.eu/" + linki[i]);
            }
            for (int i = 0; i < izhod.Count; i++)
            {
                izhod[i] = izhod[i].Trim();
            }
            return izhod;
        }
        [WebMethod]
        public List<String> stuk()
        {
            List<String> izhod = new List<String>();
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();
            //ZA ŠTUK
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://stuk.org/dogodki");
            List<string> dogodki = new List<string>();
            List<string> naslovi = new List<string>();
            List<string> linki = new List<string>();
            List<string> datumi = new List<string>();
            List<string> ura = new List<string>();
            List<string> kraj_dogajanja = new List<string>();
            List<string> vsebina = new List<string>();
            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='block-system-main']/div/div/div[1]/div"))
            {
                dogodki.Add(sumniki(li.InnerText));
            }

            for (int i = 1; i <= dogodki.Count; i++)
            {

                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("  //*[@id='block-system-main']/div/div/div[1]/div[" + i.ToString() + "]/div[2]/div/span/a"))
                {
                    linki.Add(li.Attributes["href"].Value.ToString());
                }
            }
            for (int i = 1; i <= dogodki.Count; i++)
            {//*[@id="block-system-main"]/div/div/div[1]/div[1]/div[2]/div/span/a
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("  //*[@id='block-system-main']/div/div/div[1]/div[" + i.ToString() + "]/div[2]/div/span/a"))
                {
                    naslovi.Add(sumniki(li.InnerText));
                }
            }

            for (int i = 0; i < linki.Count; i++)
            {
                doc = web.Load("http://stuk.org" + linki[i]);
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='block-views-page-event-block']/div/div/div/div/ul/li[1]/span[2]"))
                {
                    datumi.Add(sumniki(li.InnerText));
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='block-views-page-event-block']/div/div/div/div/ul/li[2]/span[2]"))
                {
                    ura.Add(sumniki(li.InnerText));
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='block-views-page-event-block']/div/div/div/div/ul/li[3]/text()"))
                {
                    kraj_dogajanja.Add(sumniki(li.InnerText));
                }
                foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//div[@class='field-item even']/p[1]"))
                {
                    vsebina.Add(sumniki(li.InnerText));
                }
            }

            for (int i = 0; i < naslovi.Count; i++)
            {
                izhod.Add(naslovi[i] + System.Environment.NewLine);
                izhod.Add(datumi[i] + " " + ura[i] + System.Environment.NewLine);
                izhod.Add(kraj_dogajanja[i] + System.Environment.NewLine + System.Environment.NewLine);
                izhod.Add(vsebina[i] + System.Environment.NewLine + System.Environment.NewLine);
                izhod.Add("http://stuk.org" + linki[i]);
            }
            for (int i = 0; i < izhod.Count; i++)
            {
                izhod[i] = izhod[i].Trim();
            }
            return izhod;
        }
        [WebMethod]

        public List<String> sportni()
        {
            List<String> izhod = new List<String>();
            HtmlAgilityPack.HtmlWeb web = new HtmlWeb();
            //ZA SPORTNE PRIREDITVE
            HtmlAgilityPack.HtmlDocument doc = web.Load("http://www.sz-maribor.com/scaronportne-prireditve.html");
            List<string> naslovi = new List<string>();
            List<string> vsebina = new List<string>();

            foreach (HtmlNode li in doc.DocumentNode.SelectNodes("//*[@id='wsite-content']/h2"))
            {
                naslovi.Add(sumniki(li.InnerText));
            }
            foreach (HtmlNode li in doc.DocumentNode.SelectNodes(" //*[@id='wsite-content']/div[@class='paragraph']"))
            {//*[@id="wsite-content"]/div[6]
                vsebina.Add(sumniki(li.InnerText));
            }
            //MessageBox.Show(vsebina[2]);
            for (int i = 0; i < naslovi.Count; i++)
            {
                izhod.Add(naslovi[i] + System.Environment.NewLine);
                izhod.Add(vsebina[i] + System.Environment.NewLine + System.Environment.NewLine);
            }

            for (int i = 0; i < izhod.Count; i++)
            {
                izhod[i] = izhod[i].Trim();
            }


            return izhod;
        }

        //jure
        MySqlConnection povezi;
        MySqlCommand ukaz;
        MySqlDataReader querry;
        string povezava = "Server=localhost; Port=3306; Database=tv_spored; Uid=keko; Password=keko; charset=utf8;";

        private void PoveziMe()
        {
            try
            {
                povezi = new MySqlConnection(povezava);
                povezi.Open();

            }
            catch (Exception ex) { }
        }

        private void ZapriPovezavo()
        {
            try
            {
                ukaz.Dispose();
                povezi.Close();
            }
            catch (Exception ex) { }
        }
        string url = "http://www.siol.net/tv-spored.aspx";

        private static string MySQLEscape(string str)
        {
            return Regex.Replace(str, @"[\x00'""\b\n\r\t\cZ\\%_]",
                delegate(Match match)
                {
                    string v = match.Value;
                    switch (v)
                    {
                        case "\x00":            // ASCII NUL (0x00) character
                            return "\\0";
                        case "\b":              // BACKSPACE character
                            return "\\b";
                        case "\n":              // NEWLINE (linefeed) character
                            return "\\n";
                        case "\r":              // CARRIAGE RETURN character
                            return "\\r";
                        case "\t":              // TAB
                            return "\\t";
                        case "\u001A":          // Ctrl-Z
                            return "\\Z";
                        default:
                            return "\\" + v;
                    }
                });
        }

        [WebMethod]
        public void dodajPrograme()
        {
            List<string> kurac = new List<string>();
            HtmlAgilityPack.HtmlDocument stran = new HtmlWeb().Load(url);
            HtmlNodeCollection programi = stran.DocumentNode.SelectNodes("//aside/span");
            int stProgramov = programi.Count;
            string urlSlike = "";
            string programiUrl = "";
            string programiNaziv = "";

            PoveziMe();
            for (int i = 0; i < stProgramov; i++)
            {
                try
                {
                    int stOtrok = int.Parse(programi[i].ChildNodes.Count.ToString());
                    if (stOtrok == 2)
                    {

                        programiUrl = programi[i].LastChild.Attributes["href"].Value;
                        programiNaziv = programi[i].LastChild.InnerText;
                        urlSlike = programi[i].FirstChild.FirstChild.Attributes["src"].Value;
                        urlSlike = MySQLEscape(urlSlike);
                    }
                    else
                    {
                        programiUrl = programi[i].LastChild.Attributes["href"].Value;
                        programiNaziv = programi[i].LastChild.InnerText;
                        urlSlike = "NULL";
                    }

                }
                catch (Exception ex)
                {
                    kurac.Add(ex.Message);
                }
                ukaz = povezi.CreateCommand();
                ukaz.CommandText = "INSERT INTO program VALUES (NULL, '" + programiNaziv + "', '" + programiUrl + "', '" + urlSlike + "')";
                ukaz.Connection = povezi;
                ukaz.ExecuteNonQuery();
            }
            ZapriPovezavo();
        }

        [WebMethod]
        //public List<string> dodajOddaje()
        public List<string> dodajOddaje()
        {
            List<int> programId = new List<int>();
            List<string> programUrl = new List<string>();
            List<string> rez = new List<string>();
            string urlStrani = "";

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT id, url_programa FROM program";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                programId.Add(querry.GetInt32("id"));
                programUrl.Add(querry.GetString("url_programa"));
            }
            ZapriPovezavo();

            PoveziMe();
            for (int i = 0; i < programId.Count; i++)
            {
                string programiUrl = url + programUrl[i] + "&p1=1&p3=0&p4=0";
                int stOddaj = 0;

                HtmlAgilityPack.HtmlDocument stranPrograma = new HtmlWeb().Load(programiUrl);
                HtmlNodeCollection oddaje = stranPrograma.DocumentNode.SelectNodes("//div[7]/div/div");
                if (oddaje != null)
                {
                    stOddaj = oddaje.Count;


                    for (int j = 0; j < stOddaj; j++)
                    {

                        MySqlCommand u = new MySqlCommand();
                        string oddajaUrl = url + oddaje[j].LastChild.FirstChild.Attributes["href"].Value; //*[@id="spored"]/p[5]
                        HtmlAgilityPack.HtmlDocument opisOddaj = new HtmlWeb().Load(oddajaUrl);

                        HtmlNodeCollection datum = opisOddaj.DocumentNode.SelectNodes("//*[@id=\"spored\"]/p[1]/text()");// //*[@id="spored"]/p[1]/text()
                        string datumcek = datum[0].InnerText;

                        HtmlNodeCollection opisOddaje = opisOddaj.DocumentNode.SelectNodes("//*[@id=\"spored\"]/p[3]");
                        if (opisOddaje == null)
                        {
                            opisOddaje.Clear();
                            opisOddaje = opisOddaj.DocumentNode.SelectNodes("//*[@id=\"spored\"]/p[5]");

                        }
                        string nazivOddaje = oddaje[j].LastChild.InnerText;
                        nazivOddaje = MySQLEscape(nazivOddaje);
                        string zacetekOddaje = oddaje[j].FirstChild.InnerText;
                        string opis = opisOddaje[0].InnerHtml;
                        opis = MySQLEscape(opis);
                        int idPrograma = programId[i];
                        DateTime dt = DateTime.Parse(datumcek + " " + zacetekOddaje);
                        zacetekOddaje = dt.ToString("yyyy-MM-dd HH:mm:ss");


                        u = povezi.CreateCommand();
                        u.CommandText = "INSERT INTO oddaja VALUES (NULL, '" + nazivOddaje + "', '" + zacetekOddaje + "', '" + opis + "', '" + idPrograma + "')";
                        u.Connection = povezi;
                        u.ExecuteNonQuery();


                        opisOddaje.Clear();
                        datum.Clear();


                    }
                }
            }
            ZapriPovezavo();
            return rez;
        }

        [WebMethod]
        public void posodobiVse()
        {

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "TRUNCATE program";
            ukaz.Connection = povezi;
            ukaz.ExecuteNonQuery();
            ZapriPovezavo();

            dodajPrograme();

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "TRUNCATE oddaja";
            ukaz.Connection = povezi;
            ukaz.ExecuteNonQuery();
            ZapriPovezavo();

            dodajOddaje();

        }

        //MYSQL POIZVEDOVANJA

        [WebMethod]
        public List<string> getVsePrograme()
        {
            List<string> nazivPrograma = new List<string>();

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT naziv_programa FROM program";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                nazivPrograma.Add(querry.GetString("naziv_programa"));
            }

            ZapriPovezavo();

            return nazivPrograma;
        }

        [WebMethod]
        public int getIdPrograma(string naziv)
        {
            int idPrograma = 0;
            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT id FROM program WHERE naziv_programa='" + naziv + "'";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                idPrograma = querry.GetInt32("id");
            }

            ZapriPovezavo();

            return idPrograma;
        }

        [WebMethod]
        public List<string> getOddajePrograma(int id)
        {
            List<string> oddaje = new List<string>();

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT * FROM oddaja WHERE program_id='" + id + "'";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                oddaje.Add(querry.GetString("naziv_oddaje"));
                oddaje.Add(querry.GetDateTime("zacetek_oddaje").ToString("HH:mm"));
                oddaje.Add(querry.GetString("opis_oddaje"));
            }

            ZapriPovezavo();

            return oddaje;
        }

        [WebMethod]
        public string getNazivPrograma(int id)
        {
            string nazivPrograma = "";
            PoveziMe();
            ukaz = povezi.CreateCommand();//SELECT TIME_FORMAT(zacetek_oddaje, '%H:%i'), naziv_oddaje, opis_oddaje FROM oddaja WHERE program_id='1'
            ukaz.CommandText = "SELECT naziv_programa FROM program WHERE id='" + id + "'";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                nazivPrograma = querry.GetString("naziv_programa");
            }

            ZapriPovezavo();

            return nazivPrograma;
        }

        [WebMethod]
        public List<string> najdiOddajo(string nazivOddaje)
        {
            List<string> oddaje = new List<string>();

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT program.naziv_programa, oddaja.naziv_oddaje, oddaja.zacetek_oddaje, oddaja.opis_oddaje FROM oddaja, program WHERE oddaja.naziv_oddaje LIKE '%" + nazivOddaje + "%' AND program.id = oddaja.program_id";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                oddaje.Add(querry.GetString("naziv_oddaje"));
                oddaje.Add(querry.GetDateTime("zacetek_oddaje").ToString());
                oddaje.Add(querry.GetString("opis_oddaje"));
                oddaje.Add(querry.GetString("naziv_programa"));
            }

            ZapriPovezavo();

            return oddaje;
        }

        //SELECT * FROM `oddaja` WHERE REPLACE(`zacetek_oddaje`, ':', '')<0700 GROUP BY program_id LIMIT 2

        [WebMethod]
        public List<string> oddajePoCasu(string cas)
        //public string oddajePoCasu(string cas)
        {
            List<string> oddaje = new List<string>();
            //DateTime dt = DateTime.Parse(cas);
            //cas = dt.ToString("yyyy-MM-dd hh:mm:ss");
            PoveziMe();
            ukaz = povezi.CreateCommand();
            //ukaz.CommandText = "SELECT program.naziv_programa, oddaja.naziv_oddaje, oddaja.zacetek_oddaje, oddaja.opis_oddaje FROM oddaja, program WHERE odddaja.zacetek_oddaje <=" + cas + " AND program.id = oddaja.program_id GROUP BY program.id";
            ukaz.CommandText = "SELECT program.naziv_programa, oddaja.naziv_oddaje, oddaja.zacetek_oddaje, oddaja.opis_oddaje FROM oddaja, program WHERE oddaja.zacetek_oddaje >= '" + cas + "' AND program.id = oddaja.program_id GROUP BY program.id";
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                oddaje.Add(querry.GetString("naziv_oddaje"));
                oddaje.Add(querry.GetDateTime("zacetek_oddaje").ToString());
                oddaje.Add(querry.GetString("opis_oddaje"));
                oddaje.Add(querry.GetString("naziv_programa"));
            }

            ZapriPovezavo();

            return oddaje;

        }


        [WebMethod]
        public List<string> trenutnaOddaja(int id)
        //public string oddajePoCasu(string cas)
        {
            List<string> naslednja = new List<string>();
            List<string> trenutna = new List<string>();
            int idNaslednje = 0;

            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT program.url_slike, program.naziv_programa, oddaja.naziv_oddaje, oddaja.zacetek_oddaje, oddaja.id_oddaje FROM program, oddaja WHERE oddaja.zacetek_oddaje BETWEEN NOW() AND oddaja.zacetek_oddaje AND program.id = oddaja.program_id AND program.id ='" + id + "' LIMIT 1"; // Dobimo naslednjo oddajo
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                idNaslednje = querry.GetInt32("id_oddaje");
                naslednja.Add(querry.GetString("naziv_programa"));
                naslednja.Add(querry.GetString("url_slike"));
                naslednja.Add(querry.GetString("naziv_oddaje"));
                naslednja.Add(querry.GetDateTime("zacetek_oddaje").ToString("HH:mm"));
            }

            ZapriPovezavo();
            int idTrenutna = idNaslednje - 1;
            PoveziMe();
            ukaz = povezi.CreateCommand();
            ukaz.CommandText = "SELECT naziv_oddaje, zacetek_oddaje FROM oddaja WHERE id_oddaje='" + idTrenutna + "'"; // Dobimo trenutno oddajo
            ukaz.Connection = povezi;
            querry = ukaz.ExecuteReader();

            while (querry.Read())
            {
                naslednja.Add(querry.GetString("naziv_oddaje"));
                naslednja.Add(querry.GetDateTime("zacetek_oddaje").ToString("HH:mm"));
            }
            ZapriPovezavo();


            return naslednja;
        }

        //jan
        String xml_fajl = @"C:\Users\klemen\Documents\kino\myXmFile.xml";

        [WebMethod]
        public void dobiPodatke()
        {
            HtmlWeb htmlWeb = new HtmlWeb();
            HtmlAgilityPack.HtmlDocument document = htmlWeb.Load("http://www.kolosej.si/filmi/A-Z/slovensko/");
            HtmlNode someNode = document.DocumentNode.SelectSingleNode("//td[@class='link']//a");

            string niz_naslov = "", niz_url = "", zvrst = "", leto = "", na_sporedu = "", dolzina = "", slika = "", drzava = "", jezik = "", rezija = "", scenarij = "", igrajo = "", vsebina = "", ocena = "";
            foreach (HtmlNode node in document.DocumentNode.SelectNodes("//td[@class='link']//a"))
            {
                niz_naslov += node.InnerText + "*";
                niz_url += node.Attributes["href"].Value + "*";
            }
            string[] seznam_url = niz_url.Split('*');
            string[] seznam_naslov = niz_naslov.Split('*');
            string naslov_org = "";

            HtmlNode podatek, podatek1;
            for (int i = 0; i < seznam_naslov.Length - 1; i++)
            {
                document = htmlWeb.Load("http://www.kolosej.si" + seznam_url[i]);

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='title-orig']");
                if (podatek != null)
                    naslov_org += podatek.InnerText.Trim() + "*";
                else
                    naslov_org += "-" + "*";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='genre']");
                if (podatek != null)
                    zvrst += podatek.InnerText.Trim() + "*";
                else
                    zvrst += "-" + "*";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='year']");
                if (podatek != null)
                    leto += podatek.InnerText.Trim() + ":";
                else
                    leto += "Leto:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='duration']");
                if (podatek != null)
                    dolzina += podatek.InnerText.Trim() + ":";
                else
                    dolzina += "Dolžina:" + "/-min" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//div[@class='movie-image']//img");
                if (podatek != null)
                    slika += podatek.Attributes["src"].Value.Trim() + "*";
                else
                    slika += "-" + "*";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='country']");
                if (podatek != null)
                    drzava += podatek.InnerText.Trim() + ":";
                else
                    drzava += "Država:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='language']");
                if (podatek != null)
                    jezik += podatek.InnerText.Trim() + ":";
                else
                    jezik += "Jezik:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='director']");
                if (podatek != null)
                    rezija += podatek.InnerText.Trim() + ":";
                else
                    rezija += "Režija:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='screenplay']");
                if (podatek != null)
                    scenarij += podatek.InnerText.Trim() + ":";
                else
                    scenarij += "Scenarij:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='actors']");
                if (podatek != null)
                    igrajo += podatek.InnerText.Trim() + ":";
                else
                    igrajo += "Igrajo:" + "-" + ":";

                podatek = document.DocumentNode.SelectSingleNode("//div[@class='summary']");
                if (podatek != null)
                    vsebina += podatek.InnerText.Trim() + "*";
                else
                    vsebina += "-" + "*";

                podatek = document.DocumentNode.SelectSingleNode("//span[@class='date-multiple']");
                podatek1 = document.DocumentNode.SelectSingleNode("//span[@class='date-single']");
                if (podatek != null)
                    na_sporedu += podatek.InnerText.Trim() + "*";
                else if (podatek1 != null)
                    na_sporedu += podatek1.InnerText.Trim() + "*";
                else
                    na_sporedu += "-" + "*";

                podatek = document.DocumentNode.SelectSingleNode("//td[@class='rating']/div[@rel='" + seznam_url[i] + "ocena/ajax/']");
                if (podatek != null)
                    ocena += podatek.Attributes["class"].Value + "*";
                else
                    ocena += "-" + "*";
            }

            string[] seznam_org_naslov = naslov_org.Split('*');
            string[] seznam_zvrst = zvrst.Split('*');
            string[] seznam_let = leto.Split(':');
            string[] seznam_dolzin = dolzina.Split(':');
            string[] seznam_slik = slika.Split('*');
            string[] seznam_drzav = drzava.Split(':');
            string[] seznam_jezikov = jezik.Split(':');
            string[] seznam_rezije = rezija.Split(':');
            string[] seznam_scenarija = scenarij.Split(':');
            string[] seznam_igralcev = igrajo.Split(':');
            string[] seznam_vsebine = vsebina.Split('*');
            string[] seznam_sporeda = na_sporedu.Split('*');
            string[] seznam_ocen = ocena.Split('*');
            int st1 = 1;
            string dolzine = "", dolzine2 = "";
            for (int i = 1; i < seznam_dolzin.Length; i += 2)
            {
                dolzine += seznam_dolzin[i] + '/';
            }
            string[] seznam_dolzine = dolzine.Split('/');
            for (int i = 1; i < seznam_dolzine.Length; i += 2)
                dolzine2 += seznam_dolzine[i].Substring(0, seznam_dolzine[i].IndexOf("min")) + '*';

            string[] seznam_dolzine2 = dolzine2.Split('*');

            string ocena2 = "";
            for (int i = 0; i < seznam_ocen.Length - 1; i++)
            {
                if (seznam_ocen[i].Length == 20)
                    ocena2 += seznam_ocen[i].Substring(19, 1) + "*";
                else
                    ocena2 += "-" + "*";
            }
            string[] seznam_ocen2 = ocena2.Split('*');

            XmlTextWriter textWriter = new XmlTextWriter(xml_fajl, null);
            textWriter.WriteStartDocument();
            textWriter.WriteStartElement("Filmi");
            for (int i = 0; i < seznam_naslov.Length - 1; i++)
            {
                textWriter.WriteStartElement("Film");
                textWriter.WriteElementString("Naslov", seznam_naslov[i]);
                textWriter.WriteElementString("Naslov_org", seznam_org_naslov[i]);
                textWriter.WriteElementString("Zvrst", seznam_zvrst[i]);

                textWriter.WriteElementString("Leto", seznam_let[st1].Replace(" ", string.Empty));

                textWriter.WriteElementString("Dolzina", seznam_dolzine2[i].Replace(" ", string.Empty));
                textWriter.WriteElementString("Ocena", seznam_ocen2[i]);
                if (seznam_slik[i] == "-")
                    textWriter.WriteElementString("Slika", seznam_slik[i]);
                else
                    textWriter.WriteElementString("Slika", "http://www.kolosej.si" + seznam_slik[i]);

                textWriter.WriteElementString("Url", "http://www.kolosej.si" + seznam_url[i]);

                textWriter.WriteElementString("Drzava", seznam_drzav[st1].Replace("\n\t", string.Empty));

                textWriter.WriteElementString("Jezik", seznam_jezikov[st1].Replace("\n\t", string.Empty).Replace(" ", string.Empty));
                textWriter.WriteElementString("Rezija", seznam_rezije[st1].Replace("\n\t", string.Empty));
                textWriter.WriteElementString("Scenarij", seznam_scenarija[st1]);
                textWriter.WriteElementString("Igrajo", seznam_igralcev[st1].Replace("\n\t", string.Empty));
                textWriter.WriteElementString("Vsebina", seznam_vsebine[i]);
                textWriter.WriteElementString("Spored", seznam_sporeda[i].Replace("\n\t", string.Empty));

                textWriter.WriteEndElement();
                st1 += 2;

            }
            textWriter.WriteEndDocument();
            textWriter.Close();
        }

        [WebMethod]
        public string iskanjePoNaslovu(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Naslov='" + Param1 + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }

        [WebMethod]
        public string iskanjePoLetu(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            //XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Leto='" + Param1 + "']");
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Leto='" + Param1 + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }

        [WebMethod]
        public string iskanjePoZvrsti(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Zvrst='" + Param1.ToLower() + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }

        [WebMethod]
        public string iskanjePoJeziku(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Jezik='" + Param1.ToLower() + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }

        [WebMethod]
        public string iskanjePoDrzavi(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Drzava='" + " " + Param1 + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }

        [WebMethod]
        public string iskanjePoOceni(string Param1)
        {
            string polje = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Ocena='" + Param1 + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "Naslov")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Naslov_org")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Zvrst")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Leto")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Ocena")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Jezik")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Dolzina")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Drzava")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Scenarij")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Rezija")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Igrajo")
                            polje += childNode.InnerText + "*";
                        if (childNode.Name == "Vsebina")
                            polje += childNode.InnerText + "*";
                    }
                }
            }
            return polje;
        }


        [WebMethod]
        public string iskanjeTopOcen(string Param1)
        {
            string polje = "";
            int stevec = 0;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_fajl);
            XmlNodeList nodes1 = xmlDoc.SelectNodes("/Filmi/Film[Ocena='" + Param1 + "']");
            if (nodes1 == null)
            {
                polje = "NI PODATKA!";
            }
            else
            {
                foreach (XmlNode node in nodes1)
                {
                    if (stevec < 3)
                    {
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if (childNode.Name == "Naslov")
                                polje += childNode.InnerText + "*";
                            if (childNode.Name == "Naslov_org")
                                polje += childNode.InnerText + "*";
                            if (childNode.Name == "Zvrst")
                                polje += childNode.InnerText + "*";
                            if (childNode.Name == "Url")
                                polje += childNode.InnerText + "*";
                        }
                        stevec++;
                    }
                }
            }
            return polje;
        }

        //lematiziranje op
        List<string> datoteke = new List<string>();
        string leme_string = "";
        List<string> besede = new List<string>();

        [WebMethod]
        public void Lematiziraj(string teme, string vsebina)
        {
            MySqlConnection connection = new MySqlConnection("server=localhost;user id=keko;password=keko;database=feri;");
            connection.Open();
            ILemmatizer lmtz = new LemmatizerPrebuiltFull(LemmaSharp.LanguagePrebuilt.Slovene);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            Regex pattern = new Regex(@"([^\W_\d]([^\W_\d]|[-'\d](?=[^\W_\d|]))*[^\W_\d])", RegexOptions.IgnorePatternWhitespace);
            string prebrano = vsebina;
            foreach (Match m in pattern.Matches(prebrano))
            {
                string lemma;
                lemma = lmtz.Lemmatize(m.Groups[1].Value.ToLower());
                leme_string = leme_string + lemma + ",";
                //LemmatizeOne(m.Groups[1].Value);
            }

            dictionary.Add(teme, leme_string);
            leme_string = "";
            besede.Clear();

            foreach (KeyValuePair<string, string> kvp in dictionary)
            {
                string datoteka = kvp.Key;
                string[] leme = kvp.Value.Split(',');
                string[] odstranjeni_duplikati = leme.Distinct().ToArray();
                int stevec = 0;
                for (int i = 0; i < odstranjeni_duplikati.Count() - 1; i++)
                {
                    for (int j = 0; j < leme.Count(); j++)
                    {
                        if (odstranjeni_duplikati[i] == leme[j])
                            stevec++;

                    }
                    string sql = "INSERT INTO tf(Lema,St_pojavitev,Dokument)VALUES (@lema,@st_pojavitev,@dokument)";
                    MySqlCommand cmd = new MySqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("@lema", odstranjeni_duplikati[i]);
                    cmd.Parameters.AddWithValue("@st_pojavitev", stevec);
                    cmd.Parameters.AddWithValue("@dokument", datoteka);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                    }
                    stevec = 0;
                }
            }
            connection.Close();
            connection.Open();
            List<int> C = new List<int>();
            List<string> Lema = new List<string>();
            MySqlCommand cmd1 = new MySqlCommand("SELECT COUNT(*) as C, Lema FROM tf GROUP BY Lema ORDER BY C Desc", connection);
            MySqlDataReader reader = cmd1.ExecuteReader();
            while (reader.Read())
            {
                C.Add(reader.GetInt32(0));
                Lema.Add(reader.GetString(1));
            }
            connection.Close();
            connection.Open();
            string strSql = "TRUNCATE TABLE df";
            MySqlCommand cmd2 = new MySqlCommand(strSql, connection);
            cmd2.ExecuteNonQuery();

            connection.Close();
            connection.Open();
            string sql1 = "INSERT INTO df(Lema,DF)VALUES (@lema,@DF)";
            for (int i = 0; i < Lema.Count(); i++)
            {
                MySqlCommand cmd3 = new MySqlCommand(sql1, connection);
                cmd3.Parameters.AddWithValue("@lema", Lema[i]);
                cmd3.Parameters.AddWithValue("@DF", C[i]);
                cmd3.ExecuteNonQuery();
            }
            connection.Close();
        }

        /*   public void LemmatizeOne(string beseda)
           {
               ILemmatizer lmtz = new LemmatizerPrebuiltFull(LemmaSharp.LanguagePrebuilt.Slovene);
               string lemma;
               lemma = lmtz.Lemmatize(beseda.ToLower());
               leme_string = leme_string + lemma + ",";
           }*/

        [WebMethod]
        public string LemmatizeTwo(string iskalni_kljuc)
        {
            Regex pattern = new Regex(@"([^\W_\d]([^\W_\d]|[-'\d](?=[^\W_\d|]))*[^\W_\d])", RegexOptions.IgnorePatternWhitespace);
            ILemmatizer lmtz = new LemmatizerPrebuiltFull(LemmaSharp.LanguagePrebuilt.Slovene);
            string lemma;
            string leme_kljuc = "";
            foreach (Match m in pattern.Matches(iskalni_kljuc))
            {
                lemma = lmtz.Lemmatize(m.Groups[1].Value.ToLower());
                leme_kljuc = leme_kljuc + "'" + lemma + "',";
            }
            return leme_kljuc;
        }
    }
}