-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 06. feb 2014 ob 22.19
-- Različica strežnika: 5.5.32
-- Različica PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Zbirka podatkov: `feri`
--
CREATE DATABASE IF NOT EXISTS `feri` DEFAULT CHARACTER SET utf8 COLLATE utf8_slovenian_ci;
USE `feri`;

-- --------------------------------------------------------

--
-- Struktura tabele `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(100) NOT NULL,
  `avtor` varchar(50) NOT NULL,
  `opis` text NOT NULL,
  `datum` datetime NOT NULL,
  `stevilo_slik` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Odloži podatke za tabelo `album`
--

INSERT INTO `album` (`id`, `naslov`, `avtor`, `opis`, `datum`, `stevilo_slik`) VALUES
(7, 'Naravoslovno-tehniški dan za Prvo gimnazijo, 14.09.2012', 'Thebenger', '', '2013-06-18 18:49:38', 12),
(10, 'Naravoslovno-tehniški dan za SERŠ, UM-FERI, 03.02.2012', 'klemenzarn', 'Avtor: Janez Pogorelc', '2013-06-18 19:16:27', 20),
(11, 'ALUMNI klub FERI 2013', 'Thebenger', 'Fotografije: R. Gomolj, B. Dugonik\r\nPreboj elektro letenja - Pipistrel', '2013-06-18 19:46:31', 5);

-- --------------------------------------------------------

--
-- Struktura tabele `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sporocilo` text COLLATE utf8_slovenian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `uporabnik` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=6 ;

--
-- Odloži podatke za tabelo `chat`
--

INSERT INTO `chat` (`id`, `sporocilo`, `datum`, `uporabnik`) VALUES
(1, 'dober dan študentje!', '2013-06-05 12:45:12', 12),
(3, 'Danes je lepo vreme.', '2013-06-05 12:45:43', 12),
(4, 'Ali imamo danes faks??', '2013-06-05 13:12:32', 12),
(5, 'lkkjfdwk', '2013-06-05 13:18:19', 12);

-- --------------------------------------------------------

--
-- Struktura tabele `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `ime` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `cpu`
--

CREATE TABLE IF NOT EXISTS `cpu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cas` text NOT NULL,
  `VrednostRAM` int(11) NOT NULL,
  `VrednostCPU` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=301 ;

--
-- Odloži podatke za tabelo `cpu`
--

INSERT INTO `cpu` (`ID`, `Cas`, `VrednostRAM`, `VrednostCPU`) VALUES
(198, '2014-01-18 16:48:02', 66, 63),
(199, '2014-01-18 16:49:27', 65, 14),
(200, '2014-01-18 16:49:37', 65, 1),
(201, '2014-01-18 16:49:47', 65, 56),
(202, '2014-01-18 16:49:57', 65, 51),
(203, '2014-01-18 16:50:07', 65, 46),
(204, '2014-01-18 19:22:08', 68, 72),
(205, '2014-01-18 20:01:00', 72, 22),
(206, '2014-01-18 20:01:20', 73, 51),
(207, '2014-01-18 20:02:20', 73, 50),
(208, '2014-01-18 20:02:30', 74, 49),
(209, '2014-01-18 20:03:00', 73, 54),
(210, '2014-01-18 20:03:10', 73, 51),
(211, '2014-01-18 20:03:20', 74, 46),
(212, '2014-01-18 20:04:20', 73, 49),
(213, '2014-01-18 20:08:19', 74, 43),
(214, '2014-01-18 20:08:59', 74, 29),
(215, '2014-01-18 20:09:09', 73, 26),
(216, '2014-01-18 20:09:19', 73, 50),
(217, '2014-01-18 20:10:19', 73, 50),
(218, '2014-01-18 20:11:19', 73, 51),
(219, '2014-01-18 20:44:43', 76, 41),
(220, '2014-01-18 20:45:06', 76, 36),
(221, '2014-01-18 20:45:16', 76, 36),
(222, '2014-01-18 20:50:04', 77, 25),
(223, '2014-01-18 20:55:20', 76, 54),
(224, '2014-01-18 21:10:15', 76, 23),
(225, '2014-01-18 21:11:35', 76, 71),
(226, '2014-01-18 21:11:45', 78, 56),
(227, '2014-01-18 21:11:55', 77, 46),
(228, '2014-01-18 21:12:05', 78, 48),
(229, '2014-01-18 21:12:36', 77, 21),
(230, '2014-01-18 21:12:46', 77, 20),
(231, '2014-01-18 21:14:21', 77, 22),
(232, '2014-01-18 21:14:31', 77, 21),
(233, '2014-01-18 21:15:21', 76, 57),
(234, '2014-01-18 21:19:56', 75, 22),
(235, '2014-01-18 21:20:16', 75, 25),
(236, '2014-01-18 21:20:56', 76, 22),
(237, '2014-01-18 21:22:36', 76, 34),
(238, '2014-01-18 23:15:12', 68, 61),
(239, '2014-01-18 23:16:32', 73, 64),
(240, '2014-01-18 23:16:42', 73, 57),
(241, '2014-01-18 23:16:52', 73, 58),
(242, '2014-01-18 23:17:02', 73, 58),
(243, '2014-01-18 23:17:12', 73, 55),
(244, '2014-01-18 23:17:22', 73, 57),
(245, '2014-01-18 23:35:19', 73, 40),
(246, '2014-01-18 23:36:19', 72, 29),
(247, '2014-01-18 23:37:19', 73, 29),
(248, '2014-01-20 13:40:18', 63, 86),
(249, '2014-01-20 13:40:38', 62, 53),
(250, '2014-01-20 13:44:43', 49, 29),
(267, '2014-01-20 21:03:34', 134, 4),
(268, '2014-01-20 21:03:44', 134, 8),
(269, '2014-01-20 21:04:16', 134, 9),
(270, '2014-01-20 21:04:26', 134, 8),
(271, '2014-01-20 21:04:36', 134, 12),
(272, '2014-01-20 21:04:46', 134, 3),
(273, '2014-01-20 21:04:56', 134, 9),
(274, '2014-01-20 21:05:06', 134, 12),
(275, '2014-01-20 21:05:16', 134, 21),
(276, '2014-01-20 21:05:26', 134, 24),
(277, '2014-01-20 21:05:50', 68, 33),
(278, '2014-01-20 21:05:56', 69, 5),
(279, '2014-01-20 21:06:06', 69, 3),
(280, '2014-01-20 21:06:16', 69, 2),
(281, '2014-01-20 21:06:26', 69, 2),
(282, '2014-01-20 21:06:36', 69, 7),
(283, '2014-01-20 21:06:46', 69, 3),
(284, '2014-01-20 21:06:56', 69, 4),
(285, '2014-01-20 21:07:06', 69, 9),
(286, '2014-01-20 21:07:16', 67, 19),
(287, '2014-01-20 21:07:26', 65, 6),
(288, '2014-01-20 21:09:36', 99, 1),
(289, '2014-01-20 21:09:46', 69, 10),
(290, '2014-01-20 21:09:56', 69, 4),
(291, '2014-01-20 21:10:06', 69, 5),
(292, '2014-01-20 21:10:16', 69, 7),
(293, '2014-01-20 21:10:26', 69, 4),
(294, '2014-01-20 21:10:36', 69, 9),
(295, '2014-01-20 21:10:46', 69, 11),
(296, '2014-01-20 21:10:56', 69, 24),
(297, '2014-01-20 21:11:06', 69, 14),
(298, '2014-01-20 21:11:16', 68, 6),
(299, '2014-01-20 21:11:26', 69, 72),
(300, '2014-01-20 21:11:36', 69, 78);

-- --------------------------------------------------------

--
-- Struktura tabele `datotekeoglas`
--

CREATE TABLE IF NOT EXISTS `datotekeoglas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `oglas` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oglas` (`oglas`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=17 ;

--
-- Odloži podatke za tabelo `datotekeoglas`
--

INSERT INTO `datotekeoglas` (`id`, `ime`, `oglas`) VALUES
(1, 'slika.jpg', 1),
(2, 'ARA 1. kolokvij rešeno.pdf', 1),
(3, 'slika.jpg', 2),
(7, 'novice.png', 16),
(10, 'klemen.jpg', 17),
(12, 'nebraska_blizu.png', 17),
(13, '', 18),
(14, '', 19),
(15, 'E&M re-itev B 2 kolokvij 11 6 2013.doc', 20),
(16, '', 21);

-- --------------------------------------------------------

--
-- Struktura tabele `df`
--

CREATE TABLE IF NOT EXISTS `df` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Lema` text NOT NULL,
  `DF` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=196 ;

--
-- Odloži podatke za tabelo `df`
--

INSERT INTO `df` (`ID`, `Lema`, `DF`) VALUES
(1, 'biti', 10),
(2, 'fin', 4),
(3, 'danes', 3),
(4, 'zelo', 3),
(5, 'nbsp', 2),
(6, 'lol', 2),
(7, 'dati', 2),
(8, 'scaron', 2),
(9, 'strong', 2),
(10, 'zadost', 2),
(11, 'oleeeo', 1),
(12, 'klepetulja', 1),
(13, 'breskvica', 1),
(14, 'span', 1),
(15, 'moba', 1),
(16, 'online', 1),
(17, 'od', 1),
(18, 'webkit-isolata', 1),
(19, 'sprememba', 1),
(20, 'zastonj', 1),
(21, 'tbody', 1),
(22, 'upload', 1),
(23, 'videoiger', 1),
(24, 'upak', 1),
(25, 'fdsfdsfsd', 1),
(26, 'style', 1),
(27, 'igrati', 1),
(28, 'wikipedia', 1),
(29, 'games', 1),
(30, 'dokoncen', 1),
(31, 'imeti', 1),
(32, 'leto', 1),
(33, 'solid', 1),
(34, 'colspan', 1),
(35, 'slikec', 1),
(36, 'jura', 1),
(37, 'zrihtati', 1),
(38, 'of', 1),
(39, 'background-image', 1),
(40, 'redlink', 1),
(41, 'oktober', 1),
(42, 'em', 1),
(43, 'novost', 1),
(44, 'najbolj', 1),
(45, 'cellspacing', 1),
(46, 'midsti', 1),
(47, 'napoved', 1),
(48, 'projekt', 1),
(49, 'http', 1),
(50, 'studio', 1),
(51, 'zaprt', 1),
(52, 'background-repeat', 1),
(53, 'podatek', 1),
(54, 'f9f9f', 1),
(55, 'text-align', 1),
(56, 'height', 1),
(57, 'benger', 1),
(58, 'vreme', 1),
(59, 'line-height', 1),
(60, 'nona', 1),
(61, 'action', 1),
(62, 'wik', 1),
(63, 'ref', 1),
(64, 'obv', 1),
(65, 'en', 1),
(66, 'right', 1),
(67, 'img', 1),
(68, 'lep', 1),
(69, 'vcgvfdg', 1),
(70, 'pri', 1),
(71, 'po', 1),
(72, 'obstajati', 1),
(73, 'razviti', 1),
(74, 'delovati', 1),
(75, 'background-position', 1),
(76, 'milijon', 1),
(77, 'vevent', 1),
(78, 'vertical-align', 1),
(79, 'px-league', 1),
(80, 'cist', 1),
(81, 'videti', 1),
(82, 'font-siza', 1),
(83, 'new', 1),
(84, 'php', 1),
(85, 'microsoft', 1),
(86, 'id', 1),
(87, 'portal', 1),
(88, 'golden', 1),
(89, 'padding', 1),
(90, 'loga', 1),
(91, 'leoeeleo', 1),
(92, 'eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf', 1),
(93, 'kekica', 1),
(94, 'znan', 1),
(95, 'stran', 1),
(96, 'battle', 1),
(97, 'april', 1),
(98, 'white-space', 1),
(99, 'igra', 1),
(100, 'tabla', 1),
(101, 'tr', 1),
(102, 'wikimedia', 1),
(103, 'blaž', 1),
(104, 'ali', 1),
(105, 'kje', 1),
(106, 'font-family', 1),
(107, 'tip', 1),
(108, 'org', 1),
(109, 'primaren', 1),
(110, 'izdan', 1),
(111, 'svoj', 1),
(112, 'prejeti', 1),
(113, 'aaaaaa', 1),
(114, 'image', 1),
(115, 'geekati', 1),
(116, 'žnidarec', 1),
(117, 'jurcek', 1),
(118, 'legends', 1),
(119, 'title', 1),
(120, 'multiplayer', 1),
(121, 'nato', 1),
(122, 'unicode-bid', 1),
(123, 'in', 1),
(124, 'prestižen', 1),
(125, 'cellpadding', 1),
(126, 'src', 1),
(127, 'bgggggggggggggggggggfgffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', 1),
(128, 'video', 1),
(129, 'sl.', 1),
(130, 'riot', 1),
(131, 'bet', 1),
(132, 'nota', 1),
(133, 'november', 1),
(134, 'border', 1),
(135, 'center', 1),
(136, 'razvijalec', 1),
(137, 'tvoj', 1),
(138, 'dober', 1),
(139, 'tale', 1),
(140, 'league', 1),
(141, 'color', 1),
(142, 'editi', 1),
(143, 'napovedan', 1),
(144, 'referenca', 1),
(145, 'cati', 1),
(146, 'izmed', 1),
(147, 'width', 1),
(148, 'border-style', 1),
(149, 'vremenski', 1),
(150, 'lt', 1),
(151, 'ta', 1),
(152, 'kratica', 1),
(153, 'href', 1),
(154, 'jo', 1),
(155, 'kot', 1),
(156, 'initial', 1),
(157, 'clovek', 1),
(158, 'background-color', 1),
(159, 'top', 1),
(160, 'alt', 1),
(161, 'znorel', 1),
(162, 'cuden', 1),
(163, 'px', 1),
(164, 'text-decoration', 1),
(165, 'amp', 1),
(166, 'windows', 1),
(167, 'ci', 1),
(168, 'kjer', 1),
(169, 'joystick', 1),
(170, 'clear', 1),
(171, 'png', 1),
(172, 'iopclimpija', 1),
(173, 'veagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf', 1),
(174, 'priden', 1),
(175, 'tudi', 1),
(176, 'ne', 1),
(177, 'arena', 1),
(178, 'do', 1),
(179, 'nowrap', 1),
(180, 'veliko', 1),
(181, 'infobox', 1),
(182, 'td', 1),
(183, 'thumb', 1),
(184, 'klemencic', 1),
(185, 'misliti', 1),
(186, 'capec', 1),
(187, 'sans-serif', 1),
(188, 'class', 1),
(189, 'index', 1),
(190, 'za', 1),
(191, 'sup', 1),
(192, 'youtuba', 1),
(193, 'nagrada', 1),
(194, 'margin', 1),
(195, 'slikati', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `dogodki`
--

CREATE TABLE IF NOT EXISTS `dogodki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `datum` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `kraj` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `izvajalec` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `vsebina` text COLLATE utf8_slovenian_ci NOT NULL,
  `povezava` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `vir` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1043 ;

--
-- Odloži podatke za tabelo `dogodki`
--

INSERT INTO `dogodki` (`id`, `naslov`, `datum`, `kraj`, `izvajalec`, `vsebina`, `povezava`, `vir`) VALUES
(1032, '15. mednarodni košarkarski turnir "Parabasket 2013"', '', '', '', 'Društvo paraplegikov Severne štajerske vabi na 15. mednarodni turnir v košarki na invalidskih vozičkih, ki bo 22. in 23. junija v dvorani Tabor, Maribor. &rdquo;PARABASKET 2013&rdquo;Več informacij o igranju košarke na vozičkih  Veselimo se vašega obiska.', 'http://www.sz-maribor.com/', 'sport'),
(1033, 'Državno prvenstvo v ulični košarki 2013', '', '', '', 'Kdaj: 22. junij 2013 - 3. turnir DP Kje: Maribor - Trg Leona &Scaron;tukljaOrganizator: Košarkarska zveza Slovenije VSI TURNIRJI V LETU 2013:1. KVALIFIKACIJSKI TURNIR SAMSUNG DP V KO&Scaron;ARKI 3x3(1. junij 2013 - Novo mesto, &Scaron;portni park Loka). 2. KVALIFIKACIJSKI TURNIR SAMSUNG DP V KO&Scaron;ARKI 3x3(8. junij 2013 - Ljubljana, Stritarjeva ulica).POKAL ADECCO (3. KVALIFIKACIJSKI TURNIR SAMSUNG DP V KO&Scaron;ARKI 3x3)(22. junij 2013 - Maribor, Trg Leona &Scaron;tuklja). 4. KVALIFIKACIJSKI TURNIR SAMSUNG DP V KO&Scaron;ARKI 3x3(29. junij 2013 - Koper,Taverna).FINALNI TURNIR SAMSUNG DP V KO&Scaron;ARKI 3x3(20. julij 2013 -Ljubljana, Prešernov trg).KLETKA STRAHU(30. avgust 2013 -Ljubljana, Kongresni trg).', 'http://www.sz-maribor.com/', 'sport'),
(1034, 'ZZrolano mesto 2013', '', '', '', 'Kdaj:   23. junij ob 15h (start rolanja ob 16h)Kje: Trg svobode, MariborOrganizator: Univerzitetna športna zveza Maribor    Ideja o rolanju po mestnih cestah je stara več kot deset let in za Zdravo zabavo je osem uspešnih prireditev, zadnji dve leti pa zaradi različnih, tehničnih ter finančnih razlogov prireditve nismo uspeli izpeljati, zato bo letošnja prireditev še toliko bolj pomembna. Letos je dogodek prvič del Festivala Lent &ndash; športnega Lenta.   ZZrolano mesto je prav gotovo poznano vsakemu/i Mariborčanu/ki. V letu 2009 se je prireditve udeležilo 750 ljubiteljev in ljubiteljic osmih koleščk.. Prireditev je rekreativnega značaja, in tako primerna prav za vsakega, ki obvlada osnove rolanja., zmeraj se nam pridružijo tudi kolesarji. V primeru dežja se prireditev prestavi za teden dni, torej na nedeljo,30. junija 2013.  PROMOCIJSKI VIDEO SI LAHKO OGLEDATE TUKAJ.', 'http://www.sz-maribor.com/', 'sport'),
(1035, '35. kolesarski maraton okoli Pohorja 2013', '', '', '', 'Kdaj: Maraton bo potekal v soboto, 29.6.2013.Kje: START: ob 9:00 uri za udeležence na 150 km dolgi progi,za ostale start ob 9.15 uri na Trgu svobode v MariboruOrganizator: Kolesarsko društvo BranikCILJ: ob 10.00 do 16.00 na Trgu svobodePROGE: 150 km, 70 km, 50 km in 30 kmPravila maratona:Na maratonu lahko sodelujejo vsi kolesarji, ki jih veseli organizirano rekreativno kolesarjenje v skupinah in se čutijo sposobni prevoziti 30, 50, 70 oz. 150 km dolgo progo.Otroci, mlajši od 14 let, se smejo udeležiti maratona v spremstvu staršev oziroma polnoletne osebe. Za vse udeležence je obvezna uporaba kolesarske čelade.Splošni predpisi:Maraton bo potekal ob vsakem vremenu, ob normalnem prometu, zato so kolesarji dolžni upoštevati cestno prometne predpise in navodila organizatorjev ter se po njih ravnati.Udeleženci maratona vozijo na lastno odgovornost, ter odgovarjajo za škodo povzročeno sebi ali drugim.Na maratonu je strogo prepovedano prehitevanje čelnih spremljevalnih vozil. Organizator ne jamči za škodo, ki jo udeleženec povzroči sebi, drugim ali tretji osebi. S svojim startom vsi kolesarji potrdijo, da so seznanjeni z določili razpisa objavljenim v dnevnem časopisju in ostalih medijih ter po ozvočenju na startu. Za varen potek prireditve bodo skrbela vozila organizatorja, rediteljska in zdravstvena služba.Nagrade in obveznosti organizatorja:Vsem udeležencem, ki bodo upoštevali splošne predpise in prispeli na cilj do 16.00 ure, bodo podeljene spominske medalje, majice in tradicionalni topel obrok s pijačo. Vsi udeleženci bodo tudi sodelovali v žrebanju bogatih nagrad ob 12:00 uri, kjer je prva nagrada KOLO. Na progi bo poskrbljeno za okrepčila.Informacije:Kolesarsko društvo Branikod 08.00 do 10.00 ure na številki 02 228 73 73 inod 07.00 do 19.00 ure na številki 041- 689 390 in 041 416 772.Predsednik organizacijskega odbora:Branko Tekmec tel. št. 041 740 081. Vsi na kolo, za čisto okolje in zdravo telo!', 'http://www.sz-maribor.com/', 'sport'),
(1036, '3. turnir za pokal HIPODROMA KAMNICA 2013 v preskakovanju zaprek', '', '', '', 'Kdaj: v soboto, 6. 7. 2013 in v nedeljo, 7. 7. 2013 od 9. do 16. ureKje: Hipodrom KamnicaOrganizator: JK Maribor in KK BerghausTudi letos bomo ponovno preskakovali ovire. Drugi turnir za Pokal hipodroma Kamnica bo potekal v soboto, 8. 6. 2013 in v nedeljo, 9. 6. 2013 na hipodromu v Kamnici. Oglejte si PROGRAMOstali turnirji, ki sledijo: 6. in 7. julija 201321. septembra 2013', 'http://www.sz-maribor.com/', 'sport'),
(1037, '3. kolesarski maraton čez Pohorje 2013', '', '', '', '&Scaron;tart maratona bo v nedeljo 13. julija, ob 09:00 uri na Radvanjskem trgu (Pohorska Ulica 9, 2000 Maribor).  Veliko kolesarjev sanja o cestah okoli  Pohorja. O cestah, ki so jih že pred več kot 30leti premagovali njihovi  starši in dedje. In tudi to je eden od razlogov, da se ponovno vidimo  pod Pohorjem na sedaj že 3. RAP Maratonu čez Pohorje. Tudi letos smo za  vas pripravli različne trase, kjer lahko probate svoje kolesarske  sposobnosti. &Scaron;tart maratonov je v Mariboru pod  Pohorjem, natančneje v krajevni skupnosti Radvanje v bližini Pohorske  vzpenjače, na Radvanjskem trgu. 65 KM maraton, bo prava  stvar za vse športnike, ki želijo preživet svoj čas aktivno ter v  prijetni družbi. Vmesna "močna" okrepčevalna postojanka na 30km (Lehen  na Pohorju, 550 m.n.v.), pa bo prišla ravno prav za prijetno  nadaljevanje maratona.  150 KM maraton velja za  težji rekreativnih maratonov, ki bo izziv marsikateremu kolesarju.  Daljši vzponi, manjši &raquo;hupserji&laquo;, dolga ravnina ter čudovita kulisa ga  naredijo posebnega in nepozabnega.  310 KM maraton je eden  najtežjih cestnih maratonov, ki od udeležencev zahteva visoko fizično  kakor tudi psihično pripravljenost. Kolesarji se bodo morali spopast s  kar šestimi različnimi vzponi na Pohorje. Tukaj se zahtevnost ne konča,  saj za vsakim vzponom sledi tudi spust, ki zahteva veliko mero  koncentracije. RAP ULTRA je preizkušnja za vse, ki bi radi v športu  dosegli nekaj več.   Več informacij in prijave na: www.rapmaraton.siOrganizator:                                                                            SMEROK d.o.o.Ul. Roberta Kukovca 402000 MariborTel.: 02/33 23 904 E-mail: info@rapmaraton.si', 'http://www.sz-maribor.com/', 'sport'),
(1038, 'Kariera pri mednarodni organizaciji združenih narodov', '', '', '', 'Organizacija  združenih narodov daje pri svojem delu velik poudarek na mlade. Zato v svoje vrste vabijo mlade, ambiciozne posameznike, ki bi  želeli zgraditi svojo kariero pri eni izmed najpomembnejših organizacij na svetu.\n&nbsp;\nOrganizacija združenih narodov išče visoko kvalifikacirane kandidate, ki si želijo ustvariti kariero pri mednarodni organizaciji z vključitvijo v njihov program rekrutiranja &#8220;The young professionals programme &#8211; YPP&#8221;. Gre za iniciativo, ki v organizacijo vsako leto pripelje nove talente, ki morajo najprej opraviti pristopne izpite. Za mlade, visoko izobražene profesionalce po svetu je ta izpit platforma za začetek kariere v organizaciji združenih narodov. Letošnje leto imajo mladi možnost začeti kariero v naslednjih poklicih:\n- pravo,\n- javne  informacije,\n- statistika,\n- administracija ,\n- finance.\nPrijavni rok za pravo, javne informacije in statistiko se je odprl 3. junija. Vsi zaniteresirani se lahko prijavite vse do  2. avgusta 2013,  za administracijo in finance pa prijavi rok traja od 8. junija ter vse do 5. septembra.\nNa razpis se lahko prijavijo vsi kandidatje, ki so mlajši od 32 let v tekočem letu, imajo vsaj diplomo prve stopnje ene izmed univerzitetnih smeri ponujenih kategorij, ter govorijo tekoče angleško ali francosko (vsaj enega izmed dveh delovnih jezikov sekretariata Združenih narodov). Prav tako morajo biti prijavljeni kandidatje iz naslednjih držav:  Alžirija, Andora Angola, Antigua and Barbuda, Azerbajdžan, Bahrain, Barbados, Belorusija, Belgija, Belize, Brazilija, Zelenortski otoki, Čad, Kitajska, Komori, Ciper, Nemčija, Grenada, Guinea-Bissau, Indonezija, Irak, Japonska, Kiribati, Latvija, Lesotho, Liberija, Libija, Liechtenstein, Litva, Luxemburg, Malezija, Mikronesija, Moldavija, Monaco, Črna Gora, Mozambik, Nizozemska, Norveška, Oman, Palau, Paragvaj, Poljska, Portugalska, Republika Koreja, Saint Lucia, Samoa, San Marino, Sao Tome and Principe, Saudska Arabija, Slovaška, Slovenija, Salomonovi otoki, South Sudan, Španija, St. Vincent and the Grenadines, Švedska, Švica, Sirija, Tadžikistan, Tonga, Turčija, Tuvalu in Združene države Amerike.\nVsi zainteresirani se morate prijaviti preko kariernega portala združenih narodov, kjer lahko najdete tudi vse ostale informacije. \nTekst: Tjaša Brod', 'http://ssum.um.si/dogodki/kariera-pri-mednarodni-organizaciji-zdruzenih-narodov/', 'studentski-svet'),
(1039, 'Banka slovenije razpisuje nagrade in štipendije', '', '', '', 'Evrosistem Banke Slovenije za leto 2013 razpisuje  nagrade za magistrska in doktorska dela ter tri kadrovske štipendije za študijsko leto 2013/2014. \n&nbsp;\nBanka Slovenije je centralna banka Republike Slovenije. Ustanovljena je bila s sprejetjem Zakona o Banki Slovenije 25. junija 1991. Je pravna oseba javnega prava, ki samostojno razpolaga z lastnim premoženjem.\nZ namenom spodbujanja raziskovalnega dela študentov na fakultetah v Republiki Sloveniji  Komisija Sveta Banke Slovenije za raziskovalno delo za leto 2013 razpisuje nagrade za magistrska in doktorska dela po dokončanem starem programu in po bolonjskem študijskem programu. Za nagrade se lahko potegujejo  kandidati, katerih področje raziskovanja je ekonomija, bančništvo, zavarovalništvo, finance, finančno pravo ali modeli poslovnega in finančnega prestrukturiranja. Upoštevana bodo magistrska oz. doktorska dela, katerih obramba je bila med 1. 7. 2012 in 1. 7. 2013, kandidati pa lahko svoje magistrsko oz. doktorsko delo pošljejo najkasneje do 8. 7. 2013. Komisija Sveta Banke Slovenije za raziskovalno delo bo izmed prejetih del izbrala največ sedem del za nagrado Banke Slovenije. Nagrada znaša 500 evrov neto.\nEvrosistem Banke Slovenije prav tako razpisuje tri kadrovske štipendije za študijsko leto 2013/2014. Ena kadrovska štipendija bo namenjena študentki oz. študentu prvega letnika bolonjskega magistrskega študija na Ekonomski fakulteti, ki bo obiskoval smer Ekonomija, Bančništvo in finance ali Uporabna statistika. Drugo kadrovsko štipendijo bo lahko pridobila študentka ali študent drugega letnika bolonjskega magistrskega študija na Ekonomski fakulteti, smer Računovodstvo in revizija. Zadnja kadrovska štipendija pa je predvidena za študentko ali študenta drugega letnika bolonjskega magistrskega študija na Fakulteti za matematiko in fiziko, smer Finančna matematika. Prijave za kadrovske štipendije so odprte do 1. julija 2013.\nKandidati, ki se potegujejo za  nagrade za magistrska in doktorska dela, naj zgoščenko, prijavni obrazec in priporočilo pošljejo priporočeno s povratnico na naslov: Banka Slovenije, Slovenska 35, 1505 Ljubljana, Komisija Sveta Banke Slovenije za raziskovalno delo, s pripisom: »Za nagrado Banke Slovenije v letu 2013 za magistrska oz. doktorska dela.« Kandidati za kadrovske štipendije pa svojo prijavo skupaj z vsemi potrebnimi obrazci in potrdili pošljejo na isti naslov s pripisom: »Vloga za kadrovsko štipendijo BS«.\nZa morebitne dodatne informacije lahko pokličete vsak dan med 9.00-12.00 na telefonsko številko (01) 4719 547.\nDodatne informacije o razpisih pa lahko najdete tudi na njihovi spletni strani pod zavihkom »O Banki Slovenije«.\nTekst: Barbara Vehovar', 'http://ssum.um.si/dogodki/banka-slovenije-razpisuje-nagrade-in-stipendije/', 'studentski-svet'),
(1040, 'Grafitanje, dj-ing, hip hop in video produkcija', '', '', '', 'Mestna občina Maribor v okviru projekta Evropska prestolnica mladih Maribor 2013 razvija mladinsko delovanje tudi s sosednjo državo Avstrijo, natančneje z mestom Leoben, s katerim so v letu 2013 združili moči in se povezali na projektu Graffiti in the City.\n&nbsp;\nTridnevni dogodek se bo odvijal v mestu Leoben, v bližini Gradca, in bo mladim ponujal priložnost spoznavanja kulturne scene na področju grafitanja, glasbe, plesa in video produkcije. Cilj projekta je spodbujanje umetnosti in ustvarjalnosti mladih v obeh partnerskih regijah, medsebojno povezovanje, spoznavanje drugih kultur in povezovanje interesov mladih iz obeh regij.\nDogodek bo spremljal pester izbor delavnic, na katerih bodo udeleženci dobili vpogled v svet grafitanja in »street art-a«. Mladi umetniki iz obeh regij bodo skupaj vodili delavnice in se imeli možnost predstaviti na zaključni prireditvi.\nDogodek bo potekal od 24. 6. do 27. 6. 2013. Vsi stroški nočitve, prehrane, prevoza ter materiala za delavnice so kriti v okviru projekta Graffiti in the City.\nMladi se lahko prijavijo kot vodje delavnic (mladi ustvarjalci, ki so že nekaj časa dejavni na svojem področju) ali zgolj kot udeleženci, ki jih tovrstna mladinska tematika zanima. Starostna omejitev udeležencev je od 16 let naprej.\nSklopi delavnic so:\n-       video produkcija,\n-       grafiti,\n-       hiphop/ breakdance/ street dance,\n-       DJ-ing.\nVsi zainteresirani za udeležbo na projektu naj pošljejo prijavo na elektronski naslov mitja.spes@maribor.si. Rok za prijavo je ponedeljek, 10. 6. 2013. Če se želite dogodka udeležiti kot vodja delavnic, zapišite področje, za katero se prijavljate, in priložite svoj življenjepis. Če želite sodelovati kot udeleženec delavnic, pošljite kratko motivacijsko pismo.\nProsta mesta bodo zapolnili glede na prejete prijave, torej do zapolnitve prostih mest. Za vse informacije o projektu lahko kontaktirate: Mitjo Špesa (mitja.spes@maribor.si) ali Špelo Žibert (epm.asistent2@maribor.si).\n&nbsp;', 'http://ssum.um.si/dogodki/grafitanje-dj-ing-hip-hop-in-video-produkcija/', 'studentski-svet'),
(1041, 'Midnight Resurrection XL Edition', 'Ponedeljek, 24.06.2013 22:00', 'Štuk, Maribor', '', 'Midnight Resurrection v najboljši izvedbi do sedaj - multimedijski klubski spektakel kot ga v Mariboru še nismo doživeli! 24. junija premikamo meje!▧▧▧▧▧▧▧ MAIN FLOOR ▧▧▧▧▧▧▧▷▷▷ underground trance - psy / progressive', 'http://stuk.org/dogodek/midnight-resurrection-xl-edition', 'zabava'),
(1042, 'Vodstvo po razstavi Sestop umetnosti na Zemljo', 'četrtek, 27. 6. 2013, 18.00 - 19.00', 'Strossmayerjeva 6', '', '0Marko Pogačnik bo pred otvoritvijo pregledne razstave Sestop umetnosti na Zemljo v UGM izvedel vodstvo po razstavi, ki je zasnovano kot uvod v otvoritev razstave. Udeležba je brezplačna.', 'http://www.maribor2012.eu/nc/dogodek/prikaz/3826933/', 'kultura');

-- --------------------------------------------------------

--
-- Struktura tabele `drugenovice`
--

CREATE TABLE IF NOT EXISTS `drugenovice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(200) COLLATE utf8_slovenian_ci NOT NULL,
  `link` varchar(200) COLLATE utf8_slovenian_ci NOT NULL,
  `vsebina` text COLLATE utf8_slovenian_ci NOT NULL,
  `vir` varchar(200) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=128 ;

--
-- Odloži podatke za tabelo `drugenovice`
--

INSERT INTO `drugenovice` (`id`, `naslov`, `link`, `vsebina`, `vir`) VALUES
(1, 'Prvi zaščitni etui z elektronskim zaslonom!', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/dogodki-in-obvestila/prvi-zascitni-etui-z-elektronskim-zaslonom.html', 'Zaščitni etuiji so postali neločljivi spremljevalec pametnih mobilnih telefonov, saj jih v primeru padca uspešno zaščitijo pred poškodbami. Ker pa so običajni zaščitni etuiji dolgočasni, so inženirji podjetja Gajah pripravili izdelek, ki je s strani obiskovalcev sejma Computex 2013 prejel veliko...', 'Rac-novice'),
(2, 'Sony uradno predstavil zelo zanimiv mobilnik Xperia M', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/sony/sony-uradno-predstavil-zelo-zanimiv-mobilnik-xperia-m.html', 'Podjetje Sony je letošnjega februarja kupcem ponudilo mobilnik Xperia Z. Ta se po svojih specifikacijah še vedno lahko brez težav kosa z vsemi trenutno najboljšimi pametnimi telefoni. Sedaj pa je podjetje Sony predstavilo še nekoliko manj zmogljivega bratca tega mobilnika, ki sliši na ime Xperia M. Ta je zelo primeren za uporabnike, ki od svojega mobilnika ne zahtevajo najboljše, kar trg tisti hip ponuja, vseeno pa ž...', 'Rac-novice'),
(3, 'Z operacijskim sistemom Windows 8.1 se vrača gumb Start!                                    1', 'http://www.racunalniske-novice.com/novice/programska-oprema/microsoft/windows/z-operacijskim-sistemom-windows-81-se-vraca-gumb-start.html', 'Microsoft je v najnovejšem videoposnetku prihajajočega operacijskega sistema Windows 8.1 (Blue) razkril tisto, kar so ljubitelji Windowsa 7 najbolj pogrešali. Tu seveda govorimo o »izgubljenem« gumbu »Start«, ki dejansko služi kot bližnjica za preklop med namizjem in novim grafičnim vmesnikom. ...', 'Rac-novice'),
(4, 'Ali ste slab community manager?', 'http://www.racunalniske-novice.com/novice/digitalni-marketing/ali-ste-slab-community-manager.html', 'V dobrih treh letih odkar se profesionalno ukvarjam z družabnimi omrežji sem videl že marsikaj. Ko spremljam kaj se dogaja na nekaterih poslovnih Facebook profilih me prime, da bi kontaktiral podjetje in jih povprašal ali so Facebook izbrali za vzdig ali uničenje lastnega ugleda....', 'Rac-novice'),
(5, 'Mobilnik Samsung Galaxy S4 s procesorjem Intel?', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/samsung/mobilnik-samsung-galaxy-s4-s-procesorjem-intel.html', 'V zadnjem času se vse več proizvajalcev pametnih mobilnih telefonov odloča za pripravo izdelkov, ki za preračunavanje podatkov uporabljajo procesorje Intel Atom z jedrom Clover Trail+. Ti namreč temeljijo na zgradbi x86 (enako kot procesorji za osebne računalnike) in zagotavlja izjemno računsko moč, ...', 'Rac-novice'),
(6, 'LevelOne stropna mrežna kamera                                    3', 'http://www.racunalniske-novice.com/novice/sporocila-za-javnost/levelone-stropna-mrezna-kamera.html', 'LevelOne FCS-3052 je 3 megapikselska kupolasta mrežna kamera z video resolucijo do 2032x1536. Kamera podpira H.264/MPEG-4/MJEPG video kompresije, zaradi česar je učinkovita in poceni rešitev video nadzora za podjetja.\n \nSkupaj z 12 infrardečih LED diod in snemljivim IR filtrom je odlična za ...', 'Rac-novice'),
(7, 'Xbox One se bo moral redno povezovati na splet', 'https://slo-tech.com/novice/t570035', 'The Verge - Da bo moral biti Xbox One ves čas povezan na internet, smo po ovinkih izvedeli že dva tedna pred predstavitvijo. Microsoft je zdaj razjasnil situacijo, ki ni tako zelo huda, a vseeno precejšen preskok glede na preteklost. Nova konzola se bo namreč res morala zelo pogosto povezovati na Mi...', 'Slo-tech'),
(8, 'Microsoft udaril po botnetu Citadel', 'https://slo-tech.com/novice/t569953', 'Microsoft - Microsoft v sodelovanju z ostalimi podjetji in organi pregona redno uničuje botnete na internetu. To pot so uničili že sedmega, in sicer so razbili široko omrežje okuženih računalnikov, ki jih je napadel Citadel. Kot sporočajo iz Redmonda, so prekinili povezave med več kot 1600 podomrežj...', 'Slo-tech'),
(9, 'Prosto dostopne ocene indijske mature na spletu kažejo nepravilnosti', 'https://slo-tech.com/novice/t569955', 'Hack A Day - Eden najpomembnejših izpitov v Indiji je ISC, ki se tam opravlja po zaključeni srednji šoli. Od njega je odvisno, na katero univerzo se bo kdo lahko vpisal. Zato dijaki težko pričakujejo rezultate in včasih uberejo kakšno bližnjico. Redko pa se zgodi, da ta razkrije velike malomarnosti ...', 'Slo-tech'),
(10, 'Izšla osma edicija Humble Indie Bundle', 'https://slo-tech.com/novice/t569924', 'Wired News - Kuler je spletna storitev podjetja Adobe, namenjena izbiranju barv. Pred kratkim so izdali tudi aplikacijo za mobilne telefone, zaenkrat zgolj za iOS platformo. Barvne teme v aplikaciji lahko izbiramo na dva načina: z barvnim krogom ali s pomočjo kamere. Barvni krog je podoben tistemu n...', 'Slo-tech'),
(11, 'Adobe izdal mobilno verzijo Kulerja', 'https://slo-tech.com/novice/t569886', 'International Business Times - Prodaja podjetja Dell se vleče kot jara kača. Januarja so se pojavila prva poročila o napovedani prodaji za podjetje, za katero se je zanimalo več snubcev, potem je kot strela z jasnega udarila novica, da bo podjetje prevzel kar ustanovitelj Michael Dell. In ko je bila...', 'Slo-tech'),
(12, 'Bo Dell vendarle odkupil Michael Dell?', 'https://slo-tech.com/novice/t569836', 'The New York Times - Da Kitajci ponarejajo precej zahodnih izdelkov in za nizke cene prodajajo manj kakovostne imitacije, je splošno znano. Pisali smo tudi že, kako Kitajci postavijo trgovino, ki je na las podobna Applovi trgovini, le da z Applom nima nič skupnega. Sedaj pa so pri The New York Times...', 'Slo-tech'),
(13, 'Slovenski Zootfly za jesen napoveduje hologramske igre', 'http://www.24ur.com/novice/it/slovenski-zootfly-za-jesen-napoveduje-hologramske-igre.html', 'Slovenski razvijalec video iger Zootfly bo za Elektronček začel razvijati hologramske kazino igre, ki jih bodo že jeseni predstavili na igralniškem sejmu. Starega posla pa ne bodo pustili ob strani.', '24-ur'),
(14, 'Razbili mrežo hekerjev, ki so ukradli več kot pol milijarde dolarjev', 'http://www.24ur.com/novice/it/razbili-mrezo-hekerjev-ki-so-ukradli-vec-kot-pol-milijarde-dolarjev.html', 'Ameriški preiskovalni urad FBI je v sodelovanju z Microsoftom razkrinkal zločinsko združbo hekerjev, ki so ukradli več kot pol milijarde dolarjev. Okužene računalnike, preko katerih so se dokopali do podatkov o bančnih računih, so našli v več kot 90 državah.', '24-ur'),
(15, 'Malomarnost pri izbiri gesel nas lahko drago stane', 'http://www.24ur.com/novice/it/malomarnost-pri-izbiri-gesel-nas-lahko-drago-stane.html', 'Številni se pri izbiri gesel še vedno zatekajo k najbolj preprostim besedam. Najpogostejša gesla so 123456, abc123, iloveyou in password, posamezniki pa ista gesla uporabljajo za več aplikacij. Tako hekerji nimajo težkega dela.', '24-ur'),
(16, '10-milijonska kazen po polomiji na borzi', 'http://www.24ur.com/novice/it/10-milijonska-kazen-po-polomiji-na-borzi.html', 'Zaradi polomije pri prodaji delnic Facebook je borza Nasdaq kaznovana z 10 milijoni dolarjev kazni. Doslej so plačali že 62 milijonov investicijskim podjetjem, navadnim delničarjem pa ne bo pomagal nihče.', '24-ur'),
(17, 'Mladi na Facebooku: Toliko sem vreden, kolikor mi drugi povedo, da sem vreden', 'http://www.24ur.com/novice/it/mladi-na-facebooku-toliko-sem-vreden-kolikor-mi-drugi-povedo-da-sem-vreden.html', 'Pri nas Facebook uporablja več kot 600.000 ljudi. Večina mladih dnevno. In če so včasih starši vedeli, ko je otrok prišel objokan iz šole, je danes otroško igrišče postal prav Facebook. In tega, kaj se zgodi tam, se ne vidi skozi okno.', '24-ur'),
(18, '''Žal mi je, a nisem dovolj močna ...'' 14-letnica v smrt zaradi Facebooka. Bo kdo odgovarjal?', 'http://www.24ur.com/novice/svet/zal-mi-je-a-nisem-dovolj-mocna-14-letnica-v-smrt-zaradi-facebooka-bo-kdo-odgovarjal.html', 'Bi moral kdo odgovarjati, ko norčevanje, zbadanje in zmerjanje na družbenem omrežju pripelje do samomora? In kakšna je odgovornost podjetja, ki storitev omogoča? Družbena omrežja se branijo, mrtvih mladih pa je vse več.', '24-ur'),
(19, 'Slovenski Zootfly za jesen napoveduje hologramske igre', 'http://www.24ur.com/novice/it/slovenski-zootfly-za-jesen-napoveduje-hologramske-igre.html', 'Slovenski razvijalec videoiger Zootfly bo za Elektronček začel razvijati hologramske igralniške igre, ki jih bodo že jeseni predstavili na igralniškem sejmu. Starega posla pa ne bodo pustili ob strani.', '24-ur'),
(38, 'Inventec s prvo 7-palčno tablico Windows 8', 'http://www.racunalniske-novice.com/novice/dogodki-in-obvestila/inventec-s-prvo-7-palcno-tablico-windows.html', 'Podjetje Inventec je napovedalo prihod prvega tabličnega računalnika z operacijskim sistemom Windows 8, ki bo za prikazovanje slik uporabljal 7-palčni zaslon....', 'Rac-novice'),
(39, 'AMD nad Intel s procesorji Richland                                    1', 'http://www.racunalniske-novice.com/novice/strojna-oprema/procesorji/amd/amd-nad-intel-s-procesorji-richland.html', 'Ker prodaja procesorjev AMD upada, procesorski gigant vse svoje upe polaga na novince z razvojno oznako Richland, ki bodo navadnim smrtnikom na voljo v prosti prodaji še pred koncem poletja. Procesorji AMD Richland temeljijo na osnovi 28-nanometrske tehnologije, so združljivi s podnožjema Socket FM2 ...', 'Rac-novice'),
(40, 'Fairphone: Prvi družbeno odgovoren pametni mobilnik', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/dogodki-in-obvestila/fairphone-prvi-druzbeno-odgovoren-pametni-mobilnik.html', 'Medtem ko je kitajski trg preplavljen z izjemno zmogljivimi in cenovno ugodnimi pametnimi mobilnimi telefoni, moramo pri nas zanje odšteti pravo malo bogastvo. Ker vodilni proizvajalci mobilnikov za nameček izkoriščajo poceni delovno silo iz daljnega vzhoda in na ta račun kujejo bajne dobičke, je ...', 'Rac-novice'),
(41, 'Pregled vseh novosti v modrih Windows 8.1', 'http://www.racunalniske-novice.com/novice/piano/piano-windows-81-pregled-vseh-novosti.html', 'Microsoft je 26. oktobra izdal operacijski sistem Windows 8 skupaj z novo strojno opremo, namenjeno upravljanju z dotikom. Le 12 mesecev po razkritju predogleda Windows 8 Release Preview so že objavili seznam posodobitev in sprememb v verziji 8.1. Ta urnik enoletnih posodobitev se bo izvajal še v ...', 'Rac-novice'),
(42, 'Tipkovnica Google Nexus za vse!', 'http://www.racunalniske-novice.com/triki/tipkovnica-google-nexus-za-vse.html', 'Mnogi uporabniki pametnih mobilnih telefonov in tabličnih računalnikov Android niso zadovoljeni s prirejeno tipkovnico, ki jo proizvajalci mobilnih naprav nameščajo kot del prilagojenega mobilnega operacijskega sistema Android. Zaradi tega so se Googlovi programerji odločili, da privzeto tipkovnico ...', 'Rac-novice'),
(43, 'Upravljanje letečega plovila z mislimi', 'http://www.racunalniske-novice.com/novice/dogodki-in-obvestila/upravljanje-letecega-plovila-z-mislimi.html', 'Znanstveniki v zadnjem času posvečajo veliko pozornosti napravam, ki jih je mogoče krmiliti kar z mislimi. Med njimi, verjeti ali ne, najdemo celo leteče plovilo, glavne zasluge za to pa gredo inovativni ekipi biotehničnih inženirjev iz minnesotske univerze (University of Minnesota). Za krmiljenje ...', 'Rac-novice'),
(80, 'Intel z revolucionarnimi procesorji za tablice Android in Windows', 'http://www.racunalniske-novice.com/novice/strojna-oprema/procesorji/intel/intel-z-revolucionarnimi-procesorji-za-tablice-android-in-windows.html', 'Intel na področju tabličnih računalnikov z inovativnimi izdelki konkurenčno podjetje ARM prehiteva po levi in desni. To je računalniški gigant ponovno dokazal s pripravo izjemno inovativnega čipovja z razvojno oznako Bay Trail, ki se odlično znajde tako v navezi z mobilnim operacijskim sistemom...', 'Rac-novice'),
(81, 'Apple s povsem prenovljenim mobilnim operacijskim sistemom iOS 7                                    14', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/apple/apple-s-povsem-prenovljenim-mobilnim-operacijskim-sistemom-ios-7.html', 'Apple udeležence konference World-Wide Developer Conference (WWDC) ni pustil hladne niti s predstavitvijo mobilnega operacijskega sistema iOS 7. Ta navdušuje s precej bolj ploščatim videzom uporabniškega vmesnika, a brez prevelikih vizualnih sprememb. Applovi programerji so tu povsem prenovili ...', 'Rac-novice'),
(82, 'OS X Mavericks: Super operacijski sistem za Applove namizne in prenosne računalnike', 'http://www.racunalniske-novice.com/novice/strojna-oprema/osebni-racunalniki/apple/apple-os-x-mavericks-super-operacijski-sistem-za-namizne-in-prenosne-racunalnike.html', 'Podjetje Apple je na konferenci World-Wide Developer Conference (WWDC) predstavilo novi operacijski sistem OS X za njegove namizne in prenosne računalnike. Novost je podjetje iz Cupertina poimenovalo OS X Mavericks in ta uporabnikom Applovih računalnikov prinaša tri ključne novosti, in sicer ...', 'Rac-novice'),
(83, 'Apple z ekstremno zmogljivimi računalniki Mac Pro', 'http://www.racunalniske-novice.com/novice/strojna-oprema/osebni-racunalniki/apple/apple-z-ekstremno-zmogljivimi-racunalniki-mac-pro.html', 'Podjetje Apple na konferenci World-Wide Developer Conference (WWDC) ni pozabilo niti na zahtevnejše uporabnike njegovega namiznega računalnika Mac Pro, saj je bil slednji deležen celovite prenove. Najbolj opazna sprememba novosti je vidna na ohišju, saj je to sedaj cilindrične oblike, kar naj bi ...', 'Rac-novice'),
(84, 'Novi MacBook Air zmogljivejši in z daljšo avtonomijo', 'http://www.racunalniske-novice.com/novice/strojna-oprema/prenosniki/apple/novi-macbook-air-zmogljivejsi-in-z-daljso-avtonomijo.html', 'Na Applovi konferenci World-Wide Developer Conference (WWDC) so prišli na svoj račun tudi ljubitelji kompaktnih osebnih računalnikov. Podjetje iz Cupertina je namreč prenovilo priljubljeno družino prenosnikov MacBook Air, ki je v primerjavi z obstoječimi modeli še hitrejša in varčnejša z dragoceno ...', 'Rac-novice'),
(85, 'KOPA je gostila cvet koroških inovacijskih podjetij, prejemnikov diplom in priznanj za najboljše inovatorje leta 2012', 'http://www.racunalniske-novice.com/novice/sporocila-za-javnost/kopa-je-gostila-cvet-koroskih-inovacijskih-podjetij-prejemnikov-diplom-in-priznanj-za-najboljse-inovatorje-leta-2012.html', 'Podjetje KOPA je potrdilo svojo zavezo ustvarjalnosti in gostilo slavnostno podelitev priznanj Gospodarske zbornice za najboljše koroške inovatorje v letu 2012. Koroška podjetja so z 21 prijavljenimi inovacijskimi projekti potrdila, da se v regiji zavedajo pomena inovacij pri zagotavljanju poslovne ...', 'Rac-novice'),
(86, 'Apple WWDC: iOS', 'https://slo-tech.com/novice/t570366', 'The Verge - V prejšnjem prispevku, ki je pokril Applove osebne računalnike, smo že omenili, da se poslavlja posnemanje tekstur iz resničnega sveta. &#x160;e bolj kot OS X je bil po tem poznan iOS, kjer je transformacija še večja.  Seveda se je spet začelo s statistiko. Vsega skupaj je Apple prodal ž...', 'Slo-tech'),
(87, 'Apple WWDC: Mac in OS X', 'https://slo-tech.com/novice/t570322', 'The Verge - Kot je običajno za Applove prireditve, na katerih predstavijo obilico novosti, so tokratno otvoritveno predstavitev razvijalske konference WWDC začeli s statistiko. Tokrat so se pohvalili predvsem z odlično predstavo svoje trgovine za mobilni operacijski sistem iOS. Poročali smo že, da s...', 'Slo-tech'),
(88, 'Mož, ki je razkril največje prisluškovanje na svetu: Edward Snowden', 'https://slo-tech.com/novice/t570345', 'Guardian - Britanski časnik Guardian je pred nekaj dnevi razkril informacije, da ameriška Agencija za nacionalno varnost (NSA) na internetu prisluškuje več milijonom Američanov in da zmore prestrezati praktično vse. Sprva je na internet pricurljal strogo zaupni nalog, ki ga je NSA izročil operaterju...', 'Slo-tech'),
(89, 'Facebookov podatkovni center v dejanskem oblaku: dež v hali', 'https://slo-tech.com/novice/t570314', 'The Register - Precej zanimiv pripetljaj v svojem podatkovnem centru je razkril Facebook. Incident se je sicer zgodil že leta 2011, javnosti pa so ga obelodalnili šele sedaj. Takrat je namreč oblačno računalništvo dobilo nov pomen, saj je v podatkovnem centru nastal resnični oblak, iz katerega je na...', 'Slo-tech'),
(90, 'Novosti s Computexa 2013', 'https://slo-tech.com/novice/t569638', 'engadget - V tajvanski prestolnici Taipei se je končal semenj Computex. Že prvi dan je bilo predstavljenih kup novih elektronskih naprav. Poudarek je bil predvsem na tablicah in poltablicah, saj naj bi oboje -- kot lahko zadnje čase beremo -- v naslednjih dveh ali treh letih dosegle in potem tudi pr...', 'Slo-tech'),
(91, 'Kitajci imajo najhitrejšega', 'http://www.racunalniske-novice.com/novice/dogodki-in-obvestila/kitajci-imajo-najhitrejsega.html', 'Kitajci so se na področju gradnje superračunalnikov ponovno izkazali, saj so v mestu Gvangdžov (po slovensko tudi Kanton) postavili trenutno najzmogljivejši računalniški sistem na modrem planetu. Superračunalnik Tianhe-2 je edinstven tudi po strojni plati, saj za preračunavanje podatkov uporablja...', 'Rac-novice'),
(92, 'Želiš Samsung S4?', 'http://www.racunalniske-novice.com/novice/digitalni-marketing/zelis-samsung-s4.html', 'Puschkin je utrjena blagovna znamka, ki se je spomladi odločila za krepitev prisotnosti na Facebooku in en, izmed številnih prijemov, je (pravkar poteka) tudi nagradna igra, v kateri se sodelujoči potegujejo za glavno nagrado - mobilni telefon Samsung S4.                                           \n ...', 'Rac-novice'),
(93, 'Instagram za Windows Phone na voljo 26. junija!', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/nokia/instagram-za-windows-phone-na-voljo-26-junija.html', 'Instagram je med uporabniki mobilnih naprav zelo priljubljen, saj gre za aplikacijo, ki ponuja vrsto posebnih učinkov in filtrov, s pomočjo katerih lahko fotografijam dodamo pridih mističnosti in jih enostavno delimo s prijatelji. Medtem ko so prednosti Instagrama doslej lahko koristili le uporabniki ...', 'Rac-novice'),
(94, 'Mobilnik LG Optimus L7 2 Dual z Androidom 4.3?', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/lg/mobilnik-lg-optimus-l7-2-dual-z-androidom-43.html', 'Čeprav podjetje Google uradno še ni naznanilo prihoda mobilnega operacijskega sistema Android 4.3 Jelly Bean, je podjetje LG Electronics na njegovi spletni strani že razkrilo prvi pametni mobilni telefon opremljen z njim. Z novim Googlovim operacijskim sistemom naj bi bil opremljen mobilnik Optimus ...', 'Rac-novice'),
(95, 'Igrane zvočne pravljice na voljo kjerkoli in kadarkoli', 'http://www.racunalniske-novice.com/novice/sporocila-za-javnost/igrane-zvocne-pravljice-na-voljo-kjerkoli-in-kadarkoli.html', 'Dežela pravljic staršem omogoča hiter in preprost dostop do igranih zvočnih pravljic kjerkoli in kadarkoli preko vseh elektronskih naprav (računalnik, telefon, tablica) z dostopom do spleta. Trenutno Dežela pravljic šteje skoraj 50 prikupnih, raznovrstnih pravljic s pozitivnim naukom.               ...', 'Rac-novice'),
(96, 'Samsung Galaxy Ace 3: Poceni mobilnik s podporo omrežju 4G/LTE', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/samsung/samsung-galaxy-ace-3-poceni-mobilnik-s-podporo-omrezju-4glte.html', 'Podjetje Samsung nas je prijetno presenetilo s pripravo pametnega mobilnega telefona Galaxy Ace 3, saj gre za cenovno ugodno napravo, ki v polni meri podpira hitro mobilno omrežje 4G/LTE. Novosti bomo zagotovo veseli tudi mi, saj največja ponudnika storitev mobilne telefonije pri nas pospešeno ...', 'Rac-novice'),
(97, 'Izstreljena peta kitajska vesoljska misija s posadko', 'https://slo-tech.com/novice/t570471', 'BBC - Včeraj ob 11.38 uri po slovenskem času je Kitajska v vesolje izstrelila plovilo Shenzhou-10, ki je v vesolje poneslo tri kitajske taikonavte. Plovilo se bo spojilo s kitajsko vesoljsko postajo Tiangong-1, ki jo od leta 2011 sestavljajo v vesolju. Tiangong-1 je prvi modul bodoče postaje, ki bo ...', 'Slo-tech'),
(98, 'Google kupil Waze', 'https://slo-tech.com/novice/t570483', 'Forbes - Googlov zadnji nakup je izraelsko podjetje Waze, ki se ukvarja z digitalnimi zemljevidi. Koliko so odšteli zanj, ni povsem točno znano, omenjajo pa se vrednosti v razponu med 1,0 in 1,3 milijarde dolarjev. Za podjetje se je zanimal tudi Facebook, zato poglejmo, kaj lahko Waze ponudi svojim ...', 'Slo-tech'),
(99, 'PlayStation 4 razkrit', 'https://slo-tech.com/novice/t570458', 'Ars Technica - Sony je na včerajšnji konferenci dokončno razkril PlayStation 4. Po njihovi februarski konferenci, ki je bila precej skopa z detajli, se je na veliko razpravljalo kakšno pot bo Sony ubral glede nekaterih ključnih vprašanj, kot so rabljene igre in potreba po internetni povezavi. Prav t...', 'Slo-tech'),
(100, 'Snowden poniknil', 'https://slo-tech.com/novice/t570453', 'Slo-Tech - Edward Snowden, ki je odgovoren za razkritje prisluškovalne afere Prism, je izginil neznano kam. V Hongkong je pripotoval 20. maja, preden je objavil dokumente, saj se je želel izogniti gotovi aretaciji v ZDA. Hongkong ima sicer podpisan sporazum o izročanju z ZDA, a pričakovati je, da bi...', 'Slo-tech'),
(101, 'Xbox One bo stal pol tisočaka, pride novembra', 'https://slo-tech.com/novice/t570356', 'BBC - Končno smo dočali še najpomembnejše informacije, ki se tičejo nove Microsoftove konzole Xbox One. Naprodaj bo od novembra, prodajala pa se bo po 500 evrov (ali 500 dolarjev, če ste v ZDA), je razkril Microsoft na E3 v Los Angelesu. To je bistveno več od 300 evrov za predhodnik Xbox 360 (bo pa ...', 'Slo-tech'),
(102, 'Google z nakupom podjetja Waze prehitel Facebook in Apple', 'http://www.24ur.com/novice/it/google-z-nakupom-podjetja-waze-prehitel-facebook-in-apple.html', 'Ameriški spletni velikan Google je za izraelsko podjetje Waze, ki se ukvarja z digitalnimi zemljevidi, odštel slabo milijardo evrov. S tem je prehitel Facebook in Apple ter si zagotovil primat na področju zemljevidov.', '24-ur'),
(103, 'Tudi vi prek spleta nakupujete v tujini?', 'http://www.24ur.com/novice/it/tudi-vi-prek-spleta-nakupujete-v-tujini.html', 'Po zadnjih podatkih MOSS malo manj kot 46 odstotkov slovenskih spletnih uporabnikov vsaj enkrat letno preko spleta nakupuje v tujini. Preko spleta v tujini tedensko ali pogosteje nakupuje 4,06 odstotka slovenskih spletnih uporabnikov.', '24-ur'),
(104, 'Nič več ''organizatorji igre'' v branži? To so Applove novosti. Kje pa je navdušenje?', 'http://www.24ur.com/novice/it/nic-vec-organizatorji-igre-v-branzi-to-so-applove-novosti-kje-pa-je-navdusenje.html', 'Apple je na konferenci WWDC prestavil novosti. Teh je kar nekaj, uporabniki so nad njimi bolj ali manj navdušeni, vlagatelji pa pogrešajo kaj zares revolucionarnega. Neizpolnjena pričakovanja so se medtem že pokazala v ceni delnic.', '24-ur'),
(105, 'Izjemen mobilni operacijski sistem Smartisan na voljo za Samsung Galaxy S3!', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/dogodki-in-obvestila/vrhunski-mobilni-sistem-smartisan-na-voljo-za-samsung-galaxy-s3.html', 'Podjetje Smartisan Co., Ltd. je aprila letos predstavilo lasten mobilni operacijski sistem Smartisan OS, ki temelji na osnovi Androida. Ker je novost med uporabniki mobilnikov Samsung požela veliko pozitivnih kritik, so programerji pošteno zavihali rokave in pripravili prvo zgodnjo različico sistema ...', 'Rac-novice'),
(106, 'Kaj so spletni piškoti, zakaj se uporabljajo in kako jih v brskalniku izključimo?                                    1', 'http://www.racunalniske-novice.com/triki/kaj-so-spletni-piskoti-zakaj-se-uporabljajo-in-kako-jih-v-brskalniku-izkljucimo.html', 'V Sloveniji se predvsem v zadnjem času veliko govori o spletnih piškotih, saj spremenjeni Zakon o elektronskih komunikacijah upravljavcem spletnih strani nalaga, da morajo uporabnike seznaniti o uporabi spletnih piškotkov, določenih vrst piškotkov pa sploh ne smejo uporabljati brez ustreznega ...', 'Rac-novice'),
(107, 'Vzglavnik s stereo zvočniki za filmofile', 'http://www.racunalniske-novice.com/novice/dogodki-in-obvestila/vzglavnik-s-stereo-zvocniki-za-filmofile.html', 'Če radi gledate televizijo v postelji boste zagotovo pritrdili, da je kakovost zvoka kljub vrhunskemu zvočniškemu sistemu bolj kot ne slaba. Z nakupom izjemno zanimivega vzglavnika SoftSound bo teh »težav« konec, saj v notranjosti skriva dva kakovostna stereo zvočnika, ki sta s televizijskim ...', 'Rac-novice'),
(108, 'Android 5.0 Key Lime Pie konec oktobra!', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/operacijski-sistemi/android/android-50-key-lime-pie-konec-oktobra.html', 'Ker podjetje Google v okviru lastne konference Google I/O maja letos ni predstavilo naslednika mobilnega operacijskega sistema Android Jelly Bean, so mnogi poznavalci prepričani, da bo Android 5.0 Key Lime Pie luč sveta ugledal konec oktobra. Ta bo skoraj zagotovo najprej nameščen na supermobilniku ...', 'Rac-novice'),
(109, 'Inovativnost se zlato sveti', 'http://www.racunalniske-novice.com/novice/sporocila-za-javnost/inovativnost-se-zlato-sveti.html', 'SAOP-ov dokumentni sistem iCenter eRegistrator je dobil zlato priznanje Gospodarske zbornice Slovenije za inovacijo in se bo potegoval za najboljšo slovensko inovacijo v letu 2012....', 'Rac-novice'),
(110, 'Apple patentiral tipke za zaslone na dotik                                    2', 'http://www.racunalniske-novice.com/novice/strojna-oprema/tablicni-racunalniki/apple/apple-patentiral-tipke-za-zaslone-na-dotik.html', 'Podjetje Apple že od nekdaj kot za stavo vlaga prošnje za patentiranje bolj ali manj inovativnih izumov in včasih celo takšnih, ki so že več let v splošni rabi. Predvsem pri patentiranju slednjih »izumov« je podjetje iz Cupertina nadvse uspešno, kar je ponovno dokazalo s patentom, ki mu ga je pred ...', 'Rac-novice'),
(111, 'Savdijci blokirajo Skype, Whatsapp', 'https://slo-tech.com/novice/t570999', 'Reuters - Medtem, ko na Zahodu (spet/začasno) razmišljamo o odhodu z GMaila, Facebooka in podobnih ameriških komunikacijskih servisov, katerih glavna dejavnost je zagotavljanje dostopa za tamkajšnje obveščevalne in policijske službe (brez skrbi, čez kak teden nam bo že povsem vseeno), imajo na Vzhod...', 'Slo-tech'),
(112, 'Nokia pripravlja optično revolucijo', 'https://slo-tech.com/novice/t571027', 'The Verge - Nokia bo 11. julija v New Yorku gostila predstavitev, povezano z velikimi spremembami na področju zumiranja. Naslov vabila na prireditev namreč pove le Zoom. Reinvented, pod naslovom pa pod povečevalnim steklom podajo prej omenjene detajle o času in lokaciji prireditve. Logično je torej ...', 'Slo-tech'),
(113, 'Haswell-E bo prinesel obilico novosti', 'https://slo-tech.com/novice/t571023', 'VR - Zone - Intel je nedavno pokazal novo generacijo svojih procesorjev, ki nosijo kodno ime Haswell. Gre za tock v Intelovem programu, kar pomeni novo arhitekturo na starem (22 nm) proizvodnem procesu. Naslednik arhitekture Sandy Bridge je jasno namenjen za prenosnike in namizne računalnike manj za...', 'Slo-tech'),
(114, 'PlayStation 4 se prodaja bolje od Xbox One', 'https://slo-tech.com/novice/t571006', 'Forbes - Že kmalu po predstavitvi dodatnih podrobnosti obeh konzol na E3 smo dobili prve smernice, kako bo potekala prodaja Microsoftove in Sonyjeve predstavnice 8. generacije konzol. Microsoftovi predstavitvi je sledilo negodovanje nad močno omejenim posojanjem in preprodajanjem iger za Xbox One, n...', 'Slo-tech'),
(115, 'Kdo vse ne priznava novega ZEKom', 'https://slo-tech.com/novice/t570998', 'Slo-Tech - Po velikem uspehu kartičnih računalnikov Raspberry Pi se je na internetu pojavila cela vrsta predelav in zanimivih načinov uporabe, kako lahko Raspberry Pi izkoristimo za najrazličnejše namene. V tem času, ko povsod odzvanjajo afere s prisluškovanjem in prestrezanjem podatkov, je zelo akt...', 'Slo-tech'),
(116, 'Raspberry Pi kot odjemalec za Tor je Onion Pi', 'https://slo-tech.com/novice/t570936', 'Slo-Tech - Takoj po izbruhu afere Prism so internetni giganti jeli zatrjevati, da oni niso prostovoljno posredovali nobenih informacij Agenciji za nacionalno varnost (NSA). A stvari so se kmalu postavile na svoje mesto. Facebook, Microsoft in Google so sodelovali z NSA, sedaj pa poizkušajo omejiti š...', 'Slo-tech'),
(117, 'Nov projekt Googla: internet z balona', 'http://www.24ur.com/novice/it/nov-projekt-googla-internet-z-balona.html', '6\n					\n					\n					Video\n	                \n					\n					\n				\n			\n			Spletni gigant Google bi želel vzpostaviti mrežo balonov, ki bi oddajali signal za internet. Prvi testni baloni so na Novi Zelandiji že poleteli, prvi uporabnik se je že povezal. S sistemom bi lahko internet imeli tudi v odročnejših krajih.', '24-ur'),
(118, 'Od danes v veljavi strožja pravila glede spletnih piškotkov', 'http://www.24ur.com/od-danes-v-veljavi-strozja-pravila-glede-spletnih-piskotkov.html', 'Cilj novih pravil je boljše varovanje zasebnosti obiskovalcev spletnih strani, saj se piškotki vse pogosteje uporabljajo za sledenje in zbiranje informacij o uporabnikih. Večina piškotkov je povsem neškodljiva ali celo nujna za normalno brskanje po spletu.', '24-ur'),
(119, 'Megan umrla med ropom iPhona: pametnim telefonom bi dodali funkcijo za ''samouničenje''', 'http://www.24ur.com/novice/it/megan-umrla-med-ropom-iphona-pametnim-bi-telefonom-dodali-funkcijo-za-samounicenje.html', 'Tatovi pametnih telefonov bi lahko že kmalu ostali brez dela. Ameriške oblasti namreč od proizvajalcev zahtevajo, da jim dodajo funkcijo za samouničenje. S tem želijo povečati varnost ljudi, ugotovili so namreč, da je pametni telefon tarča vsakega tretjega ropa.', '24-ur'),
(120, 'Vas skrbi radovednost (ameriške) vlade? Poglejte raje, kaj vse o vas zaradi spleta vedo trgovci', 'http://www.24ur.com/novice/it/vas-skrbi-radovednost-ameriske-vlade-poglejte-raje-kaj-vse-o-vas-zaradi-spleta-vedo-trgovci.html', 'Da vlade o nas zbirajo podatke, smo že vedeli. Kako natančni so pri tem, je zdaj podrobneje razložil Edward Snowden in sprožil plaz kritik na ameriško vlado. Pri tem pa se je marsikdo vprašal, ali nas mora bolj skrbeti vladno ali komercialno zbiranje podatkov.', '24-ur'),
(121, 'Zaslon v kontaktni leči', 'http://www.racunalniske-novice.com/novice/dogodki-in-obvestila/zaslon-v-kontaktni-leci.html', 'Raziskovalna skupina Jang-Ung Park Research Group je pripravila nadvse zanimivi izum, ki deluje na enak način kot večpredstavnostna očala Google Glass, le da tu vlogo očal prevzema kar kontaktna leča. Nadobudni raziskovalci so namreč v kontaktno lečo vgradili »čistokrvni« zaslon LED, pri čemer so ...', 'Rac-novice'),
(122, 'Razkrite strojne specifikacije mobilnika Motorola Moto X', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/motorola/rim/razkrite-specifikacije-mobilnika-motorola-moto-x.html', 'Glavni izvršni direktor podjetja Motorola Dennis Woodside je v začetku junija najavil, da bo njihov mobilnik Moto X v celoti izdelan v Združenih državah Amerike in bo opremljen s kar dvema procesorjema. Ker bo novost naprodaj konec letošnjega poletja oziroma najkasneje oktobra so mnogi prepričani, da ...', 'Rac-novice'),
(123, 'Intel Haswell-E: Ultrazmogljivi procesorji z osmimi jedri!', 'http://www.racunalniske-novice.com/novice/strojna-oprema/procesorji/intel/intel-haswell-e-ultrazmogljivi-procesorji-z-osmimi-jedri.html', 'Čeprav je podjetje Intel šele pred kratkim predstavilo četrto generacijo procesorjev Core s procesorskimi sredicami Haswell, ki pripomorejo k hitrejšemu delovanju namiznih in prenosnih računalnikov ob hkratnem zmanjšanju porabe električne energije, njihovi inženirji že pripravljajo njegove naslednike....', 'Rac-novice'),
(124, 'Applov iPhone ima zvestejše uporabnike kot Android', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/dogodki-in-obvestila/applov-iphone-ima-zvestejse-uporabnike-kot-android.html', 'Najnovejša raziskava analitskega podjetja Retrevo je pokazala, da so uporabniki Applovih pametnih mobilnih telefonov iPhone veliko bolj zvesti njihovim napravam od uporabnikov mobilnikov Android. V omenjeni raziskavi je namreč kar 81-odstotkov uporabnikov mobilnikov iPhone zatrdilo, da bi se ob ...', 'Rac-novice'),
(125, 'Android 5.0 Key Lime Pie konec oktobra!', 'http://www.racunalniske-novice.com/novice/mobilna-telefonija/operacijski-sistemi/android/android-50-key-lime-pie-konec-oktobra.html', 'Ker podjetje Google v okviru lastne konference Google I/O maja letos ni predstavilo naslednika mobilnega operacijskega sistema Android Jelly Bean, so mnogi poznavalci prepričani, da bo Android 5.0 Key Lime Pie luč sveta ugledal konec oktobra. Ta bo skoraj zagotovo najprej nameščen na ...', 'Rac-novice'),
(126, '&#x160;e Apple razkril podatke o številu zahtevkov za posredovanje podatkov', 'https://slo-tech.com/novice/t571098', 'Apple - Po Microsoftu in Facebooku je tudi Apple razkril, koliko podatkov so posredovali ameriškim pravosodnim organom, policiji in NSA. V polletnem obdobju od decembra lani do maja letos so prejeli 4000-5000 zahtevkov, ki so se nanašali na slabih 10.000 računov uporabnikov.   Večino zahtevkov je po...', 'Slo-tech'),
(127, 'Nov projekt Googla: internet z balona', 'http://www.24ur.com/novice/it/nov-projekt-googla-internet-z-balona.html', '0\n					\n					\n					Video\n	                \n					\n					\n				\n			\n			Spletni gigant Google bi želel vzpostaviti mrežo balonov, ki bi oddajali signal za internet. Prvi testni baloni so na Novi Zelandiji že poleteli, prvi uporabnik se je že povezal. S sistemom bi lahko internet imeli tudi v odročnejših krajih.', '24-ur');

-- --------------------------------------------------------

--
-- Struktura tabele `kategorije`
--

CREATE TABLE IF NOT EXISTS `kategorije` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategorija` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `daljse` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=5 ;

--
-- Odloži podatke za tabelo `kategorije`
--

INSERT INTO `kategorije` (`id`, `kategorija`, `daljse`) VALUES
(1, 'rit-uni', 'Računalništvo in informacijske tehnologije (UNI)'),
(2, 'rit-vs', 'Računalništvo in informacijske tehnologije (VS)'),
(3, 'itk-uni', 'Informatika in tehnologije komuniciranja(UNI)'),
(4, 'itk-vs', 'Informatika in tehnologije komuniciranja(VS)');

-- --------------------------------------------------------

--
-- Struktura tabele `koledar`
--

CREATE TABLE IF NOT EXISTS `koledar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `naslov` varchar(512) COLLATE utf8_slovenian_ci NOT NULL,
  `vsebina` varchar(512) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=13 ;

--
-- Odloži podatke za tabelo `koledar`
--

INSERT INTO `koledar` (`ID`, `datum`, `naslov`, `vsebina`) VALUES
(1, '2013-05-21', 'Konferenca za mlade razvijalce', 'Pridete na brezplačno konferenco!'),
(2, '2013-05-21', 'Lampiončki', 'Tudi letos tradicionalni lampiončki! '),
(3, '2013-05-21', 'Lampiončki', 'Tudi letos tradicionalni lampiončki! '),
(4, '2013-05-21', 'Lampiončki', 'Tudi letos tradicionalni lampiončki! '),
(5, '2013-05-21', 'Lampiončki', 'Tudi letos tradicionalni lampiončki! '),
(6, '2013-05-21', 'Lampiončki', 'Tudi letos tradicionalni lampiončki! '),
(7, '2013-06-20', 'OTS 2013', 'Konferenca ots je spet tu!'),
(9, '2013-06-19', 'Tradicionalni lampiončki 2013', 'Spet so tu!'),
(10, '2013-06-30', 'Konec študija', 'Pogostitev na FERI ob zaključku študija! ob 15.00. Prosimo potrdite udeležbo.'),
(12, '2013-06-01', 'Jurjevanje', 'Jurjevanje je tu! Prosimo potrdite udeležbo vsi, ki se boste Jurjevanja udeležili. Potekalo bo 19.6.2013 za Štukom!');

-- --------------------------------------------------------

--
-- Struktura tabele `koledar_user`
--

CREATE TABLE IF NOT EXISTS `koledar_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `koledar_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=14 ;

--
-- Odloži podatke za tabelo `koledar_user`
--

INSERT INTO `koledar_user` (`id`, `user_id`, `koledar_id`) VALUES
(4, 12, 5),
(5, 12, 3),
(6, 12, 1),
(7, 12, 9),
(8, 461, 12),
(9, 475, 12),
(11, 496, 12),
(12, 496, 7),
(13, 461, 9);

-- --------------------------------------------------------

--
-- Struktura tabele `meritve`
--

CREATE TABLE IF NOT EXISTS `meritve` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temperatura` float NOT NULL,
  `svetloba` varchar(20) NOT NULL,
  `cas` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Odloži podatke za tabelo `meritve`
--

INSERT INTO `meritve` (`id`, `temperatura`, `svetloba`, `cas`) VALUES
(1, 24.08, '30', '2014-01-15 19:11:27'),
(2, 23.33, '30', '2014-01-15 19:11:33'),
(3, 22.83, '77', '2014-01-15 19:11:39'),
(4, 23.49, '90', '2014-01-15 19:11:45'),
(5, 23.99, '90', '2014-01-15 19:11:51'),
(6, 24.08, '7', '2014-01-15 19:11:57'),
(7, 23.41, '1', '2014-01-15 19:12:03'),
(8, 22.83, '56', '2014-01-15 19:12:09'),
(9, 22.49, '62', '2014-01-15 19:12:15'),
(10, 22, '57', '2014-01-15 19:12:21'),
(11, 21.66, '56', '2014-01-15 19:12:27'),
(12, 21.5, '55', '2014-01-15 19:12:33'),
(13, 21.33, '56', '2014-01-15 19:12:39'),
(14, 21.16, '56', '2014-01-15 19:12:45'),
(15, 21, '56', '2014-01-15 19:12:51'),
(16, 21, '56', '2014-01-15 19:12:57'),
(17, 21.08, '55', '2014-01-15 19:13:03'),
(18, 21.08, '55', '2014-01-15 19:13:09'),
(19, 21.16, '56', '2014-01-15 19:13:15'),
(20, 21.16, '55', '2014-01-15 19:13:21'),
(21, 21.16, '56', '2014-01-15 19:13:27'),
(22, 21.08, '56', '2014-01-15 19:13:33'),
(23, 20.91, '55', '2014-01-15 19:13:39'),
(24, 20.83, '55', '2014-01-15 19:13:45'),
(25, 20.91, '55', '2014-01-15 19:13:51'),
(26, 20.83, '55', '2014-01-15 19:13:57'),
(27, 20.83, '55', '2014-01-15 19:14:03'),
(28, 20.66, '56', '2014-01-15 19:14:09'),
(29, 20.75, '55', '2014-01-15 19:14:15'),
(30, 20.75, '55', '2014-01-15 19:14:21'),
(31, 20.66, '55', '2014-01-15 19:14:27'),
(32, 20.58, '55', '2014-01-15 19:14:33'),
(33, 20.58, '54', '2014-01-15 19:14:39'),
(34, 20.66, '54', '2014-01-15 19:14:45'),
(35, 20.66, '54', '2014-01-15 19:14:51'),
(36, 20.66, '55', '2014-01-15 19:14:57'),
(37, 20.66, '54', '2014-01-15 19:15:03'),
(38, 20.66, '54', '2014-01-15 19:15:09'),
(39, 20.66, '54', '2014-01-15 19:15:15'),
(40, 20.66, '54', '2014-01-15 19:15:21'),
(41, 20.66, '54', '2014-01-15 19:15:27'),
(42, 20.66, '54', '2014-01-15 19:15:33'),
(43, 20.66, '54', '2014-01-15 19:15:39'),
(44, 20.66, '54', '2014-01-15 19:15:45');

-- --------------------------------------------------------

--
-- Struktura tabele `novice`
--

CREATE TABLE IF NOT EXISTS `novice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `vsebina` text COLLATE utf8_slovenian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `avtor` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `naslovna_slika` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=47 ;

--
-- Odloži podatke za tabelo `novice`
--

INSERT INTO `novice` (`id`, `naslov`, `vsebina`, `datum`, `avtor`, `naslovna_slika`) VALUES
(40, 'ČESTITAMO - Najboljši študent UM je Iztok Fister', '<p><span style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">Univerza v Mariboru je v sodelovanju z Elektrom Maribor d. d. izvedla razpis za izbor najbolj&scaron;ega &scaron;tudenta Univerze v Mariboru.&nbsp;</span><br style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;" /><br style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;" /><span style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">Izmed 25-ih odličnih &scaron;tudentov je komisija, ki so jo sestavljali prof. dr. Zoran Ren (Fakulteta za strojni&scaron;tvo UM), prof. dr. Mihaela Koletnik (Filozofska fakulteta UM), Nika Sajko (oddelek za izobraževanje Univerze v Mariboru) ter &scaron;tudentki Tea Markotić in Eva Senica, za naj &scaron;tudenta izbrala:&nbsp;</span></p>\r\n<p><strong style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;"><br />Iztoka Fisterja,</strong><span style="color: #666666; font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">&nbsp;&scaron;tudenta&nbsp;2. letnika magistrskega &scaron;tudijskega programa Računalni&scaron;tvo in informacijske tehnologije Fakultete za elektrotehniko, računalni&scaron;tvo in informatiko, ki trenutno pripravlja magistrsko nalogo &raquo;A comprehensive review of bat algorithms and their hybridization&laquo;, pod mentorstvom dr. Marjana Mernika ter somentorstvom profesorja iz Velike Britanije. Kako velik interes in veselje ima do raziskovalnega dela, kaže njegova bibliografija, ki trenutno obsega 15 enot. Med njimi je 8 izvirnih znanstvenih člankov, od tega so 3 dela objavljena v JRC revijah, in 5 konferenčnih člankov. Za svoje raziskovalno delo je prejel že vrsto nagrad &ndash; zmaga na konferenci ERK 2010 za najbolj&scaron;i prispevek, nagrada FERI 2010 za posebne dosežke, Perlachova nagrada 2011 za najbolj&scaron;o diplomo.</span></p>', '2013-06-14 16:38:40', 'klemenzarn', 'feri.jpg'),
(41, 'Medijski dan', '<p>&nbsp;&Scaron;tudentje Medijskih komunikacij že četrto leto pripravljamo Medijski dan, pri katerem lahko sodeluje&scaron; tudi ti in osvoji&scaron; &scaron;e privlačno nagrado. Prvi sklop Medijskega dne je &Scaron;tudentski FilmFest, ki se bo odvijal 5. junija v kinu Udarnik, v drugem sklopu, ki se bo odvijal 6.junija na Fakulteti za elektrotehniko, računalni&scaron;tvo in informatiko, pa se nam lahko pridruži&scaron; na zanimivih predavanjih. Več o tem si je mogoče ogledati na http://www.medijskidan.si/.</p>\r\n<p>&nbsp;</p>\r\n<p>Naj izpostavimo samo nekaj aktualnosti:</p>\r\n<p>-Natečaj za najbolj&scaron;e televizijsko reklamno sporočilo; denarna nagrada v vrednosti 250&euro;</p>\r\n<p>-Foto natečaj: tema: Mediji na poti; nagrada je izlet na Dunaj za 2 osebi</p>\r\n<p>-Natečaj za reklamno sporočilo na temo Ekolo&scaron;ko ozave&scaron;čen tisk; glavna nagrada je osebni mini računalnik v vrednosti 450&euro; in 5 tolažilnih nagrad</p>\r\n<p>-Ustvarjalni izziv: Prenova spletne strani Ecomedia.si; nagrada je polletna karta za Red Power fitness&nbsp;</p>\r\n<p>-Eko flash mob; nagrada je polletna karta za Red Power fitness&nbsp;</p>\r\n<p>-Novinarski članek na temo aktivnega in zdravega življenja; nagrada polletna karta za Red Power fitness</p>\r\n<p>-Izvedba treh delavnic pod vodstvom strokovnjakov, prijave mogoče od 20.5.2013 ob 20 uri naprej</p>\r\n<p>-Naročite si svojo majico Medijskega dne do 26.5. in postanite ustvarjalci.</p>', '2013-06-14 16:52:27', 'klemenzarn', 'samsung.jpg'),
(42, 'ČESTITAMO ZMAGOVALCEM IN FINALISTOM ŠTUDENTSKEGA TEKMOVANJA IMAGINE CUP 2013!', '<p><strong style="color: #000000; font-family: tahoma, verdana, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 16.796875px; orphans: auto; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;">Včeraj je potekala predstavitev finalistov in razglasitev zmagovalne ekipe svetovnega &scaron;tudentskega tekmovanja Imagine Cup 2013. Devet članska komisija je izmed sedmih finalistov za zmagovalca izbrala ekipo BEEZINGA, ki bo s svojo tehnolo&scaron;ko re&scaron;itvijo zastopala Slovenijo na svetovnem finalu v St. Peterburgu.&nbsp;<br /><br /></strong></p>\r\n<h2><span style="font-size: small;">Beezinga (</span><em><span style="font-size: small;">Sistem za analizo in zgodnjo obve&scaron;čanje o dogajanju v čebelnjakih)</span></em></h2>\r\n<p>&nbsp;</p>\r\n<div>Pri Beezingi razvijamo sistem za analizo in zgodnjo obve&scaron;čanje o dogajanju v čebelnjakih. Podatki se s pomočjo lastne strojne opreme avtomatsko pridobivajo iz panjev, čebelarji pa lahko stanje preprosto spremljajo čez mobilno in spletno aplikacijo. Ker so čebele eden najpomembnej&scaron;ih in tudi najbolj ogroženih akterjev v ekosistemu, &nbsp;je na&scaron;o poslanstvo čebelarjem pravočasno zagotoviti vse potrebne informacije za optimalno gojo, čim vi&scaron;jo proizvodnjo in najbolj&scaron;o za&scaron;čito čebel.</div>\r\n<div>&nbsp;</div>\r\n<div>\r\n<div>Člani ekipe:</div>\r\n<strong style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;"><strong style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;"><strong>Nenad Čikić,</strong>&nbsp;Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko.<br /><strong>Jernej &Scaron;krabec</strong>, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko.<br /><strong>Jernej Gračner</strong>, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko.<br /></strong></strong>\r\n<div>&nbsp;&nbsp;</div>\r\n<div>Mentor:</div>\r\n<div><strong>Denis &Scaron;pelič,</strong>&nbsp;Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko.</div>\r\n<p>Med finalisti so bili &scaron;e ekipa&nbsp;<strong>DORA</strong>&nbsp;(Doctors Operational Research Assistant) z razvojem sistema za brezkontaktno interakcijo z aplikacijami namenjeno zdravstvenemu osebju,<strong>JUMBOZZ</strong>&nbsp;z&nbsp;<span style="font-size: small;">ustanovitvijo družabnega omrežja, ki uporabnikom omogoča delitev dela v obliki&nbsp;</span><span style="font-size: small;">projektov in ekipa&nbsp;<strong>JIM</strong>, ki je predstavila storitev oziroma asistenta, ki povezuje fitnes uporabnike in osebnega trenerja.&nbsp;<br /></span><br />Vir:<br /><a style="color: #1c4980;" href="http://web.vecer.com/portali/vecer/v1/default.asp?kaj=3&amp;id=2013041105905171">http://web.vecer.com/portali/vecer/v1/default.asp?kaj=3&amp;id=2013041105905171</a><br /><a style="color: #1c4980;" href="http://www.imaginecup.si/sl-SI/Dokumenti/Imagine_Cup/Finalisti_2013_131.aspx">http://www.imaginecup.si/sl-SI/Dokumenti/Imagine_Cup/Finalisti_2013_131.aspx</a></p>\r\n</div>', '2013-06-14 17:01:43', 'klemenzarn', 'slika.jpeg'),
(43, 'Čestitamo ekipi DORA!!', '<p><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">&nbsp;</span><strong style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">V okviru konference Dnevi slovenske informatike je potekalo tudi tekmovanje za najbolj&scaron;i &scaron;tudentski projekt na področju informatike v letu 2013. Tričlanska komisija je za zmagovalca izbrala ekipo DORA (Doctor&rsquo;s operational research assistant). Ekipa DORA se je s svojo re&scaron;itvijo predstavila tudi na &scaron;tudentskem tekmovanju Imagine Cup 2013, kjer je zmagala v kategoriji Inovacije.&nbsp;<br /><br /><strong>DORA (Interaktiven zdravnikov asistent za zadovoljne bolnike in &scaron;e uspe&scaron;nej&scaron;e zdravnike)</strong>&nbsp;</strong></p>\r\n<p><strong style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">Cilj vsake zdravstvene ustanove je optimizirati čas trajanja operacijskega posega in povečati njegovo kakovost. DORA je interaktiven zdravnikov asistent, ki omogoča edinstven prikaz informacij o bolniku pred in med operacijskimi posegi. Je produkt znanja stroke, sodobnih informacijsko komunikacijskih tehnologij ter napredne strojne opreme, ki s preprosto uporabo brezkontaktne interakcije kraj&scaron;a čas trajanja posegov ter s tem neposredno vpliva na dolžino čakalnih dob.&nbsp;<br /><br />Člani ekipe:&nbsp;<br />Bo&scaron;tjan Arzen&scaron;ek, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko, Univerza v Mariboru.&nbsp;<br />Andraž Leitgeb, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko, Univerza v Mariboru.&nbsp;<br />Zedin Salkanović, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko, Univerza v Mariboru.&nbsp;<br /><br />Mentor:&nbsp;<br />Kristjan Ko&scaron;ič, Fakulteta za elektrotehniko, računalni&scaron;tvo in informatiko, Univerza v Mariboru.&nbsp;<br /><br />Zunanji sodelavec:<br />Matej Vogrinčič, Univerzitetni klinični center Maribor.&nbsp;<br /><br />Viri:&nbsp;<a style="color: #1c4980;" href="http://www.dsi2013.si/default.aspx?id=69&amp;l1=21">http://www.dsi2013.si/default.aspx?id=69&amp;l1=21</a>&nbsp;&nbsp;<br />Povezave:&nbsp;<a style="color: #1c4980;" href="http://www.dora-project.info/">http://www.dora-project.info</a>&nbsp;,&nbsp;<a style="color: #1c4980;" href="http://www.facebook.com/DoraProject">http://www.facebook.com/DoraProject</a>&nbsp;<br /></strong></p>', '2013-06-14 17:03:21', 'janez1234', 'slika2.jpg'),
(44, 'Odlična predstavitev na sejmu LOS 2013 v Ljubljani', '<p>V času od 17. 04. 2013 do 20.04. 2013, je na Gospodarskem razstavi&scaron;ču V Ljubljani potekal že tretji Ljubljanski obrtno podjetni&scaron;ki sejem. Organizatorja sejma sta bila OZS in GR iz Ljubljane. V okviru odbora za znanost in tehnologijo in strokovne sekcije elektronikov in mehatronikov so se predstavili: FERI &ndash; Univerza v Mariboru, Fakulteta za elektrotehniko, Univerze v Ljubljani z različnimi laboratorijami, Institut Jožef Stefan z različnimi odseki, Kemijski in&scaron;titut iz Ljubljane z različnimi laboratorijami, Univerza v Novi Gorici, Univerza v Mariboru, Center odličnosti Namaste, Center odličnosti Nanocenter, SER&Scaron;, VS&Scaron; &Scaron;C Ptuj, T&Scaron;C Kranj, &Scaron;C Velenje. Sodelovala so tudi podjetja: Miel Elektronika iz Velenja, PS d.o.o iz Logatca, National Instruments Slovenija in druga. Na sejmu so bile predstavljene nove tehnologije iz različnih področji: elektronike, mehatronike, avtomatike, robotike, IKT, bionike, nanotehnologije in drugih področji. Sejem je predstavil tudi primere povezovanja gospodarstva in znanosti, ter primere dobre prakse. Medijski partnerji sejemske predstavitve so bile revije: IRT 3000, Ventil, Avtomatika, Science Illustrated, priloga Večera Kvadrati, Računalni&scaron;ke novice in drugi.</p>', '2013-06-14 17:08:39', 'klemenzarn', 'tfg2.jpg'),
(46, 'Pripravljeni na zakon o piškotkih?', '<p>Pi&scaron;kotki itd.....</p>', '2013-06-14 18:45:03', 'janez1234', 'zakonopikotkih.jpg');

-- --------------------------------------------------------

--
-- Struktura tabele `ocenjevanje_prispevkov`
--

CREATE TABLE IF NOT EXISTS `ocenjevanje_prispevkov` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `id_prispevka` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=40 ;

--
-- Odloži podatke za tabelo `ocenjevanje_prispevkov`
--

INSERT INTO `ocenjevanje_prispevkov` (`id`, `user`, `id_prispevka`) VALUES
(17, 'PETERPL4406', 1),
(18, 'klemenzarn', 5),
(19, 'klemenzarn', 6),
(20, 'klemenzarn', 7),
(21, 'PETERPL4406', 16),
(22, 'PETERPL4406', 15),
(23, 'TheBenger', 7),
(24, 'TheBenger', 8),
(25, 'TheBenger', 6),
(26, 'TheBenger', 5),
(27, 'pipika', 5),
(28, 'pipika', 19),
(29, 'pipika', 18),
(30, 'TheBenger', 22),
(31, 'TheBenger', 23),
(32, 'pipika', 23),
(33, '', 24),
(34, '', 23),
(35, 'klemenzarn', 25),
(36, 'klemenkeko', 25),
(37, 'klemenkeko', 24),
(38, 'klemenkeko', 23),
(39, 'klemenkeko', 22);

-- --------------------------------------------------------

--
-- Struktura tabele `oglas`
--

CREATE TABLE IF NOT EXISTS `oglas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `vsebina` text COLLATE utf8_slovenian_ci NOT NULL,
  `avtor` varchar(200) COLLATE utf8_slovenian_ci NOT NULL,
  `datum` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=22 ;

--
-- Odloži podatke za tabelo `oglas`
--

INSERT INTO `oglas` (`id`, `naslov`, `vsebina`, `avtor`, `datum`) VALUES
(16, 'yfdsfds', '<p>fsdfsdfsgfgggg</p>', 'KlemenKeKo', '2013-06-13 15:45:01'),
(17, 'RIT UNI ZAGOVORI 2', '<p>PRITI</p>', 'KlemenKeKo', '2013-06-14 12:55:09'),
(18, 'Ustni izpit iz Diferencialnih enačb', '<p><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">Prvi teoretični izpitni rok iz Diferencialnih enačb bo v četrtek, 20.6. ob 10. uri v predavalnici G2-P03. Obvezno se prosim prijavite preko Moodla! Tatjana Petek</span></p>', 'klemenzarn', '2013-06-14 17:11:31'),
(19, 'Govorilne ure in ustni izpiti - prof. Pihler', '<p><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;">Ustni izpiti in govorilne ure pri prof. Jožetu Pihlerju se prestavijo iz srede 19.6.2013 na četrtek 20.6.2013 od 7.00 do 12.00.</span><br style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;" /><br style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;" /><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;">Hvala za razumevanje&nbsp;</span><br style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;" /><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px;">Jože Pihler</span></p>', 'klemenzarn', '2013-06-14 17:12:17'),
(20, 'EUN1 in TKUN1 Rešitev Ekon in management 2. kolokvij B', '<p><span style="font-family: tahoma, verdana, sans-serif; font-size: 12px; line-height: 16.796875px; text-align: justify;">Objavljam re&scaron;itve 2. kolokvija skupine&nbsp;B pri predmetu Ekonomija in management.</span></p>', 'klemenzarn', '2013-06-14 17:15:03'),
(21, 'RIT UNI VAJE PV', '<p>Danes vaje <strong>odpadejo</strong></p>', 'klemenzarn', '2013-06-14 18:52:11');

-- --------------------------------------------------------

--
-- Struktura tabele `prispevki`
--

CREATE TABLE IF NOT EXISTS `prispevki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vsebina` text COLLATE utf8_slovenian_ci NOT NULL,
  `plus` int(11) NOT NULL DEFAULT '0',
  `minus` int(11) NOT NULL DEFAULT '0',
  `uporabnik` int(11) NOT NULL,
  `tema` int(11) NOT NULL,
  `cas` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=26 ;

--
-- Odloži podatke za tabelo `prispevki`
--

INSERT INTO `prispevki` (`id`, `vsebina`, `plus`, `minus`, `uporabnik`, `tema`, `cas`) VALUES
(1, 'Danes je sončno vreme. Se strinjate?', 2, 0, 461, 1, '2013-06-12 18:41:32'),
(2, 'Danes je sončno vreme. Se strinjate?', 0, 0, 461, 2, '2013-06-12 18:41:37'),
(3, 'Danes je sončno vreme. Se strinjate?', 0, 0, 461, 3, '2013-06-12 18:41:41'),
(4, 'Danes je sončno vreme. Se strinjate?', 0, 0, 461, 4, '2013-06-12 18:41:45'),
(5, '<p>Zelo zanimivo</p>', 6, 2, 461, 5, '2013-06-12 18:47:28'),
(6, '<p>fkjdsflksd</p>', 3, 1, 461, 5, '2013-06-12 18:50:52'),
(7, '<p>Kako ste kaj?</p>', 3, 0, 461, 5, '2013-06-17 14:04:35'),
(8, '<p>kako kej kurbe</p>', 0, 2, 461, 5, '2013-06-17 14:08:21'),
(9, '<p>Danes je lep dan</p>', 0, 1, 461, 5, '2013-06-17 14:09:45'),
(10, '<p>Danes je lepo in vroče vreme</p>', 0, 1, 461, 5, '2013-06-17 14:10:51'),
(11, '<p>danes je zelo lepo vremenska napoved &scaron;upak</p>', 1, 0, 461, 5, '2013-06-17 14:16:09'),
(12, '<p>Ali mislite da bo danes čudno vreme? Ste dobro? fldskjhgf</p>', 16, 0, 461, 6, '2013-06-17 14:18:41'),
(13, '<p>Jure žnidarec klepetulja!</p>', 2, 0, 461, 6, '2013-06-17 14:19:42'),
(14, '<p>eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf veagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf vcgvfdg&lt;eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf &nbsp;eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf&nbsp;</p>', 0, 1, 461, 6, '2013-06-17 17:10:32'),
(15, '<p>bgggggggggggggggggggfgffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff</p>', 1, 1, 461, 6, '2013-06-17 17:12:35'),
(16, '<p>fdsfdsfsd</p>', 2, 0, 461, 7, '2013-06-17 18:28:50'),
(17, '<p>A bo</p>', 1, 0, 496, 5, '2013-06-17 18:57:54'),
(18, '<p>Kje ste čapci a? :D</p>\r\n<p>Vidim da sta zadost fino tole zrihtala ...</p>', 1, 1, 488, 5, '2013-06-17 21:17:41'),
(19, '<p>jurček, je fina breskvica?</p>', 1, 0, 496, 5, '2013-06-17 21:53:54'),
(20, '<p>Je bla zelo fina!</p>', 0, 0, 488, 5, '2013-06-17 21:54:11'),
(21, '<p>kekica je bil zelo priden pri tem projektu.</p>', 0, 0, 496, 5, '2013-06-17 21:55:20'),
(22, '<p><strong style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">League of Legends</strong><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">&nbsp;(znana tudi po kratici LoL) je video igra tipa&nbsp;</span><a class="new" style="text-decoration: none; color: #a55858; background-image: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;" title="MOBA (stran ne obstaja)" href="http://sl.wikipedia.org/w/index.php?title=MOBA&amp;action=edit&amp;redlink=1">MOBA</a><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">&nbsp;(Multiplayer Online Battle Arena). Razvil jo je studio&nbsp;</span><a class="new" style="text-decoration: none; color: #a55858; background-image: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;" title="Riot Games (stran ne obstaja)" href="http://sl.wikipedia.org/w/index.php?title=Riot_Games&amp;action=edit&amp;redlink=1">Riot Games</a><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">, primarno za&nbsp;</span><a style="text-decoration: none; color: #0b0080; background-image: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;" title="Microsoft Windows" href="http://sl.wikipedia.org/wiki/Microsoft_Windows">Microsoft Windows</a><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">. Napovedana je bila 7. oktobra 2008, nato je od 10. aprila 2009 do 22. oktobra 2009 delovala kot zaprta beta. Dokončno je bila izdana 27. Oktobra 2009</span><sup id="cite_ref-2" class="reference" style="line-height: 1em; unicode-bidi: -webkit-isolate; font-family: sans-serif;"><a style="text-decoration: none; color: #0b0080; background-image: none; white-space: nowrap; background-position: initial initial; background-repeat: initial initial;" href="http://sl.wikipedia.org/wiki/League_of_Legends#cite_note-2">[2]</a></sup><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">. Igra ima svoj&nbsp;</span><a style="text-decoration: none; color: #0b0080; background-image: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;" title="YouTube" href="http://sl.wikipedia.org/wiki/YouTube">YouTube</a><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">&nbsp;portal, kjer obve&scaron;čajo o novostih in spremembah v igri. Igro igra več kot 32 milijonov ljudi (podatki 18. novembra 2011)</span><sup id="cite_ref-3" class="reference" style="line-height: 1em; unicode-bidi: -webkit-isolate; font-family: sans-serif;"><a style="text-decoration: none; color: #0b0080; background-image: none; white-space: nowrap; background-position: initial initial; background-repeat: initial initial;" href="http://sl.wikipedia.org/wiki/League_of_Legends#cite_note-3">[3]</a></sup><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">. Leta 2011 je prejela nagrado Golden Joystick (ena izmed najbolj prestižnih nagrad za igre) za najbolj&scaron;o zastonj igro leta 2011</span><sup id="cite_ref-4" class="reference" style="line-height: 1em; unicode-bidi: -webkit-isolate; font-family: sans-serif;"><a style="text-decoration: none; color: #0b0080; background-image: none; white-space: nowrap; background-position: initial initial; background-repeat: initial initial;" href="http://sl.wikipedia.org/wiki/League_of_Legends#cite_note-4">[4]</a></sup><span style="font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">.</span></p>\r\n<p>&nbsp;</p>', 2, 0, 496, 8, '2013-06-17 22:03:50'),
(23, '<p>Blaž Klemenčič je čisto znorel!!!</p>', 2, 2, 488, 8, '2013-06-17 22:07:30'),
(24, '<p>benger, zmanj&scaron;aj svojo prikazano slikco, ker je prevelika!!!!!!!!!!</p>', 2, 0, 496, 8, '2013-06-17 22:11:44'),
(25, '<p>Da da :)&nbsp;<span style="color: #ff00ff;">Bila je huda mravljica</span></p>', 2, 0, 461, 8, '2013-06-18 16:10:53');

-- --------------------------------------------------------

--
-- Struktura tabele `razporeditev`
--

CREATE TABLE IF NOT EXISTS `razporeditev` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razporeditev` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT 'novice,oglasna',
  `uporabnik` int(11) NOT NULL,
  `staticno` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=21 ;

--
-- Odloži podatke za tabelo `razporeditev`
--

INSERT INTO `razporeditev` (`id`, `razporeditev`, `uporabnik`, `staticno`) VALUES
(3, 'novice,oglasna_deska,urnik,tvspored,koledar', 12, 'novice2'),
(5, 'novice,urnik,oglasna_deska,koledar,zapogl', 426, 'novice2'),
(6, 'oglasna_deska,novice,koledar,tvspored,dogodki', 475, 'novice2'),
(7, 'oglasna_deska,koledar,urnik,novice,tvspored,zapogl,kino,dogodki', 488, 'novice2'),
(8, 'novice,oglasna_deska,koledar', 493, 'novice2'),
(9, 'novice,oglasna_deska,koledar', 494, 'novice2'),
(10, 'novice,oglasna_deska,urnik,koledar', 484, 'novice2'),
(11, 'novice,oglasna_deska,urnik,kino', 461, 'novice2'),
(12, 'novice,oglasna_deska,koledar', 463, 'novice2'),
(13, 'novice,oglasna_deska,koledar', 460, 'novice2'),
(14, 'novice,oglasna_deska,urnik,koledar,kino,tvspored,dogodki', 496, 'novice2'),
(15, 'novice,oglasna_deska,koledar', 497, 'novice2'),
(16, 'novice,oglasna_deska,urnik,koledar', 499, 'novice2'),
(17, 'novice,oglasna_deska,koledar', 498, 'novice2'),
(18, 'novice,oglasna_deska,koledar', 500, 'novice2'),
(19, 'novice,oglasna_deska,koledar', 501, 'novice2'),
(20, 'novice,oglasna_deska,koledar', 507, 'novice2');

-- --------------------------------------------------------

--
-- Struktura tabele `slika`
--

CREATE TABLE IF NOT EXISTS `slika` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime_slike` varchar(50) NOT NULL,
  `velikost_slike` bigint(20) NOT NULL,
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Odloži podatke za tabelo `slika`
--

INSERT INTO `slika` (`id`, `ime_slike`, `velikost_slike`, `album_id`) VALUES
(34, 'IMG_1990.JPG', 141, 7),
(35, 'IMG_1991.JPG', 136, 7),
(36, 'IMG_1992.JPG', 174, 7),
(37, 'IMG_1993.JPG', 166, 7),
(38, 'IMG_1994.JPG', 168, 7),
(39, 'IMG_1995.JPG', 166, 7),
(40, 'IMG_1996.JPG', 178, 7),
(41, 'IMG_1997.JPG', 149, 7),
(42, 'IMG_1998.JPG', 190, 7),
(43, 'IMG_1999.JPG', 151, 7),
(44, 'IMG_2000.JPG', 173, 7),
(45, 'IMG_2001.JPG', 170, 7),
(49, 'IMG_0784.JPG', 123, 10),
(50, 'IMG_0786.JPG', 160, 10),
(51, 'IMG_0787.JPG', 146, 10),
(52, 'IMG_0788.JPG', 175, 10),
(53, 'IMG_0789.JPG', 159, 10),
(54, 'IMG_0790.JPG', 170, 10),
(55, 'IMG_0791.JPG', 142, 10),
(56, 'IMG_0792.JPG', 186, 10),
(57, 'IMG_0793.JPG', 155, 10),
(58, 'IMG_0794.JPG', 187, 10),
(59, 'IMG_0795.JPG', 192, 10),
(60, 'IMG_0796.JPG', 160, 10),
(61, 'IMG_0797.JPG', 170, 10),
(62, 'IMG_0798.JPG', 133, 10),
(63, 'IMG_0799.JPG', 211, 10),
(64, 'IMG_0800.JPG', 186, 10),
(65, 'IMG_0801.JPG', 131, 10),
(66, 'IMG_0802.JPG', 178, 10),
(67, 'IMG_0803.JPG', 174, 10),
(68, 'IMG_0804.JPG', 179, 10),
(69, '1.jpg', 106, 11),
(70, '2.jpg', 448, 11),
(71, '3.jpg', 447, 11),
(72, '4.jpg', 244, 11),
(73, '5.jpg', 463, 11);

-- --------------------------------------------------------

--
-- Struktura tabele `teme`
--

CREATE TABLE IF NOT EXISTS `teme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `url_naslov` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT ' ',
  `uporabnik` int(11) NOT NULL,
  `kategorija` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `cas_prispevka` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=9 ;

--
-- Odloži podatke za tabelo `teme`
--

INSERT INTO `teme` (`id`, `naslov`, `url_naslov`, `uporabnik`, `kategorija`, `status`, `cas_prispevka`) VALUES
(1, 'Prva tema', 'Prva-tema', 461, 1, 1, '2013-06-12 18:39:55'),
(2, 'Prva tema', 'Prva-tema', 461, 2, 0, '2013-06-12 18:40:03'),
(3, 'Prva tema', 'Prva-tema', 461, 3, 0, '2013-06-12 18:40:07'),
(4, 'Prva tema', 'Prva-tema', 461, 4, 0, '2013-06-12 18:40:21'),
(5, 'Kako ste kej?', 'Kako-ste-kej', 461, 1, 1, '2013-06-17 21:55:20'),
(6, 'Vremenska napoved', 'Vremenska-napoved', 461, 1, 0, '2013-06-17 18:23:52'),
(7, 'Danes-Je???pravo-_vreme', 'Danes-Je-pravo-vreme', 461, 1, 0, '2013-06-17 18:28:50'),
(8, 'League of Legends ', 'League-of-Legends', 496, 1, 0, '2013-06-19 12:52:47');

-- --------------------------------------------------------

--
-- Struktura tabele `tf`
--

CREATE TABLE IF NOT EXISTS `tf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Lema` varchar(100) CHARACTER SET latin1 NOT NULL,
  `St_pojavitev` int(11) NOT NULL,
  `Dokument` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=222 ;

--
-- Odloži podatke za tabelo `tf`
--

INSERT INTO `tf` (`ID`, `Lema`, `St_pojavitev`, `Dokument`) VALUES
(5, 'danes', 1, '11'),
(6, 'biti', 1, '11'),
(7, 'zelo', 1, '11'),
(8, 'lep', 1, '11'),
(9, 'vremenski', 1, '11'),
(10, 'napoved', 1, '11'),
(11, 'scaron', 1, '11'),
(12, 'upak', 1, '11'),
(13, 'ali', 1, '12'),
(14, 'misliti', 1, '12'),
(15, 'dati', 1, '12'),
(16, 'biti', 2, '12'),
(17, 'danes', 1, '12'),
(18, 'cuden', 1, '12'),
(19, 'vreme', 1, '12'),
(20, 'dober', 1, '12'),
(21, 'jura', 1, '13'),
(22, 'žnidarec', 1, '13'),
(23, 'klepetulja', 1, '13'),
(24, 'eagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf', 15, '14'),
(25, 'veagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdfeagfdsgfdgdfsgdf', 1, '14'),
(26, 'vcgvfdg', 1, '14'),
(27, 'lt', 1, '14'),
(28, 'nbsp', 2, '14'),
(29, 'bgggggggggggggggggggfgffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', 1, '15'),
(30, 'fdsfdsfsd', 1, '16'),
(31, 'biti', 1, '17'),
(32, 'kje', 1, '18'),
(33, 'biti', 2, '18'),
(34, 'capec', 1, '18'),
(35, 'videti', 1, '18'),
(36, 'dati', 1, '18'),
(37, 'zadost', 1, '18'),
(38, 'fin', 1, '18'),
(39, 'tale', 1, '18'),
(40, 'zrihtati', 1, '18'),
(41, 'jurcek', 1, '19'),
(42, 'biti', 1, '19'),
(43, 'fin', 1, '19'),
(44, 'breskvica', 1, '19'),
(45, 'biti', 2, '20'),
(46, 'zelo', 1, '20'),
(47, 'fin', 1, '20'),
(48, 'kekica', 1, '21'),
(49, 'biti', 2, '21'),
(50, 'zelo', 1, '21'),
(51, 'priden', 1, '21'),
(52, 'pri', 1, '21'),
(53, 'ta', 1, '21'),
(54, 'projekt', 1, '21'),
(55, 'strong', 4, '22'),
(56, 'style', 27, '22'),
(57, 'font-family', 17, '22'),
(58, 'sans-serif', 17, '22'),
(59, 'font-siza', 14, '22'),
(60, 'px', 31, '22'),
(61, 'line-height', 17, '22'),
(62, 'league', 7, '22'),
(63, 'of', 8, '22'),
(64, 'legends', 8, '22'),
(65, 'span', 16, '22'),
(66, 'nbsp', 9, '22'),
(67, 'znan', 1, '22'),
(68, 'tudi', 1, '22'),
(69, 'po', 1, '22'),
(70, 'kratica', 1, '22'),
(71, 'lol', 1, '22'),
(72, 'biti', 8, '22'),
(73, 'video', 1, '22'),
(74, 'igrati', 3, '22'),
(75, 'tip', 1, '22'),
(76, 'class', 8, '22'),
(77, 'new', 3, '22'),
(78, 'text-decoration', 10, '22'),
(79, 'nona', 21, '22'),
(80, 'color', 11, '22'),
(81, 'background-image', 10, '22'),
(82, 'title', 9, '22'),
(83, 'moba', 3, '22'),
(84, 'stran', 3, '22'),
(85, 'ne', 3, '22'),
(86, 'obstajati', 3, '22'),
(87, 'href', 10, '22'),
(88, 'http', 11, '22'),
(89, 'sl.', 11, '22'),
(90, 'wikipedia', 11, '22'),
(91, 'org', 11, '22'),
(92, 'index', 3, '22'),
(93, 'php', 3, '22'),
(94, 'amp', 6, '22'),
(95, 'action', 3, '22'),
(96, 'editi', 3, '22'),
(97, 'redlink', 3, '22'),
(98, 'multiplayer', 1, '22'),
(99, 'online', 1, '22'),
(100, 'battle', 1, '22'),
(101, 'arena', 1, '22'),
(102, 'razviti', 1, '22'),
(103, 'jo', 1, '22'),
(104, 'studio', 1, '22'),
(105, 'riot', 5, '22'),
(106, 'games', 5, '22'),
(107, 'primaren', 1, '22'),
(108, 'za', 3, '22'),
(109, 'microsoft', 3, '22'),
(110, 'windows', 3, '22'),
(111, 'wik', 7, '22'),
(112, 'napovedan', 1, '22'),
(113, 'oktober', 3, '22'),
(114, 'nato', 1, '22'),
(115, 'od', 1, '22'),
(116, 'april', 1, '22'),
(117, 'do', 1, '22'),
(118, 'delovati', 1, '22'),
(119, 'kot', 2, '22'),
(120, 'zaprt', 1, '22'),
(121, 'bet', 1, '22'),
(122, 'dokoncen', 1, '22'),
(123, 'izdan', 1, '22'),
(124, 'sup', 6, '22'),
(125, 'id', 3, '22'),
(126, 'ci', 6, '22'),
(127, 'ref', 3, '22'),
(128, 'referenca', 3, '22'),
(129, 'em', 7, '22'),
(130, 'unicode-bid', 3, '22'),
(131, 'webkit-isolata', 3, '22'),
(132, 'white-space', 3, '22'),
(133, 'nowrap', 3, '22'),
(134, 'background-position', 6, '22'),
(135, 'initial', 24, '22'),
(136, 'background-repeat', 6, '22'),
(137, 'nota', 3, '22'),
(138, 'imeti', 1, '22'),
(139, 'svoj', 1, '22'),
(140, 'youtuba', 3, '22'),
(141, 'portal', 1, '22'),
(142, 'kjer', 1, '22'),
(143, 'obv', 1, '22'),
(144, 'scaron', 2, '22'),
(145, 'cati', 1, '22'),
(146, 'novost', 1, '22'),
(147, 'in', 1, '22'),
(148, 'sprememba', 1, '22'),
(149, 'igra', 4, '22'),
(150, 'veliko', 1, '22'),
(151, 'milijon', 1, '22'),
(152, 'clovek', 1, '22'),
(153, 'podatek', 1, '22'),
(154, 'november', 1, '22'),
(155, 'leto', 2, '22'),
(156, 'prejeti', 1, '22'),
(157, 'nagrada', 2, '22'),
(158, 'golden', 1, '22'),
(159, 'joystick', 1, '22'),
(160, 'en', 1, '22'),
(161, 'izmed', 1, '22'),
(162, 'najbolj', 2, '22'),
(163, 'prestižen', 1, '22'),
(164, 'zastonj', 1, '22'),
(165, 'tabla', 2, '22'),
(166, 'infobox', 1, '22'),
(167, 'vevent', 1, '22'),
(168, 'background-color', 1, '22'),
(169, 'f9f9f', 1, '22'),
(170, 'border', 1, '22'),
(171, 'solid', 1, '22'),
(172, 'aaaaaa', 1, '22'),
(173, 'margin', 1, '22'),
(174, 'padding', 1, '22'),
(175, 'clear', 1, '22'),
(176, 'right', 1, '22'),
(177, 'width', 2, '22'),
(178, 'cellspacing', 1, '22'),
(179, 'cellpadding', 1, '22'),
(180, 'tbody', 2, '22'),
(181, 'tr', 4, '22'),
(182, 'td', 6, '22'),
(183, 'vertical-align', 4, '22'),
(184, 'top', 3, '22'),
(185, 'text-align', 1, '22'),
(186, 'center', 1, '22'),
(187, 'colspan', 1, '22'),
(188, 'image', 1, '22'),
(189, 'slikati', 1, '22'),
(190, 'loga', 4, '22'),
(191, 'png', 4, '22'),
(192, 'img', 1, '22'),
(193, 'border-style', 1, '22'),
(194, 'midsti', 1, '22'),
(195, 'src', 1, '22'),
(196, 'upload', 1, '22'),
(197, 'wikimedia', 1, '22'),
(198, 'thumb', 1, '22'),
(199, 'px-league', 1, '22'),
(200, 'alt', 1, '22'),
(201, 'height', 1, '22'),
(202, 'razvijalec', 2, '22'),
(203, 'videoiger', 2, '22'),
(204, 'blaž', 1, '23'),
(205, 'klemencic', 1, '23'),
(206, 'biti', 1, '23'),
(207, 'cist', 1, '23'),
(208, 'znorel', 1, '23'),
(209, 'benger', 1, '24'),
(210, 'tvoj', 1, '24'),
(211, 'slikec', 1, '24'),
(212, 'biti', 1, '24'),
(213, 'zadost', 1, '24'),
(214, 'fin', 1, '24'),
(215, 'danes', 1, '25'),
(216, 'geekati', 1, '25'),
(217, 'lol', 1, '25'),
(218, 'strong', 2, '25'),
(219, 'oleeeo', 1, '25'),
(220, 'leoeeleo', 1, '25'),
(221, 'iopclimpija', 1, '25');

-- --------------------------------------------------------

--
-- Struktura tabele `urnik`
--

CREATE TABLE IF NOT EXISTS `urnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `predmet` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `dan` int(11) NOT NULL,
  `ucilnica` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `zacetek` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `konec` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `vrsta` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `ime` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `priimek` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=42 ;

--
-- Odloži podatke za tabelo `urnik`
--

INSERT INTO `urnik` (`id`, `predmet`, `dan`, `ucilnica`, `zacetek`, `konec`, `vrsta`, `ime`, `priimek`, `user`) VALUES
(30, 'Algoritmi rač. aplikacij', 4, 'F105f', '16:15', '18:30', 'LV', 'Simon', 'Gangl', 12),
(31, 'Programski vzorci', 5, 'F104', '11:15', '13:30', 'LV', 'Dominik', 'Lentar', 12),
(32, 'Spletno programiranje', 3, 'F104', '12:15', '17:00', 'LV', 'Marko', 'Ferme', 12),
(33, 'Prevajanje programskih jezikov', 2, 'F104', '12:00', '14:00', 'LV', 'Marjan', 'Mernik', 12),
(34, 'Sistemska administracija', 1, 'F104', '7:00', '9:00', 'LV', 'Damjan', 'Časar', 174),
(35, 'SA - vaje', 1, 'F103', '9:15', '11:15', 'LV', 'Fister,', 'Casar', 370),
(36, 'Sistemska administracija', 2, 'F104', '9:15', '11:15', 'LV', 'Damjan', 'Časar', 461),
(37, 'Programski vzorci', 2, 'F-102', '11:15', '13:30', 'LV', 'Borko', 'Boškovič', 461),
(38, 'Programski vzorci', 2, 'F104', '7:00', '9:00', 'LV', 'Damjan', 'Časar', 496),
(39, 'Predstavitve SP', 3, 'F-102', '13:00', '15:00', 'SV', 'Marko', 'Ferme', 461),
(40, '', 1, '', '7:00', '7:00', 'PR', '', '', 461),
(41, 'klwčš', 1, 'kfelwfd', '7:00', '7:00', 'PR', 'klemen', '', 461);

-- --------------------------------------------------------

--
-- Struktura tabele `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ime` varchar(30) COLLATE utf8_slovenian_ci NOT NULL,
  `Priimek` varchar(30) COLLATE utf8_slovenian_ci NOT NULL,
  `Uporabnisko` varchar(30) COLLATE utf8_slovenian_ci NOT NULL,
  `Geslo` varchar(32) COLLATE utf8_slovenian_ci NOT NULL,
  `Mail` varchar(30) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Vpisna` varchar(30) COLLATE utf8_slovenian_ci NOT NULL,
  `Tip` int(11) NOT NULL,
  `Datum_rojstva` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Drzava` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Mesto` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Prvi_dostop` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Zadnji_dostop` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Slika` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Pot` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Mapa` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `CelotnaPot` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Smer` varchar(512) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Letnik` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  `x` varchar(200) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '0',
  `z` varchar(200) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '0',
  `geslopic` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=510 ;

--
-- Odloži podatke za tabelo `users`
--

INSERT INTO `users` (`ID`, `Ime`, `Priimek`, `Uporabnisko`, `Geslo`, `Mail`, `Vpisna`, `Tip`, `Datum_rojstva`, `Drzava`, `Mesto`, `Prvi_dostop`, `Zadnji_dostop`, `Slika`, `Pot`, `Mapa`, `CelotnaPot`, `Smer`, `Letnik`, `Active`, `x`, `z`, `geslopic`) VALUES
(459, 'SKELA', 'CERU', 'SKELACE3028', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1043028', 3, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(460, 'MARCEL', 'DOBRAJC', 'MARCELDO3814', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1053814', 1, NULL, NULL, NULL, NULL, '2013-06-14 17:57:27', 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(461, 'Klemen', 'Zarn', 'klemenzarn', '827ccb0eea8a706c4c34a16891f84e7b', 'klemenk12o.zarn@hotmail.com', 'E1053263', 3, '30.12.1992', '', '', '', '2014-01-22 14:14:00', 'prenos.jpg', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 1, 1, '0', '0', ''),
(462, 'MATEJA', 'HAZL', 'MATEJAHA5171', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055171', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(463, 'DARIO', 'HORVAT', 'DARIOHO3578', '827ccb0eea8a706c4c34a16891f84e7b', 'jan.grah12@gmail.com', 'E1043578', 1, '', '', '', '', '2013-06-14 13:24:26', '001.jpg', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(464, 'MARKO', 'KAVAS', 'MARKOKA3477', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1043477', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(465, 'DANILO', 'KOLUDROVIC', 'DANILOKO3482', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1043482', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(466, 'MATEJ', 'LAJH', 'MATEJLA4480', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054480', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(467, 'ROK', 'LESKOVEC', 'ROKLE3509', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1043509', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(468, 'MATIC', 'LEVA', 'MATICLE5535', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055535', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(469, 'NEJC', 'LUBEJ', 'NEJCLU4374', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054374', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(470, 'TADEJ', 'LUDVIK', 'TADEJLU5304', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055304', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(471, 'INES', 'MARKO', 'INESMA5269', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055269', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(472, 'DAMJAN', 'MARKOVIC', 'DAMJANMA4536', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054536', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(473, 'LUKA', 'MARKUS', 'LUKAMA6031', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1056031', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(474, 'ZIGA', 'MARKUS', 'ZIGAMA6013', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1056013', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(475, 'Peter', 'Plut', 'PETERPL4406', '827ccb0eea8a706c4c34a16891f84e7b', 'peter.plut@gmail.com', 'E1054406', 3, '12.11.1992', '', '', '', '2013-06-17 21:33:28', 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 0, 0, '0', '0', ''),
(476, 'JURIJ', 'PODGORSEK', 'JURIJPO4475', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054475', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(477, 'TADEJ', 'PREGL', 'TADEJPR5450', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055450', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(478, 'MATEJ', 'ROGAC', 'MATEJRO4891', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054891', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(479, 'KLEMEN', 'STEFANIC', 'KLEMENST5474', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055474', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(480, 'DALIBOR', 'TADIĆ', 'DALIBORTA3464', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1043464', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(481, 'TADEJ', 'VENGUST', 'TADEJVE5441', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055441', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(482, 'MATEJ', 'ZGUBIC', 'MATEJZG3803', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1053803', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(483, 'BISERKA', 'ZINREICH', 'BISERKAZI5328', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1055328', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(485, 'MARKO', 'ZERDIN', 'MARKOZE1656', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1031656', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(486, 'SIMON', 'ZLENDER', 'SIMONZL4462', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 'E1054462', 1, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 2, 0, '0', '0', ''),
(488, 'Jure', 'Znidarec', 'Thebenger', '827ccb0eea8a706c4c34a16891f84e7b', 'jurchy2005@hotmail.com', '', 3, '07.10.1992', 'Slovenija', 'Celje', '', '2014-01-12 22:37:05', 'prenos.jpg', 'xD.jpg', 'Server/Thebenger', 'C:xampphtdocsferiappwebroot/img/Server/Thebenger', 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 0, 1, '0', '0', ''),
(489, 'jure', 'bedak', 'juremezajebava', '3e861943ba920a8ccd4ad03c093f6dfd', 'klemenko.zarn@gmail.com', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(490, 'jure', 'bedak', 'bebikamala', '3e861943ba920a8ccd4ad03c093f6dfd', 'klemenko.zarn@gmail.com', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(491, 'jure', 'bedak', 'bebikamalad', '3e861943ba920a8ccd4ad03c093f6dfd', 'klemenko.zarn@gmail.com', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(492, 'Jure', 'fdsfsd', 'jurcek', 'c913e38114baf53f4fee7aa83bb51e64', 'klemenko1.zarn@gmail.com', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(493, 'Klemen', 'Zarn', 'FiniKeKica', '9c0413ac9421378fe2054022fc3470c3', 'klemenko.zarn12@gmail.com', 'E1055544', 3, '30.12.1992', '', '', '', '2013-06-13 16:37:15', 'privzetaSlika.png', 'privzetaSlika.jpg', NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 0, 0, '0', '0', ''),
(494, 'Klemen', 'Zarn', 'upamdadela12', 'c06db68e819be6ec3d26c6038d8e8d1f', 'test1@gmail.com', 'E1055765', 1, '30.12.1992', '', '', '', '2013-06-18 18:25:45', 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 1, 0, '0', '0', ''),
(496, 'Peter', 'Plut', 'pipika', '60474c9c10d7142b7508ce7a50acf414', 'pipika.fina@gmail.com', 'E1054332', 1, '12.11.1991', 'Slovenija', '', '', '2014-01-12 21:53:18', 'pipika.jpg', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 1, 1, '0', '0', ''),
(497, 'Igor', 'Kvartuh', 'igorjelep', '745118088c731514f44dfc59a497e0f8', 'igic.1993@gmail.com', '', 1, NULL, NULL, NULL, NULL, '2013-06-14 19:10:42', 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(498, 'Matej', 'Rogač', 'matejrogac', 'f44f48467ed700b5c264ccd5614f4c82', 'krneki@hotmail.com', '', 3, '30.12.1992', 'Slovenija', '', '', '2013-06-18 17:59:26', 'privzetaSlika.png', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 0, 0, '0', '0', ''),
(499, 'Janez', 'Fini', 'janez1234', '16d7a4fca7442dda3ad93c9a726597e4', 'jan.grah123@gmail.com', 'E1055573', 3, '', '', '', '', '2013-06-18 15:40:57', 'tfg2.jpg', NULL, NULL, NULL, 'RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE', 1, 0, '0', '0', ''),
(500, 'keko', 'fini', 'klemenkeko', '9c0413ac9421378fe2054022fc3470c3', 'klemen234.zarn@gmail.com', '', 0, '13.01.2014', '', '', NULL, '2014-01-24 11:21:27', 'suzanne.jpg', NULL, NULL, NULL, NULL, 0, 1, '13.61773386025813', '50.81402375199956', '96e79218965eb72c92a549dd5a330112'),
(501, 'test1', 'kljlk', 'test1', '9c0413ac9421378fe2054022fc3470c3', 'kurac@gmail.com', '', 0, NULL, NULL, NULL, NULL, '2013-12-30 12:58:02', 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 1, '0', '0', ''),
(502, 'miha', 'niha', 'mihaniha', '9c0413ac9421378fe2054022fc3470c3', 'klemenko.zarn@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(503, 'Klemen', 'Žarn', 'mihaniha1', '9c0413ac9421378fe2054022fc3470c3', 'test@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(504, 'Klemen', 'Žarn', 'mihaniha12', '9c0413ac9421378fe2054022fc3470c3', 'test@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(505, 'Klemen', 'Žarn', 'minaniha3', '9c0413ac9421378fe2054022fc3470c3', 'mian@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(506, 'Klemen', 'Žarn', 'mihaniha54', '9c0413ac9421378fe2054022fc3470c3', 'minahmn@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(507, 'mihaniha', 'minhaniha', 'klementest', '9c0413ac9421378fe2054022fc3470c3', 'test@gmail.com', '', 0, NULL, NULL, NULL, NULL, '2014-01-14 12:42:10', 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 1, '0', '0', ''),
(508, 'KLEMEN', 'Žarn', 'nekiii', 'a5808d6f95c55b83f7a0e37ff11b28d1', 'klemenza@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', ''),
(509, 'Klemen', 'Žarn', 'nihamiha', '9c0413ac9421378fe2054022fc3470c3', 'minfsdf@gmail.com', '', 0, NULL, NULL, NULL, NULL, NULL, 'privzetaSlika.png', NULL, NULL, NULL, NULL, 0, 0, '0', '0', '');

-- --------------------------------------------------------

--
-- Struktura tabele `users_branch`
--

CREATE TABLE IF NOT EXISTS `users_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=13 ;

--
-- Odloži podatke za tabelo `users_branch`
--

INSERT INTO `users_branch` (`id`, `user`, `branch_id`) VALUES
(4, 12, 87),
(6, 488, 87),
(7, 484, 87),
(8, 461, 87),
(9, 488, 87),
(10, 488, 2),
(11, 499, 87),
(12, 496, 87);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
