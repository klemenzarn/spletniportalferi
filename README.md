Spletni Portal Feri
=================

Izvorna koda študentskega spletnega portala

Zadeva na linuxu ne deluje (webservice v C#, povezava na SQL Server)


Baza
=================

V mapi baza je sql datoteka v kateri je shranjena mysql baza. Z PhpMyAdmin enostavno uvozimo bazo. Vse povezave z bazo imajo povezavo:

host: localhost<br />
up. ime: keko<br />
geslo: keko<br />

zato priporočam, da ustvarite uporabnika keko z geslom keko.


WebService
=================

Za parsanje podatkov z ostalih spletnih straneh inza kodiranje slik z algoritmom DCT imamo narejene spletne storitve v C# Web Service. Tukaj je potrebno nasneti MySql connector in ga dodati kot referenco v C# projekt. Ostale knjižice so že vključene


WebSocket
=================

Klepet in večigralstvo po 3D objektu je narejeno z PHP Ratchet serverjem, ki preko websocketov komunicira z odjemalci. 

Enostavno v cmd-ju vpišemo:
C:\path\to\php\php.exe -f C:\path\to\websocket\folder\bin\chat-server.php

V mojem primeru:
C:\xampp\php\php.exe -f C:\xampp\htdocs\websocket\bin\chat-server.php


FERI Spletna stran
=================

Spletna stran je narejena v CakePHP frameworku, zato je potrebno na apacheju vključiti modul rewrite. Potrebno je vključiti knjižnice, da se lahko povežemo na SQL Server.

V php.ini datoteki dodamo:

[PHP_SQLSRV]
extension=php_sqlsrv_54_ts.dll
<br />
[PHP_PDO_SQLSRV]
extension=php_pdo_sqlsrv_54_ts.dll
<br />

in v mapo php/ext dodamo ti dve knjižnici, ki jih dobimo na: http://www.microsoft.com/en-us/download/details.aspx?id=20098

Bolj podroben vodič kako zadevo vzpostaviti: http://robsphp.blogspot.com/2012/09/how-to-install-microsofts-sql-server.html


Težave
=================

<a href="mailto:klemenko.zarn@gmail.com">klemenko.zarn@gmail.com</a>