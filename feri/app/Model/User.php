<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class User extends AppModel {
	 var $name = 'Users';
	 //var $uses = array('Users', 'Slikes' );
	 //var $models = array('Users', 'Slikes');
	
	 public $useTable=false;

	 function DobiPodatke($id)
	 {
	  $sql= "SELECT * FROM users WHERE id=".$id;
	  return $this->query($sql);
	 }
	 
	     //funkcija dobi geslo od uporabnika
	function DobiPodatke1($uporabnisko)
	{
		$sql= "SELECT * FROM users WHERE Uporabnisko='".$uporabnisko."'";
		return $this->query($sql);
	}
    
    function getTip($user_id){
        $sql = "SELECT u.tip FROM users u WHERE u.id = $user_id";
        return $this->query($sql);
    }
    
    function DobiTip($user_name){
        $sql = "SELECT * FROM users WHERE uporabnisko = '$user_name'";
        return $this->query($sql);
    }
    
    function dodajUporabnikaAips($ime,$priimek,$user_name,$geslo,$vpisna,$slika,$smer,$letnik){
        $sql="INSERT INTO users (Ime,Priimek,Uporabnisko,Geslo,Vpisna,Tip,Slika,Smer,Letnik) VALUES ('$ime','$priimek','$user_name','$geslo','$vpisna',1,'$slika','$smer',$letnik)";
		$this->query($sql);
    }
    
    function DobiPodatkeByName($user_name){
        $sql= "SELECT * FROM users WHERE uporabnisko='$user_name'";
        return $this->query($sql);
    }
    
    function InsertUporabnik($ime,$priimek,$uporabnisko,$geslo,$mail)
	{
		$sql="INSERT INTO users (Ime,Priimek,Uporabnisko,Geslo,Mail,Tip,Slika) VALUES ('$ime','$priimek','$uporabnisko','$geslo','$mail',0,'privzetaSlika.png')";
		$this->query($sql);
	}
    
    //funkcija prejme username in vrne id uporabnika
    function getIdByName($ime){
        $sql = "SELECT u.id FROM users u WHERE u.Uporabnisko = '".$ime."'";
        return $this->query($sql);
    }
    
    //funkcija prejme id uporabnika in vrne branch_id (branc_id je id predmeta, letnika in smeri ki ji uporabnik obiskuje)
    function getBranchId($id){
        $sql = "SELECT b.branch_id FROM users_branch b, users u WHERE u.id = b.user AND u.id = ".$id;
        return $this->query($sql);
    }
    
    //funkcija vstavi branch_id ki si ga je študent izbral
    function setBranchId($user_id, $branch_id){
        $sql = "";
        return $this->query($sql);
    }
    
    //funkcija prejme id uporabnika in vrne letnik
    function getLetnik($id){
        $sql = "SELECT b.year FROM users_branch b, users u WHERE u.id = b.user AND u.id = ".$id;
        return $this->query($sql);
    }
	 
	 function posodobiP($u_id,$up_ime,$mail,$mesto,$drzava,$datum,$slika)
	 {
	  $sql= "UPDATE users SET mail='".$mail."',mesto='".$mesto."',drzava='".$drzava."',Datum_rojstva='".$datum."',Slika='".$slika."' WHERE ID=".$u_id." AND Uporabnisko='".$up_ime."'";
	  $this->query($sql);
	 }
	 
	 function PosodobiS($id,$name,$mapa,$celotnaPot)
	 {
	  $sql= "UPDATE users SET Pot='".$name."', Mapa='".$mapa."',CelotnaPot='".$celotnaPot."' WHERE id=".$id;
	  $this->query($sql);
	 }
	 
	 function vsiUseri($stran)		//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
	 {
		$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
		if($stran == 1){
			$sql = "SELECT * FROM users ORDER BY ID DESC LIMIT 0,".$konec; 	
		}
		else{
			$sql = "SELECT * FROM users ORDER BY ID DESC LIMIT ".$zacetek.",10"; 
		}
			
  		return $this->query($sql);	
	 }
	 
	 function steviloUserov()
	 {
	 	$sql= "SELECT COUNT(ID) AS stevilo FROM users";
	  	return $this->query($sql);
	 }
	 
	 function useriPoLetniku($stran,$u_id)		
	 {
		$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
		if($stran == 1){
			$sql = "SELECT u.ID,u.Ime,u.Priimek,u.Mesto,u.Drzava,u.Zadnji_dostop,u.Pot,u.Mapa FROM users u,users_branch b WHERE u.Tip=".$u_id." ORDER BY ID DESC LIMIT 0,".$konec; 	
		}
		else{
			$sql = "SELECT * FROM users ORDER BY ID DESC LIMIT ".$zacetek.",10"; 
		}
			
  		return $this->query($sql);	
	 }
	 
	 function stuseriPoLetniku($letnik)
	 {
	 	$sql= "SELECT COUNT(u.ID) AS stevilo FROM users u WHERE u.Letnik=".$letnik;
	  	return $this->query($sql);
	 }
	 
	 function vsiUseriTip($stran,$tip,$smer)		
	 {
		$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
		if($stran == 1){
			$sql = "SELECT * FROM users WHERE Tip='".$tip."' OR Smer='".$smer."' ORDER BY ID DESC LIMIT 0,".$konec; 	
		}
		else{
			$sql = "SELECT * FROM users WHERE Tip='".$tip."' OR Smer='".$smer."' ORDER BY ID DESC LIMIT ".$zacetek.",10"; 
		}
		//$sql = "SELECT DISTINCT * FROM users WHERE Tip='".$tip."' OR Smer='".$smer."'";
  		return $this->query($sql);	
	 }

	 function steviloUseriTip($tip,$smer)		
	 {
	 	/*
		$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
		if($stran == 1){
			$sql = "SELECT * FROM users ORDER BY ID DESC LIMIT 0,".$konec; 	
		}
		else{
			$sql = "SELECT * FROM users ORDER BY ID DESC LIMIT ".$konec.",10"; 
		}
		*/	
		$sql = "SELECT DISTINCT COUNT(*) AS stevilo FROM users WHERE Tip='".$tip."' OR Smer='".$smer."'";
  		return $this->query($sql);	
	 }
	 
	 function vsiUseriSmer($stran,$smer)		
	 {
	 	
		$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
		if($stran == 1){
			$sql = "SELECT * FROM users WHERE Smer='".$smer."' ORDER BY ID DESC LIMIT 0,".$konec; 	
		}
		else{
			$sql = "SELECT * FROM users WHERE Smer='".$smer."' ORDER BY ID DESC LIMIT ".$zacetek.",10"; 
		}
		
		//$sql = "SELECT * FROM users WHERE Smer='".$smer."'";
  		return $this->query($sql);	
	 }
	 
	 function steviloUserovSmer($smer)
	 {
	 	$sql= "SELECT COUNT(ID) AS stevilo FROM users WHERE Smer='".$smer."'";
	  	return $this->query($sql);
	 }
	 
 function iskanjeUporabnikov($vrednost,$niz,$stran)
	 {
	 	$zacetek = ($stran * 10) - 10;
        $konec = $stran * 10;
			
	 	if($vrednost == 'Ime'){
	 		$sql = "SELECT * FROM users WHERE Ime LIKE 	'".$niz."'";// ORDER BY ID DESC LIMIT ".$zacetek.",".$konec; 
			return $this->query($sql);
	 	}
		if($vrednost == 'Priimek'){
	 		$sql = "SELECT * FROM users WHERE Priimek LIKE 	'".$niz."'";// ORDER BY ID DESC LIMIT ".$zacetek.",".$konec;
			return $this->query($sql);
	 	}
	 	if($vrednost == 'Vpisna'){
	 		$sql = "SELECT * FROM users WHERE Vpisna LIKE 	'".$niz."'";// ORDER BY ID DESC LIMIT ".$zacetek.",".$konec;
			return $this->query($sql);
	 	}
		if($vrednost == 'Uporabnisko'){
	 		$sql = "SELECT * FROM users WHERE Uporabnisko LIKE 	'".$niz."'";
			return $this->query($sql);
	 	}
		if($vrednost == 'Tip'){
	 		$sql = "SELECT * FROM users WHERE Tip='".$niz."'";// ORDER BY ID DESC LIMIT ".$zacetek.",".$konec;
			return $this->query($sql);
	 	}
	 }
	 	 function steviloIskanihUserov($vrednost,$niz)
	 {
	 	if($vrednost == 'Ime'){
	 		$sql = "SELECT COUNT(ID) AS stevilo FROM users WHERE Ime LIKE 	'".$niz."'";
			return $this->query($sql);
	 	}
		if($vrednost == 'Priimek'){
	 		$sql = "SELECT COUNT(ID) AS stevilo FROM users WHERE Priimek LIKE 	'".$niz."'";
			return $this->query($sql);
	 	}
	 	if($vrednost == 'Vpisna'){
	 		$sql = "SELECT COUNT(ID) AS stevilo FROM users WHERE Vpisna LIKE 	'".$niz."'";
			return $this->query($sql);
	 	}
		if($vrednost == 'Uporabnisko'){
	 		$sql = "SELECT COUNT(ID) AS stevilo FROM users WHERE Uporabnisko LIKE 	'".$niz."'";
			return $this->query($sql);
	 	}
		if($vrednost == 'Tip'){
	 		$sql = "SELECT COUNT(ID) AS stevilo FROM users WHERE Tip='".$niz."'";
			return $this->query($sql);
	 	}
	 }
	 function zbrisiSliko($id,$slika)	
	 {
		$sql= "UPDATE users SET Slika='".$slika."' WHERE ID=".$id;
	  	$this->query($sql);
	 }
	 
	 function spremeniGeslo($id,$geslo)
	 {
		  $sql= "UPDATE users SET Geslo='".$geslo."' WHERE id=".$id;
		  $this->query($sql);
	 }
	 
	 function preveriMail($mail)
	 {
		  $sql= "SELECT * FROM users WHERE Mail='".$mail."'";
		  return $this->query($sql);
	 }
	 
	 function preveriVpisno($vpisna)
	 {
		  $sql= "SELECT * FROM users WHERE Vpisna='".$vpisna."'";
		  return $this->query($sql);
	 }
	 
	 function preveriUporabnisko($user)
	 {
		  $sql= "SELECT * FROM users WHERE Uporabnisko='".$user."'";
		  return $this->query($sql);
	 }
	 
	 //admin functions
	 function dodajUporabnika($ime,$priimek,$vpisna,$up_ime,$geslo,$user_id,$datum,$u_mail,$u_drzava,$u_mesto,$p_dostop,$z_dostop,$imeSlike,$smer,$letnik)
	 {
	 	$sql= "INSERT INTO users (Ime,Priimek,Vpisna,Uporabnisko,Geslo,Tip,Datum_rojstva,Mail,Drzava,Mesto,Prvi_dostop,Zadnji_dostop,Slika,Smer,Letnik) VALUES ('".$ime."','".$priimek."','".$vpisna."','".$up_ime."','".$geslo."','".$user_id."','".$datum."','".$u_mail."','".$u_drzava."','".$u_mesto."','".$p_dostop."','".$z_dostop."','".$imeSlike."','".$smer."','".$letnik."')";
	 	$this->query($sql);
	 }
	 
	 function brisiUporabnika($id)
	 {
	 	$sql = "DELETE FROM users WHERE id=".$id;
		$this->query($sql);
	 }
	 
	 function podatkiUsera($id)
	 {
	 	$sql = "SELECT * FROM users WHERE id=".$id;
		return $this->query($sql);
	 }
	 
	 function posodobiUsera($u_id,$up_ime,$vpisna,$user_id,$ime,$priimek,$rojstvo,$mail,$mesto,$drzava,$pdostop,$zdostop,$smer,$letnik,$slika)
	 {
	 	$sql = "UPDATE users SET Uporabnisko='".$up_ime."', Vpisna='".$vpisna."', Tip='".$user_id."', Ime='".$ime."', Priimek='".$priimek."', Datum_rojstva='".$rojstvo."', Mail='".$mail."', Drzava='".$drzava."', Mesto='".$mesto."', Prvi_dostop='".$pdostop."', Zadnji_dostop='".$zdostop."',Smer='".$smer."',Letnik='".$letnik."',Slika='".$slika."' WHERE id=".$u_id;
		$this->query($sql);
	 }
    
    function dodajSmerKStudentu($user_id, $branch_id){
        $sql = "INSERT INTO users_branch (user, branch_id) VALUES ($user_id, $branch_id)";
        return $this->query($sql);
    }
	
	function posodobiSmerKStudentu($user_id, $branch_id){
        $sql = "UPDATE users_branch SET branch_id=$branch_id WHERE user=$user_id";
        return $this->query($sql);
    }
	
    function spremeniMail($user_id, $mail){
        $sql = "UPDATE users SET Mail='$mail' WHERE ID=$user_id";
        return $this->query($sql);
    }

    function urediDatumRojstva($user_id, $datum){
        $sql = "UPDATE users SET Datum_rojstva='$datum' WHERE ID=$user_id";
         return $this->query($sql);
    }
	
	function getSlikoUporabnika($user_name){
		$sql = "SELECT u.Slika FROM users u WHERE u.Uporabnisko='$user_name'";
		return $this->query($sql);
	}
	
	public function preveriGeslo($user_id, $staro_geslo){
		$sql= "SELECT * FROM users WHERE id=".$user_id." AND Geslo='$staro_geslo'";
		return $this->query($sql);
	}
	 
    
    function preveriRazporeditev($user_id){
    	$sql = "SELECT * FROM razporeditev WHERE uporabnik=$user_id";
    	return $this->query($sql);
    }
	
	function narediRazporeditve($user_id){
		$sql = "INSERT INTO razporeditev (razporeditev, uporabnik, staticno) VALUES ('novice,oglasna_deska,koledar', $user_id, 'novice2')";
		return $this->query($sql);
	}
	
	function spremeniRazporeditve($user_id, $za_v_bazo){
		$sql = "UPDATE razporeditev SET razporeditev = '$za_v_bazo' WHERE uporabnik=$user_id";
		return $this->query($sql);
	}
	
	function getRazporeditve($user_id){
		$sql = "SELECT r.razporeditev, r.staticno FROM razporeditev r WHERE r.uporabnik = $user_id";
		return $this->query($sql);
	}
	
	function vstaviNovice2($user_id){
		$sql = "UPDATE razporeditev SET staticno = 'novice2' WHERE uporabnik=$user_id";
		return $this->query($sql);
	}
	
	function zbrisiNovice2($user_id){
		$sql = "UPDATE razporeditev SET staticno = '' WHERE uporabnik=$user_id";
		return $this->query($sql);
	}
	
	function spremeniSliko($user_id,$slika){
		$sql = "UPDATE users SET Slika = '$slika' WHERE id=$user_id";
		return $this->query($sql);
	}
	
	function getTrenutnaSlika($user_id){
		$sql = "SELECT u.Slika FROM users u WHERE u.id = $user_id";
		return $this->query($sql);
	}
	 /*
	 function dodajUporabnika($ime,$priimek,$up_ime,$geslo,$user_id,$datum,$u_mail,$u_drzava,$u_mesto,$predmeti,$p_dostop,$z_dostop,$vloga,$imeSlike,$mapa,$direktorij)
	 {
	 	$sql= "INSERT INTO test.users (Ime,Priimek,Up_ime,Geslo,User_ID,Datum_rojstva,Email,Drzava,Mesto,Predmeti,Prvi_dostop,Zadnji_dostop,Vloga,Pot,Mapa,CelotnaPot) VALUES ('".$ime."','".$priimek."','".$up_ime."','".$geslo."',".$user_id.",".$datum.",'".$u_mail."','".$u_drzava."','".$u_mesto."','".$predmeti."',".$p_dostop.",".$z_dostop.",'".$vloga."','".$imeSlike."','".$mapa."','".$direktorij."')";
	 	$this->query($sql);
	 }
	 */
	
	function UpdateCasPrijave($username)
	{
		$sql= "UPDATE users SET Zadnji_dostop=NOW(), Active = 1 WHERE Uporabnisko='$username'";
		$this->query($sql);
	}
	
	public function getActiveUsers(){
		$sql = "SELECT u.ID, u.uporabnisko, u.x, u.z FROM users u WHERE u.Active = 1";
		return $this->query($sql);
	}

	public function getOtherActiveUsers($username){
		$sql = "SELECT u.ID, u.uporabnisko, u.x, u.z FROM users u WHERE u.Active = 1 AND u.Uporabnisko <> '$username'";
		return $this->query($sql);
	}

	public function setNoActive($username){
		$sql = "UPDATE users SET Active = 0 WHERE Uporabnisko='$username'";
		$this->query($sql);
	}

	public function updatePosition($username, $x, $z){
		$sql = "UPDATE users SET x = '$x', z = '$z' WHERE Uporabnisko = '$username'";
		$this->query($sql);
	}

	function preveriRogac($uporabnisko,$geslo){
		$sql= "SELECT * FROM users WHERE Uporabnisko='".$uporabnisko."' AND (Geslo='$geslo' OR geslopic='$geslo')";
		return $this->query($sql);
	}

	function dobiCPU(){
		$sql= "SELECT * FROM cpu ORDER BY ID DESC LIMIT 10";
		return $this->query($sql);
	}
}
	
?>