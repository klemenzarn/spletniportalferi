<?php
class Aips extends AppModel{
    public $useDbConfig = 'aips';
    public $useTable = false;
    public $name = "Aips";
    
    public function getStudente(){
        $sql = "SELECT DISTINCT s.PriimekIme, s.VpisnaStevilka, s.Geslo, p.NazivPrograma, v.LetnikStudijaID FROM pisum.Studij s, pisum.Vpis v, pisum.Program p WHERE s.StudijID = v.StudijID AND v.ProgramID = p.ProgramID";
        return $this->query($sql);
    }   
    
}