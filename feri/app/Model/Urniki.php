<?php
class Urniki extends AppModel{
    public $useDbConfig = 'urniki';
    public $useTable = false;
    public $name = "Urniki";
    
    public function getPrivzeteUrnike($branch_id, $datum){
        $datum = "11.3.2013";
        /*press_coverage_date BETWEEN '2000-01-01' AND '2001-01-01')*/
       // $datum_id = $this->query("SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day");
        //$id_week = $datum_id[0][0]['Week_Id'];
        $sql = "SELECT DISTINCT course.Course_Id ,course.Name AS predmet, branch.Name AS program, urnik.Day_Id AS dan, room.Name AS ucilnica, cas.Start_Hour AS zacetek, urnik.Duration as trajanje, tip.Name as vrsta, t.Valid_From AS od, t.Valid_To AS do, prof.First_Name AS ime, prof.Last_Name as priimek 
        FROM TBBranch branch, TBCourse_Branch cb, TBCourse course, TBSchedule urnik, TBRoom room, TBTime cas, TBTurnPart tp, TBTurn t, TBCoursePart cp, TBCourseType tip, TBTurn_Tutor tt, TBTutor prof
        WHERE cb.Branch_id = branch.Branch_Id AND cb.Course_Id = course.Course_Id AND course.Course_Id = urnik.Course_Id AND urnik.Room_Id = room.Room_Id AND urnik.Time_Id = cas.Time_Id AND t.Turn_Id = tt.Turn_Id AND tt.Tutor_Id = prof.Tutor_Id AND urnik.TurnPart_Id = tp.TurnPart_Id AND tp.Turn_Id = t.Turn_Id AND t.CoursePart_Id = cp.CoursePart_Id AND cp.CourseType_Id = tip.CourseType_Id AND branch.Branch_Id = ".$branch_id." AND (SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day) >= t.Valid_From AND (SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day) <= t.Valid_To ORDER BY urnik.Day_Id ASC, cas.Start_Hour ASC";
        //AND $id_week >= t.Valid_From AND  $id_week <= t.Valid_To
        return $this->query($sql);
    }
    
    public function getPrivzeteUrnikeByDay($branch_id, $datum,$dan){
        $datum = "11.3.2013";
        /*press_coverage_date BETWEEN '2000-01-01' AND '2001-01-01')*/
       // $datum_id = $this->query("SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day");
        //$id_week = $datum_id[0][0]['Week_Id'];
        $sql = "SELECT DISTINCT course.Course_Id ,course.Name AS predmet, branch.Name AS program, urnik.Day_Id AS dan, room.Name AS ucilnica, cas.Start_Hour AS zacetek, urnik.Duration as trajanje, tip.Name as vrsta, t.Valid_From AS od, t.Valid_To AS do, prof.First_Name AS ime, prof.Last_Name as priimek 
        FROM TBBranch branch, TBCourse_Branch cb, TBCourse course, TBSchedule urnik, TBRoom room, TBTime cas, TBTurnPart tp, TBTurn t, TBCoursePart cp, TBCourseType tip, TBTurn_Tutor tt, TBTutor prof
        WHERE cb.Branch_id = branch.Branch_Id AND cb.Course_Id = course.Course_Id AND course.Course_Id = urnik.Course_Id AND urnik.Room_Id = room.Room_Id AND urnik.Time_Id = cas.Time_Id AND t.Turn_Id = tt.Turn_Id AND tt.Tutor_Id = prof.Tutor_Id AND urnik.TurnPart_Id = tp.TurnPart_Id AND tp.Turn_Id = t.Turn_Id AND t.CoursePart_Id = cp.CoursePart_Id AND cp.CourseType_Id = tip.CourseType_Id AND branch.Branch_Id = ".$branch_id." AND (SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day) >= t.Valid_From AND (SELECT Week_Id FROM TBWeek WHERE convert(datetime, '$datum', 104) BETWEEN First_Day AND Last_Day) <= t.Valid_To AND urnik.Day_Id = $dan ORDER BY urnik.Day_Id ASC, cas.Start_Hour ASC";
        //AND $id_week >= t.Valid_From AND  $id_week <= t.Valid_To
        return $this->query($sql);
    }
    
    public function getAllBranch(){
        $sql = "SELECT b.Branch_Id, b.Name, b.Year FROM TBBranch b ORDER BY b.Name ASC";
        return $this->query($sql);
    }
}