<?php
class Drugenovice extends AppModel {
	var $name = 'Drugenovice';
	var $useTable = false;		
	
	public function getNoviceStran($stran){
        $zacetek = $stran * 10 - 10;
        $konec = $stran * 10;
        $sql = "SELECT n.id, n.naslov, n.vsebina, n.link, n.vir FROM drugenovice n ORDER BY n.id DESC LIMIT ".$zacetek.",".$konec; //select .... limit 40,50
		return $this->query($sql);
    }
		
	public function getStNovic(){
        $sql = "SELECT COUNT(id) AS stevilo FROM drugenovice";
        return $this->query($sql);
    }
	
	public function dodajNovico($naslov, $link, $vsebina,$vir){
		$sql = "INSERT INTO drugenovice (naslov, link, vsebina, vir) VALUES (\"".$naslov."\", \"".$link."\", \"".$vsebina."\", '$vir')";
        return $this->query($sql);
	}
	
	public function preveriNovico($naslov, $link, $vsebina){
		$sql = "SELECT * FROM drugenovice WHERE naslov = \"$naslov\" AND link = \"$link\" AND vsebina = \"$vsebina\"";
		return $this->query($sql);
	}
	
	public function getVticnik(){
		$sql = "SELECT n.naslov, n.link, n.vir FROM drugenovice n ORDER BY n.id DESC LIMIT 0,10";
		return $this->query($sql);
	}
}
	