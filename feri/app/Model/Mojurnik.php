<?php
class Mojurnik extends AppModel{
	var $name = 'Mojurnik';
	var $useTable = false;
    
    public function getMyUrnik($id_user){
        $sql = "SELECT u.id, u.predmet, u.dan, u.zacetek, u.konec, u.vrsta, u.ucilnica, u.ime, u.priimek FROM urnik u, users us WHERE us.id = u.user AND us.id = ".$id_user;
        return $this->query($sql);
    }
    
    public function getMyUrnikByDay($id_user, $dan){
        $sql = "SELECT u.id, u.predmet, u.dan, u.zacetek, u.konec, u.vrsta, u.ucilnica, u.ime, u.priimek FROM urnik u, users us WHERE us.id = u.user AND u.dan = $dan AND us.id = ".$id_user;
        return $this->query($sql);
    }
    
    public function dodajObveznost($predmet, $dan, $ucilnica, $zacetek, $konec, $vrsta, $ime, $priimek, $user_id){
        $sql = "INSERT INTO urnik (predmet, dan, ucilnica, zacetek, konec, vrsta, user, ime, priimek) VALUES ('$predmet',$dan,'$ucilnica','$zacetek','$konec','$vrsta',$user_id,'$ime','$priimek')";
        return $this->query($sql);
    }
    
    public function spremeniObveznost($id, $predmet, $dan, $ucilnica, $zacetek, $konec, $vrsta, $ime, $priimek, $user_id){
        $sql = "UPDATE urnik SET predmet = '$predmet', ucilnica='$ucilnica', dan=$dan,zacetek='$zacetek',vrsta='$vrsta', konec='$konec', ime='$ime', priimek='$priimek' WHERE id = $id AND user = $user_id";
        return $this->query($sql);
    }
    
    public function preveri($obveznost_id){
        $sql = "SELECT u.user FROM urnik u WHERE id = ".$obveznost_id;
        return $this->query($sql);
    }
    
    public function izbrisi($id, $user_id){
        $sql = "DELETE FROM urnik WHERE id = $id AND user = $user_id";
        return $this->query($sql);
    }
    
    public function getMyUrnikById($user_id, $id_obveznosti){
        $sql = "SELECT u.id, u.predmet, u.dan, u.zacetek, u.konec, u.vrsta, u.ucilnica, u.ime, u.priimek FROM urnik u WHERE user = $user_id AND id = $id_obveznosti";
        return $this->query($sql);
    }
}