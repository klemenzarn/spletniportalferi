<?php
class Koledar extends AppModel{
	public $useTable=false;
	public $name="Koledar";
	
	public function getKoledar($stevilo){
		$sql = "SELECT k.id, k.naslov, k.datum, k.vsebina FROM koledar k ORDER BY datum DESC LIMIT 0,$stevilo";
		return $this->query($sql);
	}
	
	public function getDogodek($id){
		$sql = "SELECT k.id, k.naslov, k.datum, k.vsebina FROM koledar k WHERE k.id = $id";
		return $this->query($sql);
	}
	
	public function dodajUdelezboDogodka($user_id,$id_dogodka){
		$sql = "INSERT INTO koledar_user (user_id, koledar_id) VALUES ($user_id, $id_dogodka)";
		return $this->query($sql);
	}
	
	public function preveriUdelezbo($user_id,$id_dogodka){
		$sql = "SELECT ku.id FROM koledar_user ku WHERE user_id = $user_id AND koledar_id = $id_dogodka";
		return $this->query($sql);
	}
	
	public function izbrisiUdelezbo($user_id,$id_dogodka){
		$sql = "DELETE FROM koledar_user WHERE user_id = $user_id AND koledar_id = $id_dogodka";
		return $this->query($sql);
	}
	
	public function getStUdelezb($id){
		$sql = "SELECT COUNT(id) AS stevilo FROM koledar_user WHERE koledar_id = $id";
		return $this->query($sql);
	}
	
	public function getKoledarPoMesecu($id_mesec, $leto){
		$sql = "SELECT k.ID, k.datum, k.naslov, k.vsebina, DAY(k.datum) as dan, MONTH(k.datum) as mesec FROM koledar k WHERE MONTH(k.datum) = $id_mesec AND YEAR(k.datum)=$leto ORDER BY k.datum";
		return $this->query($sql);
	}
	
	public function dodajKoledar($naslov, $vsebina, $datum){
		$sql = "INSERT INTO koledar (datum, naslov, vsebina) VALUES ('$datum', '$naslov','$vsebina')";
		return $this->query($sql);
	}
	
	public function izbrisiKoledar($id){
		$sql = "DELETE FROM koledar WHERE id = $id";
		return $this->query($sql);
	}
	
	public function posodobiKoledar($id, $naslov, $vsebina, $datum){
		$sql = "UPDATE koledar SET datum='$datum', naslov='$naslov', vsebina='$vsebina' WHERE id=$id";
		return $this->query($sql);
	}
}