<?php
class Novice extends AppModel{
	var $name = 'Novice';
	var $useTable = false;
	
    //vrne vse novice
	public function getNovice(){
		$sql = "SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum FROM novice n";
		return $this->query($sql);
	}
    
    //vrne novice naprimer katera stran je (5 stran vrne novice od 40-50)
    public function getNoviceStran($stran){
        $zacetek = $stran * 10 - 10;
        $sql = "SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum, n.naslovna_slika FROM novice n ORDER BY n.datum DESC LIMIT ".$zacetek.",10"; //select .... limit 40,50
		return $this->query($sql);
    }
	
	public function isci($niz){
		$niz = "%" . $niz . "%";
		$sql = "
		SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum, n.naslovna_slika
		FROM novice n
		WHERE n.naslov LIKE '" . $niz . "' OR n.vsebina LIKE '" . $niz . "' ORDER BY n.datum DESC";
		return $this->query($sql);
	}
    
    public function getNoviceStranStiri($stran){
        $zacetek = $stran * 10 - 10;
        $sql = "SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum, n.naslovna_slika FROM novice n ORDER BY n.datum DESC LIMIT ".$zacetek.",10"; //select .... limit 40,50
		return $this->query($sql);
    }
    
    //vrne število vseh novic v bazi
    public function getStNovic(){
        $sql = "SELECT COUNT(id) AS stevilo FROM novice";
        return $this->query($sql);
    }
	
	//vrne novico glede na njen ID
	function getById($id){
		$sql = "SELECT n.id,n.vsebina, n.naslov, n.datum, n.avtor, n.naslovna_slika FROM novice n WHERE n.id = ".$id;
		return $this->query($sql);
	}
	
	public function getStiriNovice(){
		$sql = "SELECT n.id,n.vsebina, n.naslov, n.datum, n.avtor, n.naslovna_slika FROM novice n ORDER BY n.datum DESC LIMIT 4";
		return $this->query($sql);
	}
    
    public function zbrisiNovico($id){
		$sql = "DELETE FROM novice WHERE id=".$id;
		$this->query($sql);
	}
	
	public function dodajNovico($naslov,$vsebina,$avtor,$naslovna_slika){
		$sql = "INSERT INTO novice (naslov,vsebina,datum,avtor,naslovna_slika) VALUES ('".$naslov."','".$vsebina."',NOW(),'".$avtor."','".$naslovna_slika."')";
		$this->query($sql);
	}
	
	public function DobiZadnji(){
		$sql="SELECT DISTINCT LAST_INSERT_ID() AS zadnji FROM novice";
		return $this->query($sql);
	}
	
	public function getNoviceStranModerator($stran,$uporabnik){
		$zacetek = $stran * 4 - 4;
        $konec = $stran * 4;
        $sql = "SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum, n.naslovna_slika FROM novice n WHERE n.avtor='".$uporabnik."' ORDER BY n.datum DESC LIMIT ".$zacetek.",".$konec; //select .... limit 40,50
		return $this->query($sql);
	}
	
	public function getNoviceModerator($uporabnik){
		$sql = "SELECT n.id, n.naslov, n.vsebina, n.avtor, n.datum, n.naslovna_slika FROM novice n WHERE n.avtor='".$uporabnik."'";
		return $this->query($sql);
	}
	
	public function getStNovicModerator($uporabnik){
		$sql = "SELECT COUNT(id) AS stevilo FROM novice WHERE avtor='".$uporabnik."'";
        return $this->query($sql);
	}
	
	public function PosodobiNovico($id,$naslov,$vsebina,$avtor)
	{
		$sql = "UPDATE novice SET naslov='".$naslov."', vsebina='".$vsebina."',avtor='".$avtor."' WHERE  id=".$id;
		$this->query($sql);
	}
	
	public function PosodobiNovico1($id,$naslov,$vsebina,$avtor,$naslovna_slika)
	{
		$sql = "UPDATE novice SET naslov='".$naslov."', vsebina='".$vsebina."',avtor='".$avtor."',naslovna_slika='".$naslovna_slika."' WHERE  id=".$id;
		$this->query($sql);
	}
	
	function DobiTip($uporabnisko)
	{
		$sql= "SELECT Tip FROM users WHERE Uporabnisko='".$uporabnisko."'";
		return $this->query($sql);
	}
}