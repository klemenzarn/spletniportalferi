<?php
class Oglas extends AppModel {
	public $useTable = false;
	public $name = "Oglas";

	//vrne novico glede na njen ID
	function getById($id) {
		$sql = "SELECT o.id,o.vsebina, o.naslov, o.datum, o.avtor FROM oglas o WHERE o.id = $id ORDER BY o.datum DESC";
		return $this -> query($sql);
	}

	function getPetOglasov() {
		$sql = "SELECT o.id,o.vsebina, o.naslov, o.datum, o.avtor FROM oglas o ORDER BY o.datum DESC LIMIT 0, 5";
		return $this -> query($sql);
	}

	//funkcija vrne vse datoteke, ki so prilepljene na nek oglas
	function getDatoteke($id) {
		$sql = "SELECT d.ime, d.id FROM oglas o, datotekeoglas d WHERE d.oglas = o.id AND o.id = " . $id;
		return $this -> query($sql);
	}

	public function getStOglas() {
		$sql = "SELECT COUNT(id) AS stevilo FROM oglas";
		return $this -> query($sql);
	}
	
		function getDatoteka($id){
		$sql = "SELECT d.id,d.ime,d.oglas FROM datotekeoglas d WHERE d.id=".$id;
		return $this->query($sql);
	}
	

	
	public function getStOglasUp($uporabnik) {
		$sql = "SELECT COUNT(id) AS stevilo FROM oglas WHERE avtor='$uporabnik'";
		return $this -> query($sql);
	}

	//vrne vse datoteke
	function getVseDatoteke() {
		$sql = "SELECT o.id, d.ime FROM oglas o, datotekeoglas d WHERE d.oglas = o.id";
		return $this -> query($sql);
	}

	//vrne vse novice
	function getAll($stran) {
		$zacetek = $stran * 10 - 10;
		$sql = "SELECT o.id,o.vsebina, o.naslov, o.datum, o.avtor FROM oglas o ORDER BY o.datum DESC LIMIT $zacetek,10";
		return $this -> query($sql);
	}

	//vrne novice, ki ustrezajo iskalnem kriteriju
	public function isci($niz) {
		$niz = "%" . $niz . "%";
		$sql_query = "
			 SELECT o.id, o.naslov, o.datum, o.avtor, o.vsebina
			 FROM oglas o
			 WHERE o.naslov LIKE '" . $niz . "' OR o.vsebina LIKE '" . $niz . "' ORDER BY o.datum DESC";
		$query = $this -> query($sql_query);
		return $query;
	}

	public function zbrisiOglas($id) {

		$sql = "DELETE FROM datotekeoglas WHERE datotekeoglas.oglas=" . $id;
		$sql1 = "DELETE  FROM oglas WHERE id=" . $id;
		$this -> query($sql);
		$this -> query($sql1);
	}

	public function zbrisiDatoteka($id) {
		$sql = "DELETE FROM datotekeoglas WHERE datotekeoglas.id=" . $id;
		$this -> query($sql);
	}

	public function getModerator($uporabnik,$stran) {
		$zacetek = $stran * 10 - 10;
		$konec = $stran * 10;
		$sql = "SELECT o.id,o.vsebina, o.naslov, o.datum, o.avtor FROM oglas o WHERE avtor='" . $uporabnik . "' ORDER BY o.datum DESC LIMIT $zacetek, $konec";
		return $this -> query($sql);
	}
	
	public function getModerator1($uporabnik) {
		$sql = "SELECT o.id,o.vsebina, o.naslov, o.datum, o.avtor FROM oglas o WHERE avtor='" . $uporabnik . "' ORDER BY o.datum DESC";
		return $this -> query($sql);
	}

	public function DodajOglas($naslov, $vsebina, $avtor) {
		$sql = "INSERT INTO oglas (naslov,vsebina,avtor,datum) VALUES ('" . $naslov ."','". $vsebina . "','" . $avtor . "',NOW())";
		$this -> query($sql);
	}

	public function DodajDatoteke($imeDatoteke, $zadnji) {
		$sql = "INSERT INTO datotekeoglas (ime,oglas) VALUES ('" . $imeDatoteke . "','" . $zadnji . "')";
		$this -> query($sql);
	}

	public function DobiZadnji() {
		$sql = "SELECT DISTINCT LAST_INSERT_ID() AS zadnji FROM oglas";
		return $this -> query($sql);
	}

	public function PosodobiOglas($id_oglasa, $naslov, $vsebina, $avtor) {
		$sql = "UPDATE oglas SET naslov='" . $naslov . "', vsebina='" . $vsebina . "',avtor='" . $avtor . "' WHERE  id=" . $id_oglasa;
		$this -> query($sql);
	}

	function DobiTip($uporabnisko) {
		$sql = "SELECT Tip FROM users WHERE Uporabnisko='" . $uporabnisko . "'";
		return $this -> query($sql);
	}

}
