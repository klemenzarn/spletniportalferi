<?php
class Album extends AppModel {
	var $useTable = false;
	var $name = "Album";
	
	function dodajAlbum($naslov, $avtor, $opis, $stevilo_slik){
		$this->query("INSERT INTO album(naslov, avtor, opis, datum, stevilo_slik) VALUES ('$naslov', '$avtor', '$opis', NOW(), '$stevilo_slik')");	
	}
	
	function izbrisiAlbum($idAlbuma, $stSlik){
		if($stSlik > 0){	
			$this->query("DELETE album, slika FROM album, slika WHERE album.id=slika.album_id AND album.id='$idAlbuma'");
		}
		else{
			$this->query("DELETE FROM album WHERE album.id='$idAlbuma'");
		}
	}
	
	function seznamAlbumov(){
		return $this->query("SELECT * FROM album");
	}
	
	function dobiIdAlbuma($naslov){
		return $this->query("SELECT id FROM album WHERE naslov='$naslov'");
	}
	
	function posodobiSteviloSlik($stSlik, $idAlbuma){
		$this->query("UPDATE album SET stevilo_slik='$stSlik' WHERE id='$idAlbuma'");
	}
	
	function steviloSlik($idAlbuma){
		return $this->query("SELECT stevilo_slik FROM album WHERE id='$idAlbuma'");
	}
	
	function naslovAlbuma($idAlbuma){
		return $this->query("SELECT naslov FROM album WHERE id='$idAlbuma'");
	}
	
	function podatkiAlbuma($idAlbuma){
		return $this->query("SELECT * FROM album WHERE id='$idAlbuma'");
	}
	
	function allAlbum(){
		return $this->query("SELECT * FROM album");
	}
	
}
?>