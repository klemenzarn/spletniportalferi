<?php
class Iskanje extends AppModel{
	public $useTable=false;
	
	function st_dokumentov()
	{
		$sql="SELECT COUNT(DISTINCT Dokument) as stevilo FROM tf";
		return $this->query($sql);
	}
	
	function isci($kljuci,$D)
	{
		//Select TF.DokumentID,SUM(TF.TF*log(@D/DF.DF)) as Ocena from TF, DF where TF.lema in (beseda1, beseda2,...) and TF.lema=DF.lema group by TF.DokumentID order by Ocena desc
		$sql="Select DISTINCT (tema),tf.Dokument,SUM(tf.St_pojavitev*log($D/df.DF)) as Ocena from tf, df, prispevki where tf.Dokument=prispevki.id AND tf.Lema in (".$kljuci.") AND tf.Lema=df.Lema group by tf.Dokument order by Ocena desc LIMIT 3";
		return $this->query($sql);
	}
	/*
	function id_tem($dokument_idji)
	{
		$sql = "SELECT DISTINCT (tema) FROM `prispevki` WHERE id IN (".$dokument_idji.") LIMIT 3";
		return $this->query($sql);
	}*/
}
