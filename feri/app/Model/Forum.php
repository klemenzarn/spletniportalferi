<?php
class Forum extends AppModel {
	public $useTable = false;
	public $name = "Forum";

	//funkcija dobi vse za prvo stran foruma (kategorije, število tem po kategorijah in število prispevkov v kategorijah)
	//******* ČE HOČEMO DA TA SQL STAVEK DELUJE PRAVILNO, MORA BITI ZA VSAKO TEMO VSAJ EN PRISPEVEK!
	function getAll() {
		//BACKUP: $sql="SELECT count(t.id) AS stevilo, k.kategorija, k.id FROM teme t, kategorije k WHERE t.kategorija=k.id GROUP BY k.kategorija";
		// $sql="SELECT count(distinct t.id) AS teme, count(distinct p.id) as prispevki1, k.kategorija, k.id FROM teme t, kategorije k, prispevki p WHERE t.kategorija=k.id AND t.kategorija=p.tema GROUP BY k.kategorija";
		$sql = "SELECT count(distinct t.id) as stevilo_tem,count(distinct p.id) as stevilo_prispevkov, k.kategorija,k.daljse FROM teme t, kategorije k,prispevki p WHERE t.kategorija=k.id AND p.tema=t.id GROUP BY k.kategorija ORDER BY k.id";
		return $this -> query($sql);
	}

	function getTeme($ime) {
		//BACKUP  $sql="SELECT t.id, t.naslov, t.uporabnik, k.kategorija FROM teme t, kategorije k WHERE k.kategorija='$ime' AND t.kategorija=k.id";
		$sql = "SELECT count(p.id) as stevilo_prispevkov_v_temi, p.cas, t.id,t.status, t.naslov, t.uporabnik, u.Uporabnisko, k.kategorija FROM teme t, prispevki p, users u, kategorije k WHERE k.kategorija='$ime' AND t.kategorija=k.id AND p.tema=t.id AND u.id=t.uporabnik GROUP BY t.naslov ORDER BY t.cas_prispevka DESC";
		return $this -> query($sql);
	}
	
	function getIDteme($naslov, $kategorije) {
		$sql = "SELECT t.id, k.id FROM teme t, kategorije k WHERE t.naslov='$naslov' and k.kategorija='$kategorije'";
		return $this -> query($sql);
	}

	function getIDteme2($naslov, $kategorije) {
		$sql = "SELECT t.id FROM teme t, kategorije k WHERE t.naslov='$naslov' and k.id=$kategorije";
		return $this -> query($sql);
	}

	function getIDteme1($naslov) {
		$sql = "SELECT t.id FROM teme t WHERE t.url_naslov='$naslov'";
		return $this -> query($sql);
	}
	function getIDteme3($id_prispevka)
	{
		$sql = "SELECT p.tema FROM prispevki p WHERE p.id=$id_prispevka";
		return $this -> query($sql);
	}

	function getVsiIdTeme() {
		$sql = "SELECT t.id FROM teme t";
		return $this -> query($sql);
	}

	function getKategorijaID($kategorija) {
		$sql = "SELECT k.id FROM kategorije k WHERE k.kategorija='$kategorija'";
		return $this -> query($sql);
	}

	function getPrispevki($id_teme) {
		$sql = "SELECT p.id, p.vsebina, p.plus, p.minus, p.uporabnik, p.tema, p.cas, u.Slika, u.ID,u.Uporabnisko FROM prispevki p, users u WHERE p.tema=$id_teme AND u.id=p.uporabnik";
		return $this -> query($sql);
	}
		function getPrispevkiPaging($id_teme,$stran) {
		$zacetek = $stran * 10 - 10;
        $konec = $stran * 10;
		$sql = "SELECT p.id, p.vsebina, p.plus, p.minus, p.uporabnik,t.naslov, p.tema, p.cas,u.Slika, u.ID, u.Uporabnisko FROM prispevki p, teme t, users u WHERE t.id=p.tema AND p.tema=$id_teme AND u.id=p.uporabnik LIMIT ".$zacetek.",".$konec;
		return $this -> query($sql);
	}
	function getTemePaging($ime,$stran) {
		$zacetek = $stran * 10 - 10;
        $konec = $stran * 10;
		$sql = "SELECT count(p.id) as stevilo_prispevkov_v_temi, p.cas, t.id,t.status, t.naslov, t.uporabnik, u.Uporabnisko, k.kategorija FROM teme t, prispevki p, users u, kategorije k WHERE k.kategorija='$ime' AND t.kategorija=k.id AND p.tema=t.id AND u.id=t.uporabnik GROUP BY t.naslov ORDER BY t.cas_prispevka DESC LIMIT ".$zacetek.",".$konec;
		return $this -> query($sql);
	}
	function getSteviloPrispevkov($id_teme)
	{
		$sql="SELECT count(p.id) as stevilo FROM prispevki p, teme t WHERE p.tema=t.id AND t.id=$id_teme";
		return $this->query($sql);
		
	}
	function getPrispevki1($id_teme) {
		$sql = "SELECT p.id, p.vsebina,p.tema FROM prispevki p WHERE p.tema=$id_teme ORDER BY p.id ASC";
		return $this -> query($sql);
	}

	function getVsebinaPrispevka($id_prispevka) {
		$sql = "SELECT p.id, p.vsebina FROM prispevki p WHERE p.id=$id_prispevka";
		return $this -> query($sql);
	}

	function UrediPrispevek($id_prispevka, $vsebina,$id_teme) {
		$sql = "UPDATE prispevki p SET p.vsebina='$vsebina' WHERE p.id=$id_prispevka";
		$sql1="UPDATE teme SET cas_prispevka=NOW() WHERE id=$id_teme";
		$this->query($sql1);
		$this -> query($sql);
	}

	function getNaslovTemeByUrl($url) {
		$sql = "SELECT t.naslov,t.id FROM teme t WHERE t.url_naslov='$url'";
		return $this -> query($sql);
	}

	function narediNaslov($trenutni_naslov, $novi_naslov) {
		$sql = "UPDATE prispevki p SET p.url_naslov='" . $novi_naslov . "' WHERE p.naslov='$trenutni_naslov'";
		$this -> query($sql);
	}

	function ustvariTemo($naslov_teme, $url, $uporabnik, $kategorija) {
		$sql = "INSERT INTO teme (naslov,url_naslov,uporabnik,kategorija) VALUES('$naslov_teme','$url',$uporabnik,$kategorija)";
		$this -> query($sql);
	}

	public function dodajPrispevek($vsebina, $uporabnik, $id_teme) {
		$sql = "INSERT INTO prispevki (vsebina,uporabnik,tema,cas) VALUES('$vsebina',$uporabnik,$id_teme,NOW())";
		$sql1="UPDATE teme SET cas_prispevka=NOW() WHERE id=$id_teme";
		$this->query($sql1);
		$this -> query($sql);
	}

	public function dobiZadnjiPrispevek() {
		$sql = "SELECT MAX(cas) FROM prispevki";
		return $this -> query($sql);
	}

	public function plus($id_prispevka,$avtor) {
		$sql = "UPDATE prispevki p SET p.plus=p.plus+1 WHERE p.id=$id_prispevka";
		$sql1= "INSERT INTO ocenjevanje_prispevkov (user,id_prispevka) VALUES('$avtor',$id_prispevka)";
		$this -> query($sql);
		$this -> query($sql1);
	}

	public function minus($id_prispevka,$avtor) {

		$sql = "UPDATE prispevki p SET p.minus=p.minus+1 WHERE p.id=$id_prispevka";
		$sql1= "INSERT INTO ocenjevanje_prispevkov (user,id_prispevka) VALUES('$avtor',$id_prispevka)";
		$this -> query($sql);
		$this -> query($sql1);
	}

	public function getKategorijaName($id) {
		$sql = "SELECT k.kategorija FROM kategorije k WHERE k.id=$id";
		return $this -> query($sql);
	}

	public function getKategorijaIzPrispevka($id_prispevka) {
		$sql = "SELECT k.kategorija, t.url_naslov FROM kategorije k, prispevki p, teme t WHERE p.id=$id_prispevka AND p.tema=t.id AND t.kategorija=k.id";
		return $this -> query($sql);
	}

	public function getUporabnikIzPrispevka($id_prispevka) {
		$sql = "SELECT u.Uporabnisko FROM users u, prispevki p WHERE p.uporabnik=u.id AND p.id=$id_prispevka";
		return $this -> query($sql);
	}

	public function getUporabnikIzTeme($id_teme) {
		$sql = "SELECT u.Uporabnisko FROM users u, teme t WHERE t.uporabnik=u.id AND t.id=$id_teme";
		return $this -> query($sql);
	}

		function preveriGlas($id_prispevka,$avtor)
	{
	$sql ="SELECT * FROM ocenjevanje_prispevkov WHERE user='$avtor' AND id_prispevka=$id_prispevka";
	return $this -> query($sql);
	}
	
	
	public function posodobi($stanje, $tema) {
		$sql = "UPDATE teme t SET t.status=$stanje WHERE t.id=$tema";
		return $this -> query($sql);
	}

	public function DobiZadnji() {
		$sql = "SELECT DISTINCT LAST_INSERT_ID() AS zadnji FROM prispevki";
		return $this -> query($sql);
	}
		function nasloviskanje($id_teme) {
		$sql = "SELECT k.kategorija, t.url_naslov FROM kategorije k, teme t WHERE t.id=$id_teme AND t.kategorija=k.id";
		return $this -> query($sql);
	}
	function izbrisiLeme($id_prispevka){
		$sql = "DELETE FROM tf WHERE Dokument=$id_prispevka";
		$this -> query($sql);
	}
	
	function getSteviloTem($ime_kategorije)
	{
		$sql="SELECT count(t.id) as stevilo, t.naslov FROM teme t WHERE t.kategorija=$ime_kategorije";
		return $this->query($sql);
	}

}
