<?php
class AdministracijaController extends AppController {
	public $name = 'Administracija';
	public $uses = array('User','Novice');
	
	public function index($st = NULL){
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);

			if ($tip[0]['users']['Tip'] == 3) {
				if (empty($this -> data)) {
					if ($st == NULL) {
						$st = 1;
					}
					$this -> set('user', $this -> User -> vsiUseri($st));
					$this -> set('stevila', $this -> User -> steviloUserov());
					$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
				} else if (!empty($this -> data)) {
					if ($this -> request -> is('POST')) {
						$vrednost = $this -> request -> data('isci');
						$niz = $this -> request -> data('search');
						if ($niz == "" && $vrednost == null || $niz != "" && $vrednost == null || $niz == "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							$this -> set('user', $this -> User -> vsiUseri($st));
							$this -> set('stevila', $this -> User -> steviloUserov());
							$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
						} else if ($niz != "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							$this -> set('user', $this -> User -> iskanjeUporabnikov($vrednost, $niz, $st));
							$this->set('stevila',$this->User->steviloIskanihUserov($vrednost,$niz,$st));
							$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
						}
					}
				}
			} 
			else if($tip[0]['users']['Tip'] == 2){
				$uporabnik=$this->Session->read("uporabnik");
				$id = 1;
				$novice = $this->Novice->getNoviceStranModerator($id,$uporabnik);
				$this->set("novice",$novice);
		        $velikost_novic = $this->Novice->getStNovicModerator($uporabnik);
		        $this->set("stnovic",$velikost_novic);
				$this->render('moder');
			}
			else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}
	
}