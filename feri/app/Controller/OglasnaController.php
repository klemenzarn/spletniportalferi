<?php
App::uses('Sanitize', 'Utility');
class OglasnaController extends AppController {
	var $uses = array('Oglas', 'User');

	//Index
	function index($stran = 1) {
		//za vsako oglasno desko svojo mapo
		$query = $this -> Oglas -> getAll($stran);
		$this -> set("deska", $query);
		$datoteke = $this -> Oglas -> getVseDatoteke();
		$this -> set("datoteke", $datoteke);
		$stevilo = $this -> Oglas -> getStOglas();
		$this -> set("stevilo_oglasov", $stevilo);
	}

	//funkcija, ki išče oglase glede na iskalni niz.
	public function isci() {
		//$niz = $this->request->data('niz');  //post
		$niz = $this -> request -> query['niz'];
		// get
		$query = $this -> Oglas -> isci($niz);
		if ($query != null) {
			$this -> set("deska", $query);
			$datoteke = $this -> Oglas -> getVseDatoteke();
			$this -> set("datoteke", $datoteke);
			$this -> render("index");
		} else {
			$this -> set("deska", $query);
			$this->Session->setFlash("Ni zadetkov");
			$datoteke = $this -> Oglas -> getVseDatoteke();
			$this -> set("datoteke", $datoteke);
			$this -> render("index");
			
		}
	}

	//pogledamo vsako novico posebaj, ko kliknemo na link
	function poglej() {
		$id = $this -> request -> query['id'];
		//dobimo iz ?id=4 kter id novice želimo
		if ($id != null) {
			$oglasi = $this -> Oglas -> getById($id);
			if ($oglasi != null) {
				$this -> set("deska", $oglasi);
				$datoteke = $this -> Oglas -> getDatoteke($id);
				$this -> set("datoteke", $datoteke);
			} else {
				$this -> set("error", "Ta stran ne obstaja!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Ta stran ne obstaja!");
			$this -> render("/Errors/vpis");
		}
	}

	/*public function dodaj() {
	 if($this->Session->check("uporabnik")){
	 $tip=$this->User->DobiTip($this->Session->read("uporabnik"));
	 if($tip[0]['users']['Tip']==3 || $tip[0]['users']['Tip']==2)
	 {
	 $naslov=$this->request->data('naslov_oglasa');
	 $vsebina=$this->request->data('tiny');
	 $slika=$this->request->data('datoteke');

	 if( empty($naslov)&& empty($vsebina)&& empty($slika))
	 {
	 $this->Session->setFlash('Niste vnesli vseh podatkov');
	 }
	 } else {
	 $this->set("error","Nimate pravic za dostop do te strani.");
	 $this->render("/Errors/vpis");
	 }
	 } else {
	 $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
	 $this->render("/Errors/vpis");
	 }
	 }*/

	public function dodajoglasno() {
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		if ($this -> Session -> check("uporabnik") == FALSE) {
			$this -> set("error", "Trenutno niste prijavljeni!");
			$this -> render("/Errors/vpis");
		} else {
			$tip = $this -> requestAction('/Index/DobiTipUporabnika');
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				if ($this -> request -> is('post')) {
					$datoteke = $this -> request -> params['form'];
					$naslov = $this -> request -> data('naslov_oglasa');
					$vsebina = $this -> request -> data('tiny');
					$stDatotek = sizeof($datoteke['datoteke']['name']);

					$avtor = $this -> Session -> read("uporabnik");
					if (empty($naslov) || empty($vsebina)) {
						$this -> Session -> setFlash('Niste vnesli vseh zahtevanih podatkov!');
					} else {
						if (empty($datoteke)) {
							$this -> Oglas -> DodajOglas($naslov, Sanitize::escape($vsebina), $avtor);
						} else {

							$skupna_velikost = 0;

							for ($i = 0; $i < $stDatotek; $i++) {
								$skupna_velikost = $skupna_velikost + $datoteke['datoteke']['size'][$i];
							}

							if ($skupna_velikost < 8388608) {
								$this -> Oglas -> DodajOglas($naslov, Sanitize::escape($vsebina), $avtor);
								$zadnji_id = $this -> Oglas -> DobiZadnji();
								$dir = new Folder(WWW_ROOT . '/files/Oglasna/' . $zadnji_id[0][0]['zadnji'], true, 0755);
								for ($i = 0; $i < $stDatotek; $i++) {
									$imeDatoteke = $datoteke['datoteke']['name'][$i];
									$tmpDatoteke = $datoteke['datoteke']['tmp_name'][$i];
									$this -> Oglas -> DodajDatoteke($imeDatoteke, $zadnji_id[0][0]['zadnji']);
									move_uploaded_file($tmpDatoteke, WWW_ROOT . '/files/Oglasna/' . $zadnji_id[0][0]['zadnji'] . '/' . $imeDatoteke);
								}
								$this -> redirect('/oglasna/uredi');
							} else {
								$this -> Session -> setFlash('Prekoračili ste dovoljeno velikost datotek!');
							}
						}
					}
				}
			} else {
				$this -> set("error", "Za to opravilo niste avtorizirani");
				$this -> render("/Errors/vpis");
			}
		}
	}

	public function brisi($id) {
		$uporabnik = $this -> Session -> read("uporabnik");
		$tip = $this -> Oglas -> DobiTip($uporabnik);
		if (empty($tip)) {
			$this -> set("error", "Trenutno niste prijavljeni!");
			$this -> render("/Errors/vpis");
		} else {
			if ($tip[0]['users']['Tip'] == 1) {
				$this -> set("error", "Za to opravilo niste avtorizirani");
				$this -> render("/Errors/vpis");
			} else if ($tip[0]['users']['Tip'] == 3) {
				$datoteke = $this -> Oglas -> getDatoteke($id);
				foreach ($datoteke as $datoteka) :
					$file = new File(WWW_ROOT . "/files/Oglasna/" . $id . "/" . $datoteka['d']['ime']);
					$file -> delete();
				endforeach;
				$this -> Oglas -> zbrisiOglas($id);
				$this -> redirect('uredi');
			} else if ($tip[0]['users']['Tip'] == 2) {
				$je = 0;
				//bool če je 1 potem je njegova novica, lahko zbrise
				$oglasi = $this -> Oglas -> getModerator1($uporabnik);
				for ($i = 0; $i < count($oglasi); $i++) {
					if ($oglasi[$i]['o']['id'] == $id)
						$je = 1;
				}
				if ($je == 1) {
					$datoteke = $this -> Oglas -> getDatoteke($id);
					foreach ($datoteke as $datoteka) :
						$file = new File(WWW_ROOT . "/files/Oglasna/" . $id . "/" . $datoteka['d']['ime']);
						$file -> delete();
					endforeach;
					$this -> Oglas -> zbrisiOglas($id);
					$this -> redirect('uredi');
				} else {
					$this -> set("error", "Za to opravbgdfilo niste avtorizirani");
					$this -> render("/Errors/vpis");
				}
			}
		}
	}

	public function uredi($stran = 1) {
		$uporabnik = $this -> Session -> read("uporabnik");
		$tip = $this -> Oglas -> DobiTip($uporabnik);
		if (empty($tip)) {
			$this -> set("error", "Trenutno niste prijavljeni!");
			$this -> render("/Errors/vpis");
		} else {
			if ($tip[0]['users']['Tip'] == 3)//admin
			{
				$oglasi = $this -> Oglas -> getAll($stran);
				$this -> set("oglasi", $oglasi);
				$datoteke = $this -> Oglas -> getVseDatoteke();
				$this -> set("datoteke", $datoteke);
				$stevilo = $this -> Oglas -> getStOglas();
				$this -> set("stnovic", $stevilo);
			} else if ($tip[0]['users']['Tip'] == 2)//moderator
			{
				$uporabnik = $this -> Session -> read("uporabnik");
				$oglasi = $this -> Oglas -> getModerator($uporabnik, $stran);
				$this -> set("oglasi", $oglasi);
				$stevilo = $this -> Oglas -> getStOglasUp($uporabnik);
				$this -> set("stnovic", $stevilo);
				$datoteke = $this -> Oglas -> getVseDatoteke();
				$this -> set("datoteke", $datoteke);
			} else {
				$this -> set("error", "Za to opravilo niste avtorizirani");
				$this -> render("/Errors/vpis");
			}
		}
	}

	public function uredinovico($id_oglasa) {
		$uporabnik = $this -> Session -> read("uporabnik");
		$tip = $this -> Oglas -> DobiTip($uporabnik);
		if (empty($this -> data)) {
			if (empty($tip)) {
				$this -> set("error", "Trenutno niste prijavljeni!");
				$this -> render("/Errors/vpis");
			} else {
				$je = 0;
				//bool če je 1 potem je njegova novica, lahko zbrise
				if ($tip[0]['users']['Tip'] == 1) {
					$this -> set("error", "Za to opravilo niste avtorizirani");
					$this -> render("/Errors/vpis");
				}
				if ($tip[0]['users']['Tip'] == 2) {
					$oglasi = $this -> Oglas -> getModerator1($uporabnik);
					for ($i = 0; $i < count($oglasi); $i++) {
						if ($oglasi[$i]['o']['id'] == $id_oglasa)
							$je = 1;
					}
				}
				if ($je == 1 || $tip[0]['users']['Tip'] == 3) {
					$oglasi = $this -> Oglas -> getById($id_oglasa);
					$this -> set("oglasi", $oglasi);
					$datoteke = $this -> Oglas -> getDatoteke($id_oglasa);
					$this -> set("datoteke", $datoteke);
				} else {
					$this -> set("error", "Za to opravilo niste avtorizirani");
					$this -> render("/Errors/vpis");
				}
			}

		} else {
			$id = $this -> request -> data('id_oglasa');
			$naslov = $this -> request -> data('naslov_oglasa');
			$vsebina = $this -> request -> data('tiny');
			$datoteke = $this -> request -> params['form'];
			$stDatotek = count($datoteke['datoteke']['name']);
			$avtor = $this -> Session -> read("uporabnik");
			if (empty($naslov) || empty($vsebina)) {
				$this -> Session -> setFlash('Niste vnesli vseh zahtevanih podatkov!');
				$this -> redirect('/oglasna/uredinovico/' . $id_oglasa);
			} else {
				if ($datoteke['datoteke']['size'][0] == 0) {
					$this -> Oglas -> PosodobiOglas($id_oglasa, $naslov, Sanitize::escape($vsebina), $avtor);
				} else {
					$skupna_velikost = 0;
					for ($i = 0; $i < $stDatotek; $i++) {
						$skupna_velikost = $skupna_velikost + $datoteke['datoteke']['size'][$i];
					}

					if ($skupna_velikost < 8388608) {
						$this -> Oglas -> PosodobiOglas($id_oglasa, $naslov, Sanitize::escape($vsebina), $avtor);
						for ($i = 0; $i < $stDatotek; $i++) {
							$imeDatoteke = $datoteke['datoteke']['name'][$i];
							$tmpDatoteke = $datoteke['datoteke']['tmp_name'][$i];
							$this -> Oglas -> DodajDatoteke($imeDatoteke, $id_oglasa);
							move_uploaded_file($tmpDatoteke, WWW_ROOT . '/files/Oglasna/' . $id_oglasa . '/' . $imeDatoteke);
						}
					} else {
						$this -> Session -> setFlash('Prekoračili ste dovoljeno velikost datotek!');
					}
				}
				$this -> redirect('uredi');
			}
		}
	}

	public function brisi_datoteko($id) {
		$uporabnik = $this -> Session -> read("uporabnik");
		$tip = $this -> Oglas -> DobiTip($uporabnik);
		if (empty($tip)) {
			$this -> set("error", "Trenutno niste prijavljeni!");
			$this -> render("/Errors/vpis");
		} else {
			if ($tip[0]['users']['Tip'] == 1) {
				$this -> set("error", "Za to opravilo niste avtorizirani");
				$this -> render("/Errors/vpis");
			} else if ($tip[0]['users']['Tip'] == 3) {
				$datoteka = $this -> Oglas -> getDatoteka($id);
				$id_oglasa = $datoteka[0]['d']['oglas'];
				$file = new File(WWW_ROOT . "/files/Oglasna/" . $id_oglasa . "/" . $datoteka[0]['d']['ime']);
				$file -> delete();
				$this -> Oglas -> zbrisiDatoteka($id);
				$this -> redirect('/oglasna/uredinovico/' . $id_oglasa);
			} else if ($tip[0]['users']['Tip'] == 2) {
				$je = 0;
				//bool če je 1 potem je njegova novica, lahko zbrise
				$datoteka = $this -> Oglas -> getDatoteka($id);
				$id_oglasa = $datoteka[0]['d']['oglas'];
				$oglasi = $this -> Oglas -> getModerator($uporabnik);
				for ($i = 0; $i < count($oglasi); $i++) {
					if ($oglasi[$i]['o']['id'] == $id_oglasa)
						$je = 1;
				}
				if ($je == 1) {
					$datoteka = $this -> Oglas -> getDatoteka($id);
					$id_oglasa = $datoteka[0]['d']['oglas'];
					$file = new File(WWW_ROOT . "/files/Oglasna/" . $id_oglasa . "/" . $datoteka[0]['d']['ime']);
					$file -> delete();
					$this -> Oglas -> zbrisiDatoteka($id);
					$this -> redirect('/oglasna/uredinovico/' . $id_oglasa);
				} else {
					$this -> set("error", "Za to opravilo niste avtorizirani");
					$this -> render("/Errors/vpis");
				}
			}
		}
	}

}
