<?php
    class GalerijaController extends AppController{
    	var $uses =  array('Galerija', 'Album');
		
		function pretvoriNaslov($naslov){
			$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
			$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
			$naslov = str_replace($sumnik, $sicnik, $naslov);
			
			return $naslov;
		}
		
    	function index($id = 1){
    		$albumi = $this-> Galerija -> getStranGalerije($id);
    		$stAlbumov = sizeof($albumi); //Stevilo albumov na strani
    		
    		$stVsehAlbumov = $this -> Galerija -> getSteviloAlbumov(); // Število vseh albumov
			$stVsehAlbumov = $stVsehAlbumov[0][0]['COUNT(*)'];
			
			$album = array();
			$slike = array();

			for($i = 0; $i < $stAlbumov; $i++){
				$album[$i] = $albumi[$i]['album']['id'];
				$slike[$i] = $this->Galerija->prikaziSlikePoAlbumu($album[$i]);
				$this->set('slike', $slike);	
			}
			
			//$album = $this->Album->allAlbum();
			$this->set('albumi', $albumi);
			$this->set('stAlbumov', $stAlbumov);
			$this->set('stVsehAlbumov', $stVsehAlbumov);

    	}

    	function indexwebgl($id = 1){
    		$this->layout = false;
    		$albumi = $this-> Galerija -> getStranGalerije($id);
    		$stAlbumov = sizeof($albumi); //Stevilo albumov na strani
    		
    		$stVsehAlbumov = $this -> Galerija -> getSteviloAlbumov(); // Število vseh albumov
			$stVsehAlbumov = $stVsehAlbumov[0][0]['COUNT(*)'];
			
			$album = array();
			$slike = array();

			for($i = 0; $i < $stAlbumov; $i++){
				$album[$i] = $albumi[$i]['album']['id'];
				$slike[$i] = $this->Galerija->prikaziSlikePoAlbumu($album[$i]);
				$this->set('slike', $slike);	
			}
			
			//$album = $this->Album->allAlbum();
			$this->set('albumi', $albumi);
			$this->set('stAlbumov', $stAlbumov);
			$this->set('stVsehAlbumov', $stVsehAlbumov);
    	}
		
		function album($id = NULL){
			$id = $this->request->query['id'];
			$this->set('slike', $this->Galerija->PrikaziSlikePoAlbumu($id));
			$this->set('album', $this->Album->podatkiAlbuma($id));
		}

		function albumwebgl($id = NULL){
			$this->layout = false;
			$id = $this->request->query['id'];
			$this->set('slike', $this->Galerija->PrikaziSlikePoAlbumu($id));
			$this->set('album', $this->Album->podatkiAlbuma($id));
		}
		
		function zadnji_trije(){
			$albumi = $this -> Galerija -> getZadnjeTri();
			$stAlbumov = sizeof($albumi);
			return $albumi;
		}

    }
?>