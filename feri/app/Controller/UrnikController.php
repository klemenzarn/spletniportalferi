<?php
class UrnikController extends AppController{
	var $uses = array('Urniki','Mojurnik','User');
    public $components = array('RequestHandler');
    
    //funkcija dobi zacetek predmeta (13:00) in trajanje (2) in vrne konec -> 14:00
    private function getKonec($zacetek, $trajanje){
        //kompleksen algoritem ki spremeni trajanje v ure, prišteje to k začetku in pol še ugotovi ali je pouna ura
        list($ura, $minuta) = explode(":", $zacetek);
        $skupaj = $ura.$minuta; // dobim iz 13:00 -> 1300
        $razlika = $trajanje / 2;
        if(is_float($razlika)){
           $razlika = $razlika * 100 - 20;
        } else {
            $razlika = $razlika * 100;   
        }
        $vsota = $skupaj + $razlika;
        $dolzina = strlen($vsota);
        $konec = "";
        $prvi = "";
        $drugi = "";
        if($dolzina == 4){
            $prvi = substr($vsota,0,2);
            $drugi = substr($vsota,2,2);
            $konec = $prvi.":".$drugi;
        } else {
            $prvi = substr($vsota,0,1);
            $drugi = substr($vsota,1,2);
        }
        if($drugi == "60"){
            $konec = ($prvi+1).":00";
        }else{
            $konec = $prvi.":".$drugi;
        }
        return $konec;
    }
    
    public function izvoziUrnikVPDF(){
        if($this->request->is('post')){
            $datum = $this->request->data('datum');
            $branch_id = $this->request->data('branch_id');
            $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
             foreach($privzeto as $privzet_urnik){
                    $zacetek = $privzet_urnik[0]['zacetek'];
                    $trajanje = $privzet_urnik[0]['trajanje'];
                    $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                    $temp = array(
                        "predmet" => $privzet_urnik[0]['predmet'],
                        "dan" => $privzet_urnik[0]['dan'],
                        "ucilnica" => $privzet_urnik[0]['ucilnica'],
                        "zacetek" => $zacetek,
                        "konec" => $konec,
                        "vrsta" => $privzet_urnik[0]['vrsta'],
                        "ime" => $privzet_urnik[0]['ime'],
                        "priimek" => $privzet_urnik[0]['priimek'],
                    );
                    $urniki[] = $temp;
                }
            $appending = "";
                 $max = 0;
                        $velikost = array(0,0,0,0,0);
                        for($i = 0; $i < count($urniki); $i++){
                               $velikost[$urniki[$i]['dan'] - 1]++;
                        }
                        for($i = 0; $i < 5; $i++){
                            if($velikost[$i] > $max){
                                $max =  $velikost[$i]; 
                            }
                        }
                        for($k = 0; $k < $max; $k++){
                            $appending .= "<tr style=\"background-color: #E0D9EF;\">";
                            
                            for($l = 1; $l <= 5; $l++){
                                 $appending .= "<td style=\"border: 1px solid black;\">";
                                $izbris = 0;
                                foreach($urniki as $key => $urnik){
                                    if($urnik['dan'] == $l){
                                         $appending .= "".$urnik['zacetek']." - ". $urnik['konec']."<br />";
                                         $appending .= " ".$urnik['predmet']."<br />";
                                         $appending .= " ".$urnik['ucilnica'].", ".$urnik['vrsta']."<br />";
                                         //$appending .= " ".$urnik['ime']." ".$urnik['priimek']."<br />";
                                        unset($urniki[$key]);
                                        break;
                                    }
                                }
                                 $appending .= "</td>";
                            }
                            
                             $appending .= "</tr>";
                        }
                $this->set("test",$appending);
                //echo $appending;
                $this->layout = 'pdf'; //this will use the pdf.ctp layout 
                $this->render("output"); 
        }
    }
    
    public function izvoziMojUrnikVPDF(){
         if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            $branch_id = $this->User->getBranchId($user_id);  //dobimo od uporabnika branch id
            if(!empty($branch_id)){
                $branch_id = $branch_id[0]['b']['branch_id'];
                $today = getdate();
                $datum = $today['mday'].".".$today['mon'].".".$today['year'];
                $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
                $moji_urniki = $this->Mojurnik->getMyUrnik($user_id);
                //dva urnika združimo v celoto in posredujemo viewu...
                foreach($privzeto as $privzet_urnik){
                    $zacetek = $privzet_urnik[0]['zacetek'];
                    $trajanje = $privzet_urnik[0]['trajanje'];
                    $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                    $temp = array(
                        "predmet" => $privzet_urnik[0]['predmet'],
                        "dan" => $privzet_urnik[0]['dan'],
                        "ucilnica" => $privzet_urnik[0]['ucilnica'],
                        "zacetek" => $zacetek,
                        "konec" => $konec,
                        "vrsta" => $privzet_urnik[0]['vrsta'],
                        "ime" => $privzet_urnik[0]['ime'],
                        "priimek" => $privzet_urnik[0]['priimek'],
                    );
                    $urniki[] = $temp;
                }
                foreach($moji_urniki as $moj_urnik){
                    $temp = array(
                        "predmet" => $moj_urnik['u']['predmet'],
                        "dan" => $moj_urnik['u']['dan'],
                        "ucilnica" => $moj_urnik['u']['ucilnica'],
                        "zacetek" => $moj_urnik['u']['zacetek'],
                        "konec" => $moj_urnik['u']['konec'],
                        "vrsta" => $moj_urnik['u']['vrsta'],
                         "ime" => $moj_urnik['u']['ime'],
                        "priimek" => $moj_urnik['u']['priimek'],
                    );
                    $urniki[] = $temp;
                }
                //$this->set("urniki",$urniki);
                $appending = "";
                 $max = 0;
                        $velikost = array(0,0,0,0,0);
                        for($i = 0; $i < count($urniki); $i++){
                               $velikost[$urniki[$i]['dan'] - 1]++;
                        }
                        for($i = 0; $i < 5; $i++){
                            if($velikost[$i] > $max){
                                $max =  $velikost[$i]; 
                            }
                        }
                        for($k = 0; $k < $max; $k++){
                            $appending .= "<tr style=\"background-color: #E0D9EF;\">";
                            
                            for($l = 1; $l <= 5; $l++){
                                 $appending .= "<td style=\"border: 1px solid black;\">";
                                $izbris = 0;
                                foreach($urniki as $key => $urnik){
                                    if($urnik['dan'] == $l){
                                         $appending .= "".$urnik['zacetek']." - ". $urnik['konec']."<br />";
                                         $appending .= " ".$urnik['predmet']."<br />";
                                         $appending .= " ".$urnik['ucilnica'].", ".$urnik['vrsta']."<br />";
                                         $appending .= " ".$urnik['ime']." ".$urnik['priimek']."<br />";
                                        unset($urniki[$key]);
                                        break;
                                    }
                                }
                                 $appending .= "</td>";
                            }
                            
                             $appending .= "</tr>";
                        }
                $this->set("test",$appending);
                //echo $appending;
                $this->layout = 'pdf'; //this will use the pdf.ctp layout 
                $this->render("output"); 
            }else{
                $this->set("error","Do te strani nemorete dostopati, če niste študent ali pa si še niste izbrali svojo smer. To storite v meniju Moj profil. ");
                $this->render("/Errors/vpis");  
            }
        } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
    }
    
    
    public function mojurnik(){
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            $branch_id = $this->User->getBranchId($user_id);  //dobimo od uporabnika branch id
            if(!empty($branch_id)){
                $branch_id = $branch_id[0]['b']['branch_id'];
                $today = getdate();
                $datum = $today['mday'].".".$today['mon'].".".$today['year'];
                $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
                $moji_urniki = $this->Mojurnik->getMyUrnik($user_id);
                //dva urnika združimo v celoto in posredujemo viewu...
                foreach($privzeto as $privzet_urnik){
                    $zacetek = $privzet_urnik[0]['zacetek'];
                    $trajanje = $privzet_urnik[0]['trajanje'];
                    $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                    $temp = array(
                        "predmet" => $privzet_urnik[0]['predmet'],
                        "dan" => $privzet_urnik[0]['dan'],
                        "ucilnica" => $privzet_urnik[0]['ucilnica'],
                        "zacetek" => $zacetek,
                        "konec" => $konec,
                        "vrsta" => $privzet_urnik[0]['vrsta'],
                        "ime" => $privzet_urnik[0]['ime'],
                        "priimek" => $privzet_urnik[0]['priimek'],
                    );
                    $urniki[] = $temp;
                }
                foreach($moji_urniki as $moj_urnik){
                    $temp = array(
                        "predmet" => $moj_urnik['u']['predmet'],
                        "dan" => $moj_urnik['u']['dan'],
                        "ucilnica" => $moj_urnik['u']['ucilnica'],
                        "zacetek" => $moj_urnik['u']['zacetek'],
                        "konec" => $moj_urnik['u']['konec'],
                        "vrsta" => $moj_urnik['u']['vrsta'],
                         "ime" => $moj_urnik['u']['ime'],
                        "priimek" => $moj_urnik['u']['priimek'],
                    );
                    $urniki[] = $temp;
                }
                $this->set("urniki",$urniki);
            }else{
                $this->set("error","Do te strani nemorete dostopati, če niste študent ali pa si še niste izbrali svojo smer. To storite v meniju Moj profil. ");
                $this->render("/Errors/vpis");  
            }
        } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
    }
    
    public function obveznosti(){
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            $moji_urniki = $this->Mojurnik->getMyUrnik($user_id);
            $urniki = array();
            foreach($moji_urniki as $moj_urnik){
                $dan = $moj_urnik['u']['dan'];
                switch($dan){
                 case 1:{
                     $dan = "Pon";
                     break;
                 }
                case 2:{
                     $dan = "Tor";
                     break;
                 }
                case 3:{
                     $dan = "Sre";
                     break;
                 }
                case 4:{
                     $dan = "Čet";
                     break;
                 }
                case 5:{
                     $dan = "Pet";
                     break;
                 }
                    
                }
                $temp = array(
                    "id"=>$moj_urnik['u']['id'],
                    "predmet" => $moj_urnik['u']['predmet'],
                    "dan" => $dan,
                    "ucilnica" => $moj_urnik['u']['ucilnica'],
                    "zacetek" => $moj_urnik['u']['zacetek'],
                    "konec" => $moj_urnik['u']['konec'],
                    "vrsta" => $moj_urnik['u']['vrsta'],
                    "ime" => $moj_urnik['u']['ime'],
                    "priimek" => $moj_urnik['u']['priimek'],
                );
                $urniki[] = $temp;
            }
            $this->set("moje_obveznosti",$urniki);
        } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis"); 
        }
    }
    
    //dodajObveznost($predmet, $dan, $ucilnica, $zacetek, $konec, $vrsta, $ime, $priimek, $user_id)
    public function dodaj(){
        if($this->request->is('post')){
           if($this->Session->check('uporabnik')){
               $uporabnik = $this->Session->read('uporabnik');
               $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
               $user_id = $user_id[0]['u']['id'];
               $predmet = $this->request->data('predmet');
               $ucilnica = $this->request->data('ucilnica');
               $tutor = $this->request->data('tutor');
               $dan = $this->request->data('dan');
               $vrsta = $this->request->data('vrsta');
               $ura_zac = $this->request->data('ura_zac');
               $min_zac = $this->request->data('min_zac');
               $ura_kon = $this->request->data('ura_kon');
               $min_kon = $this->request->data('min_kon');
               list($ime, $priimek) = explode(" ", $tutor);
               $zacetek = $ura_zac.":".$min_zac;
               $konec = $ura_kon.":".$min_kon;
               $insert = $this->Mojurnik->dodajObveznost($predmet, $dan, $ucilnica, $zacetek, $konec, $vrsta, $ime, $priimek, $user_id);
               $this->redirect(array("controller"=>"urnik","action"=>"obveznosti"));
           }else {
               $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
                $this->render("/Errors/vpis");
           }
        } else {
            $this->set("error","Nimate pravic za to opravilo!");
            $this->render("/Errors/vpis");  
        }
    }
    public function urediobv(){
        if($this->request->is('post')){
            if($this->Session->check('uporabnik')){
                $uporabnik = $this->Session->read('uporabnik');
                $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
                $user_id = $user_id[0]['u']['id'];
                $id = $this->request->data('id');
                $predmet = $this->request->data('predmet');
                $ucilnica = $this->request->data('ucilnica');
                $tutor = $this->request->data('tutor');
                $dan = $this->request->data('dan');
                $vrsta = $this->request->data('vrsta');
                $ura_zac = $this->request->data('ura_zac');
                $min_zac = $this->request->data('min_zac');
                $ura_kon = $this->request->data('ura_kon');
                $min_kon = $this->request->data('min_kon');
                list($ime, $priimek) = explode(" ", $tutor);
                $zacetek = $ura_zac.":".$min_zac;
                $konec = $ura_kon.":".$min_kon;
                $insert = $this->Mojurnik->spremeniObveznost($id,$predmet, $dan, $ucilnica, $zacetek, $konec, $vrsta, $ime, $priimek, $user_id);
                $this->redirect(array("controller"=>"urnik","action"=>"obveznosti"));
            }else {
               $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
                $this->render("/Errors/vpis");
           }
        } else {
            $this->set("error","Nimate pravic za to opravilo!");
            $this->render("/Errors/vpis");  
        }
    }
    
    
    //funkcija 
    public function uredi() {
        if($this->Session->check('uporabnik')){
            if($this->request->is('get')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            $obveznost_id = $this->request->query['id'];
            $obv = $this->Mojurnik->getMyUrnikById($user_id, $obveznost_id);
            $moji_urniki = $this->Mojurnik->getMyUrnik($user_id);
            $urniki = array();
            foreach($moji_urniki as $moj_urnik){
                $dan = $moj_urnik['u']['dan'];
                switch($dan){
                 case 1:{
                     $dan = "Pon";
                     break;
                 }
                case 2:{
                     $dan = "Tor";
                     break;
                 }
                case 3:{
                     $dan = "Sre";
                     break;
                 }
                case 4:{
                     $dan = "Čet";
                     break;
                 }
                case 5:{
                     $dan = "Pet";
                     break;
                 }
                    
                }
                $temp = array(
                    "id"=>$moj_urnik['u']['id'],
                    "predmet" => $moj_urnik['u']['predmet'],
                    "dan" => $dan,
                    "ucilnica" => $moj_urnik['u']['ucilnica'],
                    "zacetek" => $moj_urnik['u']['zacetek'],
                    "konec" => $moj_urnik['u']['konec'],
                    "vrsta" => $moj_urnik['u']['vrsta'],
                    "ime" => $moj_urnik['u']['ime'],
                    "priimek" => $moj_urnik['u']['priimek'],
                );
                $urniki[] = $temp;
            }
           
            if(empty($obv)){
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis");   
            } else {
                $this->set("urnik",$obv);   
                $this->set("moje_obveznosti",$urniki);
            }
            } else{
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis");  
            }
        } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");
        }
    }
    
    public function izbrisi(){
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
            $obveznost_id = $this->request->query['id'];
            $rez = $this->Mojurnik->izbrisi($obveznost_id, $user_id);
            if(empty($rez)){
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis");   
            } else {
                $this->set('msg', 'uspešno');
                $this->render('izbrisi', 'ajax'); 
            }
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis");   
            }
        } else {
           $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");
        }
    }
    
    public function index() {
        $all_branch = $this->Urniki->getAllBranch();
        $this->set("vsi",$all_branch);
        if($this->request->is('post')){
            $datum = $this->request->data('datum');
            $branch_id = $this->request->data('branch_id');
            $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
            $urniki = null;
            foreach($privzeto as $privzet_urnik){
                $zacetek = $privzet_urnik[0]['zacetek'];
                $trajanje = $privzet_urnik[0]['trajanje'];
                $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                $temp = array(
                    "predmet" => $privzet_urnik[0]['predmet'],
                    "dan" => $privzet_urnik[0]['dan'],
                    "ucilnica" => $privzet_urnik[0]['ucilnica'],
                    "zacetek" => $zacetek,
                    "konec" => $konec,
                    "vrsta" => $privzet_urnik[0]['vrsta'],
                    "ime" => $privzet_urnik[0]['ime'],
                    "priimek" => $privzet_urnik[0]['priimek'],
                );
                $urniki[] = $temp;
            }
            $this->set("bran_id",$branch_id);
            $this->set("dat",$datum);
            $this->set("urniki",$urniki);
        } 
	}

    public function webgl() {
        $this->layout = false;
        $all_branch = $this->Urniki->getAllBranch();
        $this->set("vsi",$all_branch);
        if($this->request->is('post')){
            $datum = $this->request->data('datum');
            $branch_id = $this->request->data('branch_id');
            $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
            $urniki = null;
            foreach($privzeto as $privzet_urnik){
                $zacetek = $privzet_urnik[0]['zacetek'];
                $trajanje = $privzet_urnik[0]['trajanje'];
                $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                $temp = array(
                    "predmet" => $privzet_urnik[0]['predmet'],
                    "dan" => $privzet_urnik[0]['dan'],
                    "ucilnica" => $privzet_urnik[0]['ucilnica'],
                    "zacetek" => $zacetek,
                    "konec" => $konec,
                    "vrsta" => $privzet_urnik[0]['vrsta'],
                    "ime" => $privzet_urnik[0]['ime'],
                    "priimek" => $privzet_urnik[0]['priimek'],
                );
                $urniki[] = $temp;
            }
            $this->set("bran_id",$branch_id);
            $this->set("dat",$datum);
            $this->set("urniki",$urniki);
        } 
    }

    
}