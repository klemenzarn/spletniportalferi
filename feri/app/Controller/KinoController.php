<?php

class KinoController extends AppController{
	var $name = 'Kino';

	private function iskanjePoNaslovu($niz)
	{		
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz;
		$result = $client->iskanjePoNaslovu($params);
		$this->set('rezultat',$result);
	}
	
	private function iskanjePoLetu($niz)
	{
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz; 
		$result = $client->iskanjePoLetu($params);
		$this->set('rezultat',$result);
	}
		
	private function iskanjePoJeziku($niz)
	{
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz; 
		$result = $client->iskanjePoJeziku($params);

		$this->set('rezultat',$result);
	}
		
	private function iskanjePoDrzavi($niz)
	{
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz; 
		$result = $client->iskanjePoDrzavi($params);

		$this->set('rezultat',$result); 
	}

	private function iskanjePoZvrsti($niz)
	{	
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz; 
		$result = $client->iskanjePoZvrsti($params);
		$this->set('rezultat',$result);
	}
	
	private function iskanjePoOceni($niz)
	{	
		$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
		$params = new stdClass;
		$params->Param1 = new stdClass;
		//$params = (object)null;
		$params->Param1 = $niz; 
		$result = $client->iskanjePoOceni($params);

		$this->set('rezultat',$result);
	}


	public function index(){
		if (!empty($this->data)){
		   	if ($this -> request -> is('POST')) {
				//$vrednost = $this->request->data('isci');
				$niz = $this->request->data('search');
				$izbira = $this->request->data('izbira');
				$zvrst = $this->request->data('zvrsti');
				$ocena = $this->request->data('ocene');

				if($niz != "" && $izbira != 'ni'){
					if($izbira == "naslov"){
						$this->iskanjePoNaslovu($niz);
					}
					if($izbira == "leto"){
					 	$this->iskanjePoLetu($niz);
					}
					if($izbira == "jezik"){
						$this->iskanjePoJeziku($niz);
					}
					if($izbira == "drzava"){
						$this->iskanjePoDrzavi($niz);
					}
				}
				if($izbira == "zvrst" && $zvrst != 'ni'){
					$this->iskanjePoZvrsti($zvrst);
				}
				if($izbira == "ocena" && $ocena != 'ni'){
					$this->iskanjePoOceni($ocena);
				}
			}
		}
	}
}
	