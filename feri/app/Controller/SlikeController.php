<?php
class SlikeController extends AppController {
	var $uses = array('Album', 'Slike', 'User');

	function spremeniNaslov($naslov) {
		$sumnik = array("Č", "č", "Š", "š", "Ž", "ž", "Ć", "ć", "Đ", "đ");
		$sicnik = array("C", "c", "S", "s", "Z", "z", "C", "c", "Dj", "dj");
		$naslov = str_replace($sumnik, $sicnik, $naslov);

		return $naslov;
	}
	
	public function index() {
		$this->redirect(array("controller"=>"Slike","action"=>"prikaz_slik_po_albumu"));
	}

	function dodaj_slike() {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				$neuspeleSlike = array();
				$this -> set('albumi', $this -> Album -> seznamAlbumov());

				if ($this -> request -> is('post')) {

					$slika = $this -> request -> params['form'];
					if (isset($slika)) {

						$naslovAlbuma = $this -> request -> data['izbira-album'];

						$naslovAlbumcka = $this -> spremeniNaslov($naslovAlbuma);
						$nedovoljeniZnaki = array("\\", "/", ":", "*", "?", "\"", "<", ">", "|");
						$naslovAlbumcka = str_replace($nedovoljeniZnaki, "", $naslovAlbumcka);

						$id = $this -> Slike -> idAlbuma($naslovAlbuma);
						// Dobimo ime albuma, kateremu najdemo index
						$id = $id[0]['album']['id'];
						// index albuma v katerega shranimo slike

						$tipi = $slika['slika']['type'];
						$i = 0;

						foreach ($tipi as $tip) {
							$tip = strtolower(substr($tip, 6));
							if (($tip == 'jpeg' || $tip == 'jpg' || $tip == 'bmp' || $tip == 'png') && $slika['slika']['size'][$i] <= 2097152) {
								$imeSlike = $slika['slika']['name'][$i];
								$velikostSlike = $slika['slika']['size'][$i] / 1024;
								$tmpSlike = $slika['slika']['tmp_name'][$i];

								move_uploaded_file($tmpSlike, WWW_ROOT . '/images/' . $naslovAlbumcka . '/' . $imeSlike);

								$this -> Slike -> dodajSliko($imeSlike, $velikostSlike, $id);

								$nw = 150;
								$nh = 150;
								$source = 'images/' . $naslovAlbumcka . '/' . $imeSlike;
								$dest = 'thumbnails/' . $naslovAlbumcka . '/' . $imeSlike;

								$size = getimagesize($source);
								$w = $size[0];
								$h = $size[1];

								switch($tip) {
									case 'jpeg' :
										$simg = imagecreatefromjpeg($source);
										break;
									case 'jpg' :
										$simg = imagecreatefromjpeg($source);
										break;
									case 'png' :
										$simg = imagecreatefrompng($source);
										break;
									case 'bmp' :
										$simg = imagecreatefromwbmp($source);
										break;
								}

								$dimg = imagecreatetruecolor($nw, $nh);
								$wm = $w / $nw;
								$hm = $h / $nh;
								$h_height = $nh / 2;
								$w_height = $nw / 2;

								if ($w > $h) {
									$adjusted_width = $w / $hm;
									$half_width = $adjusted_width / 2;
									$int_width = $half_width - $w_height;
									imagecopyresampled($dimg, $simg, -$int_width, 0, 0, 0, $adjusted_width, $nh, $w, $h);
								} else if (($w < $h) || ($w == $h)) {
									$adjusted_height = $h / $wm;
									$half_height = $adjusted_height / 2;
									$int_height = $half_height - $h_height;

									imagecopyresampled($dimg, $simg, 0, -$int_height, 0, 0, $nw, $adjusted_height, $w, $h);
								} else {
									imagecopyresampled($dimg, $simg, 0, 0, 0, 0, $nw, $nh, $w, $h);
								}
								imagejpeg($dimg, $dest, 100);
								$this -> Session -> setFlash('Slike so bile uspešno naložene!');

							} else {
								$neuspeleSlike[] = $slika['slika']['name'][$i];
								$niz = implode(", ", $neuspeleSlike);
								$this -> Session -> setFlash('Prišlo je do napake pri nalaganju datoteke: ' . $niz);
							}

							$i++;
						}

						$stSlikVAlbumu = $this -> Slike -> prestejSlike($id);
						$stSlikVAlbumu = $stSlikVAlbumu[0][0]['COUNT(*)'];
						// Trenutnetno število slik v albumu
						$this -> Album -> posodobiSteviloSlik($stSlikVAlbumu, $id);
						// Posodobimo število slik v albumu
					}

				}
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}

	}

	function prikaz_slik_po_albumu() {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {

				$this -> set('albumi', $this -> Album -> seznamAlbumov());
				if ($this -> params['url'] != NULL) {
					$naslovAlbuma = $this -> request -> query['izbira-album'];
					//$this -> params['url']['izbira-album'];
					$this -> set('naslovAlbuma', $naslovAlbuma);

					$id = $this -> Slike -> idAlbuma($naslovAlbuma);
					// Dobimo ime albuma, kateremu najdemo index
					if (isset($id[0]['album']['id'])) {
						$id = $id[0]['album']['id'];
						// index albuma v katerega shranimo slike

						$stSlikVAlbumu = $this -> Album -> steviloSlik($id);
						$stSlikVAlbumu = $stSlikVAlbumu[0]['album']['stevilo_slik'];
						// Trenutno število slik v albumu

						$this -> set('slikeVAlbumu', $this -> Slike -> prikaziSlikeAlbuma($id));
						$this -> set('stSlikVAlbumu', $stSlikVAlbumu);
					}
				} else {
					$this -> set('slikeVAlbumu', NULL);
					$this -> set('stSlikVAlbumu', NULL);
				}

			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function izbrisi($id) {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				App::uses('Folder', 'Utility');
				App::uses('File', 'Utility');

				$slika = $this -> Slike -> prikaziSliko($id);
				$idAlbuma = $slika[0]['slika']['album_id'];
				// ID albuma v katerem se nahaja slika

				$imeSlike = $slika[0]['slika']['ime_slike'];
				// Ime izbrane slike

				$naslovAlbuma = $this -> Album -> naslovAlbuma($idAlbuma);
				$naslovAlbuma = $naslovAlbuma[0]['album']['naslov'];
				$this -> set('naslovAlbuma', $naslovAlbuma);

				$sumnik = array("Č", "č", "Š", "š", "Ž", "ž", "Ć", "ć", "Đ", "đ");
				$sicnik = array("C", "c", "S", "s", "Z", "z", "C", "c", "Dj", "dj");
				$naslovAlbuma = str_replace($sumnik, $sicnik, $naslovAlbuma);

				//BRISANJE SLIK IZ DISKA
				$file = new File('images/' . $naslovAlbuma . '/' . $imeSlike);
				$file -> delete();
				$file = new File('thumbnails/' . $naslovAlbuma . '/' . $imeSlike);
				$file -> delete();

				//BRISANJE SLIKE IZ DB
				$this -> Slike -> izbrisiSliko($id);

				//POSODOBITEV ŠTEVILA SLIK V ALBUMU
				$stSlik = $this -> Slike -> prestejSlike($idAlbuma);
				$stSlik = $stSlik[0][0]['COUNT(*)'];

				$this -> Album -> posodobiSteviloSlik($stSlik, $idAlbuma);

				$this -> Session -> setFlash("Uspešno ste izbrisali sliko!");
				$this -> redirect(array("action" => 'prikaz_slik_po_albumu', '?' => array('izbira-album' => $naslovAlbuma, '&' => array('prikazi' => 'Prikaži'))));
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function brisanje_slik() {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				App::uses('Folder', 'Utility');
				App::uses('File', 'Utility');

				if ($this -> request -> is('post')) {
					$this -> set('slike', $this -> request -> data);
					$naslovAlbuma = $this -> request -> data('naziv-albuma');
					$slikeId = $this -> request -> data('izbira-slike');

					$sumnik = array("Č", "č", "Š", "š", "Ž", "ž", "Ć", "ć", "Đ", "đ");
					$sicnik = array("C", "c", "S", "s", "Z", "z", "C", "c", "Dj", "dj");
					$naslovAlbuma = str_replace($sumnik, $sicnik, $naslovAlbuma);

					// ID albuma v katerem se nahaja slika
					$idAlbuma = $this -> Album -> dobiIdAlbuma($naslovAlbuma);
					$idAlbuma = $idAlbuma[0]['album']['id'];

					foreach ($slikeId as $slikaId) {
						$slika = $this -> Slike -> prikaziSliko($slikaId);
						$imeSlike = $slika[0]['slika']['ime_slike'];

						//BRISANJE SLIK IZ DISKA
						$file = new File('images/' . $naslovAlbuma . '/' . $imeSlike);
						$file -> delete();
						$file = new File('thumbnails/' . $naslovAlbuma . '/' . $imeSlike);
						$file -> delete();

						//BRISANJE SLIKE IZ DB
						$this -> Slike -> izbrisiSliko($slikaId);
					}

					// PREŠTEJEMO ŠTEVILO SLIK, KI SO V BAZI V NEKEM ALBUMU, PO IZBRISU
					$stSlikVAlbumu = $this -> Slike -> prestejSlike($idAlbuma);
					$stSlikVAlbumu = $stSlikVAlbumu[0][0]['COUNT(*)'];

					// POSODOBIMO ŠTEVILO SLIK
					$this -> Album -> posodobiSteviloSlik($stSlikVAlbumu, $idAlbuma);

					$this -> Session -> setFlash("Uspešno ste izbrisali željene slike!");
					$this -> redirect(array("action" => 'prikaz_slik_po_albumu', '?' => array('izbira-album' => $naslovAlbuma, '&' => array('prikazi' => 'Prikaži'))));

				}
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}

	}

}
?>