<?php
class MojprofilController extends AppController {
	var $uses = array('Urniki','Mojurnik','User');
    
    //funkcija dobi zacetek predmeta (13:00) in trajanje (2) in vrne konec -> 14:00
    private function getKonec($zacetek, $trajanje){
        //kompleksen algoritem ki spremeni trajanje v ure, prišteje to k začetku in pol še ugotovi ali je pouna ura
        list($ura, $minuta) = explode(":", $zacetek);
        $skupaj = $ura.$minuta; // dobim iz 13:00 -> 1300
        $razlika = $trajanje / 2;
        if(is_float($razlika)){
           $razlika = $razlika * 100 - 20;
        } else {
            $razlika = $razlika * 100;   
        }
        $vsota = $skupaj + $razlika;
        $dolzina = strlen($vsota);
        $konec = "";
        $prvi = "";
        $drugi = "";
        if($dolzina == 4){
            $prvi = substr($vsota,0,2);
            $drugi = substr($vsota,2,2);
            $konec = $prvi.":".$drugi;
        } else {
            $prvi = substr($vsota,0,1);
            $drugi = substr($vsota,1,2);
        }
        if($drugi == "60"){
            $konec = ($prvi+1).":00";
        }else{
            $konec = $prvi.":".$drugi;
        }
        return $konec;
    }
    
	function index(){
		 if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            $podatki = $this->User->DobiPodatke($user_id);
            $tip = $this->User->getTip($user_id);
            $tip = $tip[0]['u']['tip'];
            $this->set("tip",$tip);
            $this->set("podatki",$podatki);
            $branch_id = $this->User->getBranchId($user_id);  //dobimo od uporabnika branch id
            $razporeditev1 = $this->User->getRazporeditve($user_id);
			$razporeditev = $razporeditev1[0]['r']['razporeditev'];
			$staticno = $razporeditev1[0]['r']['staticno'];
			$this->set("staticno",$staticno);
			$razporeditev = explode(",", $razporeditev);
			$this->set("razporeditev",$razporeditev);
            if(!empty($branch_id)){
                $branch_id = $branch_id[0]['b']['branch_id'];
                $today = getdate();
                $datum = $today['mday'].".".$today['mon'].".".$today['year'];
                $privzeto = $this->Urniki->getPrivzeteUrnike($branch_id, $datum);
                $moji_urniki = $this->Mojurnik->getMyUrnik($user_id);
                //dva urnika združimo v celoto in posredujemo viewu...
                foreach($privzeto as $privzet_urnik){
                    $zacetek = $privzet_urnik[0]['zacetek'];
                    $trajanje = $privzet_urnik[0]['trajanje'];
                    $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
                    $temp = array(
                        "predmet" => $privzet_urnik[0]['predmet'],
                        "dan" => $privzet_urnik[0]['dan'],
                        "ucilnica" => $privzet_urnik[0]['ucilnica'],
                        "zacetek" => $zacetek,
                        "konec" => $konec,
                        "vrsta" => $privzet_urnik[0]['vrsta'],
                        "ime" => $privzet_urnik[0]['ime'],
                        "priimek" => $privzet_urnik[0]['priimek'],
                    );
                    $urniki[] = $temp;
                }
                foreach($moji_urniki as $moj_urnik){
                    $temp = array(
                        "predmet" => $moj_urnik['u']['predmet'],
                        "dan" => $moj_urnik['u']['dan'],
                        "ucilnica" => $moj_urnik['u']['ucilnica'],
                        "zacetek" => $moj_urnik['u']['zacetek'],
                        "konec" => $moj_urnik['u']['konec'],
                        "vrsta" => $moj_urnik['u']['vrsta'],
                         "ime" => $moj_urnik['u']['ime'],
                        "priimek" => $moj_urnik['u']['priimek'],
                    );
                    $urniki[] = $temp;
                }
				if(isset($urniki)){
					$this->set("urniki",$urniki);
					$all_branch = $this->Urniki->getAllBranch();
	                $this->set("vsi",$all_branch);
				}else{
					$this->set("urniki","ni");
	                $all_branch = $this->Urniki->getAllBranch();
	                $this->set("vsi",$all_branch);
				}
                
            } else {
                $this->set("urniki","ni");
                $all_branch = $this->Urniki->getAllBranch();
                $this->set("vsi",$all_branch);
				
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
	
	public function spremeniizgled() {
		if($this->Session->check('uporabnik')){
			if($this->request->is('post')){
            	$uporabnik = $this->Session->read('uporabnik');
          	    $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
				$user_id = $user_id[0]['u']['id'];
				$output = array();
				$novice = $this->request->data('novice');
				$oglasna = $this->request->data('deska');
				$urnik = $this->request->data('urnik');
				$koledar = $this->request->data('koledar');
				$novice2 = $this->request->data('novice2');
				$tvspored = $this->request->data('tvspored');
				$zapogl = $this->request->data('zapogl');
				$kino = $this->request->data('kino');
				$dogodki = $this->request->data('dogodki');
				if($novice == "da"){
					$output[] = "novice";
				}
				if($oglasna == "da"){
					$output[] = "oglasna_deska";
				}
				if($urnik == "da"){
					$output[] = "urnik";
				}
				if($koledar == "da"){
					$output[] = "koledar";
				}
				if($tvspored == "da"){
					$output[] = "tvspored";
				}
				if($zapogl == "da"){
					$output[] = "zapogl";
				}
				if($kino == "da"){
					$output[] = "kino";
				}
				if($dogodki == "da"){
					$output[] = "dogodki";
				}
				$za_v_bazo = "";
				for($i = 0; $i < count($output);$i++){
					if($i<count($output)-1){
						$za_v_bazo .= $output[$i].",";
					} else {
						$za_v_bazo .= $output[$i];
					}
				}
				$obstaja = $this->User->preveriRazporeditev($user_id);
				if(empty($obstaja)){
					$obstaja = $this->User->narediRazporeditve($user_id);
				}
				if($novice2 == "da"){
					$this->User->vstaviNovice2($user_id);
				} else {
					$this->User->zbrisiNovice2($user_id);
				}
				$this->User->spremeniRazporeditve($user_id, $za_v_bazo);
				$this->redirect("index");
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
		} else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
	
	public function uredisliko() {
		if($this->Session->check('uporabnik')){
			$uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
			if($this->request->is('post')){
				$slika = $this->request->params['form'];

				if (isset($slika)) {
					$poljeTipa = explode("/", $slika['datoteka']['type']);
					
					if($poljeTipa[0] == 'image'){
						App::uses('Folder', 'Utility');
						App::uses('File', 'Utility');
						$trenutna = $this->User->getTrenutnaSlika($user_id);
						$trenutna = $trenutna[0]['u']['Slika'];
						$pot = WWW_ROOT ."/img/Server/".$uporabnik."/".$trenutna."";
						echo $pot;
						$file = new File($pot);
						$file->delete();
						$this->User->spremeniSliko($user_id,$slika['datoteka']['name']);
						$this->Session->write("slika",$slika['datoteka']['name']);
						move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT.'/img/Server/'.$uporabnik.'/'.$slika['datoteka']['name']);
						$this->redirect(array("controller"=>"mojprofil","action"=>"index"));
					}
				}
			} else {
				$this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
			}
		} else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
    }
    
    public function uredimail() {
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
                $mail = $this->request->data('mail');
                $this->User->spremeniMail($user_id,$mail);
                $this->redirect(array("controller"=>"mojprofil","action"=>"index"));
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
    
    public function shranismer() {
         if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
                $branch = $this->request->data('branch_id');
                $this->User->dodajSmerKStudentu($user_id,$branch);
                $this->redirect(array("controller"=>"mojprofil","action"=>"index"));
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
	
	public function posodobismer() {
         if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
                $branch = $this->request->data('branch_id');
                $this->User->posodobiSmerKStudentu($user_id,$branch);
                $this->redirect(array("controller"=>"mojprofil","action"=>"index"));
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
    
    public function uredirojdan() {
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
                $datum = $this->request->data('rojdan');
                $this->User->urediDatumRojstva($user_id,$datum);
                $this->redirect(array("controller"=>"mojprofil","action"=>"index"));
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
    
    public function spremenigeslo() {
        if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
            if($this->request->is('post')){
                $gesloprej = $this->request->data('prejpass');
                $novo1 = $this->request->data('novoena');
                $novo2 = $this->request->data('novodva');
                if($novo1 == $novo2){
                    $rezultat = $this->User->preveriGeslo($user_id, md5($gesloprej));
					if(empty($rezultat)){
						$this->set("error","Staro geslo ni pravilno!");
                		$this->render("/Errors/vpis"); 
					} else {              
						$this->User->spremeniGeslo($user_id, md5($novo1));
                  	  	$this->redirect(array("controller"=>"mojprofil","action"=>"index"));
					}
                } else {
                    $this->set("error","Vpisani gesli nista enaki!");
                	$this->render("/Errors/vpis"); 
                }
            } else {
                $this->set("error","Nimate pravic za to opravilo!");
                $this->render("/Errors/vpis"); 
            }
         } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
	}
	
}