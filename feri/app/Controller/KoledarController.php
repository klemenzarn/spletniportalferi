<?php
class KoledarController extends AppController{
	var $uses = array('Koledar','User');
	
	public function index(){
		$datum = date("n");
		$leto = date("Y");
		$rezultat = $this->Koledar->getKoledarPoMesecu($datum,$leto);
		$this->set("dogodki",$rezultat);
		$this->set("mesec",$datum);
		$this->set("leto",$leto);
		$tip = 1;
		if($this->Session->check('uporabnik')){
			$uporabnik = $this->Session->read('uporabnik');
       	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
       		$user_id = $user_id[0]['u']['id'];
			$tip = $this->User->getTip($user_id);
        	$tip = $tip[0]['u']['tip'];
		}
		$this->set('tip',$tip);
	}
	
	public function poglej($id = null) {
		if($id != null){
			$dogodek = $this->Koledar->getDogodek($id);
			$this->set("dogodek",$dogodek);
			$stevilo_udelezb = $this->Koledar->getStUdelezb($id);
			$this->set("stevilo",$stevilo_udelezb);
			$preveri = null;
			$tip = 1;
			if($this->Session->check('uporabnik')){
				$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$preveri = $this->Koledar->preveriUdelezbo($user_id,$id);
				$tip = $this->User->getTip($user_id);
        		$tip = $tip[0]['u']['tip'];
			}
			$this->set('tip',$tip);
			$this->set("udelezba",$preveri);
		} else {
			$this->set("error","Do te strani nemorete dostopati ali nimate pravic za to opravilo!");
			$this->render("/Errors/vpis");
		}
	}
	
	public function udelezba(){
		 if($this->Session->check('uporabnik')){
		 	if($this->request->is('post')){
		 		$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$id_dogodka = $this->request->data('id');
				$this->Koledar->dodajUdelezboDogodka($user_id,$id_dogodka);
				$this->redirect(array("controller"=>"koledar","action"=>"poglej",$id_dogodka));
		 	} else {
		 		$this->set("error","Nimate pravic za to opravilo!");
         	   	$this->render("/Errors/vpis");  
		 	}
		 } else {
		 	$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		 }
	}
	
	public function neudelezba() {
 		if($this->Session->check('uporabnik')){
		 	if($this->request->is('post')){
		 		$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$id_dogodka = $this->request->data('id');
				$this->Koledar->izbrisiUdelezbo($user_id,$id_dogodka);
				$this->redirect(array("controller"=>"koledar","action"=>"poglej",$id_dogodka));
		 	} else {
		 		$this->set("error","Nimate pravic za to opravilo!");
         	   	$this->render("/Errors/vpis");  
		 	}
		 } else {
		 	$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		 }
	}
	
	public function mesec(){
		if($this->request->is('post')){
			$mesec = $this->request->data('mesec');
			$leto = $this->request->data('leto');
			if(empty($leto)){
				$leto = date("Y");
			}
			$rezultat = $this->Koledar->getKoledarPoMesecu($mesec,$leto);
			$this->set("dogodki",$rezultat);
			$this->set("mesec",$mesec);
			$this->set("leto",$leto);
			$tip = 1;
			if($this->Session->check('uporabnik')){
				$uporabnik = $this->Session->read('uporabnik');
	       	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
	       		$user_id = $user_id[0]['u']['id'];
				$tip = $this->User->getTip($user_id);
	        	$tip = $tip[0]['u']['tip'];
			}
			$this->set('tip',$tip);
			$this->render("index");
		}else{
			$this->set("error","Nimate pravic za to opravilo!");
         	$this->render("/Errors/vpis");  
		}
	}
	
	public function dodaj(){
		if($this->Session->check('uporabnik')){
		 	if($this->request->is('post')){
		 		$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$tip = $this->User->getTip($user_id);
            	$tip = $tip[0]['u']['tip'];
            	if($tip == 2 || $tip == 3){
            		$datum = $this->request->data('datum');
					$vsebina = $this->request->data('vsebina');
					$naslov = $this->request->data('naslov');
					$this->Koledar->dodajKoledar($naslov, $vsebina, $datum);
					$this->redirect(array("controller"=>"Koledar","action"=>"index"));
            	} else {
            		$this->set("error","Nimate pravic za to opravilo!");
         			$this->render("/Errors/vpis");
            	}
		 	}
		 } else {
		 	$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		 }
	}

	public function zbrisi($id=null) {
		if($id == null){
			$this->set("error","Nimate pravic za to opravilo!");
         	$this->render("/Errors/vpis");
		}else{
			if($this->Session->check('uporabnik')){
				$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$tip = $this->User->getTip($user_id);
            	$tip = $tip[0]['u']['tip'];
            	if($tip == 2 || $tip == 3){
            		$this->Koledar->izbrisiKoledar($id);
					$this->redirect(array("controller"=>"Koledar","action"=>"index"));
				} else {
					$this->set("error","Nimate pravic za to opravilo!");
         			$this->render("/Errors/vpis");
				}
			} else {
		 	$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		 	}
		}
	}
	
	public function uredi($id = null) {
		if($id == null){
			$this->set("error","Nimate pravic za to opravilo!");
         	$this->render("/Errors/vpis");
		}else{
			if($this->Session->check('uporabnik')){
				$uporabnik = $this->Session->read('uporabnik');
           	 	$user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
           		$user_id = $user_id[0]['u']['id'];
				$tip = $this->User->getTip($user_id);
            	$tip = $tip[0]['u']['tip'];
            	if($tip == 2 || $tip == 3){
            		if($this->request->is('post')){
            			$datum = $this->request->data('datum');
						$vsebina = $this->request->data('vsebina');
						$naslov = $this->request->data('naslov');
						$this->Koledar->posodobiKoledar($id, $naslov, $vsebina, $datum);
						$this->redirect(array("controller"=>"Koledar","action"=>"index"));
            		} else {
            			$dogodek = $this->Koledar->getDogodek($id);
						$this->set("dogodek",$dogodek);
            		}
				} else {
					$this->set("error","Nimate pravic za to opravilo!");
         			$this->render("/Errors/vpis");
				}
			} else {
		 	$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		 	}
		}
	}
	
}
