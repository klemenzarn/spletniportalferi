<?php
App::import('Vendor', 'nusoap');
class TvSporedController extends AppController {
	var $uses = array('TvSpored');

	function index() {
		$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
		$client -> soap_defencoding = 'UTF-8';
		$client -> decode_utf8 = false;
		$programi = $client -> call('getVsePrograme');
		$programi = $programi['getVseProgrameResult']['string'];
		$this -> set('programi', $programi);

		if ($this -> request -> is('post')) {
			//Iz comboboxa dobimo naziv programa
			$naslovPrograma = $this -> request -> data('program-naziv');
			$this -> set('nazivPrograma', $naslovPrograma);

			//Dobimo id programa, glede na naziv programa, ki ga dobimo iz comboboxa
			$param = array('naziv' => $naslovPrograma);
			$idPrograma = $client -> call('getIdPrograma', array('parameters' => $param, ",", false, true));
			$idPrograma = $idPrograma['getIdProgramaResult'];

			/*echo '<pre>';
			 print_r($idPrograma);
			 echo '</pre>';*/

			// Dobimo oddaje programa glede na prej dobljen id
			$parameters = array('id' => $idPrograma);
			$oddaje = $client -> call('getOddajePrograma', array('parameters' => $parameters, ",", false, true));

			if(isset($oddaje['getOddajeProgramaResult']['string'])){
				$oddaje = $oddaje['getOddajeProgramaResult']['string'];
				$this -> set('oddaje', $oddaje);
			}else{
				$this->set('oddaje', null);
			}

		}
	}

	function iskanje_oddaje() {
		$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
		$client -> soap_defencoding = 'UTF-8';
		$client -> decode_utf8 = false;

		$ura = $this -> request -> data('ura');
		$minute = $this -> request -> data('minute');

		if ($this -> request -> is('post')) {
			$nazivOddaje = $this -> request -> data('naziv-oddaje');
			$datum = date("Y-m-d");
			$cas = $ura . ":" . $minute . ":00";
			$zacetekOddaje = $datum . " " . $cas;

			if ($nazivOddaje != "" && $ura == "" && $minute == "") {
				$param = array('nazivOddaje' => $nazivOddaje);
				$oddaje = $client -> call('najdiOddajo', array('parameters' => $param, ",", false, true));
				$this -> set('oddaje', $oddaje['najdiOddajoResult']);
			}

			if ($ura != "" && $minute != "" && $nazivOddaje == "") {
				$param = array('cas' => $zacetekOddaje);
				$oddaje = $client -> call('oddajePoCasu', array('parameters' => $param, ",", false, true));
				$this -> set('oddaje', $oddaje['oddajePoCasuResult']);

			}

			if ($ura != "" && $minute != "" && $nazivOddaje != "") {
				echo '<script>alert("Izberete lahko samo 1 kriterij, po katerem boste iskali!")</script>';
			}

		}
	}

}
?>