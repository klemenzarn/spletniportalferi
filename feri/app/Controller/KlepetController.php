<?php
class KlepetController extends AppController{
	var $uses = array('User','Klepet');
    public $components = array('RequestHandler');
	
	public function index(){
		if($this->Session->check('uporabnik')){
			$uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
			
		} else {
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");
		}
	}
	
	public function poslji(){
		if($this->Session->check('uporabnik')){
			if($this->request->is('post')){
				$uporabnik = $this->Session->read('uporabnik');
                $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
                $user_id = $user_id[0]['u']['id'];
				$sporocilo = $this->request->data('msg');
				$this->Klepet->dodajSporocilo($sporocilo, $user_id);
				$this->set('msg', $uporabnik.": ".$sporocilo);
                $this->render('poslji', 'ajax'); 
			} else {
				$this->set("error","Ta stran ne obstaja ali pa nimate pravice za dostop do nje.");
            	$this->render("/Errors/vpis");
			}
		} else {
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");
		}
	}
	
	public function preveri(){
		if($this->request->is('post')){
			$stevilo = $this->request->data('stevilo');
			ini_set('max_execution_time', 300); //300 seconds = 5 minutes
			$trenutno = $this->Klepet->getStevilo();
			$trenutno = $trenutno[0][0]['stevilo'];
			while(true){
				$trenutno1 = $this->Klepet->getStevilo();
				$trenutno = $trenutno1[0][0]['stevilo'];
				if($trenutno != $stevilo)
					break;
			}

			$sporocila = $this->Klepet->getAll();
			$this->set("msg",$sporocila);
			$this->render('poslji', 'ajax');
		} else {
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");
		}
	}
}