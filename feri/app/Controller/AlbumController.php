<?php
class AlbumController extends AppController {
	var $uses = array('Album', 'Slike', 'User');

	function spremeniNaslov($naslov) {
		$sumnik = array("Č", "č", "Š", "š", "Ž", "ž", "Ć", "ć", "Đ", "đ");
		$sicnik = array("C", "c", "S", "s", "Z", "z", "C", "c", "Dj", "dj");
		$naslov = str_replace($sumnik, $sicnik, $naslov);

		return $naslov;
	}

	function dodaj_album() {
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		App::uses('Sanitize', 'Utility');
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {

				$avtor = $this -> Session -> read("uporabnik");
				$this -> set('avtor', $avtor);

				if ($this -> request -> is('post')) {
					$naslov = $this -> request -> data('naslov');
					$opis = $this -> request -> data('opis');

					if ($naslov != "") {
						$naslov = Sanitize::escape($naslov);
						$this -> Album -> dodajAlbum($naslov, $avtor, $opis, 0);
						// Dodamo album v bazo

						$nedovoljeniZnaki = array("\\", "/", ":", "*", "?", "\"", "<", ">", "|");
						$naslov = str_replace($nedovoljeniZnaki, "", $naslov);
						$naslov = $this -> spremeniNaslov($naslov);

						$folder = new Folder();
						$direktorij = WWW_ROOT . '/images/' . $naslov;
						$folder -> create($direktorij);
						// Ustvarimo mapo za album, v katerega bomo kasneje shranili slike
						$folder -> chmod($direktorij, 0700);
						// Nastavimo pravice mape

						$folderThumb = new Folder();
						$direktorijThumb = WWW_ROOT . '/thumbnails/' . $naslov;
						$folderThumb -> create($direktorijThumb);
						// Ustvarimo mapo za album, v katerega bomo kasneje shranili slike
						$folderThumb -> chmod($direktorijThumb, 0700);
						// Nastavimo pravice mape
						//mkdir(WWW_ROOT.'/images/'.$naslov, 0700);
						$this -> Session -> setFlash('Uspešno ste ustvarili nov album!');

					} else {
						$this -> Session -> setFlash('Vnesti je potrebno naslov albuma!');
					}
				}
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function index() {
		$this -> redirect(array("controller" => "Album", "action" => "seznam_albumov"));
	}

	function seznam_albumov() {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				$this -> set('albumi', $this -> Album -> seznamAlbumov());
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function izbrisi_album() {
		if ($this -> Session -> check("uporabnik")) {
			$tip = $this -> User -> DobiTip($this -> Session -> read("uporabnik"));
			if ($tip[0]['users']['Tip'] == 3 || $tip[0]['users']['Tip'] == 2) {
				if ($this -> request -> is('post')) {
					$albumi = $this -> request -> data;
					$this -> set('albumi', $albumi);

					$i = 0;
					if (isset($albumi['izbira-albuma'])) {
						$albumi = $albumi['izbira-albuma'];
						foreach ($albumi as $albumId) {
							$naslov = $this -> Album -> naslovAlbuma($albumId);

							$naslovAlbuma = $this -> spremeniNaslov($naslov[0]['album']['naslov']);
							$nedovoljeniZnaki = array("\\", "/", ":", "*", "?", "\"", "<", ">", "|");
							$naslovAlbuma = str_replace($nedovoljeniZnaki, "", $naslovAlbuma);

							$stSlik = $this -> Album -> steviloSlik($albumId);
							$stSlik = $stSlik[0]['album']['stevilo_slik'];

							if ($stSlik > 0) {
								$slike = $this -> Slike -> prikaziSlikeAlbuma($albumId);
								foreach ($slike as $slika) {
									unlink('images/' . $naslovAlbuma . "/" . $slika['slika']['ime_slike']);
									unlink('thumbnails/' . $naslovAlbuma . "/" . $slika['slika']['ime_slike']);
								}
								rmdir('images/' . $naslovAlbuma);
								rmdir('thumbnails/' . $naslovAlbuma);
								$this -> Session -> setFlash('Uspešno ste izbrisali album!');
							} else {
								rmdir('images/' . $naslovAlbuma);
								rmdir('thumbnails/' . $naslovAlbuma);
								$this -> Session -> setFlash('Uspešno ste izbrisali album!');
							}

							$this -> Album -> izbrisiAlbum($albumId, $stSlik);

							$i++;
						}
					}

					$this -> redirect(array("controller" => 'Album', "action" => 'seznam_albumov'));
				}
			} else {
				$this -> set("error", "Nimate pravic za dostop do te strani.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

}
?>