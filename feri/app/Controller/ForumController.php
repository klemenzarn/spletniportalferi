<?php
App::import('Vendor', 'nusoap');

class ForumController extends AppController {
	public $helpers = array('Js');
	public $uses = array('Forum', 'User');
	public $components = array('RequestHandler');

	public function index() {
		$vse = $this -> Forum -> getAll();
		$this -> set("vse", $vse);
	}

	public function webgl() {
		 $this->layout = false;
		$vse = $this -> Forum -> getAll();
		$this -> set("vse", $vse);
	}

	public function kategorije($ime, $kr = null) {
		if ($kr != null) {
			$temp = str_replace("-", " ", $kr);
			$id_teme = $this -> Forum -> getIDteme1($kr);
			$id_teme = $id_teme[0]['t']['id'];
			$steviloprispevkov_v_temi = $this -> Forum -> getSteviloPrispevkov($id_teme);
			$this -> set("steviloprispevkov", $steviloprispevkov_v_temi);
			if ($this -> request -> is('get')) {
				$stran = $this -> request -> query('stran');
				if ($stran == 0) {
					$stran = 1;
				}
			}
			$prispevki = $this -> Forum -> getPrispevkiPaging($id_teme, $stran);
			//$prispevki = $this -> Forum -> getPrispevki($id_teme);
			$avtor_teme = $this -> Forum -> getUporabnikIzTeme($id_teme);
			$this -> set("avtorteme", $avtor_teme);
			$this -> set("prispevki", $prispevki);
			$this -> set("ime_smeri", $ime);
			$this -> set("ime_teme", $kr);

			$this -> render("prispevki");
		} else {
			$id_kategorije=$this->Forum->getKategorijaID($ime);
			$stevilo_tem = $this -> Forum -> getSteviloTem($id_kategorije[0]['k']['id']);
			//print_r($stevilo_tem);
			$this -> set("stevilotem", $stevilo_tem);
			if ($this -> request -> is('get')) {
				$stran = $this -> request -> query('stran');
				if ($stran == 0) {
					$stran = 1;
				}
			}
			
			//$prispevki = $this -> Forum -> getPrispevkiPaging($id_teme, $stran);
			$this -> set("ime_smeri", $ime);
			
			$teme = $this -> Forum -> getTemePaging($ime,$stran);

			$this -> set("teme", $teme);
		}

		//posredujemo $ime (RIT-UNI npr.) viewu za kreiranje novih tem
		$this -> set("ime_kategorije", $ime);
	}

	public function kategorijewebgl($ime, $kr = null) {
		 $this->layout = false;
		if ($kr != null) {
			$temp = str_replace("-", " ", $kr);
			$id_teme = $this -> Forum -> getIDteme1($kr);
			$id_teme = $id_teme[0]['t']['id'];
			$steviloprispevkov_v_temi = $this -> Forum -> getSteviloPrispevkov($id_teme);
			$this -> set("steviloprispevkov", $steviloprispevkov_v_temi);
			if ($this -> request -> is('get')) {
				$stran = $this -> request -> query('stran');
				if ($stran == 0) {
					$stran = 1;
				}
			}
			$prispevki = $this -> Forum -> getPrispevkiPaging($id_teme, $stran);
			//$prispevki = $this -> Forum -> getPrispevki($id_teme);
			$avtor_teme = $this -> Forum -> getUporabnikIzTeme($id_teme);
			$this -> set("avtorteme", $avtor_teme);
			$this -> set("prispevki", $prispevki);
			$this -> set("ime_smeri", $ime);
			$this -> set("ime_teme", $kr);

			$this -> render("prispevkiwgl");
		} else {
			$id_kategorije=$this->Forum->getKategorijaID($ime);
			$stevilo_tem = $this -> Forum -> getSteviloTem($id_kategorije[0]['k']['id']);
			//print_r($stevilo_tem);
			$this -> set("stevilotem", $stevilo_tem);
			if ($this -> request -> is('get')) {
				$stran = $this -> request -> query('stran');
				if ($stran == 0) {
					$stran = 1;
				}
			}
			
			//$prispevki = $this -> Forum -> getPrispevkiPaging($id_teme, $stran);
			$this -> set("ime_smeri", $ime);
			
			$teme = $this -> Forum -> getTemePaging($ime,$stran);

			$this -> set("teme", $teme);
		}

		//posredujemo $ime (RIT-UNI npr.) viewu za kreiranje novih tem
		$this -> set("ime_kategorije", $ime);
	}

	public function dodaj($ime_kategorije, $ime_teme) {
		$vsebina = $this -> request -> data('tiny');
		//$naslovna_slika=$this->request->data('naslovna_slika');
		$avtor = $this -> Session -> read("uporabnik");
		$id_kategorije = $this -> Forum -> getKategorijaID($ime_kategorije);
		$id_kategorije = $id_kategorije[0]['k']['id'];
		$naslov_teme = $this -> Forum -> getNaslovTemeByUrl($ime_teme);
		// $koncan_id_teme=$this->Forum->getNaslovTemeByUrl($ime_teme);
		$id_teme = $this -> Forum -> getIDteme($naslov_teme[0]['t']['naslov'], $ime_kategorije);
		$id_teme = $id_teme[0]['t']['id'];
		$avtor = $this -> User -> getIdByName($avtor);
		$avtor = $avtor[0]['u']['id'];
		$this -> Forum -> dodajPrispevek($vsebina, $avtor, $id_teme);
		$zadnji = $this -> Forum -> DobiZadnji();
		$param = array('teme' => $zadnji[0][0]['zadnji'], 'vsebina' => $vsebina);
		$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
		$client -> soap_defencoding = 'UTF-8';
		$result = $client -> call('Lematiziraj', array('parameters' => $param), '', '', false, true);
		$this -> redirect(array('controller' => 'forum', 'action' => 'kategorije/' . $ime_kategorije . '/' . $ime_teme, $naslov_teme));
		//to mislm da je to
	}

	public function ustvari_temo1($ime_kategorije) {
		$this -> set("ime_smeri", $ime_kategorije);
		$naslov = $this -> request -> data('naslov_teme');
		$vsebina = $this -> request -> data('tiny');
		$url = Inflector::slug($naslov, '-');
		$avtor = $this -> Session -> read("uporabnik");
		$avtor = $this -> User -> getIdByName($avtor);
		$avtor = $avtor[0]['u']['id'];
		$ime_kategorije = $this -> Forum -> getKategorijaID($ime_kategorije);
		$ime_kategorije = $ime_kategorije[0]['k']['id'];
		$this -> Forum -> ustvariTemo($naslov, $url, $avtor, $ime_kategorije);
		//Za ustvarjeno temo moramo sedaj ustvariti prispevek
		$id_teme = $this -> Forum -> getIDteme2($naslov, $ime_kategorije);
		// $id_teme=$id_teme[0]['t']['id'];
		$this -> Forum -> dodajPrispevek($vsebina, $avtor, $id_teme[0]['t']['id']);
		$zadnji = $this -> Forum -> DobiZadnji();
		$param = array('teme' => $zadnji[0][0]['zadnji'], 'vsebina' => $vsebina);
		$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
		$client -> soap_defencoding = 'UTF-8';
		$result = $client -> call('Lematiziraj', array('parameters' => $param), '', '', false, true);
		$ime_kategorije2 = $this -> Forum -> getKategorijaName($ime_kategorije);
		$ime_kategorije2 = $ime_kategorije2[0]['k']['kategorija'];
		$this -> redirect(array("controller" => "forum", "action" => "kategorije/" . $ime_kategorije2, $url));
	}

	public function ustvari_temo($ime_kategorije) {
		$this -> set("ime_smeri", $ime_kategorije);

	}

	public function odgovori($ime_smeri, $ime_teme) {
		//$naslov=$this->request->data('naslov_novice');
		$vsebina = $this -> request -> data('tiny');
		//$naslovna_slika=$this->request->data('naslovna_slika');
		$avtor = $this -> Session -> read("uporabnik");
		//$this->Forum->dodajPrispevek($vsebina,$avtor,$ime_teme);
		// echo $ime_smeri." ".$ime_teme;
		$this -> set("ime_teme", $ime_teme);
		$this -> set("ime_smeri", $ime_smeri);
	}

	public function urediPrispevek($id_prispevka) {
		if ($this -> Session -> check('uporabnik')) {
			$puporabnik = $this -> Forum -> getUporabnikIzPrispevka($id_prispevka);
			if ($puporabnik[0]['u']['Uporabnisko'] == $this -> Session -> read('uporabnik')) {
				$vsebina = $this -> Forum -> getVsebinaPrispevka($id_prispevka);
				$this -> set("vsebina", $vsebina);
			} else {
				$this -> set("error", "Nimate pravic za ogled te strani!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Niste prijavljeni, prosimo prijavite se!");
			$this -> render("/Errors/vpis");
		}

	}

	public function shraniUrejenPrispevek($id_prispevka) {
		$vsebina = $this -> request -> data('tiny');
		$temp=$this->Forum->getIDteme3($id_prispevka);
		$tema=$temp[0]['p']['tema'];
		$this -> Forum -> UrediPrispevek($id_prispevka, $vsebina,$tema);
		$kategorija = $this -> Forum -> getKategorijaIzPrispevka($id_prispevka);

		/*    echo "<pre>";
		 print_r($kategorija);
		 echo "</pre>";*/
		$kategorija2 = $kategorija[0]['k']['kategorija'];
		$url = $kategorija[0]['t']['url_naslov'];
		$this -> redirect(array("controller" => "forum", "action" => "kategorije/" . $kategorija2, $url));
	}

		public function plus() {
		$id_prispevka = $this -> request -> data('ocene');
		$avtor = $this -> Session -> read("uporabnik");
		$glas=$this->Forum->preveriGlas($id_prispevka,$avtor);
		if($glas==null)
		{
		$this -> Forum -> plus($id_prispevka,$avtor);
		$this -> set("msg", "ok");
		$this -> render("ocenjevanje", "ajax");	
		}
		else {
			$this->set("msg","FAIL");
			$this -> render("ocenjevanje", "ajax");	
		}
	}

	public function minus() {
		$avtor = $this -> Session -> read("uporabnik");
		$id_prispevka = $this -> request -> data('ocene');
		//$this -> set("msg", $id_prispevka);
		$glas=$this->Forum->preveriGlas($id_prispevka,$avtor);
		if($glas==null)
		{
			$this -> set("msg", "ok");
		$this -> Forum -> minus($id_prispevka,$avtor);
		$this -> render("ocenjevanje", "ajax");	
		}
		else {
			$this -> set("msg", "FAIL!");
			$this -> render("ocenjevanje", "ajax");	
		}
	}

	public function stanje() {
		$stanje = $this -> request -> data('izbrano_Stanje');
		$tema = $this -> request -> data('idneketeme');
		$this -> Forum -> posodobi($stanje, $tema);
		$this -> redirect($_SERVER['HTTP_REFERER']);
	}

}
