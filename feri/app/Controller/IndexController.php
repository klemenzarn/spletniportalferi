<?php
App::uses('AppController', 'Controller');
App::import('Vendor','nusoap');
class IndexController extends AppController {
	public $name = 'Index';
	public $components = array('RequestHandler');
	public $uses = array('Oglas','Novice','User','Urniki','Mojurnik','Koledar','Dogodki','Drugenovice');

	public function index() {
		if($this->Session->check('uporabnik')){
			$uporabnik = $this->Session->read('uporabnik');
            $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
            $user_id = $user_id[0]['u']['id'];
			$razporeditev1 = $this->User->getRazporeditve($user_id);
			$razporeditev = $razporeditev1[0]['r']['razporeditev'];
			$staticno = $razporeditev1[0]['r']['staticno'];
			$this->set("staticno",$staticno);
			if($staticno == "novice2"){
				$novice2 = $this->Drugenovice->getVticnik();
				$this->set("novice2",$novice2);
			}
			$razporeditev = explode(",", $razporeditev);
			$this->set("razporeditev",$razporeditev);
			foreach ($razporeditev as $raz) {
				if($raz == 'novice'){
					$novice = $this->Novice->getStiriNovice();
					$this->set("novice",$novice);
				}
				if($raz == 'oglasna_deska'){
					$oglasi = $this->Oglas->getPetOglasov();
					$this->set("deska",$oglasi);
					$datoteke = $this->Oglas->getVseDatoteke(); // to morem še neki spremenit samo tako da vem!!!!!
					$this->set("datoteke",$datoteke);
				}
				if($raz == 'urnik'){
					$branch_id = $this->User->getBranchId($user_id);  //dobimo od uporabnika branch id
	            	if(!empty($branch_id)){
		                $branch_id = $branch_id[0]['b']['branch_id'];
		                $today = getdate();
		                $datum = $today['mday'].".".$today['mon'].".".$today['year'];
		                $dan = $today['wday'];
		                $privzeto = $this->Urniki->getPrivzeteUrnikeByDay($branch_id, $datum, $dan);
		                $moji_urniki = $this->Mojurnik->getMyUrnikByDay($user_id, $dan);
		                $urniki = null;
		                foreach($privzeto as $privzet_urnik){
		                    $zacetek = $privzet_urnik[0]['zacetek'];
		                    $trajanje = $privzet_urnik[0]['trajanje'];
		                    $konec = $this->getKonec($zacetek, $trajanje);  //izračunamo konec glede na začetek in trajanje
		                    $temp = array(
		                        "predmet" => $privzet_urnik[0]['predmet'],
		                        "dan" => $privzet_urnik[0]['dan'],
		                        "ucilnica" => $privzet_urnik[0]['ucilnica'],
		                        "zacetek" => $zacetek,
		                        "konec" => $konec,
		                        "vrsta" => $privzet_urnik[0]['vrsta'],
		                        "ime" => $privzet_urnik[0]['ime'],
		                        "priimek" => $privzet_urnik[0]['priimek'],
		                    );
		                    $urniki[] = $temp;
		                }
		                foreach($moji_urniki as $moj_urnik){
		                    $temp = array(
		                        "predmet" => $moj_urnik['u']['predmet'],
		                        "dan" => $moj_urnik['u']['dan'],
		                        "ucilnica" => $moj_urnik['u']['ucilnica'],
		                        "zacetek" => $moj_urnik['u']['zacetek'],
		                        "konec" => $moj_urnik['u']['konec'],
		                        "vrsta" => $moj_urnik['u']['vrsta'],
		                         "ime" => $moj_urnik['u']['ime'],
		                        "priimek" => $moj_urnik['u']['priimek'],
		                    );
		                    $urniki[] = $temp;
		                }
		                $this->set("urniki",$urniki);
					}
				}
				if($raz == 'koledar'){
					$datum = date("n");
					$leto = date("Y");
					$rezultat = $this->Koledar->getKoledarPoMesecu($datum,$leto);
					$this->set("koledar",$rezultat);
					$this->set("mesec",$datum);
					$this->set("leto",$leto);
				}
				if($raz == 'tvspored'){
					$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
					$client -> soap_defencoding = 'UTF-8';
					$client -> decode_utf8 = false;
			
					$oddaje = array();
					
					$idji = range(1, 33);
					shuffle($idji);
					for ($i = 0; $i < 3; $i++) {
						$id = $idji[$i];
						
						$param = array('id' => $id);
						$oddaje[] = $client -> call('trenutnaOddaja', array('parameters' => $param, ",", false, true));
					}
					$this -> set('oddaje', $oddaje);
				}
				if($raz == 'zapogl'){
					$client  = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
					$client->soap_defencoding = 'UTF-8';
					$client->decode_utf8 = false;
					$result = $client->call('DobiZadnjeTri', '', '', '', false, true);
					
					if ($client->fault) {
						$this->set('message', $result);
					} else {
						$err = $client->getError();
						if ($err) {
							$this->set('message', $err);
						} else {
							$this->set('data', $result);
						}
					}
				}
				if($raz == 'kino'){
					$client = new SoapClient("http://localhost:55457/Service1.asmx?WSDL");
					$params = new stdClass;
					$params->Param1 = new stdClass;
					//$params = (object)null;
					$params->Param1 = 5; 
					$result = $client->iskanjeTopOcen($params);
			
					$this->set('rezultat_kino',$result);
				}
				if($raz == 'dogodki'){
					$dogodki=$this->Dogodki->vticnik();
    			    $this->set("dogodki",$dogodki);
				}
			}
		} else {
			$novice2 = $this->Drugenovice->getVticnik();
			$this->set("novice2",$novice2);
			$oglasi = $this->Oglas->getPetOglasov();
			$this->set("deska",$oglasi);
			$novice = $this->Novice->getStiriNovice();
			$this->set("novice",$novice);
			$datoteke = $this->Oglas->getVseDatoteke(); // to morem še neki spremenit samo tako da vem!!!!!
			$this->set("datoteke",$datoteke);
			$datum = date("n");
			$leto = date("Y");
			$rezultat = $this->Koledar->getKoledarPoMesecu($datum,$leto);
			$this->set("koledar",$rezultat);
			$this->set("mesec",$datum);
			$this->set("leto",$leto);
		}
	}

	public function spremeniPozicije(){
		if($this->Session->check('uporabnik')){
			if($this->request->is('post')){
				$uporabnik = $this->Session->read('uporabnik');
                $user_id = $this->User->getIdByName($uporabnik); //dobimo id uporabnika iz seje
                $user_id = $user_id[0]['u']['id'];
				$podatki = $this->request->data('pozicijeArray');
				$za_v_bazo = "";
				for($i = 0;$i<count($podatki);$i++){
					if($i < count($podatki)-1){
						$za_v_bazo .= $podatki[$i].",";
					} else {
						$za_v_bazo .= $podatki[$i];
					}
				}
				$this->set("msg",$za_v_bazo);
				$this->User->spremeniRazporeditve($user_id,$za_v_bazo);
				$this->render("spremenipozicije","ajax");
			} else {
				$this->set("error","Nimate pravic za to opravilo!");
            	$this->render("/Errors/vpis");  
			}
		} else {
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		}
	}
	
    public function odjava(){
    	$this->User->setNoActive($this->Session->read('uporabnik'));
		$this->Session->delete('uporabnik');
		$this->Session->delete('slika');
        $this->redirect($_SERVER['HTTP_REFERER']);
	}
    
	/*public function getSlika($user = null) {
		if($this->Session->check('uporabnik')){
            $uporabnik = $this->Session->read('uporabnik');
			if($uporabnik == $user && $user != null){
				$slika = $this->User->getSlikoUporabnika($user);
				return $slika[0]['u']['Slika'];
			} else {
				$this->set("error","Nimate pravic za to opravilo!");
          		$this->render("/Errors/vpis");  
			}
		}else {
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
		}
	}*/

	public function prijava() {

		$uporabnisko = $this -> request -> data('user');
		$geslo = $this -> request -> data('pas');

		if (!empty($uporabnisko) && !empty($geslo)) {
			$vsebinafajla = "";
			$query = $this -> User -> preveriRogac($uporabnisko, md5($geslo));
			if ($query == null) {
				$this -> set('msg', "Napaka pri prijavi!");
				$vsebinafajla = "0";
				$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/feri/datoteka.txt", "wb");
				fwrite($fp, $vsebinafajla);
				fclose($fp);
				$this -> render('prijava', 'ajax');
			} else {
				$obstaja = $this -> User -> preveriRazporeditev($query[0]['users']['ID']);
				if (empty($obstaja)) {
					$obstaja = $this -> User -> narediRazporeditve($query[0]['users']['ID']);
				}
				$this -> Session -> write('uporabnik', $uporabnisko);
				$this -> Session -> write('slika', $query[0]['users']['Slika']);
				$this -> Session -> write('tip', $query[0]['users']['Tip']);
				//$uporabnik111=$this->Session->read("uporabnik");
				$vsebinafajla = "1";
				$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/feri/datoteka.txt", "wb");
				fwrite($fp, $vsebinafajla);
				fclose($fp);
				$this -> User -> UpdateCasPrijave($uporabnisko);
				$this -> set('msg', $_SERVER['HTTP_REFERER']);
				$this -> render('prijava', 'ajax');

			}

		} else {
			$this -> set('msg', "Vnesite uporabniško ime in geslo");
			$this -> render('prijava', 'ajax');
		}
	}
	
	public function registracija(){
		$ime=$this->request->data('name');
		$priimek=$this->request->data('surname');
		$uporabnisko=$this->request->data('username');
		$geslo=$this->request->data('pas1');
		$pwdlen=strlen($geslo);
		$ponovno_geslo=$this->request->data('pas2');
		$mail=$this->request->data('mail');
		$vpisna="";
		if( empty($ime)|| empty($priimek)|| empty($uporabnisko)|| empty($geslo)|| empty($mail))
		{
			//$this->Session->setFlash('Niste vnesli vseh podatkov');
			$this->set('msg',"Niste vnesli vseh podatkov!");
			$this->render('registracija','ajax');
		}
		else
		{
			if($pwdlen<6)
			{
				$this->set('msg',"Vnesite geslo dolžine vsaj 6 znakov!");
				$this->render('registracija','ajax');
			}
			else if($ponovno_geslo!=$geslo)
			{
					//$this->Session->setFlash('Gesli se ne ujemata');
					$this->set('msg',"Gesli se ne ujemata!");
					$this->render('registracija','ajax');
			}
			else
			{
			//regex za mail svašta :D
				$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
				if (preg_match($pattern, $mail) === 1) {
					//preveri še če uporabnik z tem uporabniškim že obstaja
					$query=$this->User->getIdByName($uporabnisko);
					if(empty($query))
					{
						$hash_geslo= md5($geslo);
						$this->User->InsertUporabnik($ime,$priimek,$uporabnisko,$hash_geslo,$mail);
						//$this->redirect(array("controller"=>"index","action"=>"index"));
						$folder = new Folder();
						if ($folder->create(WWW_ROOT.'/img/Server/'.$uporabnisko)) {
						    $slika = WWW_ROOT.'/img/Server/Special/privzetaSlika.png';		
							$file = new File($slika);
							if ($file->exists()) 
							{
							    $dir = new Folder(WWW_ROOT.'/img/Server/'.$uporabnisko, true);
							    $file->copy($dir->path . DS . $file->name);
							}
							
						}
						$this->set('msg',"OK");
					}
					else
					{
						$this->set('msg',"Uporabnik z tem uporabniškim imenom že obstaja!");
						$this->render('registracija','ajax');
					}
				}
				else {
				$this->set('msg',"Vnesli ste nepopoln oz. napačen e-mail naslov!");
				$this->render('registracija','ajax');
				}
			}
		}
	}
    
    public function error(){
        
    }
    
    //funkcija dobi zacetek predmeta (13:00) in trajanje (2) in vrne konec -> 14:00
    private function getKonec($zacetek, $trajanje){
        //kompleksen algoritem ki spremeni trajanje v ure, prišteje to k začetku in pol še ugotovi ali je pouna ura
        list($ura, $minuta) = explode(":", $zacetek);
        $skupaj = $ura.$minuta; // dobim iz 13:00 -> 1300
        $razlika = $trajanje / 2;
        if(is_float($razlika)){
           $razlika = $razlika * 100 - 20;
        } else {
            $razlika = $razlika * 100;   
        }
        $vsota = $skupaj + $razlika;
        $dolzina = strlen($vsota);
        $konec = "";
        $prvi = "";
        $drugi = "";
        if($dolzina == 4){
            $prvi = substr($vsota,0,2);
            $drugi = substr($vsota,2,2);
            $konec = $prvi.":".$drugi;
        } else {
            $prvi = substr($vsota,0,1);
            $drugi = substr($vsota,1,2);
        }
        if($drugi == "60"){
            $konec = ($prvi+1).":00";
        }else{
            $konec = $prvi.":".$drugi;
        }
        return $konec;
    }
    
    public function DobiTipUporabnika() {
		$uporabnik=$this->Session->read("uporabnik");
		$query=$this->User->DobiTip($uporabnik);
		return $query;
	} 

}
