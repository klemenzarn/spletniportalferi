<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class UsersController extends AppController {
	var $name = 'Users';
	var $uses = array('Users', 'Aips', 'User');
	var $helpers = array('Js' => array('Jquery'));
	//var $uses = array('Users', 'Slikes' );

	function ogled($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$podatki = $this -> User -> DobiPodatkeByName($uporabnik);

			if ($id == $podatki[0]['users']['ID']) {
				$this -> set('user', $this -> User -> DobiPodatke($id));
			} else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	private function sendMail($prejemnik, $zadeva, $vsebina) {
		$email = new CakeEmail('gmail');
		$email -> from('klemenko.zarn@gmail.com');
		$email -> to($prejemnik);
		$email -> subject($zadeva);
		$email -> send($vsebina);
	}

	function shrani($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$podatki = $this -> User -> DobiPodatkeByName($uporabnik);
			$u_id = $this -> request -> data('u_id');
			$tip = $this -> User -> DobiTip($uporabnik);

			if ($tip[0]['users']['Tip'] == 3) {
				$this -> redirect(array('action' => 'uredi', $podatki[0]['users']['ID']));
			} else {

				if ($id == $podatki[0]['users']['ID'] || $u_id == $podatki[0]['users']['ID']) {
					if (empty($this -> data)) {
						$this -> set('user', $this -> User -> DobiPodatke($id));
					} else {
						if (!empty($this -> data)) {
							if ($this -> request -> is('POST')) {
								$u_id = $this -> request -> data('u_id');
								$slika = $this -> request -> params['form'];

								if (isset($slika)) {
									$poljeTipa = explode("/", $slika['datoteka']['type']);

									$u_id = $this -> request -> data('u_id');
									$up_ime = $this -> request -> data('up_ime');
									$mail = $this -> request -> data('u_mail');
									$mesto = $this -> request -> data('u_mesto');
									$drzava = $this -> request -> data('u_drzava');
									$datum = $this -> request -> data('datum');
									$user_mail = $this -> User -> preveriMail($mail);
									$stevilka = count($user_mail);

									if ($poljeTipa[0] == 'image' && $u_id != NULL) {
										if (($slika['datoteka']['size'] / 1024) < 1000) {

											if ($stevilka == 0) {
												$user_slika = $this -> User -> podatkiUsera($u_id);
												App::uses('File', 'Utility');
												$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
												$file -> delete();

												move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
												//$this -> Session -> write('slika', $slika['datoteka']['name']);
												$this -> User -> posodobiP($u_id, $up_ime, $mail, $mesto, $drzava, $datum, $slika['datoteka']['name']);
											} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID']) {
												$user_slika = $this -> User -> podatkiUsera($u_id);
												App::uses('File', 'Utility');
												$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
												$file -> delete();

												move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
												//$this -> Session -> write('slika', $slika['datoteka']['name']);
												$this -> User -> posodobiP($u_id, $up_ime, $mail, $mesto, $drzava, $datum, $slika['datoteka']['name']);
											} else {
												$this -> Session -> setFlash("Neveljavni e-mail!");
												$this -> redirect(array('action' => 'shrani', $u_id));
											}

											$this -> redirect(array('action' => 'ogled', $u_id));
										} else {
											$this -> Session -> setFlash("Velikost slike je prevelika!");
											$this -> redirect(array('action' => 'shrani', $u_id));
										}

									} else {
										if ($slika['datoteka']['name'] == "") {
											if ($stevilka == 0) {
												$user_slika = $this -> User -> podatkiUsera($u_id);
												//App::uses('File', 'Utility');
												//$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
												//$file -> delete();

												//move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
												//$this -> Session -> write('slika', $slika['datoteka']['name']);
												$this -> User -> posodobiP($u_id, $up_ime, $mail, $mesto, $drzava, $datum, $user_slika[0]['users']['Slika']);
											} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID']) {
												$user_slika = $this -> User -> podatkiUsera($u_id);
												$this -> User -> posodobiP($u_id, $up_ime, $mail, $mesto, $drzava, $datum, $user_slika[0]['users']['Slika']);
											} else {
												$this -> Session -> setFlash("Neveljavni e-mail!");
												$this -> redirect(array('action' => 'shrani', $u_id));

											}
											$this -> redirect(array('action' => 'ogled', $u_id));
										} else {
											$this -> Session -> setFlash("Datoteka ni v pravilnem formatu!");
											$this -> redirect(array('action' => 'shrani', $u_id));
										}
									}
								}
							}
						}
					}
				} else {
					$this -> set("error", "Do te strani nemorete dostopati!");
					$this -> render("/Errors/vpis");
				}
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function sodelujoci($st = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$podatki = $this -> User -> DobiPodatkeByName($uporabnik);
			$tip = $this -> User -> DobiTip($uporabnik);

			if ($tip[0]['users']['Tip'] == 1) {

				if (empty($this -> data)) {
					if ($st == NULL) {
						$st = 1;
					}
					$this -> set('user', $this -> User -> vsiUseriSmer($st, $podatki[0]['users']['Smer']));
					$this -> set('stevila', $this -> User -> steviloUserovSmer($podatki[0]['users']['Smer']));
				} else if (!empty($this -> data)) {
					if ($this -> request -> is('POST')) {
						$vrednost = $this -> request -> data('isci');
						$niz = $this -> request -> data('search');
						if ($niz == "" && $vrednost == null || $niz != "" && $vrednost == null || $niz == "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							//$this->set('user',$this->User->vsiUseriSmer($st,$podatki[0]['users']['Smer']));
							//$this->set('stevila',$this->User->steviloUserov());

						} else if ($niz != "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							//$this->set('stevila',$this->User->steviloIskanihUserov($vrednost,$niz));
							//$this->set('user',$this->User->iskanjeUporabnikov($vrednost,$niz,$st));
						}
					}
				}
			} else if ($tip[0]['users']['Tip'] == 2) {
				if (empty($this -> data)) {
					if ($st == NULL) {
						$st = 1;
					}
					$this -> set('user', $this -> User -> vsiUseriTip($st, $tip[0]['users']['Tip'], $tip[0]['users']['Smer']));
					$this -> set('stevila', $this -> User -> steviloUseriTip($tip[0]['users']['Tip'], $tip[0]['users']['Smer']));
				} else if (!empty($this -> data)) {
					if ($this -> request -> is('POST')) {
						$vrednost = $this -> request -> data('isci');
						$niz = $this -> request -> data('search');
						if ($niz == "" && $vrednost == null || $niz != "" && $vrednost == null || $niz == "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							//$this->set('user',$this->User->vsiUseriTip($st,$tip[0]['users']['Tip'],$tip[0]['users']['Smer']));
							//$this->set('stevila',$this->User->steviloUserov());

						} else if ($niz != "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							//$this->set('stevila',$this->User->steviloIskanihUserov($vrednost,$niz));
							//$this->set('user',$this->User->iskanjeUporabnikov($vrednost,$niz,$st));
						}
					}
				}
			} else if ($tip[0]['users']['Tip'] == 3) {
				if ($st == NULL) {
						$st = 1;
					}
				//$this -> redirect(array('action' => 'ogled_users'));
				$this -> set('user', $this -> User -> vsiUseriTip($st, $tip[0]['users']['Tip'], $tip[0]['users']['Smer']));
					$this -> set('stevila', $this -> User -> steviloUseriTip($tip[0]['users']['Tip'], $tip[0]['users']['Smer']));
			} else {
				$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function uporabnik($id = NULL){
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$ogled_user = $this -> User -> DobiPodatke($id);
			$this -> set('user', $this -> User -> DobiPodatke($id));
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function ogled_sodelujocih($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$ogled_user = $this -> User -> DobiPodatke($id);
			$podatki = $this -> User -> DobiPodatkeByName($uporabnik);
			if ($uporabnik == $podatki[0]['users']['Uporabnisko']) {
				if ($id == $ogled_user[0]['users']['ID']) {
					if ($ogled_user[0]['users']['Tip'] == $podatki[0]['users']['Tip']) {
						$this -> set('user', $this -> User -> DobiPodatke($id));
					} else {
						$this -> set("error", "Do te strani nemorete dostopati!");
						$this -> render("/Errors/vpis");
					}
				} else {
					$this -> set("error", "Do te strani nemorete dostopati!");
					$this -> render("/Errors/vpis");
				}
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	/*function zbrisi($id=NULL){

	 $this->set('user',$this->User->DobiPodatke($id));
	 $podatki = $this->User->DobiPodatke($id);
	 $slika = WWW_ROOT.'/img/Server/Special/privzetaSlika.png';

	 App::uses('Folder', 'Utility');
	 $folder = new Folder(WWW_ROOT.'/img/'.$podatki[0]['users']['Mapa']);
	 $folder->delete(WWW_ROOT.'/img/'.$podatki[0]['users']['Mapa']);
	 $direktorij = WWW_ROOT.'/img/'.$podatki[0]['users']['Mapa'];
	 $file = new File($slika);

	 if ($file->exists())
	 {
	 $dir = new Folder(WWW_ROOT.'/img/'.$podatki[0]['users']['Mapa'], true);
	 $file->copy($dir->path . DS . $file->name);
	 }
	 $mapa = $podatki[0]['users']['Mapa'];
	 $folder->create($direktorij);
	 $folder->chmod($direktorij, 0700);

	 $this->User->zbrisiSliko($id,'privzetaSlika.png',$mapa,$direktorij);
	 $this->redirect(array('action'=>'ogled',$id));
	 }*/

	//admin functions
	function ogled_users($st = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);

			if ($tip[0]['users']['Tip'] == 3) {
				if (empty($this -> data)) {
					if ($st == NULL) {
						$st = 1;
					}
					$this -> set('user', $this -> User -> vsiUseri($st));
					$this -> set('stevila', $this -> User -> steviloUserov());
					$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
				} else if (!empty($this -> data)) {
					if ($this -> request -> is('POST')) {
						$vrednost = $this -> request -> data('isci');
						$niz = $this -> request -> data('search');
						if ($niz == "" && $vrednost == null || $niz != "" && $vrednost == null || $niz == "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							$this -> set('user', $this -> User -> vsiUseri($st));
							$this -> set('stevila', $this -> User -> steviloUserov());
							$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
						} else if ($niz != "" && $vrednost != null) {
							if ($st == NULL) {
								$st = 1;
							}
							$this -> set('user', $this -> User -> iskanjeUporabnikov($vrednost, $niz, $st));
							$this->set('stevila',$this->User->steviloIskanihUserov($vrednost,$niz,$st));
							$this -> set('podatek', $this -> User -> DobiPodatkeByName($uporabnik));
						}
					}
				}
			} else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function dodaj(){
		if($this->Session->check("uporabnik")){
			$uporabnik=$this->Session->read("uporabnik");
			$tip = $this->User->DobiTip($uporabnik);
			if($tip[0]['users']['Tip'] == 3){	
				if (!empty($this->data)){
				   	if ($this -> request -> is('POST')) {
						$slika = $this -> request -> params['form'];
		
						if (isset($slika)) {
							$poljeTipa = explode("/", $slika['datoteka']['type']);
									
							$ime = $this->request->data('ime');
						    $priimek = $this->request->data('priimek');
							$vpisna = $this->request->data('vpisna');
						    $up_ime = $this->request->data('up_ime');
						    $geslo = $this->request->data('geslo');			
							$user_id = $this->request->data('user_id');
							$smer = $this->request->data('smer');
							$letnik = $this->request->data('letnik');
							$datum = $this->request->data('datum');
						    $u_mail = $this->request->data('u_mail');
						    $u_drzava = $this->request->data('u_drzava');
						    $u_mesto = $this->request->data('u_mesto');
							//$predmeti = $this->request->data('predmeti');
						    $p_dostop = $this->request->data('p_dostop');
						    $z_dostop = $this->request->data('z_dostop');	
							//$vloga = $this->request->data('vloga');				
							if($user_id == 0 || $user_id == 3){
								$smer = "";
								$letnik = "";
							}
							
							if($smer == 0){
								$smer = "RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE";
							}
							
							if($poljeTipa[0] == 'image'){
								if(($slika['datoteka']['size']/1024) < 1000){
									
									$user_name = $this->User->preveriUporabnisko($up_ime);
									$stevilka2 = count($user_name);
									if($stevilka2 == 0)
									{			
										$user_mail = $this->User->preveriMail($u_mail);
										$user_vpisna = $this->User->preveriVpisno($vpisna);
										$stevilka = count($user_mail);
										$stevilka1 = count($user_vpisna);
										
										if($user_id == 0 || $user_id == 3){
											$vpisna = "";
											$stevilka1 = 0;
										}
										
										if($stevilka == 0){
											if($stevilka1 == 0){
												App::uses('Folder', 'Utility');
												$folder = new Folder(WWW_ROOT.'/img/Server/'.$up_ime);
												$folder->delete(WWW_ROOT.'/img/Server/'.$up_ime);
												$direktorij = WWW_ROOT.'/img/Server/'.$up_ime;
												$mapa = 'Server/'.$up_ime;
												$folder->create($direktorij); // Ustvarimo mapo za album, v katerega bomo kasneje shranili slike
												$folder->chmod($direktorij, 0700); // Nastavimo pravice mape
							
												move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT.'/img/Server/'.$up_ime.'/'.$slika['datoteka']['name']);
										
												$this->User->dodajUporabnika($ime,$priimek,$vpisna,$up_ime,md5($geslo),$user_id,$datum,$u_mail,$u_drzava,$u_mesto,$p_dostop,$z_dostop,$slika['datoteka']['name'],$smer,$letnik);
												$this->redirect(array('action'=>'ogled_users')); 
											}
											else {
												$this->Session->setFlash("Neveljavna vpisna številka!");
												$this->redirect(array('action'=>'dodaj'));  
											}
										}
										else {
											$this->Session->setFlash("Neveljavni e-mail!");
											$this->redirect(array('action'=>'dodaj'));  
										}
									}
									else
										{
											$this->Session->setFlash("Uporabniško ime že obstaja!");
											$this->redirect(array('action'=>'dodaj'));  
										}
								}
								else
									{
										$this->Session->setFlash("Velikost slike je prevelika!");
										$this->redirect(array('action'=>'dodaj'));
									}
								}
								else if($poljeTipa[0] == '')
								{
									$ime = $this->request->data('ime');
								    $priimek = $this->request->data('priimek');
									$vpisna = $this->request->data('vpisna');
								    $up_ime = $this->request->data('up_ime');
								    $geslo = $this->request->data('geslo');			
									$user_id = $this->request->data('user_id');
									$smer = $this->request->data('smer');
									$letnik = $this->request->data('letnik');
									$datum = $this->request->data('datum');
								    $u_mail = $this->request->data('u_mail');
								    $u_drzava = $this->request->data('u_drzava');
								    $u_mesto = $this->request->data('u_mesto');
									//$predmeti = $this->request->data('predmeti');
								    $p_dostop = $this->request->data('p_dostop');
								    $z_dostop = $this->request->data('z_dostop');	
									//$vloga = $this->request->data('vloga');				
									
									$user_name = $this->User->preveriUporabnisko($up_ime);
									$stevilka2 = count($user_name);
									if($stevilka2 == 0)
									{										
										$user_mail = $this->User->preveriMail($u_mail);
										$user_vpisna = $this->User->preveriVpisno($vpisna);
										$stevilka = count($user_mail);
										$stevilka1 = count($user_vpisna);
										
										if($user_id == 0 || $user_id == 3){
											$vpisna = "";
											$stevilka1 = 0;
										}
			
										if($stevilka == 0){
											if($stevilka1 == 0){
												$slika = WWW_ROOT.'/img/Server/Special/privzetaSlika.png';			
							
												App::uses('Folder', 'Utility');
												$folder = new Folder(WWW_ROOT.'/img/Server/'.$up_ime);
												$folder->delete(WWW_ROOT.'/img/Server/'.$up_ime);
												$direktorij = WWW_ROOT.'/img/Server/'.$up_ime;
												$file = new File($slika);
										
												if ($file->exists()) 
												{
												    $dir = new Folder(WWW_ROOT.'/img/Server/'.$up_ime, true);
												    $file->copy($dir->path . DS . $file->name);
												}
												$mapa = 'Server/'.$up_ime;
												$folder->create($direktorij); 
												$folder->chmod($direktorij, 0700); 
										
												$this->User->dodajUporabnika($ime,$priimek,$vpisna,$up_ime,md5($geslo),$user_id,$datum,$u_mail,$u_drzava,$u_mesto,$p_dostop,$z_dostop,'privzetaSlika.png',$smer,$letnik);
												$this->redirect(array('action'=>'ogled_users')); 
											}
											else {
												$this->Session->setFlash("Neveljavna vpisna številka!");
												$this->redirect(array('action'=>'dodaj'));
											}
										}
										else {
											$this->Session->setFlash("Neveljavni e-mail!");
											$this->redirect(array('action'=>'dodaj'));
										}
									}
									else
									{
										$this->Session->setFlash("Uporabniško ime že obstaja!");
										$this->redirect(array('action'=>'dodaj'));
									}
								}
								else {
									$this->Session->setFlash("Slika ni v pravilnem formatu!");
									$this->redirect(array('action'=>'dodaj'));
								}
								
							}
							else
							{
								$ime = $this->request->data('ime');
							    $priimek = $this->request->data('priimek');
								$vpisna = $this->request->data('vpisna');
							    $up_ime = $this->request->data('up_ime');
							    $geslo = $this->request->data('geslo');			
								$user_id = $this->request->data('user_id');
								$smer = $this->request->data('smer');
								$letnik = $this->request->data('letnik');
								$datum = $this->request->data('datum');
							    $u_mail = $this->request->data('u_mail');
							    $u_drzava = $this->request->data('u_drzava');
							    $u_mesto = $this->request->data('u_mesto');
							    $p_dostop = $this->request->data('p_dostop');
							    $z_dostop = $this->request->data('z_dostop');				
								
								$user_name = $this->User->preveriUporabnisko($up_ime);
								$stevilka2 = count($user_name);
								if($stevilka2 == 0)
								{									
									$user_mail = $this->User->preveriMail($u_mail);
									$user_vpisna = $this->User->preveriVpisno($vpisna);
									$stevilka = count($user_mail);
									$stevilka1 = count($user_vpisna);
									
									if($user_id == 0 || $user_id == 3){
										$vpisna = "";
										$stevilka1 = 0;
									}
									
									if($stevilka == 0){
										if($stevilka1 == 0){
											$slika = WWW_ROOT.'/img/Server/Special/privzetaSlika.png';			
						
											App::uses('Folder', 'Utility');
											$folder = new Folder(WWW_ROOT.'/img/Server/'.$up_ime);
											$folder->delete(WWW_ROOT.'/img/Server/'.$up_ime);
											$direktorij = WWW_ROOT.'/img/Server/'.$up_ime;
											$file = new File($slika);
									
											if ($file->exists()) 
											{
											    $dir = new Folder(WWW_ROOT.'/img/Server/'.$up_ime, true);
											    $file->copy($dir->path . DS . $file->name);
											}
											$mapa = '/Server/'.$up_ime;
											$folder->create($direktorij); 
											$folder->chmod($direktorij, 0700); 
									
											$this->User->dodajUporabnika($ime,$priimek,$vpisna,$up_ime,md5($geslo),$user_id,$datum,$u_mail,$u_drzava,$u_mesto,$p_dostop,$z_dostop,$slika['datoteka']['name'],$smer,$letnik);
											$this->redirect(array('action'=>'ogled_users'));	
										}
										else {
											$this->Session->setFlash("Neveljavna vpisna številka!");
											$this->redirect(array('action'=>'dodaj'));
										}
									}
									else {
										$this->Session->setFlash("Neveljavni e-mail!");
										$this->redirect(array('action'=>'dodaj'));
										
									}
								}
								else
								{
									$this->Session->setFlash("Uporabniško ime že obstaja!");
									$this->redirect(array('action'=>'dodaj'));
								}
							}		  
					}
				}
			}
			else{
				$this->set("error","Do te strani nemorete dostopati!");
				$this->render("/Errors/vpis"); 
			}
		}
		else{
			$this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this->render("/Errors/vpis"); 
		}
	}
	function uredi($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);

			if ($tip[0]['users']['Tip'] == 3) {
				if (empty($this -> data)) {
					$this -> set('user', $this -> User -> podatkiUsera($id));
				} else {
					if (!empty($this -> data)) {
						if ($this -> request -> is('POST')) {
							$u_id = $this -> request -> data('u_id');
							$ime = $this -> request -> data('uporabnik');
							$slika = $this -> request -> params['form'];

							if (isset($slika)) {
								$poljeTipa = explode("/", $slika['datoteka']['type']);

								$podatki = $this -> User -> DobiPodatke($u_id);
								$u_id = $this -> request -> data('u_id');
								$up_ime = $this -> request -> data('up_ime');
								$vpisna = $this -> request -> data('vpisna');
								//$geslo = $this->request->data('geslo');
								$user_id = $this -> request -> data('user_id');
								$smer = $this -> request -> data('smer');
								$letnik = $this -> request -> data('letnik');
								$ime = $this -> request -> data('ime');
								$priimek = $this -> request -> data('priimek');
								$rojstvo = $this -> request -> data('datumRojstva');
								$mail = $this -> request -> data('u_mail');
								$mesto = $this -> request -> data('u_mesto');
								$drzava = $this -> request -> data('u_drzava');
								$pdostop = $this -> request -> data('p_dostop');
								$zdostop = $this -> request -> data('z_dostop');

								$user_mail = $this -> User -> preveriMail($mail);
								$user_vpisna = $this -> User -> preveriVpisno($vpisna);
								$user_name = $this -> User -> preveriUporabnisko($up_ime);
								$stevilka = count($user_mail);
								$stevilka1 = count($user_vpisna);
								$stevilka2 = count($user_name);
									
								if($user_id == 0 || $user_id == 3){
									$smer = "";
									$letnik = "";
								}
								
								if($user_id == 0 || $user_id == 3){
									$vpisna = "";
									$stevilka1 = 0;
								}
									
									
								if ($smer == 0) {
									$smer = "RAČUNALNIŠTVO IN INFORMACIJSKE TEHNOLOGIJE";
								}

								if ($poljeTipa[0] == 'image' && $u_id != NULL) {
									if (($slika['datoteka']['size'] / 1024) < 1000) {

										if ($stevilka == 0 && $stevilka1 == 0 && $stevilka2 == 0) {
											$user_slika = $this -> User -> podatkiUsera($u_id);

											$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
											$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
												$this -> Session -> write('uporabnik', $up_ime);
											}

											rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 0 && $stevilka2 == 0) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
											$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;

											rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $up_ime . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
												$this -> Session -> write('uporabnik', $up_ime);
											}

											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 0 && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 0) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
											$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
												$this -> Session -> write('uporabnik', $up_ime);
											}

											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 0 && $stevilka1 == 0 && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											//$direktorij1 = WWW_ROOT.'/img/Server/'.$user_slika['0']['users']['Uporabnisko'];
											//$direktorij2 = WWW_ROOT.'/img/Server/'.$up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											//rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 0) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
											$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
												$this -> Session -> write('uporabnik', $up_ime);
											}

											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 0 && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											//$direktorij1 = WWW_ROOT.'/img/Server/'.$user_slika['0']['users']['Uporabnisko'];
											//$direktorij2 = WWW_ROOT.'/img/Server/'.$up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											//rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 0 && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											//$direktorij1 = WWW_ROOT.'/img/Server/'.$user_slika['0']['users']['Uporabnisko'];
											//$direktorij2 = WWW_ROOT.'/img/Server/'.$up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											//rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
											$user_slika = $this -> User -> podatkiUsera($u_id);
											//$direktorij1 = WWW_ROOT.'/img/Server/'.$user_slika['0']['users']['Uporabnisko'];
											//$direktorij2 = WWW_ROOT.'/img/Server/'.$up_ime;
											App::uses('File', 'Utility');
											$file = new File(WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'] . '/' . $user_slika[0]['users']['Slika']);
											$file -> delete();

											//rename($direktorij1, $direktorij2);
											move_uploaded_file($slika['datoteka']['tmp_name'], WWW_ROOT . '/img/Server/' . $up_ime . '/' . $slika['datoteka']['name']);
											//$this -> Session -> write('slika', $slika['datoteka']['name']);
											$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $slika['datoteka']['name']);
											$this -> redirect(array('action' => 'ogled_users'));
										} else {
											if ($stevilka >= 1 && $u_id != $user_mail[0]['users']['ID']) {
												$this -> Session -> setFlash("Neveljavni e-mail!");
												$this -> redirect(array('action' => 'uredi', $u_id));
											}
											if ($stevilka1 >= 1 && $u_id != $user_vpisna[0]['users']['ID']) {
												$this -> Session -> setFlash("Neveljavna vpisna številka!");
												$this -> redirect(array('action' => 'uredi', $u_id));
											}
											if ($stevilka2 >= 1 && $u_id != $user_name[0]['users']['ID']) {
												$this -> Session -> setFlash("Neveljavno uporabniško ime!");
												$this -> redirect(array('action' => 'uredi', $u_id));
											}

										}

									} else {
										$this -> Session -> setFlash("Velikost slike je prevelika!");
										$this -> redirect(array('action' => 'uredi', $u_id));
									}

								} else if ($slika['datoteka']['name'] == "") {
									if ($stevilka == 0 && $stevilka1 == 0 && $stevilka2 == 0) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
										$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;

										rename($direktorij1, $direktorij2);

										if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
											$this -> Session -> write('uporabnik', $up_ime);
										}

										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 0 && $stevilka2 == 0) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
										$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;

										rename($direktorij1, $direktorij2);

										if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
											$this -> Session -> write('uporabnik', $up_ime);
										}

										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 0 && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 0) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
										$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;

										rename($direktorij1, $direktorij2);

										if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
											$this -> Session -> write('uporabnik', $up_ime);
										}

										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 0 && $stevilka1 == 0 && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 0) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$direktorij1 = WWW_ROOT . '/img/Server/' . $user_slika['0']['users']['Uporabnisko'];
										$direktorij2 = WWW_ROOT . '/img/Server/' . $up_ime;

										rename($direktorij1, $direktorij2);

										if ($uporabnik == $user_slika['0']['users']['Uporabnisko']) {
											$this -> Session -> write('uporabnik', $up_ime);
										}

										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 0 && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 0 && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else if ($stevilka == 1 && $u_id == $user_mail[0]['users']['ID'] && $stevilka1 == 1 && $u_id == $user_vpisna[0]['users']['ID'] && $stevilka2 == 1 && $u_id == $user_name[0]['users']['ID']) {
										$user_slika = $this -> User -> podatkiUsera($u_id);
										$this -> User -> posodobiUsera($u_id, $up_ime, $vpisna, $user_id, $ime, $priimek, $rojstvo, $mail, $mesto, $drzava, $pdostop, $zdostop, $smer, $letnik, $user_slika['0']['users']['Slika']);
										$this -> redirect(array('action' => 'ogled_users'));
									} else {
										if ($stevilka >= 1 && $u_id != $user_mail[0]['users']['ID']) {
											$this -> Session -> setFlash("Neveljavni e-mail!");
											$this -> redirect(array('action' => 'uredi', $u_id));
										}
										if ($stevilka1 >= 1 && $u_id != $user_vpisna[0]['users']['ID']) {
											$this -> Session -> setFlash("Neveljavna vpisna številka!");
											$this -> redirect(array('action' => 'uredi', $u_id));
										}
										if ($stevilka2 >= 1 && $u_id != $user_name[0]['users']['ID']) {
											$this -> Session -> setFlash("Neveljavno uporabniško ime!");
											$this -> redirect(array('action' => 'uredi', $u_id));
										}

									}
								} else {
									$this -> Session -> setFlash("Datoteka ni v pravilnem formatu!");
									$this -> redirect(array('action' => 'uredi', $u_id));
								}
							} else {
								echo "Ni v pravilnem formatu!";
							}
						}
					}
				}
			} else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function brisi($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);
			if ($tip[0]['users']['Tip'] == 3) {
				$podatki = $this -> User -> DobiPodatke($id);

				App::uses('Folder', 'Utility');
				$folder = new Folder(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko']);
				if (!empty($podatki[0]['users']['Uporabnisko']))
					$folder -> delete(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko']);
				$this -> User -> brisiUporabnika($id);
				$this -> redirect(array('action' => 'ogled_users'));
			} else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function zbrisiSliko($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);
			if ($tip[0]['users']['Tip'] == 3) {
				$this -> set('user', $this -> User -> DobiPodatke($id));
				$podatki = $this -> User -> DobiPodatke($id);
				$slika = WWW_ROOT . '/img/Server/Special/privzetaSlika.png';

				App::uses('File', 'Utility');
				$file = new File(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko'] . '/' . $podatki[0]['users']['Slika']);
				$file -> delete();

				$file = new File($slika);

				if ($file -> exists()) {
					$dir = new Folder(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko'], true);
					$file -> copy($dir -> path . DS . $file -> name);
				}

				$this -> User -> zbrisiSliko($id, 'privzetaSlika.png');
				$this -> redirect(array('action' => 'ogled_users'));
			} else {
				$u_data = $this -> User -> DobiPodatkeByName($uporabnik);

				if ($id == $u_data[0]['users']['ID']) {
					$this -> set('user', $this -> User -> DobiPodatke($id));
					$podatki = $this -> User -> DobiPodatke($id);
					$slika = WWW_ROOT . '/img/Server/Special/privzetaSlika.png';

					App::uses('File', 'Utility');
					$file = new File(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko'] . '/' . $podatki[0]['users']['Slika']);
					$file -> delete();

					$file = new File($slika);

					if ($file -> exists()) {
						$dir = new Folder(WWW_ROOT . '/img/Server/' . $podatki[0]['users']['Uporabnisko'], true);
						$file -> copy($dir -> path . DS . $file -> name);
					}

					$this -> User -> zbrisiSliko($id, 'privzetaSlika.png');
					$this -> redirect(array('action' => 'ogled', $id));
				} else {
					$this -> set("error", "Do te strani nemorete dostopati!");
					$this -> render("/Errors/vpis");
				}
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

	function index() {
		$this -> redirect(array("action" => "ogled_users"));
	}

	function sprememba_gesla($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$podatki = $this -> User -> DobiPodatkeByName($uporabnik);
			$u_id = $this -> request -> data('u_id');
			if ($id == $podatki[0]['users']['ID'] || $u_id == $podatki[0]['users']['ID']) {
				if (empty($this -> data)) {
					$this -> set('user', $this -> User -> DobiPodatke($id));
					//$this->redirect(array('action'=>'sprememba_gesla',$id));
				} else {
					if (!empty($this -> data)) {
						if ($this -> request -> is('POST')) {
							$u_id = $this -> request -> data('u_id');
							$st_geslo = $this -> request -> data('staro_geslo');
							$tr_geslo = $this -> request -> data('tr_geslo');
							$nv_geslo = $this -> request -> data('novo_geslo');
							$nv2_geslo = $this -> request -> data('novo_geslo2');

							if ($st_geslo != "" && $nv_geslo != "" && $nv2_geslo != "") {

								if (md5($st_geslo) == $tr_geslo) {
									if ($nv_geslo == $nv2_geslo) {
										$this -> User -> spremeniGeslo($u_id, md5($nv_geslo));
										$this -> redirect(array('action' => 'ogled', $u_id));
									} else {
										$this -> Session -> setFlash("Gesli se ne ujemata!");
										$this -> redirect(array('action' => 'sprememba_gesla', $u_id));
									}
								} else {
									$this -> Session -> setFlash("Gesli se ne ujemata!");
									$this -> redirect(array('action' => 'sprememba_gesla', $u_id));
								}
							} else {
								$this -> redirect(array('action' => 'sprememba_gesla', $u_id));
							}
						}
					}

				}
			} else {
				$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati!");
			$this -> render("/Errors/vpis");
		}
	}

	public function uvozi() {
		if ($this -> Session -> check('uporabnik')) {
			$uporabnik = $this -> Session -> read('uporabnik');
			$user_id = $this -> User -> getIdByName($uporabnik);
			//dobimo id uporabnika iz seje
			$user_id = $user_id[0]['u']['id'];
			$tip = $this -> User -> getTip($user_id);
			$tip = $tip[0]['u']['tip'];
			if ($tip == 3) {
				$from_aips = $this -> Aips -> getStudente();
				$vsi = array();
				foreach ($from_aips as $student) {
					list($priimek, $ime) = explode(' ', $student[0]['PriimekIme']);
					$ime = str_replace("Č", "C", $ime);
					$ime = str_replace("Š", "S", $ime);
					$ime = str_replace("Ž", "Z", $ime);
					$ime = str_replace("č", "c", $ime);
					$ime = str_replace("š", "s", $ime);
					$ime = str_replace("ž", "z", $ime);
					$priimek = str_replace("Č", "C", $priimek);
					$priimek = str_replace("Š", "S", $priimek);
					$priimek = str_replace("Ž", "Z", $priimek);
					$priimek = str_replace("č", "c", $priimek);
					$priimek = str_replace("š", "s", $priimek);
					$priimek = str_replace("ž", "z", $priimek);
					$temp1 = substr($priimek, 0, 2);
					$temp2 = substr($student[0]['VpisnaStevilka'], -4);
					$user_name = $ime . $temp1 . $temp2;
					$preveri = $this -> User -> DobiPodatkeByName($user_name);
					if (empty($preveri)) {
						$this -> User -> dodajUporabnikaAips($ime, $priimek, $user_name, md5($student[0]['Geslo']), $student[0]['VpisnaStevilka'], "privzetaSlika.png", $student[0]['NazivPrograma'], $student[0]['LetnikStudijaID']);
						$temp = array("ime" => $ime, "priimek" => $priimek, "vpisna" => $student[0]['VpisnaStevilka'], "user_name" => $user_name);
						$vsi[] = $temp;
						$folder = new Folder();
						if ($folder -> create(WWW_ROOT . '/img/Server/' . $user_name)) {
							$slika = WWW_ROOT . '/img/Server/Special/privzetaSlika.png';
							$file = new File($slika);
							if ($file -> exists()) {
								$dir = new Folder(WWW_ROOT . '/img/Server/' . $user_name, true);
								$file -> copy($dir -> path . DS . $file -> name);
							}

						}
					}
				}
				$this -> set("rez", $vsi);

			}
		}
	}

	function poenostgesla($id = NULL) {
		if ($this -> Session -> check("uporabnik")) {
			$uporabnik = $this -> Session -> read("uporabnik");
			$tip = $this -> User -> DobiTip($uporabnik);
			if ($tip[0]['users']['Tip'] == 3) {
				if (!empty($id)) {
					$this -> User -> spremeniGeslo($id, md5("test12345"));
					$this -> redirect(array('action' => 'uredi', $id));
				}
			} else {
				$this -> set("error", "Do te strani nemorete dostopati!");
				$this -> render("/Errors/vpis");
			}
		} else {
			$this -> set("error", "Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
			$this -> render("/Errors/vpis");
		}
	}

}
