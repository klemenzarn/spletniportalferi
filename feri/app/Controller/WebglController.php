<?php
App::import('Vendor','nusoap');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class WebglController extends AppController {
	var $layout = false;
	public $components = array('RequestHandler');
	public $uses = array('Oglas','Novice','User','Urniki','Mojurnik','Koledar','Dogodki','User');

	public function index(){
		//$rezultat = $this->User->getActiveUsers();
		$rezultat = $this->User->getOtherActiveUsers($this->Session->read('uporabnik'));
		$output = json_encode($rezultat);
		$this->set("uporabniki",$output);
		/*$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl' , false, false, false, false, 0, 300);
		$args = array('user_name' => 'klemenkeko');
		$result = $client->call('getVseSlike',array($args));
		//$this->printR($result);
		$result = $result['getVseSlikeResult']['string'];
		
		$this->set('slike',$result);*/
	}

	public function test(){
		$rezultat = $this->User->getOtherActiveUsers($this->Session->read('uporabnik'));
		$this->set("test",$rezultat);

	}

	public function galerijaDCT(){

 	}

	public function brisi($id){
		if($this->Session->check('uporabnik')){
			$pot = 'img/Server/'.$this->Session->read('uporabnik').'/dct/';
			$bin=explode('.',$id);
			$file = new File(WWW_ROOT . $pot.$id, false, 0777);
			$file2 = new File(WWW_ROOT . $pot.$bin[0].".bin", false, 0777);
			$file->delete();
			$file2->delete();
			$this->redirect('galerijadct');
		}
	}

	public function printR($msg){
		echo "<pre>";
		print_r($msg);
		echo "</pre>";
	}

	public function oglasna($stran = 1) {
		//za vsako oglasno desko svojo mapo
		$query = $this -> Oglas -> getAll($stran);
		$this -> set("deska", $query);
		$datoteke = $this -> Oglas -> getVseDatoteke();
		$this -> set("datoteke", $datoteke);
		$stevilo = $this -> Oglas -> getStOglas();
		$this -> set("stevilo_oglasov", $stevilo);
	}


	public function novice(){
		$novice = $this->Novice->getNoviceStran(1);
		//$this->printR($novice);
		$this->set("novice",$novice);
	}

	public function koledar(){
		
	}

	public function updatePosition(){
		if($this->Session->check('uporabnik')){
			$x = $this->request->data('x');
			$z = $this->request->data('z');
			$this->User->updatePosition($this->Session->read('uporabnik'), $x, $z);
			$rezultat = $this->User->getOtherActiveUsers($this->Session->read('uporabnik'));
			$output = json_encode($rezultat);
			$this->set("msg",$output);
			$this->render('ajax_output','ajax');
		}
	}

	public function shraniSliko(){
		/*if ( isset($_POST["image"]) && !empty($_POST["image"]) ) {    
		    // Init dataURL variable
		    $dataURL = $_POST["image"];  
		    $dataURL=str_replace(" ", "+", $dataURL);
		    // Extract base64 data (Get rid from the MIME & Data Type)
		    $parts = explode(',', $dataURL);  
		    $data = $parts[1];  
		    // Decode Base64 data
		    $data = base64_decode($data);  
		    // Save data as an image
		    $fp = fopen('delejkurba1.jpg', 'w');  
		    fwrite($fp, $data);  
		    fclose($fp); 

		    $fp = fopen('davidmo.txt', 'w');  
		    fwrite($fp, $dataURL);  
		    fclose($fp); 
		}*/
		if($this->Session->check('uporabnik')){
			if($this->request->is('post')){
				$uporabnik = $this->Session->read('uporabnik');
				$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
				$slika = $this->request->data('image');
				$slika= str_replace(" ", "+", $slika);
				$args = array('user_name' => $uporabnik,'base64'=>$slika);
				$result = $client->call('ShraniSliko', array($args));
				$slika = '<img src="'.$slika.'" />';
				$this->set("msg",$result);
				$this->render('ajax_output','ajax');
			}
		}else {
			$this->set("msg",'Uporabnik ni prijavljen!');
			$this->render('ajax_output','ajax');
		}
	}

	public function getvse(){
		if($this->Session->check('uporabnik')){
			$uporabnik = $this->Session->read('uporabnik');
			$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl' , false, false, false, false, 0, 500);
			$args = array('user_name' => $uporabnik);
			$result = $client->call('getVseSlike',array($args));
			$temp = $result['getVseSlikeResult'];
			if($temp == null){
				$this->set('slike',false);
			} else {
				$result = $result['getVseSlikeResult']['string'];
				$this->set('slike',$result);
			}
				
			
		} else {
			
		}
	}

	public function prijava(){

	}

	public function odjava(){
    	$this->User->setNoActive($this->Session->read('uporabnik'));
		$this->Session->delete('uporabnik');
		$this->Session->delete('slika');
        $this->redirect('index');
	}


	public function registracija(){
		$ime=$this->request->data('name');
		$priimek=$this->request->data('surname');
		$uporabnisko=$this->request->data('username');
		$geslo=$this->request->data('pas1');
		$pwdlen=strlen($geslo);
		$ponovno_geslo=$this->request->data('pas2');
		$mail=$this->request->data('mail');
		$vpisna="";
		if( empty($ime)|| empty($priimek)|| empty($uporabnisko)|| empty($geslo)|| empty($mail))
		{
			//$this->Session->setFlash('Niste vnesli vseh podatkov');
			$this->set('msg',"Niste vnesli vseh podatkov!");
			$this->render('prijava1','ajax');
		}
		else
		{
			if($pwdlen<6)
			{
				$this->set('msg',"Vnesite geslo dolžine vsaj 6 znakov!");
				$this->render('prijava1','ajax');
			}
			else if($ponovno_geslo!=$geslo)
			{
					//$this->Session->setFlash('Gesli se ne ujemata');
					$this->set('msg',"Gesli se ne ujemata!");
					$this->render('prijava1','ajax');
			}
			else
			{
			//regex za mail svašta :D
				$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
				if (preg_match($pattern, $mail) === 1) {
					//preveri še če uporabnik z tem uporabniškim že obstaja
					$query=$this->User->getIdByName($uporabnisko);
					if(empty($query))
					{
						$hash_geslo= md5($geslo);
						$this->User->InsertUporabnik($ime,$priimek,$uporabnisko,$hash_geslo,$mail);
						//$this->redirect(array("controller"=>"index","action"=>"index"));
						$folder = new Folder();
						if ($folder->create(WWW_ROOT.'/img/Server/'.$uporabnisko)) {
						    $slika = WWW_ROOT.'/img/Server/Special/privzetaSlika.png';		
							$file = new File($slika);
							if ($file->exists()) 
							{
							    $dir = new Folder(WWW_ROOT.'/img/Server/'.$uporabnisko, true);
							    $file->copy($dir->path . DS . $file->name);
							}
							
						}
						$this->set('msg',"OK");
						$this->render('prijava1','ajax');
						//$this->redirect(array("controller"=>"webgl","action"=>"index"));
					}
					else
					{
						$this->set('msg',"Uporabnik z tem uporabniškim imenom že obstaja!");
						$this->render('prijava1','ajax');
					}
				}
				else {
				$this->set('msg',"Vnesli ste nepopoln oz. napačen e-mail naslov!");
				$this->render('prijava1','ajax');
				}
			}
		}
	}

	public function login(){
		$uporabnisko=$this->request->data('user');
		$geslo=$this->request->data('pas');
		
		if(!empty($uporabnisko) && !empty($geslo))
		{
			$query=$this->User->DobiPodatke1($uporabnisko);
			if($query==null)
			{
				$this->set('msg',"Uporabniško ime ne obstaja!");
				$this->render('prijava1','ajax');
			}
			else if(($query!=null) &&($query[0]['users']['Geslo']!=md5($geslo)))
			{
				$this->set('msg',"Napačno geslo");
				$this->render('prijava1','ajax');
			}
			else if ($query!=null && ($query[0]['users']['Geslo']==md5($geslo)))
			{
				$obstaja = $this->User->preveriRazporeditev($query[0]['users']['ID']);
				if(empty($obstaja)){
					$obstaja = $this->User->narediRazporeditve($query[0]['users']['ID']);
				}
			 	$this->Session->write('uporabnik',$uporabnisko);
				$this->Session->write('slika',$query[0]['users']['Slika']);
				$this->Session->write('tip',$query[0]['users']['Tip']);
				//$uporabnik111=$this->Session->read("uporabnik");
				$this->User->UpdateCasPrijave($uporabnisko);
				$this->set('msg',$_SERVER['HTTP_REFERER']);
				$this->redirect('index');
			}
		}
		else
		{
			$this->set('msg',"Vnesite uporabniško ime in geslo");
			$this->render('prijava1','ajax');
		}
	}

	public function cpu() {
		$zadnji = $this -> User -> dobiCPU();
		$this -> set("cpu", $zadnji);
	}

	public function cpu2() {
		$zadnji = $this -> User -> dobiCPU();
		$this -> set("msg", json_encode($zadnji));
		$this -> render("ajax_output2", 'ajax');
	}

}