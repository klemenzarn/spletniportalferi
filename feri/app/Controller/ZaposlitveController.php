<?php
App::import('Vendor','nusoap');
class ZaposlitveController extends AppController{
	
	public function index($id=1) {
		if ($this -> request -> is('post')) {
			$podrocje=$this->request->data('select1');
			$regija=$this->request->data('select2');
			$izobrazba=$this->request->data('select3');
			$client  = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
			$client->soap_defencoding = 'UTF-8';
			$client->decode_utf8 = false;
			$param = array('regija' => $regija,
							'podrocje' =>$podrocje,
							'zaht_izobrazba'=>$izobrazba);	
			$result = $client->call('DobiZaPodrocjeRegijoIzobrazbo', array('parameters' =>$param), '', '', false, true);
			
			if ($client->fault) {
				$this->set('message', $result);
			} else {
				$err = $client->getError();
				if ($err) {
					$this->set('message', $err);
				} else {
					$this->set('data', $result);
				}
			}
		}
		else {
			$podatki=$this->request->data('podatki');
			for($i=0;$i<$id*10;$i++)
			{
				print_r($podatki);
			}
		}
	}
}
	