<?php
App::import('Vendor','nusoap');
class DrugeNoviceController extends AppController{
	var $uses = array('Drugenovice');
	
	public function index($id = 1){
		$novice = $this->Drugenovice->getNoviceStran($id); //dobimo novice glede na stran
		$this->set("novice",$novice);
        $velikost_novic = $this->Drugenovice->getStNovic(); //za paging
        $this->set("stnovic",$velikost_novic);
	}

	public function uvozidrugenovice(){
		$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
		//$client->soap_defencoding = 'UTF-8'; 
		$client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;		
		$result = $client->call('parse');
		$rezultat = $result['parseResult']['string'];
		$st_novic = 6;
		for($i = 0;$i < $st_novic; $i++){
			$naslov = $rezultat[$i];
			$link = $rezultat[$i+6];
			$vsebina = $rezultat[$i+12];
			$preveri = $this->Drugenovice->preveriNovico($naslov,$link,$vsebina);
			if(empty($preveri))
				$this->Drugenovice->dodajNovico($naslov,$link,$vsebina,"Rac-novice");
		}
		for($i = $st_novic*3;$i < $st_novic*3+$st_novic; $i++){
			$naslov = $rezultat[$i];
			$link = $rezultat[$i+6];
			$vsebina = $rezultat[$i+12];
			$preveri = $this->Drugenovice->preveriNovico($naslov,$link,$vsebina);
			if(empty($preveri))
				$this->Drugenovice->dodajNovico($naslov,$link,$vsebina,"Slo-tech");
		}
		for($i = $st_novic*6;$i < $st_novic*6+$st_novic; $i++){
			$naslov = $rezultat[$i];
			$link = $rezultat[$i+6];
			$vsebina = $rezultat[$i+12];
			$preveri = $this->Drugenovice->preveriNovico($naslov,$link,$vsebina);
			if(empty($preveri))
				$this->Drugenovice->dodajNovico($naslov,$link,$vsebina,"24-ur");
		}
		$this->set("result",$rezultat);
		$this->render("index");
		//$this->redirect(array("controller"=>"index","action"=>"index"));
	}

	public function isci() {
		if($this->request->is('get')){
			$niz = $this->request->query['niz'];
			$vir = $this->request->query['vir'];
			if($niz == null){
				$niz = "vse";
			}
			$client = new nusoap_client('http://localhost:55457/Service1.asmx?wsdl', 'wsdl');
			$client->soap_defencoding = 'UTF-8';
			$client->decode_utf8 = false;
			$param = array('vir' => $vir, 'niz'=>$niz);
			$rezultat = $client -> call('isci', array('parameters' => $param, ",", false, true));
			$this->set("novica",$rezultat['isciResult']['string']);
		} else {
			$this->set("error","Ta stran ne obstaja ali nimate pravic za to opravilo!");
			$this->render("/Errors/vpis");
		}
	}

}
