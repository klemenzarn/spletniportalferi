<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('Sanitize', 'Utility');
class NoviceController extends AppController{
	var $uses = array('Novice','User');
	
	function index($id = 1){
		$novice = $this->Novice->getNoviceStran($id); //dobimo novice glede na stran
		$this->set("novice",$novice);
        $velikost_novic = $this->Novice->getStNovic(); //za paging
        $this->set("stnovic",$velikost_novic);
	}
	
	//funkcija, ki išče oglase glede na iskalni niz.
	public function isci() {
		//$niz = $this->request->data('niz');  //post
		$niz = $this -> request -> query['niz'];
		// get
		$query = $this -> Novice -> isci($niz);
		if ($query != null) {
			$this -> set("novice", $query);
			$this -> render("index");
		} else {
			$this -> set("novice", $query);
			$this->Session->setFlash("Ni zadetkov");
			$this -> render("index");
		}
	}
	
	function poglej(){
        if($this->request->is('get')){
            $id = $this->request->query['id']; //dobimo iz ?id=4 kter id novice želimo
            if($id != null){
                $novice = $this->Novice->getById($id);
                if($novice != null){
                    $this->set("novice", $novice);
                }else{
                    $this->Session->setFlash('Ne obstaja');
                    $this->render("error");
                }
            } else {
                $this->Session->setFlash('Ne obstaja');
                $this->render("error");
            }
        }
	}

	private function resizeDodaj($slika,$zadnji_id) {
		$dir = new Folder(WWW_ROOT .'/files/Novice/'. $zadnji_id[0][0]['zadnji'], true, 0755);

		
		$size = getimagesize($slika['naslovna_slika']["tmp_name"]);
		$w = $size[0];
		$h = $size[1];
		$nova_visina = 500 / $w;
		$nova_visina = intval($nova_visina * $h);
		//shranimo si atribute slike
		$postvars = array(
		"image_tmp"    => $slika['naslovna_slika']["tmp_name"],
		"sirina"    => 500,
		"visina"   => $nova_visina
		);
		
		$ext = explode(".", $slika['naslovna_slika']["name"]);
		$ext = $ext[count($ext) - 1];
		
		if($ext == "jpg" || $ext == "jpeg"){
		$image = imagecreatefromjpeg($postvars["image_tmp"]);
		}
		else if($ext == "gif"){
		$image = imagecreatefromgif($postvars["image_tmp"]);
		}
		else if($ext == "png"){
		$image = imagecreatefrompng($postvars["image_tmp"]);
		}
		// Dobimo velikost slike.
		list($width,$height) = getimagesize($postvars["image_tmp"]);
		
		//nova sirina in visina
		$newwidth = $postvars["sirina"];
		$newheight = $postvars["visina"];

		//ustvarimo začasen file
		$tmp = imagecreatetruecolor($newwidth,$newheight);
		imagecopyresampled($tmp,$image,0,0,0,0,$newwidth,$newheight,$width,$height);
		// ustvarimo sliko z 100% kvaliteto
		imagejpeg($tmp,WWW_ROOT .'/files/Novice/'.$zadnji_id[0][0]['zadnji'].'/'.$slika['naslovna_slika']['name'],100);
		imagedestroy($image);
		imagedestroy($tmp);
		}
    
    private function resizeUredi($slika,$zadnji_id) {
		$dir = new Folder(WWW_ROOT .'/files/Novice/'. $zadnji_id[0][0]['zadnji'], true, 0755);

		
		$size = getimagesize($slika['naslovna_slika']["tmp_name"]);
		$w = $size[0];
		$h = $size[1];
		$nova_visina = 500 / $w;
		$nova_visina = intval($nova_visina * $h);
		//shranimo si atribute slike
		$postvars = array(
		"image_tmp"    => $slika['naslovna_slika']["tmp_name"],
		"sirina"    => 500,
		"visina"   => $nova_visina
		);
		
		$ext = explode(".", $slika['naslovna_slika']["name"]);
		$ext = $ext[count($ext) - 1];
		
		if($ext == "jpg" || $ext == "jpeg"){
		$image = imagecreatefromjpeg($postvars["image_tmp"]);
		}
		else if($ext == "gif"){
		$image = imagecreatefromgif($postvars["image_tmp"]);
		}
		else if($ext == "png"){
		$image = imagecreatefrompng($postvars["image_tmp"]);
		}
		// Dobimo velikost slike.
		list($width,$height) = getimagesize($postvars["image_tmp"]);
		
		//nova sirina in visina
		$newwidth = $postvars["sirina"];
		$newheight = $postvars["visina"];

		//ustvarimo začasen file
		$tmp = imagecreatetruecolor($newwidth,$newheight);
		imagecopyresampled($tmp,$image,0,0,0,0,$newwidth,$newheight,$width,$height);
		// ustvarimo sliko z 100% kvaliteto
		imagejpeg($tmp,WWW_ROOT .'/files/Novice/'.$zadnji_id.'/'.$slika['naslovna_slika']['name'],100);
		imagedestroy($image);
		imagedestroy($tmp);
		}
	
    public function dodajnovico() {
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		if($this->Session->check("uporabnik")==FALSE)
		{
			$this->set("error","Trenutno niste prijavljeni!");
			$this->render("/Errors/vpis");
		}
		else {
			$tip=$this->requestAction('/Index/DobiTipUporabnika');
			if($tip[0]['users']['Tip']==3 || $tip[0]['users']['Tip']==2)
			{
					if ($this->request->is('post')) {
							$slika = $this->request -> params['form'];
							$naslov = $this->request->data('naslov_novice_dodaj');
							$vsebina = $this->request->data('tiny_dodaj');
							$avtor = $this->Session->read("uporabnik");
							$ime = $slika['naslovna_slika']['name'];
							$tmpSlike = $slika['naslovna_slika']['tmp_name'];
							$velikost = $slika['naslovna_slika']['size'];
							if( empty($naslov)|| empty($vsebina)|| empty($ime))
							{
									$this->Session->setFlash('Niste vnesli vseh podatkov!');
							}
							else {
								$tip = explode(".", $ime);
								$tip = $tip[count($tip) - 1];
								if($tip=="jpg"||$tip=="gif"||$tip=="png"||$tip=="jpeg"){
									if($velikost<8388608)
									{
										$size = getimagesize($tmpSlike);
										$w = $size[0];
										$h = $size[1];
										if($w<500 && $h<300)
										{
										$this->Novice->DodajNovico($naslov, Sanitize::escape($vsebina),$avtor,$ime);
										$zadnji_id=$this->Novice->DobiZadnji();
										$dir = new Folder(WWW_ROOT .'/files/Novice/'. $zadnji_id[0][0]['zadnji'], true, 0755);
										move_uploaded_file($tmpSlike, WWW_ROOT . '/files/Novice/' . $zadnji_id[0][0]['zadnji'] .'/'. $ime);
										$this->redirect('/novice');
										}
										else {
											//resize
											$this->Novice->DodajNovico($naslov, Sanitize::escape($vsebina),$avtor,$ime);
											$zadnji_id=$this->Novice->DobiZadnji();
											$this->resizeDodaj($slika,$zadnji_id);
											$this->redirect('/novice');
										}
									}
									else{
										$this->Session->SetFlash('Prekoračili ste dovoljeno velikost datoteke!');
									}
								}
								else {
									$this->Session->SetFlash('Dovoljene so le datoteke tipa jpg,gif in png!');
								}
							}
						}
				}
				else{
						$this->set("error","Za to opravilo niste avtorizirani");
						$this->render("/Errors/vpis");
					}
			}
	}
    
    public function dodaj(){
        if($this->Session->check("uporabnik")){
            $tip=$this->User->DobiTip($this->Session->read("uporabnik"));
            if($tip[0]['users']['Tip']==3)
            {
                $naslov=$this->request->data('naslov_novice');
                $vsebina=$this->request->data('tiny');
                $slika=$this->request->data('naslovna_slika');
                
                if( empty($naslov)&& empty($vsebina)&& empty($slika))
                {
                        $this->Session->setFlash('Niste vnesli vseh podatkov');
                }
            } else {
                $this->set("error","Nimate pravic za dostop do te strani.");
                $this->render("/Errors/vpis");   
            }
        } else {
            $this->set("error","Do te strani nemorete dostopati če niste prijavljeni! Najprej se prijavite.");
            $this->render("/Errors/vpis");   
        }
    }
    
    public function brisi($id) {
        App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$uporabnik=$this->Session->read("uporabnik");
		$tip=$this->Novice->DobiTip($uporabnik);
		if(empty($tip))
		{
			$this->set("error","Trenutno niste prijavljeni!");
			$this->render("/Errors/vpis");
		}
		else {
			if($tip[0]['users']['Tip']==3) //admin
			{
				$this->Novice->zbrisiNovico($id);
				$folder = new Folder();
				$folder->delete(WWW_ROOT . '/files/Novice/'.$id.'/'); 
				$this->redirect('uredi');
			}
			else if($tip[0]['users']['Tip']==2)
			{
				$je=0;
				$vse_novice_moderatorja=$this->Novice->getNoviceModerator($uporabnik);
				for($i=0;$i<count($vse_novice_moderatorja);$i++)
				{
					if($vse_novice_moderatorja[$i]['n']['id']==$id)
						$je=1;
				}
				if($je==1)
				{
					$this->Novice->zbrisiNovico($id);
					$folder = new Folder();
					$folder->delete(WWW_ROOT . '/files/Novice/'.$id.'/'); 
					$this->redirect('uredi');
				}
				else
				{
					$this->set("error","Za to opravilo niste avtorizirani");
					$this->render("/Errors/vpis");
				}
			}
			else {
				$this->set("error","Za to opravilo niste avtorizirani");
				$this->render("/Errors/vpis");
			}
		}
	}
	
    function uredi($id = 1){
        $uporabnik=$this->Session->read("uporabnik");
		$tip=$this->Novice->DobiTip($uporabnik);
		if(empty($tip))
		{
			$this->set("error","Trenutno niste prijavljeni!");
			$this->render("/Errors/vpis");
		}
		else {
			if($tip[0]['users']['Tip']==3) //admin
			{
			$novice = $this->Novice->getNoviceStran($id);
			$this->set("novice",$novice);
	        $velikost_novic = $this->Novice->getStNovic();
	        $this->set("stnovic",$velikost_novic);
			}
			else if($tip[0]['users']['Tip']==2) //moderator
			{
			$uporabnik=$this->Session->read("uporabnik");
			$novice = $this->Novice->getNoviceStranModerator($id,$uporabnik);
			$this->set("novice",$novice);
	        $velikost_novic = $this->Novice->getStNovicModerator($uporabnik);
	        $this->set("stnovic",$velikost_novic);
			}
			else {
				$this->set("error","Za to opravilo niste avtorizirani");
				$this->render("/Errors/vpis");
			}
		}
	}
    
    function uredinovico($id_novice){
 		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		
		if(empty($this->data))
		{
			$uporabnik=$this->Session->read("uporabnik");
			$tip=$this->Novice->DobiTip($uporabnik);
			if(empty($tip))
			{
				$this->set("error","Trenutno niste prijavljeni!");
				$this->render("/Errors/vpis");
			}
			else {
				if($tip[0]['users']['Tip']==3) //admin
				{
				$novica=$this->Novice->getById($id_novice);
				$this->set("novica",$novica);
				}
				else if($tip[0]['users']['Tip']==2)
				{
					$je=0;
					$vse_novice_moderatorja=$this->Novice->getNoviceModerator($uporabnik);
					for($i=0;$i<count($vse_novice_moderatorja);$i++)
					{
						if($vse_novice_moderatorja[$i]['n']['id']==$id_novice)
							$je=1;
					}
					if($je==1)
					{
						$novica=$this->Novice->getById($id_novice);
						$this->set("novica",$novica);
					}
					else {
						$this->set("error","Za to opravilo niste avtorizirani");
						$this->render("/Errors/vpis");
					}
				}
			}
		}
		else {
			$id=$this->request->data('id_novice');
			$naslov=$this->request->data('naslov_novice');
			$vsebina=$this->request->data('tiny');
			$naslovna_slika=$this->request -> params['form'];
			$avtor=$this->Session->read("uporabnik");
			if(empty($naslov) || empty($vsebina))
			{
				$novica=$this->Novice->getById($id);
				$this->set("novica",$novica);
				$this->Session->SetFlash('Niste vnesli vseh podatkov!');
			}
			else {
					if(empty($naslovna_slika['name']))
					{
					$this->Novice->PosodobiNovico($id,$naslov, Sanitize::escape($vsebina),$avtor);
					$this->redirect('uredi');
					}
					else {
						$imeDatoteke = $naslovna_slika['naslovna_slika']['name'];
						$tmpDatoteke = $naslovna_slika['naslovna_slika']['tmp_name'];
						$velikost = $naslovna_slika['naslovna_slika']['size'];
						
						$tip = explode(".", $imeDatoteke);
						$tip = $tip[count($tip) - 1];
						if($tip=="jpg"||$tip=="gif"||$tip=="png"||$tip=="jpeg")
						{
							if($velikost<8388608)
							{
								$size = getimagesize($tmpDatoteke);
								$w = $size[0];
								$h = $size[1];
								if($w<500 && $h<300)
								{
									$files = glob(WWW_ROOT . '/files/Novice/' . $id_novice. '/*');
										foreach($files as $file){ 
										  if(is_file($file))
										    unlink($file); 
									}
									
									move_uploaded_file($tmpDatoteke, WWW_ROOT . '/files/Novice/' . $id_novice. '/' . $imeDatoteke);
									$this->Novice->PosodobiNovico1($id,$naslov,$vsebina,$avtor,$imeDatoteke);
									$this->redirect('uredi');
								}
								else{
									//resize
									$files = glob(WWW_ROOT . '/files/Novice/' . $id_novice. '/*');
										foreach($files as $file){ 
										  if(is_file($file))
										    unlink($file);
										
										$this->Novice->PosodobiNovico1($id,$naslov, Sanitize::escape($vsebina),$avtor,$imeDatoteke);
										$this->resizeUredi($naslovna_slika,$id_novice);
										$this->redirect('uredi');
										} 
								}
							}
							else{
								$novica=$this->Novice->getById($id);
								$this->set("novica",$novica);
								$this->Session->SetFlash('Prekoračili ste dovoljeno velikost datoteke!');
							}
						}
						else {
							$novica=$this->Novice->getById($id);
							$this->set("novica",$novica);
							$this->Session->SetFlash('Dovoljene so le datoteke tipa jpg,gif in png!');
						}
					}
				}
		}
    }
}