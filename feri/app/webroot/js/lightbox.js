$(document).ready(function() {
	function lightbox(link) {
		lightboxImage = '<div class="lightbox-frame"><div class="lightbox-zapri"></div><div class="lightbox-naslednji" /><div class="lightbox-prejsnji" /><div class="lightbox-content"><img src="' + link + '" width="600px"/></div></div>';
		return lightboxImage;
	}
	
	$("#tv-spored tr").click(function() {
		index = $(this).index();
		index = "." + index;
		if ($(index).is(":hidden")) {
			$(index).slideDown("slow");
			$(index).css("background-color", "white");
			$(this).css("background-color", "#3c9dd0");
		} else {
			$(index).slideUp("slow");
			$(this).css("background-color", "");
		}
	});

	$('#izberi-program').click(function() {

	});

	$("#ura").on({
		'keyup' : function(e) {
			if (e.keyCode != 27) {
				$('.napaka').remove();
				if ($('#ura').val() != "") {
					if ($.isNumeric($('#ura').val()) == false) {
						$("#iskanje-oddaje").append('<div class="napaka">Napaka!</div>');
					}
				}
			}
		}
	});

/*
 id, naziv_oddaje, zacetek_oddaje, program_id
 id,naziv_programa, url_programa*/

	function naslednji() {
		if ($("#galer img").hasClass('naslednja')) {
			$('.lightbox-frame').remove();
			//Zapremo sliko oz. okvir

			naslednja = $('.naslednja').attr('src');
			//Poiščemo linke naslednjic in predhodnic ...
			naslednja = naslednja.replace('thumbnails', 'images');

			$('.prejsnja').removeClass('prejsnja');
			$('.trenutna').removeClass('trenutna').addClass('prejsnja');
			$('.naslednja').removeClass('naslednja').addClass('trenutna');
			$('.trenutna').next().addClass('naslednja');

			$('#change').append(lightbox(naslednja));

			$('.lightbox-frame').css({
				"position" : "absolute",
				"top" : "10%",
				"left" : "35%"
			});

		} else {
			$('.lightbox-frame').remove();
			$('.lightbox-background').remove();
			location.reload();
		}
	}

	function prejsnji() {

		//Zapremo sliko oz. okvir
		if ($("#galer img").hasClass('prejsnja')) {
			$('.lightbox-frame').remove();
			prejsnja = $('.prejsnja').attr('src');
			//Poiščemo linke naslednjic in predhodnic ...
			prejsnja = prejsnja.replace('thumbnails', 'images');

			$('.naslednja').removeClass('naslednja');
			$('.trenutna').removeClass('trenutna').addClass('naslednja');
			$('.prejsnja').removeClass('prejsnja').addClass('trenutna');
			$('.trenutna').prev().addClass('prejsnja');

			$('#change').append(lightbox(prejsnja));

			$('.lightbox-frame').css({
				"position" : "absolute",
				"top" : "10%",
				"left" : "35%"
			});

		} else {
			$('.lightbox-frame').remove();
			$('.lightbox-background').remove();
			location.reload();
		}
	}


	$('#galer img').click(function() {
		$(this).addClass('trenutna');
		$(this).next().addClass('naslednja');
		$(this).prev().addClass('prejsnja');

		linkSlike = $(this).attr("src");
		linkSlike = linkSlike.replace('thumbnails', 'images');

		$('#change').append('<div class="lightbox-background"></div>');

		$('#change').append(lightbox(linkSlike));

		$('.lightbox-frame').css({
			"position" : "absolute",
			"top" : "10%",
			"left" : "35%"
		});

		var odmik = $('.lightbox-background').height();
		odmik = (0.1 * odmik);
		//ODMIK ZA 5% TOP
		window.scrollTo(0, $('.lightbox-frame').position().top - odmik);

		$(document).on({
			'keyup' : function(e) {

				if (e.keyCode == 27) {
					$('body').css({
						"overflow-y" : "visible",
						"overflow-x" : "visible"
					});
					$('.lightbox-background').remove();
					$('.lightbox-frame').remove();
					location.reload();
				}

				if (e.keyCode == 37) {
					if ($('#change div').hasClass('lightbox-frame')) {
						prejsnji();
					}
				}

				if (e.keyCode == 39) {
					if ($('#change div').hasClass('lightbox-frame')) {
						naslednji();
					}

				}
			},
			'click' : function() {
				$(".lightbox-naslednji").click(function() {
					naslednji();
				});

				$(".lightbox-prejsnji").click(function() {
					prejsnji();
				});

				$('.lightbox-zapri, .lightbox-background').click(function() {
					$('.lightbox-background').remove();
					$('.lightbox-frame').remove();
					location.reload();
				});

			}
		});
	});

	//http://onwebdev.blogspot.com/2011/04/jquery-lightbox-tutorial.html

	//BRISANJE VEČIH SLIK NA ENKRAT - IZBERE VSE SLIKE
	$('#izberi-vse').click(function() {
		if ($(this).is(':checked')) {
			$("input[type='checkbox']").each(function() {
				this.checked = true;

			});
		} else {
			$(':checkbox').each(function() {
				this.checked = false;
			});
		}
	});
});

