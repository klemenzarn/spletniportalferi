<?php
		echo $this->Html->script('colorpicker');
		echo $this->Html->css('colorpicker');
?>
<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<div id='klepet'>
		<div id='vsebina_klepeta'></div>
		<textarea id='vsebina_poslji' placeholder="Vpiši sporočilo"></textarea>
		<div id="color_picker"></div>
		<input type='button' id='poslji_klepet' value='Pošlji sporočilo!' />
	</div>

</div>
<script type="text/javascript">
			$(document).ready(function(){
			barva = "#000";
				$('#color_picker').ColorPicker({
				color: '#000000',
				onShow: function (colpkr) {
					$(colpkr).fadeIn(500);
					return false;
				},
				onHide: function (colpkr) {
					$(colpkr).fadeOut(500);
					return false;
				},
				onChange: function (hsb, hex, rgb) {
					$('#color_picker').css('backgroundColor', '#' + hex);
					barva = '#' + hex;
			}
			});
				
				//164.8.212.106
				var conn = new WebSocket('ws://localhost:8000');
				conn.onopen = function(e) {
					
				};
				
				conn.onerror = function(e){
					alert(e.data);
				};
				
				conn.onmessage = function(e) {
					$("#vsebina_klepeta").append(e.data);
				};
				
				 $("#vsebina_poslji").on({
     'keyup' : function(e) {
      if (e.keyCode == 13) {
       var ime = "<?php echo $this->Session->read('uporabnik');  ?>";
       var color = barva;
       var output = "<span style='font-weight:bold;'>"+ ime + ":</span> " +"<span style='color:" + color + ";'>"+$("#vsebina_poslji").val()+"</span><br />";
       conn.send(output);
       $("#vsebina_klepeta").append(output);
       $("#vsebina_poslji").val("");
      }
     }
     
    });
				
				$("#poslji_klepet").click(function(){
					var ime = "<?php echo $this->Session->read('uporabnik');  ?>";
					var color = barva;
					var output = "<span style='font-weight:bold;'>"+ ime + ":</span> " +"<span style='color:" + color + ";'>"+$("#vsebina_poslji").val()+"</span><br />";
					conn.send(output);
					$("#vsebina_klepeta").append(output);
					$("#vsebina_poslji").val("");
				});
			});

</script>
