<?php
echo $this->Html->css('cake.generic');
echo $this->Html->css('lightbox');
echo $this->Html->script('jquery-1.8.2');
echo $this->Html->script('html2canvas');
echo $this->Html->script('jquery.plugin.html2canvas'); 
?>
<script type="text/javascript">
$(document).ready(function(){

	  $("#save-picture").click(function(){
    html2canvas($("body"),{
      background:'#ff0000',
      onrendered: function (canvas) {
        
        var image = canvas.toDataURL("image/jpg");
        
        //$('body').append(canvas);
        console.log(image);
        $.ajax({        
              type : "post",
              url: "<?php echo $this->Html->url(array('controller'=>'webgl','action'=>'shraniSliko')); ?>",
              data: "image="+image
            }).done(function(msg){
              console.log(msg);
            }); 
      }
    });
  });
});
</script>
<style type="text/css">
 body{
      background-color: white;
    }
    body,html{
        padding: 0;
        margin: 0;
}
</style>
<h1>Galerija - Albumi<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this->Html->image('save.jpg', array("width"=>"45")); ?></a></h1>
<hr />
<div id='vse'>
<div class="album">
<?php

for ($i = 0; $i < $stAlbumov; $i++) {
$naslov = $albumi[$i]['album']['naslov'];
$datum = date('d.m.Y, H:i', strtotime($albumi[$i]['album']['datum']));
$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
$naslov = str_replace($sumnik, $sicnik, $naslov);
$stSlik =$albumi[$i]['album']['stevilo_slik'];
if($stSlik > 0){
$idSlike = rand(0, $albumi[$i]['album']['stevilo_slik'] - 1);
$imeSlike = $slike[$i][$idSlike]['slika']['ime_slike'];
$id = $albumi[$i]['album']['id'];
$link = $this->Html->url(array("controller" => "galerija","action" => "albumwebgl","?" => array("id" => $id)));
?>
<div id="album">
	<div id="naslov"><?php echo $albumi[$i]['album']['naslov']; ?></div>
	<div id="podatki_album">
		<div id="slika-albuma"><a href="<?php echo $link; ?>"><img src="<?php echo $this -> Html -> url('/thumbnails/' . $naslov . '/' . $imeSlike); ?>" /></a></div>
		<div id="podatki-o-albumu">
			<div><b>Avtor:</b> <?php echo $albumi[$i]['album']['avtor']; ?></div>
			<div><b>Naloženo dne:</b> <?php echo $datum; ?></div>
			<div><b>Opis:</b> <?php echo $albumi[$i]['album']['opis']; ?></div>
			<div class="st-slik-album"><b>Število slik:</b> <?php echo $stSlik; ?></div>
		</div>
	</div>
</div>
<?php } } ?>

    <?php 
        $strani = ceil($stVsehAlbumov / 10);
        if($strani > 1){
            echo "<div id='strani'>";
            for($i = 0; $i < $strani; $i++){
                echo "<a class='st_strani' href='".$this->Html->url(array("controller"=>"galerija","action"=>"index",$i+1))."' >".($i+1)."</a>";
            } 
            echo "</div>";
        }
        ?>


</div>
</div>