<div id="album">
<div id="naslov_the_shortest">Zadnji 3 albumi</div>
<div id="zadnji-trije">
	<?php 
	$i = 0;
	foreach($albumi as $album){
		$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
		$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
		$naslov = str_replace($sumnik, $sicnik, $album['album']['naslov']);
		$idSlike = rand(0, $album['album']['stevilo_slik'] - 1);
		$imeSlike = $slike[$i][$idSlike]['slika']['ime_slike'];
		$idAlbuma = $album['album']['id'];
		$link = $this->Html->url(array("controller" => "galerija","action" => "album","?" => array("id" => $idAlbuma)));
	?>
	<div class="album">
		<div id="naslov-albuma"><b><?php echo $album['album']['naslov']; ?></b></div>
		<div id="slika-albuma"><a href="<?php echo $link; ?>"><img src="<?php echo $this -> Html -> url('/thumbnails/' . $naslov . '/' . $imeSlike); ?>" /></a></div>
	</div>
	<?php 
		$i++;
	} 
	?>
</div>
</div>
