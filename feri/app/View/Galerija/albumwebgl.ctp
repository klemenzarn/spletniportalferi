<html>
<head>
	<title>Albumi</title>

<?php
echo $this->Html->css('cake.generic');
echo $this->Html->css('lightbox');
echo $this->Html->script('jquery-1.8.2');
echo $this->Html->script('lightbox');
echo $this->Html->script('html2canvas');
echo $this->Html->script('jquery.plugin.html2canvas'); 
echo $this->Html->css('bjqs'); 
echo $this->Html->css('demo'); 
echo $this->Html->script('bjqs-1.3.min');
?>
<style type="text/css">
 body{
      background-color: white;
    }
    body,html{
        padding: 0;
        margin: 0;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#banner-fade').bjqs({
    animtype      : 'slide',
    height        : 380,
    width         : 730,
    nexttext : 'Naprej',
	prevtext : 'Nazaj'
  });

$("#save-picture").click(function(){
   	  html2canvas($("body"),{
      background:'#ff0000',
      onrendered: function (canvas) {
        var image = canvas.toDataURL("image/jpg");
        //$('body').append(canvas);
        console.log(image);
        $.ajax({        
              type : "post",
              url: "<?php echo $this->Html->url(array('controller'=>'webgl','action'=>'shraniSliko')); ?>",
              data: "image="+image
            }).done(function(msg){
              console.log(msg);
            }); 
        }
    });
  });
});
</script>
</head>
<body>

<h1>Galerija - Slike<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this->Html->image('save.jpg', array("width"=>"45")); ?></a></h1>
<hr />
<div id='vse'>
<div id="galer">

<div id="slike-v-albumu">
	<div id="oglasi"><?php echo $album[0]['album']['naslov']; ?></div>
	<div><b>Avtor:</b> <?php echo $album[0]['album']['avtor']; ?></div>
	<div><b>Naloženo dne:</b> <?php echo date('d.m.Y, H:i', strtotime($album[0]['album']['datum'])); ?></div>

<?php
$naslov = $album[0]['album']['naslov'];
$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
$naslov = str_replace($sumnik, $sicnik, $naslov);

$stSlik = $album[0]['album']['stevilo_slik'];
?>
 <div id="banner-fade">
	        <ul class="bjqs">

<?php
for($k = 0; $k < $stSlik; $k++){	
	echo '<li><img src="'. $this->Html->url('/images/' . $naslov . '/' . $slike[$k]['slika']['ime_slike']) .'" id="slika" width="730"/></li>';
}

?>
 </ul>
</div> 
<?php
?>
</div>
</div>
</div>
</body>
</html>
