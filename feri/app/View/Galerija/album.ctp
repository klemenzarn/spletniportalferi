<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
<div id="galer">

<div id="slike-v-albumu">
	<div id="oglasi"><?php echo $album[0]['album']['naslov']; ?></div>
	<div><b>Avtor:</b> <?php echo $album[0]['album']['avtor']; ?></div>
	<div><b>Naloženo dne:</b> <?php echo date('d.m.Y, H:i', strtotime($album[0]['album']['datum'])); ?></div>

<?php
$naslov = $album[0]['album']['naslov'];
$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
$naslov = str_replace($sumnik, $sicnik, $naslov);

$stSlik = $album[0]['album']['stevilo_slik'];
$stStolpcev = 4;
$stVrstic = $stSlik/$stStolpcev;
$stVrstic = ceil($stVrstic);

$k = 0;
for($i = 0; $i < $stVrstic; $i++){
	for($j = 0; $j < $stStolpcev; $j++){	
		echo '<img src="'. $this->Html->url('/thumbnails/' . $naslov . '/' . $slike[$k]['slika']['ime_slike']) .'" id="slika"/>';
		$k++;
		if($k == $stSlik){
			break;
		}
	}
}

?>
</div>
</div>
</div>
