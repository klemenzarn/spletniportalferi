  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<?php echo $this->Form->create(null, array("url"=>array("controller"=>"koledar","action"=>"mesec"))); ?>
	<select name='mesec'>
		<option value="1" <?php if($mesec==1) echo "selected";  ?> >Januar</option>
		<option value="2" <?php if($mesec==2) echo "selected";  ?> >Febrauar</option>
		<option value="3" <?php if($mesec==3) echo "selected";  ?> >Marec</option>
		<option value="4" <?php if($mesec==4) echo "selected";  ?> >April</option>
		<option value="5" <?php if($mesec==5) echo "selected";  ?> >Maj</option>
		<option value="6" <?php if($mesec==6) echo "selected";  ?> >Junij</option>
		<option value="7" <?php if($mesec==7) echo "selected";  ?> >Julij</option>
		<option value="8" <?php if($mesec==8) echo "selected";  ?> >Avgust</option>
		<option value="9" <?php if($mesec==9) echo "selected";  ?> >September</option>
		<option value="10" <?php if($mesec==10) echo "selected";  ?> >Oktober</option>
		<option value="11" <?php if($mesec==11) echo "selected";  ?> >November</option>
		<option value="12" <?php if($mesec==12) echo "selected";  ?> >December</option>
	</select>
	<input type="text" value="<?php echo $leto; ?>" name='leto' placeholder='Vnesi leto' />
	<input type="submit" value="Prikaži" />
	</form>
	<!--<pre>
		<?php //print_r($dogodki); ?>
	</pre>-->
	<br />
	<?php 
	$index = 1;
	$num = cal_days_in_month(CAL_GREGORIAN, $mesec, $leto);
	$index_dogokov = 0;
	for($i=0;$i<7;$i++){
		for($j=0;$j<5;$j++){
			if($index <= $num){
				if($index_dogokov<count($dogodki)){
					if($dogodki[$index_dogokov][0]['dan'] == $index){
						echo "<div class='pos_dan dogodek'>".$index."<br /><div class='naslov_dogodka' style='	text-overflow: ellipsis;' title='".$dogodki[$index_dogokov]['k']['naslov']."'>".$this->Html->link($dogodki[$index_dogokov]['k']['naslov'],array("controller"=>"koledar","action"=>"poglej",$dogodki[$index_dogokov]['k']['ID']))."</div></div>";
						$index_dogokov++;
					} else {
						echo "<div class='pos_dan'>".$index."</div>";
					}
				} else {
					echo "<div class='pos_dan'>".$index."</div>";
				}
			}
			$index++;
		}
	}
	
	if($tip == 2 || $tip == 3){
	?>
	<div class='prva_stran' id='dodaj_koledar'>
		<div id='naslov'><div id='ime_naslova'>Dodaj nov dogodek v koledar (Moderatorji in administratorji)</div></div>
		<div id='vsi_oglasi'>
			<?php 
			echo $this->Form->create(null,array("url"=>array("controller"=>"koledar","action"=>"dodaj"))); 
			?>
			Naslov: <input type="text" name="naslov" style="width: 200px"/> <br />
			Datum: <input type="text" name="datum" id='datepicker' style="width: 200px"/><br />
			Vsebina: <br />
			<textarea name='vsebina' style="width:720px;height: 100px; resize: none; margin-top: 3px; margin-left:5px;"></textarea>
			<br />
			<input type="submit" value="Shrani" />
		</form>
	</div>
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
	 $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
</script>