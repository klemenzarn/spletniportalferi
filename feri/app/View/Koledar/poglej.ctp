<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<div id='poglej_novico'>
		 <div id="naslov_novice"><?php echo $dogodek[0]['k']['naslov'];   ?></div>
		 <div id="informacije">
              <hr/>
              <?php 
                    $datum = $dogodek[0]['k']['datum'];
                    $timestamp = strtotime($datum);
                    $output_date = date('j.n.Y',$timestamp); //j.n je format d.m brez tistih ničel...
                    echo "Datum: ".$output_date.", Tega dogodka se bo udeležilo: ".$stevilo[0][0]['stevilo']; 
              ?>
                    <hr/>
        </div>
        <div id='vsebina_novice'>
            <?php echo $dogodek[0]['k']['vsebina']; ?>
        
        </div>
	<?php
		if($this->Session->check("uporabnik")){
			if($tip == 2 || $tip == 3){
				echo $this->Html->link($this->Html->image('uredi.png'),array("controller"=>"Koledar","action"=>"uredi",$dogodek[0]['k']['id']),array('escape'=>false));
				echo $this->Html->link($this->Html->image('brisi.png'),array("controller"=>"Koledar","action"=>"zbrisi",$dogodek[0]['k']['id']),array('escape'=>false,'confirm'=>"Res želite izbrisati ta dogodek?"));	
			}
			if($udelezba != null){
				echo $this->Form->create(null,array("url"=>array("controller"=>"koledar","action"=>"neudelezba")));
				echo "<input type='hidden' name='id' value='".$dogodek[0]['k']['id']."' />";
				echo $this->Form->end("Prekliči udeležbo dogodka");
			} else {
				echo $this->Form->create(null,array("url"=>array("controller"=>"koledar","action"=>"udelezba")));
				echo "<input type='hidden' name='id' value='".$dogodek[0]['k']['id']."' />";
				echo $this->Form->end("Potrdi udeležbo dogodka");
			}
		}
	?>
</div>
</div>
