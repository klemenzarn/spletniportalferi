<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
     <div class='short_div'>  
        <div id='naslov_short'><div id='ime_naslova_short'>Dodaj obveznost</div></div>
        <div id='forma_dodaj'>
            <?php
                echo $this->Form->create(null,array( 'url' =>array("controller"=>"urnik","action"=>"dodaj"), "class"=>"form"));
            ?>
            <input type="text" class="dodaj_input" name="predmet" placeholder="Vpiši predmet"/>
            <input type="text" class="dodaj_input" name="tutor" placeholder="Vpiši predavatelja/asistenta (ime in priimek)"/>
            <input type="text" class="dodaj_input" name="ucilnica" placeholder="Vpiši učilnico"/>
            <label for='dan'>Izberi dan:</label>
            <select id='dan' name='dan'>
                <option value="1">Ponedeljek</option>
                <option value="2">Torek</option>
                <option value="3">Sreda</option>
                <option value="4">Četrtek</option>
                <option value="5">Petek</option>
            </select>
            
            <label for='vrsta'>Vrsta:</label>
            <select  id='vrsta' name='vrsta'>
                <option value="PR">Predavanja</option>
                <option value="LV">Labor. vaje</option>
                <option value="SE">Seminar</option>
                <option value="SV">Seminarske vaje</option>
            </select>
            <div style="float:left; width:300px;">
            <label for='ura_zac'>Začetek:</label>
            <select style="float:left;" id='ura_zac' name='ura_zac'>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
            </select>
            <select style="float:left;"  id='min_zac' name='min_zac'>
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
            </select>
          </div>
        <div style="float:left; width:300px;">
           <label for='ura_kon'>Konec:</label>
            <select style="float:left;"  id='ura_kon' name='ura_kon'>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
            </select>
            <select style="float:left;"  id='min_kon' name='min_kon'>
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
            </select>
                 </div>
            <?php
                echo $this->Form->end("Dodaj obveznost");
            ?>
        </div>
    </div>
    
    
     <div class='short_div'>
        <div id='naslov_short'><div id='ime_naslova_short'>Vse obveznosti</div></div>
        <div id='vse_obv'>
            	<?php foreach ($moje_obveznosti as $obveznost): ?>
                    <div class='pozamezna_obveznost1'><?php echo "<span style='font-weight:bold;'>".$obveznost['predmet']."</span>, ".$obveznost['dan'].", ".$obveznost['vrsta'].", ".$obveznost['zacetek']." - ".$obveznost['konec'];
                     echo $this->Html->link($this->Html->image('uredi.png'), array("controller"=>"urnik", "action"=>"uredi","?"=>array("id"=>$obveznost['id'])), array('escape'=>false));
// OP AJAX 
                    echo $this->Js->link($this->Html->image('brisi.png', array("class"=>"izbrisi_image")), array("controller"=>"urnik", "action"=>"izbrisi"), array("update"=>"#output",'data'=>array('id'=>$obveznost['id']),'escape'=>false));

// OP AJAX
                        ?>
            
            <div id="output"></div>
                    </div>
            
                <?php endforeach; ?>
 <div id="success"></div>
         </div>
    </div>
    
</div>