<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
     <div class='short_div'>  
        <div id='naslov_short'><div id='ime_naslova_short'>Spremeni obveznost</div></div>
        <div id='forma_dodaj'>
            <?php
                echo $this->Form->create(null,array( 'url' =>array("controller"=>"urnik","action"=>"urediobv"), "class"=>"form"));
            ?>
            <input type="hidden" value="<?php echo $urnik[0]['u']['id'];  ?>" name='id' />
            <input type="text" class="dodaj_input" name="predmet" placeholder="Vpiši predmet" value="<?php echo $urnik[0]['u']['predmet']  ?>"/>
            <input type="text" class="dodaj_input" name="tutor" placeholder="Vpiši predavatelja/asistenta (ime in priimek)" value="<?php echo $urnik[0]['u']['ime']." ".$urnik[0]['u']['priimek']  ?>"/>
            <input type="text" class="dodaj_input" name="ucilnica" placeholder="Vpiši učilnico" value="<?php echo $urnik[0]['u']['ucilnica']  ?>"/>
            <label for='dan'>Izberi dan:</label>
            <select id='dan' name='dan'>
                <?php 
                    $ime = array("Ponedeljek","Torek","Sreda","Četrtek","Petek");
                    $dan = $urnik[0]['u']['dan'];
                    for($i = 0; $i < 5; $i++){
                        if(($i+1) == $dan){
                            echo "<option value='".($i+1)."' selected>".$ime[$i]."</option>";
                        } else {
                            echo "<option value='".($i+1)."'>".$ime[$i]."</option>";
                        }
                    }
                
                 ?>
            </select>
            
            <label for='vrsta'>Vrsta:</label>
            <select  id='vrsta' name='vrsta'>
                <option value="PR" <?php if($urnik[0]['u']['vrsta'] == 'PR') echo "selected";  ?> >Predavanja</option>
                <option value="LV" <?php if($urnik[0]['u']['vrsta'] == 'LV') echo "selected";  ?> >Labor. vaje</option>
                <option value="SE" <?php if($urnik[0]['u']['vrsta'] == 'SE') echo "selected";  ?> >Seminar</option>
                <option value="SV" <?php if($urnik[0]['u']['vrsta'] == 'SV') echo "selected";  ?>>Seminarske vaje</option>
            </select>
            <div style="float:left; width:300px;">
            <label for='ura_zac'>Začetek:</label>
            <select style="float:left;" id='ura_zac' name='ura_zac'>
                  <?php 
                    list($ura,$minuta) = explode(":",$urnik[0]['u']['zacetek']);
                    for($i = 7; $i < 21; $i++){
                        if($i == $ura){
                            echo "<option value='".$i."' selected>".$i."</option>";
                        } else {
                            echo "<option value='".$i."'>".$i."</option>";
                        }
                    }
                
                 ?>
            </select>
            <select style="float:left;"  id='min_zac' name='min_zac'>
                <option value="00" <?php if($minuta == '00') echo "selected";?> >00</option>
                <option value="15" <?php if($minuta == '15') echo "selected";?> >15</option>
                <option value="30" <?php if($minuta == '30') echo "selected";?> >30</option>
                <option value="45" <?php if($minuta == '45') echo "selected";?> >45</option>
            </select>
          </div>
        <div style="float:left; width:300px;">
           <label for='ura_kon'>Konec:</label>
            <select style="float:left;"  id='ura_kon' name='ura_kon'>
                    <?php 
                    list($ura,$minuta) = explode(":",$urnik[0]['u']['konec']);
                    for($i = 7; $i < 21; $i++){
                        if($i == $ura){
                            echo "<option value='".$i."' selected>".$i."</option>";
                        } else {
                            echo "<option value='".$i."'>".$i."</option>";
                        }
                    }
                
                 ?>
            </select>
            <select style="float:left;"  id='min_kon' name='min_kon'>
                <option value="00" <?php if($minuta == '00') echo "selected";?> >00</option>
                <option value="15" <?php if($minuta == '15') echo "selected";?> >15</option>
                <option value="30" <?php if($minuta == '30') echo "selected";?> >30</option>
                <option value="45" <?php if($minuta == '45') echo "selected";?> >45</option>
            </select>
                 </div>
            <?php
                echo $this->Form->end("Spremeni obveznost");
            ?>
        </div>
    </div>
    
    
     <div class='short_div'>
        <div id='naslov_short'><div id='ime_naslova_short'>Vse obveznosti</div></div>
        <div id='vse_obv'>
            	<?php foreach ($moje_obveznosti as $obveznost): ?>
                    <div class='pozamezna_obveznost'><?php echo $obveznost['predmet'].", ".$obveznost['dan'].", ".$obveznost['vrsta'].", ".$obveznost['zacetek']." - ".$obveznost['konec'];
                     echo $this->Html->link($this->Html->image('uredi.png'), array("controller"=>"urnik", "action"=>"uredi","?"=>array("id"=>$obveznost['id'])), array('escape'=>false));
// OP AJAX 
                    echo $this->Js->link($this->Html->image('brisi.png', array("class"=>"izbrisi_image")), array("controller"=>"urnik", "action"=>"izbrisi"), array("update"=>"#output",'data'=>array('id'=>$obveznost['id']),'escape'=>false));

// OP AJAX
                        ?>
            
            <div id="output"></div>
                    </div>
            
                <?php endforeach; ?>
 <div id="success"></div>
         </div>
    </div>
    
</div>