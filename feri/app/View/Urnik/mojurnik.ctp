<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
    <div id='urniki'>  
        <div id='naslov'><div id='ime_naslova'>Moj urnik</div></div>
            <div id='urnik_div'>
                <table class='tabela_urniki'>
                    <tr>
                        <td>Pon</td>
                        <td>Tor</td>
                        <td>Sre</td>
                        <td>Čet</td>
                        <td>Pet</td>
                    </tr>
                    <?php 
                        $max = 0;
                        $velikost = array(0,0,0,0,0);
                        for($i = 0; $i < count($urniki); $i++){
                               $velikost[$urniki[$i]['dan'] - 1]++;
                        }
                        for($i = 0; $i < 5; $i++){
                            if($velikost[$i] > $max){
                                $max =  $velikost[$i]; 
                            }
                        }
                        for($k = 0; $k < $max; $k++){
                            echo "<tr>";
                            
                            for($l = 1; $l <= 5; $l++){
                                echo "<td>";
                                $izbris = 0;
                                foreach($urniki as $key => $urnik){
                                    if($urnik['dan'] == $l){
                                        echo "- ".$urnik['zacetek']." - ". $urnik['konec']."</br>";
                                        echo "- ".$urnik['predmet']."</br>";
                                        echo "- ".$urnik['ucilnica'].", ".$urnik['vrsta']."</br>";
                                        echo "- ".$urnik['ime']." ".$urnik['priimek']."</br>";
                                        unset($urniki[$key]);
                                        break;
                                    }
                                }
                                echo "</td>";
                            }
                            
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        
        <?php echo $this->Html->link("Moje obveznosti",array("controller"=>"urnik","action"=>"obveznosti"),array("id"=>"link_obv"))."<br />"; ?>
        <?php echo $this->Html->link("Izvozi moj urnik",array("controller"=>"urnik","action"=>"izvoziMojUrnikVPDF"),array("id"=>"link_izvozi")); ?>
        </div>
</div>