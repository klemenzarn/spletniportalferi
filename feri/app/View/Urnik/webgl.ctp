<?php
echo $this->Html->css('cake.generic');
echo $this->Html->css('jquery-ui');
echo $this->Html->script('jquery-1.8.2');
echo $this->Html->script('jquery-ui-1.9.0.custom.min');
echo $this->Html->script('html2canvas');
echo $this->Html->script('jquery.plugin.html2canvas'); 
?>
<style type="text/css">
 body{
      background-color: white;
    }
    body,html{
        padding: 0;
        margin: 0;
}
</style>
<div id='vse'>
  <h1>Urnik<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this->Html->image('save.jpg', array("width"=>"45")); ?></a></h1>
<hr />
          <div id='urnik_div'>
              <span>Izberite si smer in poglejte urnik! Če ste študent lahko grete pod Moj profil in Moj urnik, kjer si lahko izberete smer in urnik tudi urejate.</span>
            <?php echo $this->form->create(null, array("url"=>array("controller"=>"urnik","action"=>"webgl"))); ?>
              Program:         
              <select name='branch_id'>
                  <?php foreach($vsi as $branch): ?>
                  <option value='<?php echo $branch[0]['Branch_Id']; ?>'><?php echo $branch[0]['Name']."  ".$branch[0]['Year'].". letnik"; ?></option>
                  <?php endforeach; ?>
              </select>
              </br>Datum: <input type="text" id="datepicker" name='datum' />
              <?php echo $this->form->end("Prikaži urnik"); ?>
              <?php if(!empty($urniki)){?>
              <table class='tabela_urniki'>
                    <tr>
                        <td>Pon</td>
                        <td>Tor</td>
                        <td>Sre</td>
                        <td>Čet</td>
                        <td>Pet</td>
                    </tr>
                    <?php 
                        $max = 0;
                        $velikost = array(0,0,0,0,0);
                        for($i = 0; $i < count($urniki); $i++){
                               $velikost[$urniki[$i]['dan'] - 1]++;
                        }
                        for($i = 0; $i < 5; $i++){
                            if($velikost[$i] > $max){
                                $max =  $velikost[$i]; 
                            }
                        }
                        for($k = 0; $k < $max; $k++){
                            echo "<tr>";
                            
                            for($l = 1; $l <= 5; $l++){
                                echo "<td>";
                                $izbris = 0;
                                foreach($urniki as $key => $urnik){
                                    if($urnik['dan'] == $l){
                                        echo "<span style='font-weight:bold;font-style:italic; font-size: 12px;'>".$urnik['zacetek']." - ". $urnik['konec']."</span></br>";
                                        echo "<span style='font-weight:bold; font-size: 12px;'>".$urnik['predmet']."</span></br>";
                                        echo $urnik['ucilnica'].", ".$urnik['vrsta']."</br>";
                                        echo $urnik['ime']." ".$urnik['priimek']."</br>";
                                        unset($urniki[$key]);
                                        break;
                                    }
                                }
                                echo "</td>";
                            }
                            
                            echo "</tr>";
                        }
                    ?>
                </table>
                <div id='urnik_noga'>
        <?php echo $this->Form->create(null,array("url"=>array("controller"=>"urnik","action"=>"izvoziUrnikVPDF"))); ?>
        <input type="hidden" name="branch_id" value="<?php echo $bran_id; ?>" />
         <input type="hidden" name="datum" value="<?php echo $dat; ?>" />
        <?php echo $this->Form->end("Izvozi urnik v pdf"); ?>
        </div>
              <?php }?>
        </div>
  <script>
$(document).ready(function(){
  $("#save-picture").click(function(){
    html2canvas($("body"),{
      background:'#ff0000',
      onrendered: function (canvas) {
        
        var image = canvas.toDataURL("image/jpg");
        
        //$('body').append(canvas);
        console.log(image);
        $.ajax({        
              type : "post",
              url: "<?php echo $this->Html->url(array('controller'=>'webgl','action'=>'shraniSliko')); ?>",
              data: "image="+image
            }).done(function(msg){
              console.log(msg);
            }); 
      }
    });
  });

  $(function() {
        $("#datepicker").datepicker({ dateFormat: 'd.m.yy' });
        $("#datepicker").datepicker("setDate", new Date());
  });
});
  </script>