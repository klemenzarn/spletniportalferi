<?php 
App::import('Vendor','xtcpdf');  
$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("Feri Urnik sistemi"); 
$tcpdf->SetAutoPageBreak( true ); 
$tcpdf->setHeaderFont(array($textfont,'',40)); 
//$tcpdf->xheadertext = 'KBS Homes & Properties'; 
//$tcpdf->xfootertext = 'Copyright Â© %d KBS Homes & Properties. All rights reserved.'; 

// add a page (required with recent versions of tcpdf) 
$tcpdf->addPage( 'L', 'LETTER' );


//$html = '<div style="text-align:center; background-color: #086ca2;color:white;font-weight:bold;font-size:20px;"><br />Fakulteta za elektrotehniko, racunalništvo in informatiko<br /></div>';
//$tcpdf->writeHTML($html, true, false, true, false, '');

$html = '<table style=" border-collapse: collapse; border: 1px solid black;font-size:10px;"><tr> <td style="background-color: #BFB7D3;
	border: 1px solid black; text-align: center;">Pon</td> <td style="background-color: #BFB7D3;
	border: 1px solid black;text-align: center;">Tor</td> <td style="background-color: #BFB7D3;
	border: 1px solid black;text-align: center;">Sre</td><td style="background-color: #BFB7D3;
	border: 1px solid black;text-align: center;">Cet</td><td style="background-color: #BFB7D3;
	border: 1px solid black;text-align: center;">Pet</td></tr>'.$test.'</table>';
$html = str_replace("č","c",$html);
$html = str_replace("Č","C",$html);
$tcpdf->writeHTML($html, true, false, true, false, '');

echo $tcpdf->Output('urnik.pdf', 'D'); 

?>