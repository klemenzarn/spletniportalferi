<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>

	<div class='prva_stran' id='novice'>
		<div id='naslov'><div id='ime_naslova'>Urejanje uporabnikovega profila: <?php echo $user[0]['users']['Ime'] . " " . $user[0]['users']['Priimek']; ?></div></div>
		<div id='vsi_oglasi'>
				<?php echo $this->Session->flash(); ?>
			<?php
	
	echo "<script>";
				
					echo "function nastavi()
				{
					var vrednost = document.getElementsByName('user_id')[0].value;
					if(vrednost == 0){
						document.getElementsByName('smer')[0].disabled = true;
						document.getElementsByName('letnik')[0].disabled = true;
						document.getElementsByName('vpisna')[0].disabled = true;
					}
					else if(vrednost == 3){
						document.getElementsByName('smer')[0].disabled = true;
						document.getElementsByName('letnik')[0].disabled = true;
						document.getElementsByName('vpisna')[0].disabled = true;
					}
					else {
						document.getElementsByName('smer')[0].disabled = false;
						document.getElementsByName('letnik')[0].disabled = false;
						document.getElementsByName('vpisna')[0].disabled = false;
					}
				} 
				
				$(document).ready(function(){
					nastavi();
				}) 
				
				
				"; 	
	echo "</script>";
				
	
	echo $this->Form->create('User',array('controller'=>'Users','action' => 'uredi','type'=>'file','name'=>'formUredi'));
	echo "<div id='leva_stran'>";
	echo $this->Form->input('Ime: <br />',array('value'=>$user[0]['users']['Ime'],'name'=>'ime','pattern'=>'[A-Ž]{1}[a-žA-Ž]{2,15}','title'=>'primer Janez','placeholder'=>'Ime','required'=>'required','label' => '*Ime:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Priimek: <br />',array('value'=>$user[0]['users']['Priimek'],'name'=>'priimek','pattern'=>'[A-Ž]{1}[a-žA-Ž]{2,15}','title'=>'primer Novak','placeholder'=>'Priimek','required'=>'required','label' => '*Priimek:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Uporabniško <none>ime:</none> <br />',array('value'=>$user[0]['users']['Uporabnisko'],'name'=>'up_ime','pattern'=>'[\w]{4,15}','title'=>'primer janez1234','placeholder'=>'Uporabniško ime','required'=>'required','label' => '*Uporabniško:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Vpisna <none>številka</none>: <br />',array('name'=>'vpisna','pattern'=>'[E]{1}[0-9]{7}','value'=>$user[0]['users']['Vpisna'],'title'=>'primer E0000000','placeholder'=>'Vpisna številka','required'=>'required','label' => '*Vpisna številka:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	//echo $this->Form->input('Geslo: <br />',array('value'=>$user[0]['users']['Geslo'],'name'=>'geslo','pattern' => '^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]{8,14}'));
	
	//echo $this->Form->input('Tip <none>uporabnika</none>: <br />',array('value'=>$user[0]['users']['Tip'],'name'=>'userID','pattern' => '[0-3]{1}','label' => 'Tip uporabnika:', 'div'=>'polje','id'=>'shrani'));
	echo $this->Form->input('Tip <none>uporabnika</none>: <br />',array('name'=>'user_id','onchange' => 'nastavi();','escape' => false,'options' => array(0 => '0', 1 => '1', 2 => '2', 3 => '3'),'value'=>$user[0]['users']['Tip'],'label' => '*Tip uporabnika:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Smer: <br />',array('name'=>'smer','options' => array(0 => 'R-IT'),'label' => '*Smer:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Letnik <br />',array('options' => array(1 => '1', 2 => '2', 3 => '3'),'value'=>$user[0]['users']['Letnik'],'title'=>'primer 1','placeholder'=>'Letnik','name'=>'letnik','pattern'=>'[1-3]{1}','required'=>'required','label' => '*Letnik:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Datum <none>rojstva</none>: <br />',array('value'=>$user[0]['users']['Datum_rojstva'],'title'=>'primer 22.06.2013','placeholder'=>'Datum rojstva','name'=>'datumRojstva','pattern'=>'[0-3]{1}[0-9]{1}[.][0-1]{1}[0-9]{1}[.](19|20)\d{2}$','label' => 'Datum rojstva:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('E-mail: <br />',array('value'=>$user[0]['users']['Mail'],'type'=>'email','name'=>'u_mail','pattern'=>'[a-zA-Z]{1}[a-zA-Z0-9 . _ -]{1,15}[@]{1}[a-z]{1,10}[.]{1}[a-z]{2,3}','title'=>'primer janez90@gmail.com','placeholder'=>'Vnesite e-mail','required'=>'required','label' => '*E-mail:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo "</div>";
	echo "<div id='desna_stran'>";
	echo $this->Form->input('Država: <br />',array('value'=>$user[0]['users']['Drzava'],'name'=>'u_drzava','pattern'=>'[A-Ž]{1}[a-žA-ž]{4,12}','title'=>'primer Slovenija','placeholder'=>'Država','label' => 'Država:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Mesto: <br />',array('value'=>$user[0]['users']['Mesto'],'name'=>'u_mesto','pattern'=>'[A-Ž]{1}[a-žA-ž]{3,12}','title'=>'primer Maribor','placeholder'=>'Mesto','label' => 'Mesto:', 'div'=>'polje','id'=>'shrani'));
	//echo $this->Form->input('Predmeti: <br />',array('value'=>$user[0]['users']['Predmeti'],'name'=>'predmeti','pattern'=>'[a-zA-Z]{0,12}'));
	echo "<br />";
	echo $this->Form->input('Prvi <none>dostop</none>: <br />',array('value'=>$user[0]['users']['Prvi_dostop'],'name'=>'p_dostop','placeholder'=>'Prvi dostop','label' => 'Prvi dostop:', 'div'=>'polje','id'=>'shrani'));
	echo "<br />";
	echo $this->Form->input('Zadnji <none>dostop</none>: <br />',array('value'=>$user[0]['users']['Zadnji_dostop'],'name'=>'z_dostop','placeholder'=>'Zadnji dostop','label' => 'Zadnji dostop:', 'div'=>'polje','id'=>'shrani'));
	//echo $this->Form->input('Vloga: <br />',array('value'=>$user[0]['users']['Vloga'],'name'=>'vloga'));
	echo "<br />";
	echo "Slika:<br />";
	echo $this->Html->image("Server/".$user[0]['users']['Uporabnisko']. "/" .$user[0]['users']['Slika'],array('alt'=>'profilna_slika','width'=>'120'));
	echo $this->Form->input('Spremeni <none>sliko</none>: <br />',array('name'=>'datoteka','type'=>'file'));
	
	echo $this->Form->input('id',array('value'=>$user[0]['users']['ID'],'name'=>'u_id','type'=>'hidden'));
	echo $this->Form->end('Potrdi',array('name'=>'potrdi','type'=>'submit'));
	echo "</div>";
	echo "<div id='spodna_stran'>";
		echo $this->Html->link("Nazaj",array('action'=>'ogled_users'));
	echo "<br />";
	echo $this->Html->link('Poenostavitev gesla',array('controller' => 'Users','action' => 'poenostgesla',$user[0]['users']['ID']));
	echo "<br />";

	{	
		echo $this->Html->link('Zbriši sliko', array('controller' => 'Users','action' => 'zbrisiSliko',$user[0]['users']['ID']),
		array('escape' => false,'confirm' => 'Ali res želite izbrisati sliko?'));
	}
	
	?>
	</div>
			</div>
			</div>
	
	</div>


