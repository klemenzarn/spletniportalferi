<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	
	<div class='prva_stran' id='novice'>
		<div id='naslov'><div id='ime_naslova'>Sprememba gesla</div></div>
		<div id='vsi_oglasi'>
	<?php
	
	echo $this->Form->create('User',array('controller'=>'Users','action' => 'sprememba_gesla','name'=>'formGeslo'));
	echo $this->Form->input('Trenutno <none>geslo:</none> <br />',array('name'=>'staro_geslo','type' => 'password','pattern' => '^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]{8,14}','placeholder'=>'Trenutno geslo','label' => 'Trenutno geslo:', 'div'=>'polje','id'=>'shrani'));
	echo $this->Form->input('Trenutno geslo:',array('name' => 'tr_geslo','value' => $user[0]['users']['Geslo'],'type' => 'password', 'type' => 'hidden'));
	echo $this->Form->input('Novo <none>geslo</none>: <br />',array('name'=>'novo_geslo','type' => 'password','pattern' => '^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]{8,14}','placeholder'=>'Novo geslo','label' => 'Novo geslo:', 'div'=>'polje','id'=>'shrani'));
	echo $this->Form->input('Ponovite <none>geslo</none>: <br />',array('name' => 'novo_geslo2','type' => 'password','pattern' => '^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]{8,14}','placeholder'=>'Ponovite geslo','label' => 'Ponovite geslo:', 'div'=>'polje','id'=>'shrani'));
	echo $this->Form->input('id',array('value' => $user[0]['users']['ID'],'name'=>'u_id','type'=>'hidden'));
	echo $this->Form->end('Potrdi',array('name'=>'potrdi','type'=>'submit'));
	
	echo '<br />';
	echo $this->Html->link("Nazaj",array('action'=>'ogled',$user[0]['users']['ID']));
	
	?>
	
	</div>
	</div>
	</div>


