<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
	<div class='prva_stran' id='novice'>
		<div id='naslov'><div id='ime_naslova'>Dodaj uporabnika</div></div>
		<div id='vsi_oglasi'>
				<?php
				
		
				echo "<script>";
				
				echo "function nastavi()
				{
					var vrednost = document.getElementsByName('user_id')[0].value;
					if(vrednost == 0){
						document.getElementsByName('smer')[0].disabled = true;
						document.getElementsByName('letnik')[0].disabled = true;
						document.getElementsByName('vpisna')[0].disabled = true;
					}
					else if(vrednost == 3){
						document.getElementsByName('smer')[0].disabled = true;
						document.getElementsByName('letnik')[0].disabled = true;
						document.getElementsByName('vpisna')[0].disabled = true;
					}
					else {
						document.getElementsByName('smer')[0].disabled = false;
						document.getElementsByName('letnik')[0].disabled = false;
						document.getElementsByName('vpisna')[0].disabled = false;
					}
				} 
				
				$(document).ready(function(){
					nastavi();
				}) 
				
				
				"; 	
		 		echo "</script>";
				
				echo $this->Form->create('User',array('controller'=>'users','action' => 'dodaj','type'=>'file','name'=>'formDodaj'));
				echo "<div id='leva_stran'>";
				echo $this->Form->input('Ime: <br />',array('name'=>'ime','pattern' => '[A-Ž]{1}[a-žA-Ž]{0,15}','title'=>'primer Janez','placeholder'=>'Ime','required'=>'required','label' => '*Ime:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Priimek: <br />',array('name'=>'priimek','pattern' => '[A-Ž]{1}[a-žA-Ž]{0,15}','title'=>'primer Novak','placeholder'=>'Priimek','required'=>'required','label' => '*Priimek:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Uporabniško <none>ime</none>: <br />',array('name'=>'up_ime','pattern'=>'[\w]{4,15}','title'=>'primer janez1234','placeholder'=>'Uporabniško ime','required'=>'required','label' => '*Uporabniško:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Vpisna <none>številka</none>: <br />',array('name'=>'vpisna','pattern'=>'[E]{1}[0-9]{7}','title'=>'primer E0000000','placeholder'=>'Vpisna številka','required'=>'required','label' => '*Vpisna številka:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Geslo: <br />',array('name'=>'geslo','value' => 'test12345','pattern' => '^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]{8,14}','title'=>'primer geslo123','placeholder'=>'Geslo','required'=>'','label' => '*Geslo:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Tip <none>uporabnika</none>: <br />',array('name'=>'user_id','onchange' => 'nastavi();','escape' => false,'options' => array(0 => '0', 1 => '1', 2 => '2', 3 => '3'),'label' => '*Tip uporabnika:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Smer: <br />',array('name'=>'smer','options' => array(0 => 'R-IT'),'label' => '*Smer:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Letnik <br />',array('name'=>'letnik','options' => array(1 => '1', 2 => '2', 3 => '3'),'label' => '*Letnik:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Html->link("Nazaj",array('action'=>'ogled_users'));
				echo "</div>";
				echo "<div id='desna_stran'>";
				echo $this->Form->input('Datum <none>rojstva:</none> <br />',array('name'=>'datum','pattern' => '[0-3]{1}[0-9]{1}[.][0-1]{1}[0-9]{1}[.](19|20)\d{2}$','title'=>'primer 22.06.2013','placeholder'=>'Datum rojstva','label' => 'Datum rojstva:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('E-mail: <br />',array('type'=>'email','name'=>'u_mail','pattern'=>'[a-zA-Z]{1}[a-zA-Z0-9 . _ -]{1,15}[@]{1}[a-z]{1,10}[.]{1}[a-z]{2,3}','title'=>'primer janez90@gmail.com','placeholder'=>'Vnesite e-mail','required'=>'required','label' => '*E-mail:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Drzava: <br />',array('name'=>'u_drzava','pattern'=>'[A-Ž]{1}[a-žA-ž]{0,15}','title'=>'primer Slovenija','placeholder'=>'Država','label' => 'Država:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Mesto: <br />',array('name'=>'u_mesto','pattern'=>'[A-Ž]{1}[a-žA-ž]{0,15}','title'=>'primer Maribor','placeholder'=>'Mesto','label' => 'Mesto:', 'div'=>'polje','id'=>'shrani'));
				//echo $this->Form->input('Predmeti: <br />',array('name'=>'predmeti'));
				echo "<br />";
				echo $this->Form->input('Prvi <none>dostop:</none> <br />',array('name'=>'p_dostop','placeholder'=>'Prvi dostop','label' => 'Prvi dostop:', 'div'=>'polje','id'=>'shrani'));
				echo "<br />";
				echo $this->Form->input('Zadnji <none>dostop:</none> <br />',array('name'=>'z_dostop','placeholder'=>'Zadnji dostop','label' => 'Zadnji dostop:', 'div'=>'polje','id'=>'shrani'));
				//echo $this->Form->input('Vloga: <br />',array('name'=>'vloga'));
				echo "<br />";
				echo $this->Form->input('Dodaj <none>sliko:</none> <br />',array('name'=>'datoteka','type'=>'file','fullBase' => 'true'));
				
				echo $this->Form->end('Dodaj uporabnika',array('name'=>'potrdi','type'=>'submit'));
				echo "</div>";
				
	
				 ?>
				</div>

</div>
</div>

