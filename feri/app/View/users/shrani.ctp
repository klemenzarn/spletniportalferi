<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
	<div class='prva_stran' id='novice'>
		<div id='naslov'><div id='ime_naslova'>Urejanje profila: <?php echo $user[0]['users']['Ime'] . " " . $user[0]['users']['Priimek']; ?></div></div>
		<div id='vsi_oglasi'>
		<?php 
		
		echo $this->Form->create('User',array('controller'=>'Users','action' => 'shrani','type'=>'file','name'=>'formNalozi'));
		echo "<div id='leva_stran'>";
		echo $this->Form->input('Ime: ',array('value'=>$user[0]['users']['Ime'],'disabled'=>true,'name'=>'ime','placeholder'=>'Ime','label' => 'Ime:', 'div'=>'polje','id'=>'shrani'));
		echo $this->Form->input('Ime',array('value'=>$user[0]['users']['Uporabnisko'],'type'=>'hidden','name'=>'up_ime'));
		echo '<br />';
		echo $this->Form->input('Priimek: ',array('value'=>$user[0]['users']['Priimek'],'disabled'=>true,'name'=>'priimek','placeholder'=>'Priimek','label' => 'Priimek:', 'div'=>'polje','id'=>'shrani'));
		echo '<br />';
		echo $this->Form->input('Uporabniško: ',array('value'=>$user[0]['users']['Uporabnisko'],'name'=>'u_uporabnisko','pattern'=>'[\w]{4,15}','disabled'=>true,'title'=>'primer janez1234','placeholder'=>'Uporabniško ime','required'=>'required','label' => 'Uporabniško:', 'div'=>'polje','id'=>'shrani'));
		//echo $this->Form->input('Predmeti: <br />',array('value'=>$user[0]['users']['Predmeti'],'disabled'=>true,'name'=>'predmeti'));
		echo '<br />';

		echo $this->Form->input('E-mail: ',array('value'=>$user[0]['users']['Mail'],'type'=>'email','name'=>'u_mail','pattern'=>'[a-zA-Z]{1}[a-zA-Z0-9 . _ -]{1,15}[@]{1}[a-z]{1,10}[.]{1}[a-z]{2,3}','title'=>'primer janez90@gmail.com','placeholder'=>'Vnesite e-mail','required'=>'required','label' => 'E-mail:', 'div'=>'polje','id'=>'shrani'));
		echo '<br />';
		echo $this->Form->input('Datum <none>rojstva:</none> <br />',array('value'=>$user[0]['users']['Datum_rojstva'],'name'=>'datum','pattern' => '[0-3]{1}[0-9]{1}[.][0-1]{1}[0-9]{1}[.](19|20)\d{2}$','title'=>'primer 22.06.2013','placeholder'=>'Datum rojstva','label' => 'Datum rojstva:', 'div'=>'polje','id'=>'shrani'));
			echo "<br />";
		
		echo $this->Form->input('Drzava: ',array('value'=>$user[0]['users']['Drzava'],'name'=>'u_drzava','pattern'=>'[A-Ž]{1}[a-žA-Ž]{4,15}','title'=>'primer Slovenija','placeholder'=>'Država','label' => 'Država:', 'div'=>'polje','id'=>'shrani'));
		echo '<br />';
		echo $this->Form->input('Mesto: ',array('value'=>$user[0]['users']['Mesto'],'name'=>'u_mesto','pattern'=>'[A-Ž]{1}[a-žA-Ž]{3,15}','title'=>'primer Maribor','placeholder'=>'Mesto','label' => 'Mesto:', 'div'=>'polje','id'=>'shrani'));
		echo '<br />';
					echo "</div>";
	echo "<div id='desna_stran'>";
		echo $this->Form->input('Prvi <none>dostop</none>: ',array('value'=>$user[0]['users']['Prvi_dostop'],'disabled'=>true,'name'=>'p_dostop','placeholder'=>'Prvi dostop','label' => 'Prvi dostop:', 'div'=>'polje','id'=>'shrani'));
		echo '<br />';
		echo $this->Form->input('Zadnji <none>dostop</none>: ',array('value'=>$user[0]['users']['Zadnji_dostop'],'disabled'=>true,'name'=>'z_dostop','placeholder'=>'Zadnji dostop','label' => 'Zadnji dostop:', 'div'=>'polje','id'=>'shrani'));
		
		echo "<br />";
		
		echo "Slika:<br />";
		echo $this->Html->image('Server/'.$user[0]['users']['Uporabnisko']."/".$user[0]['users']['Slika'],array('alt'=>'profilna_slika','width'=>'120'));
		echo $this->Form->input('Spremeni <none>sliko</none>: <br />',array('name'=>'datoteka','type'=>'file'));
		echo "Slika:<br />";

		echo $this->Form->input('id',array('value'=>$user[0]['users']['ID'],'name'=>'u_id','type'=>'hidden'));
		echo $this->Form->end('Potrdi',array('name'=>'potrdi','type'=>'submit'));
			echo "</div>";
	echo "<div id='spodna_stran'>";

				
			echo $this->Html->link('Zbriši sliko', array('controller' => 'Users','action' => 'zbrisiSliko',$user[0]['users']['ID']),
			array('escape' => false,'confirm' => 'Ali res želite izbrisati sliko?'));
			
			//echo $this->Html->link('Zbriši sliko',array('onClick'=>'confirm(\''."Ali res želite zbrisati sliko?".'\');','escape' => false )); 
			echo '<br />';
			echo $this->Html->link("Nazaj",array('action'=>'ogled',$user[0]['users']['ID']));
		
		
		?>
		</div>
			</div>
			</div>
		</div>

