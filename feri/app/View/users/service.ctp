	<?php
		echo $this->Html->script('jquery-1.8.2');
  		echo $this->Html->script('jquery-ui-1.9.0.custom.min');
		echo $this->Html->script('sorttable.js');
		echo $this->fetch('script');
		echo "<br />";

		echo "<script>";
		
		echo "function nastavi()
		{
			var vrednost = document.getElementById('izberi').value;		
			
			if(vrednost == 'zvrst'){
				document.getElementById('ocena').disabled = true;
				document.getElementById('zvrst').disabled = false;
				document.getElementById('iskanje').disabled = true;
			}
			else if(vrednost == 'ocena'){
				document.getElementById('zvrst').disabled = true;
				document.getElementById('ocena').disabled = false;
				document.getElementById('iskanje').disabled = true;
			}
			else {
				document.getElementById('ocena').disabled = true;
				document.getElementById('zvrst').disabled = true;
				document.getElementById('iskanje').disabled = false;
			}			
		}"; 	
 		
 		echo "</script>";
		?>
		<center>
		<div id='formi'>
		<center><h1>Iskanje filmov</h1> </center>
		<form method='POST' name='formIsci' action=''>

		<select name='izbira' id='izberi' onchange='nastavi()'>
		  <option value='ni'>Izberi</option>
		  <option value='naslov'>Naslov</option>
		  <option value='leto'>Leto</option>
		  <option value='jezik'>Jezik</option>
		  <option value='ocena'>Ocena</option>
		  <option value='drzava'>Država</option>
		  <option value='zvrst'>Zvrst</option>
		</select>  
		
		<select name='zvrsti' id='zvrst' disabled=true>
		  <option value='ni'>Zvrst</option>
		  <option value='akcija'>Akcija</option>
		  <option value='akcijska drama'>Akcijska drama</option>
		  <option value='animirani'>Animirani</option>
		  <option value='animirani, komedija, družinski'>Animirani, komedija, družinski</option>
		  <option value='biografija'>Biografija</option>
		  <option value='biografska pustolovska drama'>Biografska pustolovska drama</option>
		  <option value='dokumentarni'>Dokumentarni</option>
		  <option value='domišljiski'>Domišljiski</option>
		  <option value='drama'>Drama</option>
		  <option value='družinsku'>Družinski</option>
		  <option value='glasba'>Glasba</option>
		  <option value='grozljivka'>Grozljivka</option>
		  <option value='komedija'>Komedija</option>
		  <option value='komedija, romantični'>Komedija, romantični</option>
		  <option value='kratki'>Kratki</option>
		  <option value='kriminalka'>Kriminalka</option>
		  <option value='misterij'>Misterij</option>
		  <option value='muzikal'>Muzikal</option>
		  <option value='ostali'>Ostalo</option>
		  <option value='otroški ali mladinski film'>Otroški ali mladinski film</option>
		  <option value='potopis'>Potopis</option>
		  <option value='pustolovščina'>Pustolovščina</option>
		  <option value='romantičen'>Romantičen</option>
		  <option value='romantična komedija'>Romantična komedija</option>
		  <option value='shrljivka'>Shrljivka</option>
		  <option value='triler'>Triler</option>
		  <option value='vestern'>Vestern</option>
		  <option value='vojni'>Vojni</option>
		  <option value='zgodovinski'>Zgodovinski</option>
		  <option value='zgodovinska akcijska drama'>Zgodovinska akcijska drama</option>
		  <option value='znanstvena fantastika'>Znanstvena fantastika</option>
		  <option value='znanstveno-fantastični akcijski triler'>Znanstveno-fantastični akcijski triler</option>
		</select>  
		<select name='ocene' id='ocena' disabled=true>
		  <option value='ni'>Ocena</option>
		  <option value='1'>1</option>
		  <option value='2'>2</option>
		  <option value='3'>3</option>
		  <option value='4'>4</option>
		  <option value='5'>5</option>
		</select>  
		<input type='text' name='search' id='iskanje' placeholder='Iskanje' disabled=false>	
		<input type='submit' value='Iskanje' name='potrdi'/>
		</div>
	<br />
	</center>
	<?php  
	
	if(isset($rezultat->iskanjePoNaslovuResult))
	{
		if($rezultat->iskanjePoNaslovuResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoNaslovuResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	if(isset($rezultat->iskanjePoLetuResult))
	{
		if($rezultat->iskanjePoLetuResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoLetuResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	if(isset($rezultat->iskanjePoJezikuResult))
	{
		if($rezultat->iskanjePoJezikuResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoJezikuResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	if(isset($rezultat->iskanjePoDrzaviResult))
	{
		if($rezultat->iskanjePoDrzaviResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoDrzaviResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	if(isset($rezultat->iskanjePoZvrstiResult))
	{
		if($rezultat->iskanjePoZvrstiResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoZvrstiResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	if(isset($rezultat->iskanjePoOceniResult))
	{
		if($rezultat->iskanjePoOceniResult != NULL)
		{
			$podatki = explode("*", $rezultat->iskanjePoOceniResult);
			echo "<div id='formi2'>";
			for($i=0; $i<count($podatki)-1; $i+=12){
				echo "<div id='polje2'>";
				echo "<h2>" . $podatki[$i]. "</h2>";
				echo "<h5>" . $podatki[$i+1]. "</h5>";
				echo "<h5>" . $podatki[$i+2]. "</h5>";
				echo "<a><b>Leto: </b></a>" . $podatki[$i+3] . "<br />";
				echo "<a><b>Dolžina: </b></a>" . $podatki[$i+4] . " min<br />";
				echo "<a><b>Ocena: </b></a>" . $podatki[$i+5] . "<br />";
				echo "<a><b>Država: </b></a>" . $podatki[$i+6] . "<br />";
				echo "<a><b>Jezik: </b></a>" . $podatki[$i+7] . "<br />";
				echo "<a><b>Režija: </b></a>" . $podatki[$i+8] . "<br />";
				echo "<a><b>Scenarij: </b></a><span>" . $podatki[$i+9] . "</span>";
				echo "<a><b>Igrajo: </b></a><span>" . $podatki[$i+10] . "</span>";
				echo "<a><b>Vsebina: </b></a><br /><span>" . $podatki[$i+11] . "</span>";
				if(count($podatki) > 12)
				{
					echo "<hr style='color:#ffffff;'/>";	
				}
				echo "</div>";
			}
			echo "</div>";
		}
	}
	
	echo "<br />";
?>

