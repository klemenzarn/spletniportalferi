<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
	<?php
	if($this->Session->check("uporabnik"))
	{
		{?>
			   <div id="vse_novice">
			        <?php foreach ($novice as $novica):?>
		        		<div id="novica">
		                	<div id='naslov' style="text-overflow:ellipsis;"><?php echo $novica['n']['naslov'];?></div>
	               			 <div id="podatki-novice">
	               				 <div id="slika-novice"><img src="/feri/files/Novice/<?php echo $novica['n']['id']."/".$novica['n']['naslovna_slika'];  ?>" width="265px" /></div>
                				 <div id="podatki-novica">
                      			 <?php
		                            $vsebina = strip_tags($novica['n']['vsebina']);
		                            if(strlen($vsebina) > 500){
		                                $vsebina = substr($vsebina,0,400)." ...";
		                            } 
		                            			                            echo $vsebina;
									echo "<div><b>Objavljeno dne: </b>". date('d.m.Y, H:i', strtotime($novica['n']['datum']))."</div>";
									echo "<div><b>Avtor: </b>".$novica['n']['avtor']."</div>";
									echo "<div class='uredi-izbrisi'>";
									echo "<div style='float: left; padding-left: 5px;'><a href='/feri/novice/uredinovico/".$novica['n']['id']."' >".$this->Html->image('uredi.png')."</a></div>";
									echo "<div style='float: right; padding-left: 5px;'><a id='brisi_novico' href='/feri/novice/brisi/".$novica['n']['id']."'>".$this->Html->image('brisi.png')."</a></div>";   
									echo "</div>";
							 	 ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
			  </div>
	
			        <?php
			        $stevilo = $stnovic[0][0]['stevilo'];    //dobimo število useh novic iz baze
			        $strani = ceil($stevilo / 10);  //zaokrožimo navzgor
			        echo "<div id='strani'>";
			        for($i = 0; $i < $strani; $i++){
			            echo "<a class='st_strani' href='/feri/novice/uredi/".($i+1)."' >".($i+1)."</a>";
			        } 
			        echo "</div>";	    
		}
	}
	else
	{
			$this->set("error","Trenutno niste prijavljeni!");
			$this->render("Errors/vpis");
	}
	?>
</div>
		