<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
    <div id="vse_novice">
    	<div id='isci_novice'>
    		<?php
    			echo $this->Form->create(null,array('type' => 'get',"url"=>array("controller"=>"drugenovice","action"=>"isci")));
    		?>
    		<label for='niz'>Išči novice: </label><input type='text' name='niz' placeholder="Vnesi niz" />
    		<select name='vir'>
    			<option value='vse'>Vsi viri</option>
    			<option value='Rac-novice'>Računalniške novice</option>
    			<option value='Slo-tech'>Slo-tech</option>
    			<option value='24-ur'>24-ur</option>
    		</select>
    		<input type='submit' value='Išči' />
    	</form>
    	</div>
        <?php foreach ($novice as $novica): ?>
            <div class='prva_stran' id='novice'>
                <div id='naslov'>
                    <div id='ime_naslova' style="text-overflow:ellipsis;">
                    <?php 
                        //izpiše se datum in novica v glavo novice... skrajšamo datum
                        /*$datum = $novica['n']['datum'];
                        $timestamp = strtotime($datum);
                        $output_date = date('j.n',$timestamp); //j.n je format d.m brez tistih ničel...
                        echo $output_date." - ".$novica['n']['naslov'];*/
                        echo $this->Html->link($novica['n']['naslov']." - Vir: ".$novica['n']['vir'],$novica['n']['link'],array("id"=>"drugenovicelink",'target'=>'_blank'));
                    ?>
                        </div>
                </div>
               
                    <div class="info" style="padding: 5px;">
                        <div><?php
                            $vsebina = $novica['n']['vsebina'];

                            echo $vsebina;   
                        ?></div>
 
                </div>
            </div>
        <?php endforeach; ?>

    <?php 
        $stevilo = $stnovic[0][0]['stevilo'];    //dobimo število useh novic iz baze
        $strani = ceil($stevilo / 10);  //zaokrožimo navzgor
        if($strani > 1){
            echo "<div id='strani'>";
            for($i = 0; $i < $strani; $i++){
                echo "<a class='st_strani' href='".$this->Html->url(array("controller"=>"drugenovice","action"=>"index",$i+1))."' >".($i+1)."</a>";
            } 
            echo "</div>";
        }
        ?>
    </div>
</div>