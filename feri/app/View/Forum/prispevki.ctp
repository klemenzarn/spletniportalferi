<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<script type="text/javascript">
	$(document).ready(function() {
			$('.dajplus').click(function() {
				var id_prispevka = $(this).attr('id');

				$.ajax({
					type : "POST",
					url : "/feri/forum/plus",
					data : {
						ocene : id_prispevka
					}
				}).done(function(msg) {
					if(msg == "ok"){
						var value = $('.'+id_prispevka).html();
			    		value++;
			  			$('.'+id_prispevka).html(value);
			  		} else {
			  			alert("Ste že glasovali!");
			  		}
					//alert(msg);
				});
			});
			$('.dajminus').click(function() {
				
				var id_prispevka = $(this).attr('id');

				$.ajax({
					type : "POST",
					url : "/feri/forum/minus",
					data : {
						ocene : id_prispevka
					}
				}).done(function(msg) {
					if(msg == "ok"){
						var value = $('.'+id_prispevka+"-").html();
		    			value++;
		  				 $('.'+id_prispevka+"-").html(value);
			  		} else {
			  			alert("Ste že glasovali!");
			  		}
				});
			});
			});
			</script>
			<div id='navigacija_forum'>
<a href='/feri/forum'>Forum</a>/<a href='./' ><?php echo strtoupper($ime_smeri); ?></a>
</div>
		<?php
		echo "<div id='naslov_teme'>".$prispevki[0]['t']['naslov']."</div>";
		$index = 0;
		foreach ($prispevki as $temp) {
			if($index == 0){
				echo "<div class='pos_pris1'>";
				$index++;
			}else {
				echo "<div class='pos_pris'>";
			}
			?>
				<div class='info_up'>
					<div class='slika_up'>
						<?php echo $this->Html->image('Server/'.$temp['u']['Uporabnisko']."/".$temp['u']['Slika'],array("width"=>"110px")); ?>
					</div>
					<div class='up_ime'>
						<?php echo $this->Html->link($temp['u']['Uporabnisko'],array("controller"=>"Users","action"=>"uporabnik",$temp['u']['ID']));  ?>
					</div>
				</div>
				<div class='vsebina_for'>
					<div class='datum_for'><?php 
						$datum = $temp["p"]["cas"];
	                    $timestamp = strtotime($datum);
	                    $output_date = date('j.n.Y H:m',$timestamp); //j.n je format d.m brez tistih ničel...
						echo $output_date;
						?>
					</div>
					<div class='ocen_for'>
						<?php
						echo "<div class='thumbs'>".$this -> Html -> image('thumbup.png', array('id' => $temp["p"]["id"], 'class' => 'dajplus', 'title' => 'Plus', 'alt' => 'Povečaj', 'height' => '15px', 'width' => '15px')) . " <div style='color:green; float:left; margin-left:3px;' class='" . $temp['p']['id'] . "'>" . $temp["p"]["plus"] . "</div></div>";

						echo "<div class='thumbs'>".$this -> Html -> image('thumbdown.png', array('id' => $temp["p"]["id"], 'class' => 'dajminus', 'title' => 'Minus', 'alt' => 'Minus', 'height' => '15px', 'width' => '15px')) . "<div style='color:red; float:left; margin-left:3px;' class='" . $temp['p']['id'] . "-'>" . $temp["p"]["minus"] . "</div></div> ";
						if ($this -> Session -> read('uporabnik') == $temp['u']['Uporabnisko']) {
							echo "<div class='thumbs'>".$this -> Html -> link($this->Html->image('uredi.png'), array('controller' => 'forum', 'action' => 'urediPrispevek', $temp["p"]["id"]),array("escape"=>false)) . "</div>";
						}
						?>
					</div>
					
					<div class='vseb_for'>
					<?php
						echo $temp["p"]["vsebina"];
					?>
					</div>
				</div>
			</div>
			<?php
			
		}
		
		/*
		foreach ($prispevki as $temp) {
			echo "<tr>";
			echo "<td>" . $temp["p"]["vsebina"] . "</td>";
			echo "<td>" . $this -> Html -> image('thumbup.png', array('id' => $temp["p"]["id"], 'class' => 'dajplus', 'title' => 'Plus', 'alt' => 'Povečaj', 'height' => '15px', 'width' => '15px')) . " <div class='" . $temp['p']['id'] . "'>" . $temp["p"]["plus"] . "</div></td>";

			echo "<td>" . $this -> Html -> image('thumbdown.png', array('id' => $temp["p"]["id"], 'class' => 'dajminus', 'title' => 'Minus', 'alt' => 'Minus', 'height' => '15px', 'width' => '15px')) . "<div class='" . $temp['p']['id'] . "-'>" . $temp["p"]["minus"] . "</div> </td>";

			//echo "<td><img src='https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash4/c44.44.548.548/s160x160/427044_4383987682332_1406082479_n.jpg' height='40px' width='40px'/>" . $temp["u"]["Uporabnisko"] . "</td>";
			echo "<td>".$this->Html->image('Server/'.$temp['u']['Uporabnisko']."/".$temp['u']['Slika'],array("width"=>"50px"))."<br />" .$temp["u"]["Uporabnisko"] . "</td>";
			echo "<td>" . $temp["p"]["cas"] . "</td>";
			if ($this -> Session -> read('uporabnik') == $temp['u']['Uporabnisko']) {
				echo "<td>" . $this -> Html -> link("Uredi prispevek", array('controller' => 'forum', 'action' => 'urediPrispevek', $temp["p"]["id"])) . "</td>";
			}
			/*  echo "<td>".$this->Html->link(
			 "+",             array('controller' => 'forum', 'action' => 'povecaj', $temp["p"]["id"]))."</td>";
			 echo "<td>".$this->Html->link(
			 "-",             array('controller' => 'forum', 'action' => 'zmanjsaj', $temp["p"]["id"]))."</td>";
			echo "</tr>";
		}*/
		?>
<div id='noga_forum'>
<?php
//$trenutni=$this->here;
if($this->Session->check('uporabnik'))
{
echo "<div class='ustvaritemo'>".$this->Html->link(
"<img src='http://cdn1.iconfinder.com/data/icons/prettyoffice/256/edit.png' width='30px' height='30px'/> Odgovori",
array('controller' => 'forum', 'action' => 'odgovori', $ime_smeri,$ime_teme),array('escape'=>false))."</div>";

/* echo $this->Html->link(
"Odgovori", array('controller' => 'forum', 'action' => 'odgovori', $ime_smeri,$ime_teme));*/
if($this->Session->read('uporabnik')==$avtorteme[0]['u']['Uporabnisko'])
{
?>

<form method='POST' name='webService' action="<?php echo $this -> Html -> url(array("controller" => "Forum", "action" => "stanje")); ?>">
	<input type='hidden' value='<?php echo $prispevki[0]["p"]["tema"]?>' name='idneketeme'/>
	<select id="select3" name='izbrano_Stanje'>
		<option value="0" selected>- Možnosti -</option>
		<option  value="1">Zadovoljen z odgovori</option>
		<option  value="0">Nezadovoljen z odgovori</option>
	</select>
	<input type="submit" value="Potrdi" id="poisci"/>
</form>
<?php
//echo $avtorteme[0]['u']['Uporabnisko'];
}
//echo "<pre>";
// print_r ($avtorteme);
// echo "</pre>";
}
?>

<?php
$stevilo = $steviloprispevkov[0][0]['stevilo'];
//dobimo število useh novic iz baze
$strani = ceil($stevilo / 10);
//zaokrožimo navzgor
if ($strani > 1) {
	echo "<div id='strani'>";
	for ($i = 0; $i < $strani; $i++) {
		// echo "<li><a href='".$this->Html->url(array("controller"=>"forum","action"=>"kategorije",$ime_smeri,$ime_teme,"?"=>array('stran'=>$i+1)))."'>".($i+1)."</a></li>";
		echo "<a class='st_strani' href='" . $this -> Html -> url(array("controller" => "forum", "action" => "kategorije", $ime_smeri, $ime_teme, "?" => array('stran' => $i + 1))) . "' >" . ($i + 1) . "</a>";
	}
	echo "</div>";
}
?>
</div>
</div>