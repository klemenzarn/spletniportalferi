<?php
echo $this->Html->css('cake.generic');
?>
<style type="text/css">
    body,html{
        padding: 0;
        margin: 0;
}
</style>
<div id='vse'>

<a href='/feri/forum/webgl'>Forum</a>/<a href='' ><?php echo strtoupper($ime_kategorije); ?></a>
<table id="tabela_forum">
<thead>
<tr>
<th scope="col">Teme</th>
<th scope="col">Število odgovorov</th>
<th scope="col">Avtor teme</th>
    <th scope='col'>Stanje</th>
</tr>
</thead>
    <tbody>
<?php
foreach ($teme as $temp) {
	$url_naslov = $temp["t"]["naslov"];
	$naslov = Inflector::slug($url_naslov, '-');
	//metoda, ki bo presledke v naslovu sprmenila v - : namesto Naslov%teme bo ratalo Naslov-teme
	echo "<tr>";
	echo "<td>" . $this -> Html -> link($temp["t"]["naslov"], array('controller' => 'forum', 'action' => 'kategorijewebgl/' . $temp["k"]["kategorija"], $naslov)) . "</td>";
	echo "<td>" . $temp['0']['stevilo_prispevkov_v_temi'] . "</td>";
	echo "<td><a href=''>" . $temp["u"]["Uporabnisko"] . "</a></td>";
	if ($temp['t']['status'] == 1) {
		echo "<td><img src='http://icons.iconarchive.com/icons/deleket/sleek-xp-basic/256/Ok-icon.png' height='15px' width='15px'/></td>";
	} else {
		echo "<td><img src='http://rationalwiki.org/w/images/1/19/Question_icon.svg' height='15px' width='15px'/></td>";
	}
	echo "</tr>";
}
?>
   
<?php
if ($this -> Session -> check('uporabnik')) {
	echo "<td class='ustvaritemo'>" . $this -> Html -> link("<img src='http://cdn1.iconfinder.com/data/icons/musthave/256/Add.png' width='30px' height='30px'/> Ustvari temo", array('controller' => 'forum', 'action' => 'ustvari_temo', $teme[0]['k']['kategorija']), array('escape' => false)) . "</td>";

}
?>
   </tbody>
</table>  
<?php
$stevilo = $stevilotem[0][0]['stevilo'];
//dobimo število useh novic iz baze
$strani = ceil($stevilo / 10);
//zaokrožimo navzgor
if ($strani > 1) {
	echo "<div id='strani'>";
	for ($i = 0; $i < $strani; $i++) {
		// echo "<li><a href='".$this->Html->url(array("controller"=>"forum","action"=>"kategorije",$ime_smeri,$ime_teme,"?"=>array('stran'=>$i+1)))."'>".($i+1)."</a></li>";
		echo "<a class='st_strani' href='" . $this -> Html -> url(array("controller" => "forum", "action" => "kategorijewebgl", $ime_smeri,  "?" => array('stran' => $i + 1))) . "' >" . ($i + 1) . "</a>";
	}
	echo "</div>";
}
?>
</div>