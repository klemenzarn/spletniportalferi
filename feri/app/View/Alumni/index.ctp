<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<div id='poglej_novico'>
     <div id="naslov_novice">Alumni</div>
     <br />
     <div id='vsebina_novice'>
Kaj je oz. kdo so alumni?
<br /><br />
Alumnus v latinščini pomeni gojenec, alumna pa gojenka in oba se nanašata predvsem na almo mater, latinsko za mati rednico, kasneje pa sinonim za univerzo. Alumni je množinska oblika, kadar je prisoten vsaj en moški, zgolj žensko (z)druženje bi bilo alumnae. Skladno s tem bomo v nadaljevanju uporabljali zgolj moške oblike, mišljena pa sta oba spola.
<br /><br />
Alumni je mednarodno popularen izraz za absolvente (tako diplomante kot tiste, ki študija formalno niso zaključili) določene univerze, fakultete ali šole.
<br /><br />
Zakaj Alumni klub UM FERI?
<br /><br />
Združenja alumni imajo dolgo tradicijo v anglosaškem izobraževalnem sistemu, kjer izpolnjujejo predvsem tri vloge:
<br /><br />
skrbijo za druženje nekdanjih sošolcev in kolegov,
so ambasadorji svojih šol/fakultet/univerz in so vpeti v dogajanje na njih,
v kolikor vidijo v tem korist, finančno podpirajo svojo almo mater.
Na celinski Evropi so se alumni klubi začeli ustanavljati predvsem iz potrebe po boljši povezanosti izobraževalnih inštitucij z njihovim okoljem. Tako nudijo izmenjavo informacij oz. dosežkov (na raziskovalnem, gospodarskem, pedagoškem področju) med izobraževalno inštitucijo in organizacijami, v katerih so alumni zaposleni. Na podlagi tega sodelovanja pogosto pride do izmenjave kadrov, skupnega mentorstva zaključnih nalog, skupnih predstavitvenih nastopov in do skupnih razvojnih projektov.
<br /><br />
V Sloveniji se je ob nazivu Alumni klub uveljavil tudi naziv Društvo diplomantov.
<br /><br />
Na UM FERI smo se odločili, da se tesneje in formalno povežemo z našimi alumni zaradi naslednjih treh ciljev, h katerim stremi fakulteta:
<br /><br />
S pomočjo naših diplomantov, ki so se v okolju izkazali z znanjem pridobljenim na naši fakulteti, bi želeli povišati ugled znanstvenih in strokovnih področij, na katerih deluje fakulteta.
Naši diplomanti, ki poznajo fakulteto in so »rasli« z njo, nam lahko dajo ne samo infomacije, kako se gleda na fakulteto »od zunaj« (v gospodarstvu, politiki, kulturi), pač pa nam lahko svetujejo, kaj bi bilo potrebno narediti, da bi se povečal ugled fakultete in bi s tem rasla tudi vrednost njenih (trenutnih in bodočih) diplomantov.
Fakulteta se bo najlažje povezovala z okoljem preko konkretnih projektov, nastalih med ljudmi, ki redno osebno (formalno in neformalno) komunicirajo. V okviru alumni kluba želimo poskrbeti, da bodo ustvarjeni pogoji za takšno komunikacijo.
</div>
</div>
</div>