<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
<?php
echo $this->Session->flash();
echo "<br/>";
?>
<script type="text/javascript" src="http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<form method='POST' id="myform" name='dodajNovico' onsubmit="return validateForm()" action="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"dodajnovico"));?>" enctype="multipart/form-data">
*Naslov novice:<input type="text" name="naslov_novice_dodaj"/></br></br>
<textarea name='tiny_dodaj'></textarea><br/>
<script type="text/javascript">
    tinyMCE.init({
       theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect,help",
       theme_advanced_buttons2 : "search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
       theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions",
       theme : "advanced",
       theme_advanced_font_sizes : "10px,12px,14px,16px,24px",
       theme_advanced_text_colors : "FF00FF,FFFF00,000000",
       mode : "textareas",
       width: '750',
       height: '450', 
       convert_urls : false
    });
</script> 
*Naslovna slika:<input type="file" id="slika_dodaj" name="naslovna_slika"/><br/>
<input type="submit" value="Potrdi" id="potrdi"/>
</form>
</div>

	 <script>
	 var userConfirmed=false;
	 var w;
	 var h;
	 
	 var url = window.URL || window.webkitURL;
	 $("#slika_dodaj").change(function(e) {
	    if( this.disabled ){
	        alert('Vaš brskalnik ne podpira file-upload-a.');
	    }else{
	        var chosen = this.files[0];
	        var image = new Image();
	        image.onload = function() {
	            w=this.width;
	            h=this.height;
	        };
	        image.src = url.createObjectURL(chosen);                    
	     }
	});
	 
	 function validateForm()
	 {
		
		if(w>500 || h>300)
		{
		 if ( userConfirmed ) {
			       	 userConfirmed = false;
	        	    return true;
	    		}
	    		
		if (false) {
	        return false;
	    }
		
		$("#opac").click(function() {
				$("#novo_okno").css("display", "none");
				$("html").css("overflow", "auto");
				document.body.scroll = "yes";
			});
		
			
			$("#zapri_b1").click(function() {
					$("#novo_okno").css("display", "none");
					$("html").css("overflow", "auto");
					document.body.scroll = "yes";
				});
		
			$('#opozorilo').css("visibility","visible");
			$('#opozorilo').css("display","inline");
			$('html').css("overflow", "hidden");
			
				//skrijemo scrollbar
				$('#novo_okno').css("display", "block");
				//pokažemo črno ozadje
				var vscroll = (document.all ? document.scrollTop : window.pageYOffset);
				$('#novo_okno').css("top", vscroll + "px");
				//na tej pa tej poziciji
				document.body.scroll = "no";
				
				$("#klik1").click(function() {
					userConfirmed=true;
					$("#myform").submit();
					$("#novo_okno").css("display", "none");
					$("html").css("overflow", "auto");
					document.body.scroll = "yes";
				});
				
			$("#klik2").click(function() {
					$("#novo_okno").css("display", "none");
					$("html").css("overflow", "auto");
					document.body.scroll = "yes";
				});
			   return false;
		}
		else
			return true;
	}
	</script>
	<div id='novo_okno'>
        <div id='opac'></div>
        <div id='opravljanje1'>
                <div id="opozorilo">
                <h2>Opozorilo</h2>
                Naložena slika ni ustrezne velikosti(500x300px), zato bo prilagojena, ali to želite? <br/><br/>
                <input type="button"  class="vpis" id="klik1" value="DA"/>
                <input type="button" class="vpis" id="klik2" value="NE"/>
                <a class="zapri1" id="zapri_b1"></a>
                </div>
         </div>
     </div>

