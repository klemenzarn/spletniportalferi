<?php
		echo $this->Html->script('jquery-ui-tabs-rotate');
		echo $this->Html->script('jquery.ticker');
		echo $this->Html->css('ticker-style');

?>
<div id='levo'>
<div id="cssmenu">
  <ul>
   <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/Drugenovice">Novice od drugod</a></li>
   <li id='neki'><a href="/feri/Dogodki">Dogodki</a></li>
   <li id='neki'><a href="/feri/Kino">Kino</a></li>
   <li id='neki'><a href="/feri/Klepet">Klepet</a></li>
   <li id='neki'><a href="/feri/Tvspored">Tv spored</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
<?php
if($this->Session->check('uporabnik')){
		if($staticno == "novice2"){
			echo "<ul id='js-news' class='js-hidden'>";
			foreach($novice2 as $novica){
				echo "<li class='news-item'><a href='".$novica['n']['link']."'  target='_blank' title='".$novica['n']['naslov']."'>".$novica['n']['naslov']." (Vir: ".$novica['n']['vir'].")</a></li>";
				} 
			echo "</ul>";
 } 	}?>
	<div id='vse1'>
<?php 
if($this->Session->check('uporabnik')){
	foreach($razporeditev as $raz){
		if($raz == "novice"){
			?>
					<div class='prva_stran' id='novice'>
						<div id='naslov'><div id='ime_naslova' class="zasort">Novice</div></div>
						<div id="featured" >
							  <ul class="ui-tabs-nav">
								 <?php
								//sliko moramo spremeniti v 500px pri nalaganju slike!!!!!!!!!
							  
					            //skrajšujem naslove novic za prikazovanje v slideshowu
								$naslov = array();
								for($i = 0; $i < 4; $i++){
									if(strlen($novice[$i]['n']['naslov']) > 40){
										$naslov[] = substr($novice[$i]['n']['naslov'],0,40)."...";
									} else {
										$naslov[] = $novice[$i]['n']['naslov'];
									}
								}
							  ?>  
						        <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[0];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika']);    ?>"  width="80px" height="50px" alt="" /><span><?php echo $naslov[1];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[2];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[3];?></span></a></li>
								
						      </ul>
					
						    <!-- First Content -->
						    <div id="fragment-1" class="ui-tabs-panel" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id']))); ?>" ><?php echo $novice[0]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[0]['n']['vsebina']);?></p>
								 </div>
						    </div>
					
						    <!-- Second Content -->
						    <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id']))); ?>" ><?php echo $novice[1]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[1]['n']['vsebina']);?></p>
								 </div>
						    </div>
					
						    <!-- Third Content -->
						    <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id']))); ?>" ><?php echo $novice[2]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[2]['n']['vsebina']);?></p>
						         </div>
						    </div>
					
						    <!-- Fourth Content -->
						    <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id']))); ?>" ><?php echo $novice[3]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[3]['n']['vsebina']);?></p>
						         </div>
						    </div>
					
					</div>
					</div>
			<?php
		}
		if($raz == 'oglasna_deska'){
			?>
				<!-- oglasna deska -->
				<div class='prva_stran'  id='oglasna_deska'>
				<div id='naslov'><div id='ime_naslova' class="zasort">Oglasna deska</div>
				
				    <!--
				<div id='isci'>
					<form method="get" action="<?php echo $this->Html->url(array("controller" => "oglasna","action" => "isci")); ?>">
						<input type='text' name='niz' />
						<input id='isci_button' type='submit' value='Išči' />
					</form>
				</div> -->
				<div id='isci'>
	<form method="get" action="<?php echo $this->Html->url(array("controller" => "oglasna","action" => "isci")); ?>">
		<input type='text' name='niz' />
		<input id='isci_button' type='submit' value='Išči' />
	</form>
</div>
				</div>
					<div id='vsi_oglasi'>
					<?php foreach ($deska as $oglas): ?>
						<div class='novica'>
						<div class='zacetek_novice'>
						<?php 
							echo $oglas['o']['naslov']." - ";
				            echo $oglas['o']['avtor'];
							echo "<div class='plus'></div>";
							//echo $this->Html->image('plus.png', array('class' => 'plus'))
						?>
						</div>
						<div class='vsebina_oglasna'>
							<?php 
								echo strip_tags($oglas['o']['vsebina']);
								if($datoteke != null){
									echo "<ul>";
				                    //izpišemo fajle
									foreach ($datoteke as $file){
										if($file['o']['id'] == $oglas['o']['id']){
											if(!empty($file['d']['ime']))
											echo "<li><a href='./files/Oglasna/".$oglas['o']['id']."/".$file['d']['ime']."'>".$file['d']['ime']."</a></li>";

										}
									}
									echo "</ul>";
								}
							?>
						</div>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			<?php
		}
		if($raz == 'urnik'){
			?>
				<div class='short_div' id='urnik'>
				<?php $dnevi = array("Nedelja","Ponedeljek","Torek","Sreda","Četrtek","Petek","Sobota"); ?>
				<div id='naslov_short'><div id='ime_naslova_short' class='zasort'>Moj urnik (<?php  $today = getdate(); echo $dnevi[$today['wday']]; ?>)</div></div>
				    <div id='vse_obv'>
				    <?php if(!empty($urniki)){foreach ($urniki as $obveznost): ?>
				        <div class='pozamezna_obveznost1'>
				            <?php echo "<span style='font-weight:bold;'>".$obveznost['predmet']."</span>, ".$obveznost['vrsta'].", ".$obveznost['zacetek']." - ".$obveznost['konec'];  ?>
				            
				        </div>
				    <?php endforeach;} else echo "Danes nimate obveznosti!"; ?>
				    </div>
				</div>
			<?php
		}
		if($raz == 'koledar'){
			?>
			<div class='short_div' id='koledar'>
				<div id='naslov_short'><div id='ime_naslova_short' class='zasort'>Koledar dogodkov</div></div>
			    <div id='vse_obv1'>
					<?php
						foreach ($koledar as $dogodek) {
							?>
							<div class='pos_kol'>
								<div class='datum_znak'>
									<?php 
										$mesec = date("F", mktime(0, 0, 0, $dogodek[0]['mesec'], 10));
										$mesec = strtoupper(substr($mesec, 0,3)); 
									?>
									<span id="mesec"><?php echo $mesec; ?></span>
									<span id="dan"><?php echo $dogodek[0]['dan']; ?></span>
								</div>
								<div class='vsebina_koledar' style='text-overflow: ellipsis;'>
									<?php echo $this->Html->link($dogodek['k']['naslov']." (".$dogodek['k']['vsebina'].")",array("controller"=>"koledar","action"=>"poglej",$dogodek['k']['ID']),array("title"=>$dogodek['k']['naslov']." (".$dogodek['k']['vsebina'].")")); ?>
								</div>
							</div>
							
							<?php
							//echo  $dogodek['k']['datum'].": ".$this->Html->link($dogodek['k']['naslov'],array("controller"=>"koledar","action"=>"poglej",$dogodek['k']['ID']))."<br />";
						}
					?>
			    </div>
			</div>
			<?php
		}
		if($raz == 'zapogl'){
			?>
			<div class='short_div' id='zapogl'>
<div id='naslov_short'><div id='ime_naslova_short' class='zasort'>Zadnji 3 oglasi</div></div>
    <div id='vse_obv'>
    <?php
				if(!empty($data)){
					if(!empty($data['DobiZadnjeTriResult']['string']))
					{
						$st_oglasov=count($data['DobiZadnjeTriResult']['string']);
						
						if($st_oglasov==1)
						{//"<span id='datum'>".$datum."<span/>
								list($naslov, $datum, $opis,$kraji_dela,$delodajalec,$izobrazba,$naloge,$placilo,$povezava) = split('[*]', $data['DobiZadnjeTriResult']['string']);
								echo "<a id='oglasi2'  href=".$povezava.">".$naslov."</a>";
								echo" <br/>";
								echo "<br/>";
								echo "<b>".$delodajalec."</b>, ".$kraji_dela."<br/>";
								if(strlen($opis) > 200){
                                $opis = substr($opis,0,200)."...";
                            	} 
								$newtext = wordwrap($opis, 50, "<br />\n");
								echo "<b>Opis:</b><br/>".$newtext;
								echo "<a href=".$povezava.">Več</a>";
								if($izobrazba!="")
								{
								echo "<b>Izobrazba:</b>";
								echo $izobrazba;
								echo "<br/>";
								}
								if($placilo!="")
								{
								echo "<b>Placilo:</b>";
								echo "<br/>";
								echo $placilo;
								echo "<br/>";
								}
								echo "<br/>";
						}
						else
						{
							//"<span id='datum'>".$datum."<span/>
							for($i=0;$i<$st_oglasov;$i++)
							{
								list($naslov, $datum, $opis,$kraji_dela,$delodajalec,$izobrazba,$naloge,$placilo,$povezava) = split('[*]', $data['DobiZadnjeTriResult']['string'][$i]);
								echo "<a id='oglasi2'  href=".$povezava.">".$naslov."</a>";
								echo "<br/>";
								echo "<br/>";
								echo "<b>".$delodajalec."</b>,".$kraji_dela."<br/>";
								if(strlen($opis) > 150){
                                 $opis = substr($opis,0,150)."...";
                            	} 
								$newtext = wordwrap($opis, 50, "<br />\n");
								echo "<b>Opis:</b><br/>".$newtext;
								echo "<a href=".$povezava.">Več</a><br/>";
								if($izobrazba!="")
								{
								echo "<b>Izobrazba:</b>";
								echo $izobrazba;
								echo "<br/>";
								}
								if($placilo!="")
								{
								echo "<b>Placilo:</b>";
								echo "<br/>";
								echo $placilo;
								echo "<br/>";
								}
								echo "<br/>";
							}
						}
					}
					else {
						echo "Za izbrane kriterije trenutno ni objavljenih aktualnih oglasov. Poskusite spremeniti iskalne kriterije.";
					}
				}else if(isset($message)){
					print_r($message);
					echo "Prišlo je do napake!";
				}
		?>
    </div>
</div>
			
			<?php
		}
		if($raz == 'dogodki'){
			?>
			<div class='short_div' id='dogodki'>
<div id='naslov_short'><div id='ime_naslova_short' class='zasort'>Dogodki</div></div>
        <div id='vse_obv'>
    <?php
    foreach($dogodki as $temp)
    {
        echo "<strong>".$temp['d']['naslov']."</strong>";
        if($temp['d']['datum']!="")
        {
            if($temp['d']['kraj']!="")
            {
                 echo "<br/><i>".$temp['d']['datum'].", ".$temp['d']['kraj']."</i><br/>";
            }
            else
            {
            echo "<br/><i>".$temp['d']['datum']."</i><br/>";
            }
        }
        else
        {
         echo "<br/>";   
        }
        echo substr($temp['d']['vsebina'],0,250)." ... ";
        echo "<a href='".$temp['d']['povezava']."' target='_blank'>Podrobnosti</a><br/><hr>";
    }?>
        </div>
            
</div>
			<?php
		}
		if($raz == 'tvspored'){
			$vel = count($oddaje);
?>
<div class="short_div" id='tvspored'>
<div id="naslov_short">
	<div id="ime_naslova_short" class="zasort">Trenutno na TV ...</div>
</div>
<div id="vse_obv">
		<?php 
		for($j = 0; $j < $vel; $j++){
			if(isset($oddaje[$j]['trenutnaOddajaResult']['string'])){
			$i = 0;
		?>
		<div id="podatki-tv-spored">
			<div id=program-logo>
				<img src="<?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 1]?>" />
			</div>
		 	<div id="dve-oddaje">
				<div><?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4]; ?></div>
				<div><b>Trenutno: <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 5]; ?></b> - <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 4]; ?></div>
				<div><b>Sledi: <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 3]; ?></b> - <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 2]; ?></div>
			</div>
		</div>
		<?php 
		$i++;
			}
else{
 ?>
 	<div id="podatki-tv-spored">
 		<div id=program-logo>
		</div>
		 <div id="dve-oddaje">
			<div></div>
			<div><b>Trenutno: -</b></div>
			<div><b>Sledi: -</b></div>
		</div>
 	</div>
 <?php
}
			} 
		?>
</div>
</div>
			<?php
		}
		if($raz == "kino"){
			?>
			<div class="short_div" id='kino'>
<div id="naslov_short">
	<div id="ime_naslova_short" class="zasort">Kino</div>
</div>
<div id="vse_obv1">
	<?php
			
			if(isset($rezultat_kino->iskanjeTopOcenResult)){
				if($rezultat_kino->iskanjeTopOcenResult != NULL){
					$podatki = explode("*", $rezultat_kino->iskanjeTopOcenResult);
					
					for($i=0; $i<count($podatki)-1; $i+=4){
						echo "<a href=".$podatki[$i+3].">".$podatki[$i]."</a><br />";
						echo $podatki[$i+1]."<br />";
						echo $podatki[$i+2]."<br />";
						echo "<hr/>";
					}
				}
			}
			
			?>
</div>
</div>
			
			<?php
		}
	}
	} else {
		
		echo "<ul id='js-news' class='js-hidden'>";
			foreach($novice2 as $novica){
				echo "<li class='news-item'><a href='".$novica['n']['link']."'  target='_blank' title='".$novica['n']['naslov']."'>".$novica['n']['naslov']." (Vir: ".$novica['n']['vir'].")</a></li>";
				} 
			echo "</ul>";
			?>
					<div class='prva_stran' id='novice'>
						<div id='naslov'><div id='ime_naslova' class="zasort">Novice</div></div>
						<div id="featured" >
							  <ul class="ui-tabs-nav">
							  <?php
								//sliko moramo spremeniti v 500px pri nalaganju slike!!!!!!!!!
							  
					            //skrajšujem naslove novic za prikazovanje v slideshowu
								$naslov = array();
								for($i = 0; $i < 4; $i++){
									if(strlen($novice[$i]['n']['naslov']) > 40){
										$naslov[] = substr($novice[$i]['n']['naslov'],0,40)."...";
									} else {
										$naslov[] = $novice[$i]['n']['naslov'];
									}
								}
							  ?>  
						        <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[0];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika']);    ?>"  width="80px" height="50px" alt="" /><span><?php echo $naslov[1];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[2];?></span></a></li>
						        <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[3];?></span></a></li>
								
						      </ul>
					
						    <!-- First Content -->
						    <div id="fragment-1" class="ui-tabs-panel" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id']))); ?>" ><?php echo $novice[0]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[0]['n']['vsebina']);?></p>
								 </div>
						    </div>
					
						    <!-- Second Content -->
						    <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id']))); ?>" ><?php echo $novice[1]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[1]['n']['vsebina']);?></p>
								 </div>
						    </div>
					
						    <!-- Third Content -->
						    <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id']))); ?>" ><?php echo $novice[2]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[2]['n']['vsebina']);?></p>
						         </div>
						    </div>
					
						    <!-- Fourth Content -->
						    <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">
								<?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id'])),array("escape"=>false)); ?>
								 <div class="info" >
									<h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id']))); ?>" ><?php echo $novice[3]['n']['naslov'];?></a></h2>
									<p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[3]['n']['vsebina']);?></p>
						         </div>
						    </div>
					
					</div>
					</div>
		
<!-- oglasna deska -->
				<div class='prva_stran'  id='oglasna_deska'>
				<div id='naslov'><div id='ime_naslova' class="zasort">Oglasna deska</div>
				    <!--
				<div id='isci'>
					<form method="get" action="<?php echo $this->Html->url(array("controller" => "oglasna","action" => "isci")); ?>">
						<input type='text' name='niz' />
						<input id='isci_button' type='submit' value='Išči' />
					</form>
				</div> -->
								<div id='isci'>
	<form method="get" action="<?php echo $this->Html->url(array("controller" => "oglasna","action" => "isci")); ?>">
		<input type='text' name='niz' />
		<input id='isci_button' type='submit' value='Išči' />
	</form>
</div>
				</div>
					<div id='vsi_oglasi'>
					<?php foreach ($deska as $oglas): ?>
						<div class='novica'>
						<div class='zacetek_novice'>
						<?php 
							echo $oglas['o']['naslov']." - ";
				            echo $oglas['o']['avtor'];
							echo "<div class='plus'></div>";
							//echo $this->Html->image('plus.png', array('class' => 'plus'))
						?>
						</div>
						<div class='vsebina_oglasna'>
							<?php 
								echo strip_tags($oglas['o']['vsebina']);
								if($datoteke != null){
									echo "<ul>";
				                    //izpišemo fajle
									foreach ($datoteke as $file){
										if($file['o']['id'] == $oglas['o']['id']){
											if(!empty($file['d']['ime']))
											echo "<li><a href='./files/Oglasna/".$oglas['o']['id']."/".$file['d']['ime']."'>".$file['d']['ime']."</a></li>";

										}
									}
									echo "</ul>";
								}
							?>
						</div>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
			<div class='short_div' id='koledar'>
				<div id='naslov_short'><div id='ime_naslova_short' class='zasort'>Koledar dogodkov</div></div>
			    <div id='vse_obv1'>
					<?php
						foreach ($koledar as $dogodek) {
							?>
							<div class='pos_kol'>
								<div class='datum_znak'>
									<?php 
										$mesec = date("F", mktime(0, 0, 0, $dogodek[0]['mesec'], 10));
										$mesec = strtoupper(substr($mesec, 0,3)); 
									?>
									<span id="mesec"><?php echo $mesec; ?></span>
									<span id="dan"><?php echo $dogodek[0]['dan']; ?></span>
								</div>
								<div class='vsebina_koledar' style='text-overflow: ellipsis;'>
									<?php echo $this->Html->link($dogodek['k']['naslov']." (".$dogodek['k']['vsebina'].")",array("controller"=>"koledar","action"=>"poglej",$dogodek['k']['ID']),array("title"=>$dogodek['k']['naslov']." (".$dogodek['k']['vsebina'].")")); ?>
								</div>
							</div>
							
							<?php
							//echo  $dogodek['k']['datum'].": ".$this->Html->link($dogodek['k']['naslov'],array("controller"=>"koledar","action"=>"poglej",$dogodek['k']['ID']))."<br />";
						}
					?>
			    </div>
			</div>
<?php		
	}
?>
</div>
</div>
<script type="application/javascript">

		$('#js-news').ticker({
            speed: 0.10,
            fadeInSpeed: 600,
            titleText: 'Zadnje novice od drugod'
        });
	<?php if($this->Session->check('uporabnik')){ 
		$razporeditev2="";
		foreach ($razporeditev as $raz) {
			$razporeditev2 .= "\"".$raz."\",";
		}
		$razporeditev2 = substr($razporeditev2,0,strlen($razporeditev2)-1);
		?>
        var pozicije = new Array(<?php echo $razporeditev2; ?>);
		 function mesta(sprememba_id,div){
			zacetek = sprememba_id;
			konec = 0;
			for(i=0;i<pozicije.length;i++){
				if(pozicije[i]==div){
					konec = i;
					break;
				}
			}
			spremenjen_div = div;
			spremenjen_div2 = pozicije[zacetek];
			
			if(zacetek < konec){
				pozicije.splice(konec,1);
				novo_polje = new Array();
				
				for(i=0;i<zacetek;i++){
					novo_polje.push(pozicije[i]);
				}
				novo_polje.push(spremenjen_div);
				for(i=zacetek;i<konec;i++){
					novo_polje.push(pozicije[i]);
				}
				for(i=konec;i<pozicije.length;i++){
					novo_polje.push(pozicije[i]);
				}
				pozicije = novo_polje;
			}
			if(zacetek > konec){
				temp1 = zacetek;
				zacetek = konec;
				konec = temp1;
				pozicije.splice(zacetek,1);
				novo_polje = new Array();
				
				for(i=0;i<zacetek;i++){
					novo_polje.push(pozicije[i]);
				}
	
				for(i=zacetek;i<konec;i++){
					novo_polje.push(pozicije[i]);
				}
				novo_polje.push(spremenjen_div);
				for(i=konec;i<pozicije.length;i++){
					novo_polje.push(pozicije[i]);
				}
				pozicije = novo_polje;
			}

			$.ajax({        
			       type: "POST",
			       url: "Index/spremeniPozicije",
			       data: { pozicijeArray : pozicije }
			    }).done(function(msg)
			    {
			    	//alert(msg);
			    }); 
			    
		 }



  $("#vse1").sortable({
  handle: ".zasort", //hendlaš sam glede na class 'header'
placeholder:'placeholder',
    start: function(e, ui){
       ui.placeholder.height(ui.helper.outerHeight());//ui.placeholder.height(ui.item.height());
    },
  stop: function(event, ui) {
        var id = $(ui.item).attr("id");
       // alert("Pozicija: " + ui.item.index()+" "+id);
       mesta(ui.item.index(),id);
	}
  }); 
  $('.zasort').css('cursor', 'move');
  <?php } ?>
	
</script>
