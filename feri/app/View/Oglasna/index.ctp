<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
		<?php echo $this->Session->flash(); ?>
<div class='prva_stran' id='oglasna_deska'>
<div id='naslov'><div id='ime_naslova'>Oglasna deska</div>
<div id='isci'>
	<form method="get" action="<?php echo $this->Html->url(array("controller" => "oglasna","action" => "isci")); ?>">
		<input type='text' name='niz' />
		<input id='isci_button' type='submit' value='Išči' />
	</form>
</div>
    </div>
    <div id='vsi_oglasi'>
	<?php foreach ($deska as $oglas): ?>
		<div class='novica'>
		<div class='zacetek_novice'>
		<?php 
			echo $oglas['o']['naslov']." - ";
            echo $oglas['o']['avtor'];
			echo "<div class='plus'></div>";
			//echo $this->Html->image('plus.png', array('class' => 'plus'))
		?>
		</div>
		<div class='vsebina_oglasna'>
			<?php 
				echo strip_tags($oglas['o']['vsebina']);
				if($datoteke != null){
					echo "<ul>";
                    //izpišemo fajle
					foreach ($datoteke as $file){
						if($file['o']['id'] == $oglas['o']['id']){
							if(!empty($file['d']['ime']))
								echo "<li><a href='./files/Oglasna/".$oglas['o']['id']."/".$file['d']['ime']."'>".$file['d']['ime']."</a></li>";

						}
					}
					echo "</ul>";
				}
			?>
		</div>
		</div>
	<?php endforeach; ?>
          <?php 
          if(isset($stevilo_oglasov)){
        $stevilo = $stevilo_oglasov[0][0]['stevilo'];    //dobimo število useh novic iz baze
        $strani = ceil($stevilo / 10);  //zaokrožimo navzgor
        if($strani > 1){  
            echo "<div id='strani'>";
            for($i = 0; $i < $strani; $i++){
                echo "<a class='st_strani'  href='".$this->Html->url(array("controller"=>"oglasna","action"=>"index",$i+1))."' >".($i+1)."</a>";
            } 
            echo "</div>";
        }
		  }
        ?>
</div>
    <!--
<table id='tabela'>
	<tr>
		<th>Naslov</th><th>Smer</th><th>Letnik</th><th>Avtor</th><th>Datum</th>
	</tr>
	<?php foreach ($deska as $oglas): ?>
	 <tr>
	 	<td>
			<a href="<?php echo $this->Html->url(array("controller" => "Oglasna","action" => "poglej","?" => array("id" => $oglas['o']['id']))); ?>"><?php echo $oglas['o']['naslov']; ?></a>
		</td>
        <td>
			<a href="<?php echo $this->Html->url(array("controller" => "Oglasna","action" => "isci","?" => array("niz" => $oglas['c']['naziv']))); ?>"><?php echo $oglas['c']['naziv']; ?></a>
		</td>
        <td><?php echo $oglas['o']['letnik']; ?></td>
        <td><?php echo $oglas['o']['avtor']; ?></td>
        <td><?php echo $oglas['o']['datum']; ?></td>
    </tr>
     <?php endforeach; ?>
</table>
-->
</div>
</div>