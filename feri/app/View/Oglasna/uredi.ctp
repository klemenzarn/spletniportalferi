<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
	   <div id="vse_novice">
	        <?php foreach ($oglasi as $oglas): ?>
	        	<div id="novica">
	                <div id='naslov' style="text-overflow:ellipsis;"><?php echo $oglas['o']['naslov'];?></div>
	                <div id="podatki-novice">
	                        <?php
	                            $vsebina = strip_tags($oglas['o']['vsebina']);
	                            if(strlen($vsebina) > 500){
	                                $vsebina = substr($vsebina,0,500)."...";
	                            } 
								
								echo "<div id='podatki-oglasna'>";
	                            echo $vsebina;
							
								
									foreach ($datoteke as $file){
										if(!empty($file['d']['ime'])){
										echo "<ul>";
											if($file['o']['id'] == $oglas['o']['id']){
												echo "<li><a href='./files/Oglasna/".$oglas['o']['id']."/".$file['d']['ime']."'>".$file['d']['ime']."</a></li>";
											}
										echo "</ul>";
										}
									}
								echo "</div>";
								echo "<b>Avtor: </b>".$oglas['o']['avtor'] . " | <b>Objavljeno dne: </b>".date('d.m.Y, H:i', strtotime($oglas['o']['datum']));
								echo "<div class='uredi-izbrisi'>";
								echo "<div style='float: left; padding-left: 5px;'><a href='".$this->Html->url(array("controller"=>"oglasna","action"=>"uredinovico",$oglas['o']['id']))."'>".$this->Html->image('uredi.png')."</a></div>";
								echo "<div style='float: right; padding-left: 5px;'><a id='brisi_novico' href='".$this->Html->url(array("controller"=>"oglasna","action"=>"brisi",$oglas['o']['id']))."'>".$this->Html->image('brisi.png')."</a></div>";
								echo "</div>";    
	        ?>
	        		</div>
	        	</div>
	        
	        <?php endforeach; ?>
	  </div>
	
	        <?php
	        
	        $stevilo = $stnovic[0][0]['stevilo'];    //dobimo število useh novic iz baze
	        $strani = ceil($stevilo / 10);  //zaokrožimo navzgor
	        echo "<div id='strani'>";
	        for($i = 0; $i < $strani; $i++){
	            echo "<a class='st_strani' href='".$this->Html->url(array("controller"=>"oglasna","action"=>"uredi",$i+1))."' >".($i+1)."</a>";
	        } 
	        echo "</div>";
			     
/*echo "<pre>";
print_r($novice);
echo "</pre>S";
 */
	?>
</div>