<html>
	<head>
		<title>3D FERI</title>
		<?php echo $this->Html->css('webgl'); ?>
	</head>
	<body>
	
		<?php 
			echo $this->Html->script('jquery-1.8.2');
			echo $this->Html->script('jquery.cycle.lite');
			echo $this->Html->script('soundmanager2-jsmin');
		 ?>
</style>

		<script src="/feri/js/viewer/three.js"></script>
		
		<script src="/feri/js/viewer/TrackballControls.js"></script>
		<script src="/feri/js/viewer/MTLLoader.js"></script>
		<script src="/feri/js/viewer/OBJMTLLoader.js"></script>
		<script src="/feri/js/viewer/OBJLoader.js"></script>
		<script src="/feri/js/viewer/Detector.js"></script>
		<script src="/feri/js/viewer/PointerLockControls.js"></script>
		<script src="/feri/js/viewer/stats.min.js"></script>
		<script src="/feri/js/viewer/CSS3DRenderer.js"></script>

		<script src="http://stemkoski.github.io/Three.js/fonts/gentilis_bold.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/gentilis_regular.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/optimer_bold.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/optimer_regular.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/helvetiker_bold.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/helvetiker_regular.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/droid_sans_regular.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/droid_sans_bold.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/droid_serif_regular.typeface.js"></script>
		<script src="http://stemkoski.github.io/Three.js/fonts/droid_serif_bold.typeface.js"></script>
		<div id='loader'>
			<div id='loader_napis'>
		<div>NALAGANJE</div>
			    <div class="spinner">
      <div class="cube1"></div>
      <div class="cube2"></div>
    </div>
			<?php //echo $this->Html->image('loading.gif',array("width"=>"120")); ?>
			</div>
		</div>
		<div id='test'></div>
<div id="blocker">
    <div id="instructions">
            <span style="font-size:30px">KLIKNITE ZA ZAČETEK</span>
            <br />
          <span style="font-weight:normal;">(W, A, S, D, Miška = Premikanje, P - Naredi screenshot, Enter - Klepet)</span>
    </div>

</div>
<div id='klepet'>
<div style='text-align:center; font-family:arial; margin-top:10px;'>KLEPET</div>
<div style='margin-left:10px; margin-top:10px'><textarea id='sporoc' placeholder='Vpiši sporočilo' style='width:180px; height:50px; resize:none'></textarea></div>
<div id='chat-room'></div>
<div style="margin-left:10px">
<?php 
	if($this->Session->check('uporabnik')){
		echo "<div style='margin-top:5px; width:170px;'>Prijavljen kot: ".$this->Session->read('uporabnik')."</div>";
		echo $this->Form->create(null,array('url'=>array('controller'=>'webgl','action'=>'odjava')));
        echo "<input type='submit' value='Odjava' id='potrdi'/>";
		echo "</form>";
	} else {
		echo "<div style='width:170px; margin-top:5px;'>Prijavljen kot: Gost</div>";
		echo "<button id='prijava_b'>Prijava</button>";
	}

?>
</div>
</div>
<div id='helper'></div>

<div id='novo_okno'>
        <div id='opac'></div>
        <div id='opravljanje'>
                <div id="prijava">
                <h2>Prijava</h2>
               	<label id="error1"></label><br/>

                   <label>*Uporabniško ime:</label><input type="text" id="prijava_username" name="uporabnisko"/><br/>
                   <label>*Geslo:</label><input type="password"  id="prijava_password" name="geslo"/><br/>

                <input type="submit" class="vpis" id="prijava_b1" value="Vpiši se"/>
                 <input type="button" class="vpis" id="napravapic" value="Prijava z napravo" style='background-color:red;'/>
                <input type="button" class="vpis" id="register_b" value="Nimate še uporabniškega?"/>
                <a class="zapri" id="zapri_b1"></a>
           
                </div>
                
                <div id='registracija' style="visibility:hidden">
                  <h2>Registracija</h2>
                  <label id="error2"></label><br/>

                  <label>*Ime:</label><input type="text" id="registracija_name" name="ime"/><br/>
                  <label>*Priimek:</label><input type="text" id="registracija_surname" name="priimek"/><br/>
                  <label>*Uporabniško ime:</label><input type="text" id="registracija_username" name="uporabnisko1"/><br/>
                  <label>*Geslo:</label><input type="password" id="pass1" name="geslo1"/><br/>
                  <label>*Ponovno geslo:</label><input type="password" id="pass2" name="ponovno_geslo"/><br/>
                  <label>*Mail:</label><input type="text" id="mail" name="mail"/><br/>
                <input type="submit" value="Potrdi" class='vpis' id="registracija_b"/> 
               <a class="zapri1" id="zapri_b2"></a>

                </div>
        </div>
    </div>
<script>
$(document).ready(function(){
		
	$('#prijava_b').click(function(){

		$('#prijava').css("visibility","visible");
		$('#prijava').css("display","inline");
		$('#registracija').css("display","none");
		$('html').css("overflow", "hidden");
		
			//skrijemo scrollbar
			$('#novo_okno').css("display", "block");
			//pokažemo črno ozadje
			var vscroll = (document.all ? document.scrollTop : window.pageYOffset);
			$('#novo_okno').css("top", vscroll + "px");
			//na tej pa tej poziciji
			document.body.scroll = "no";
		});
		$("#opac").click(function() {
			$("#novo_okno").css("display", "none");
			$("html").css("overflow", "auto");
			document.body.scroll = "yes";
		});
	
		$("#zapri_b1").click(function() {
				$("#novo_okno").css("display", "none");
				$("html").css("overflow", "auto");
				document.body.scroll = "yes";
			});
			
		$("#zapri_b2").click(function() {
				$("#novo_okno").css("display", "none");
				$("html").css("overflow", "auto");
				document.body.scroll = "yes";
			});

				$('#register_b').click(function(){
			$("#registracija").css("visibility","visible");
			$("#prijava").css("display","none");
			$("#registracija").css("display","inline");
		});

		$("#napravapic").click(function(){
			//alert("KRNEKI");
			$.get('http://localhost/PICGESLO.txt', function(result) {
				if (result == 'ON') {
					alert('ON');
				} else if (result == 'OFF') {
					alert('OFF');
				} else {
					$( "#prijava_password" ).val(result);
					if($("#prijava_username").val()!=""){
						$( "#prijava_b1" ).click();
					}else{
						alert("Vpišite vaše uporabniško ime!");
					}
				}
			});
		});

		$('#prijava_b1').click(function(){
			var user = $('#prijava_username').val();
			var pas = $('#prijava_password').val();
						$.ajax({
			type : 'post',
			url : "/feri/index/prijava",
			data : "user="+user+"&pas="+pas,
			}).done(function(msg) {
				var test = msg.substr(0,4);
				if(test == "http"){
					window.location.href = msg;
				}else{
					$("#error1").empty();
					$("#error1").append(msg+"\n");
				}
			});
		});
		
		$('#registracija_b').click(function(){
			var name = $('#registracija_name').val();
			var surname =$('#registracija_surname').val();
			var username =$('#registracija_username').val();
			var pas1 = $('#pass1').val();
			var pas2 = $('#pass2').val();
			var mail =$('#mail').val();
			
						$.ajax({
			type : 'post',
			url : "/feri/webgl/registracija",
			data : "name="+name+"&surname="+surname+"&username="+username+"&pas1="+pas1+"&pas2="+pas2+"&mail="+mail,
			}).done(function(msg) {
			if(msg=="OK")
			{
				location.reload();
			}
			else
			{
				$("#error2").empty();
				$("#error2").append(msg+"\n");
			}
			});
		});


		$(window).load(function() {
			$('#loader').fadeOut(2000);
			var oglasna_iframe = $('#oglasna').contents().find('#save-picture');
			$(oglasna_iframe).click(function( event ) {
				setTimeout(osveziDCT, 2000);
			});
			var novice_iframe = $('#novice').contents().find('#save-picture');
			$(novice_iframe).click(function( event ) {
				setTimeout(osveziDCT, 2000);
			});
			var galerija_iframe = $('#galerija').contents().find('#save-picture');
			$(galerija_iframe).click(function( event ) {
				setTimeout(osveziDCT, 2000);
			});
			var urniki_iframe = $('#urniki').contents().find('#save-picture');
			$(urniki_iframe).click(function( event ) {
				setTimeout(osveziDCT, 2000);
			});
			var dctGalerija_iframe = $('#galerijadct').contents().find('#save-picture');
			$(dctGalerija_iframe).click(function( event ) {
				setTimeout(function (){
					document.getElementById('slike').src = document.getElementById('slike').src;
				}, 2000);
			});
		});
});

	function osveziDCT(){
		//console.log('update');
		document.getElementById('galerijadct').src = document.getElementById('galerijadct').src;
		document.getElementById('slike').src = document.getElementById('slike').src;
	}

	var id_render = 0;
	//http://164.8.212.106/feri/webgl
	var connection = new WebSocket("ws://164.8.212.106:8000");
	var pavza = 0;
	connection.onopen = function(e) {
		izpisi(e);
		updatePosition();
	};

	connection.onerror = function(e){
		izpisi(e.data);
	};

	var firstTime = true;

	connection.onmessage = function(e) {
		//izpisi(uporabniki);
		var data = JSON.parse(e.data);
		var najdeno = false;
		var index = 0;

		//če gre za chat...
		if(data[0] == 12){
			$("#chat-room").append("<span style='font-style:italic;font-weight:bold;'>"+data[1]+"</span>: "+data[2]+"<br />");
			$("#chat-room").scrollTop($("#chat-room").height());
			return;
		}
		
		for(var i=0;i<uporabniki.length;i++){
			if(uporabniki[i].uporabnik == data[0]){
				najdeno = true;
				index = i;
				break;
			}
		}
		if(!najdeno){
			addPeople(data[1],data[4],data[2], data[0], data[3]);
		} else {
			uporabniki[index].model.position.x = data[1];
			uporabniki[index].model.position.z = data[2];
			uporabniki[index].model.position.y = data[4];
			uporabniki[index].model.rotation.y = de2ra(180) + data[3];
			uporabniki[index].napis.position.x = data[1] ;//- (uporabniki[index].dolzinaNapisa / 2);
			uporabniki[index].napis.position.z = data[2];
			uporabniki[index].napis.position.y = data[4]+40;
			uporabniki[index].napis.rotation.y = de2ra(180) + data[3];
		}
	};

	var cube;
	var scene, camera, renderer;
	var controls;
	var rendererCSS,cssScene;
	var WIDTH = window.innerWidth - 200;
	var HEIGHT = window.innerHeight;
	var clock = new THREE.Clock();
	var loader, texture, floor_tex;
	var ballTexture;
	var json_neki = '<?php echo $uporabniki; ?>';
	var upor_data = JSON.parse(json_neki);
	var model_cloveka;
	var spotlight2;
	var tempY = 100;
	var tla = true;
	var pisanje = false;
	var prizgana = true;
	var pointLight;
	var vrata;
	var elevatorPosition = 30;
	var trenutnoNadstropje = 0;
	var prvic = true;
	var prvicnapisano = true;
	var dvigalo;
	var odprta = false;
	var drsnaVrata;
	var odprto = true;
	var uporabniki = new Array();
	var napisi = new Array();
	var spotLights = new Array();
	var arrayVrata = new Array();
	var vrataInfo = new Array();

	var time = Date.now();
    var blocker = document.getElementById( 'blocker' );
    var instructions = document.getElementById( 'instructions' );
    var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

    if ( havePointerLock ) {
            var element = document.body;
            var pointerlockchange = function ( event ) {
                    if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {
                            controls.enabled = true;
                            blocker.style.display = 'none';
                    } else {
                            controls.enabled = false;
                            blocker.style.display = '-webkit-box';
                            blocker.style.display = '-moz-box';
                            blocker.style.display = 'box';
                            instructions.style.display = '';
                    }
            }
            var pointerlockerror = function ( event ) {
                    instructions.style.display = '';
            }
            document.addEventListener( 'pointerlockchange', pointerlockchange, false );
            document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
            document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );
            document.addEventListener( 'pointerlockerror', pointerlockerror, false );
            document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
            document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );
            instructions.addEventListener( 'click', function ( event ) {
                    instructions.style.display = 'none';
                    element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;

                    if ( /Firefox/i.test( navigator.userAgent ) ) {
                            var fullscreenchange = function ( event ) {
                                    if ( document.fullscreenElement === element || document.mozFullscreenElement === element || document.mozFullScreenElement === element ) {
                                            document.removeEventListener( 'fullscreenchange', fullscreenchange );
                                            document.removeEventListener( 'mozfullscreenchange', fullscreenchange );
                                            element.requestPointerLock();
                                    }
                            }
                            document.addEventListener( 'fullscreenchange', fullscreenchange, false );
                            document.addEventListener( 'mozfullscreenchange', fullscreenchange, false );
                            element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
                            element.requestFullscreen();
                    } else {
                            element.requestPointerLock();
                    }

            }, false );
    } else {
            instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';
    }

	init();
	animate();

	
	function init(){
		//naredimo sceno
		scene = new THREE.Scene();

		//kamero
		camera = new THREE.PerspectiveCamera(45, WIDTH/HEIGHT,0.1,1000);
		camera.position.z = 10;
		camera.position.y = 10;

		//controle za premikanje po objektu
		controls = new THREE.PointerLockControls( camera );
        scene.add( controls.getObject() );
        controls.initialPosition(84,-364,30);

		//lights
		var ambient = new THREE.AmbientLight(0xbbbbbb);
		scene.add(ambient);

		pointLight = new THREE.PointLight(0xffffff);
		pointLight.intensity = 0.4;
   	    pointLight.position.set(34,100,252);
    	scene.add(pointLight);
		var lightTarget = new THREE.Object3D();
		lightTarget.position.set(34,0,252);
		scene.add(lightTarget);
		pointLight.target = lightTarget;

		renderer = new THREE.WebGLRenderer( {
			antialias:true,
			preserveDrawingBuffer: true 
		});

		renderer.setSize(WIDTH, HEIGHT);
		//renderer.shadowMapEnabled = true; 
		document.body.appendChild( renderer.domElement );
		
		makeVrata(-149,0,161,180,0);
		makeVrataVhod(63,0,-271,180,1);
		makeVrata(-149,0,-23,180,2);
		makeVrata(-149,0,360,180,3);
		//makeSpotLight(0xffffff, 0, 100, -40, 0.70, 1.0, true);
        makeSpotLight(0xffffff, -190, 100, 210, 0.70, 1.0, true);
        makeSpotLight(0xffffff, -320, 100, 210, 0.70, 1.0, true);
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( '/feri/objekti/male/male02.obj', '/feri/objekti/male/male02.mtl', function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					child.castShadow = true;
				}
			});
			object.scale = new THREE.Vector3(0.2,0.2,0.2);
			model_cloveka = object;
		} );

		addMTLObject('/feri/objekti/feri/feri.obj','/feri/objekti/feri/feri.mtl',0,100,10,5,false,true,0);
		makeDvigalo();
		makeDrsnaVrata();
		initHtml();
		
		rendererCSS	= new THREE.CSS3DRenderer({
			preserveDrawingBuffer: true 
		});
		rendererCSS.setSize( WIDTH, HEIGHT );
		rendererCSS.domElement.style.position = 'absolute';
		rendererCSS.domElement.style.top	  = 0;
		rendererCSS.domElement.style.margin	  = 0;
		rendererCSS.domElement.style.padding  = 0;
		document.body.appendChild( rendererCSS.domElement );
		renderer.domElement.style.position = 'absolute';
		renderer.domElement.style.top      = 0;
		renderer.domElement.style.zIndex   = 1;
		rendererCSS.domElement.appendChild( renderer.domElement );
	}

	function makeDvigalo(){
		//187,0,-184,3,false,true,-90
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( '/feri/objekti/dvigalo/dvigalo.obj','/feri/objekti/dvigalo/dvigalo.mtl', function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					//child.castShadow = true;
				}
			});
			object.scale = new THREE.Vector3(6.2,6.2,5.2);
			object.position.x = 330;
			object.position.z = -169;
			object.position.y = 0;
			object.rotation.y = de2ra(180);
			scene.add( object );
			dvigalo = object;
		} );
	}

	function makeSpotLight(barvaLuci, x, y, z, shadowDarkness, intensity, castShadow){
		var temp_light = new THREE.SpotLight( barvaLuci);
		temp_light.position.set(x,y,z);
		scene.add(temp_light);
		//temp_light.shadowCameraVisible = true;
		//temp_light.shadowDarkness = shadowDarkness;
		temp_light.intensity = intensity;
		//temp_light.castShadow = castShadow;
		var lightTarget = new THREE.Object3D();
		lightTarget.position.set(x,0,z);
		scene.add(lightTarget);
		temp_light.target = lightTarget;
		spotLights.push(temp_light);
		//console.log(temp_light);
	}

	function addIframeObject(link, width, height, pageWidth, angleY, x, y , z, id){
		var planeMaterial   = new THREE.MeshBasicMaterial({color: 0x000000, opacity: 0.1, side: THREE.DoubleSide });
		var planeWidth = width;
	    var planeHeight = height;
		var planeGeometry = new THREE.PlaneGeometry( planeWidth, planeHeight );
		var planeMesh= new THREE.Mesh( planeGeometry, planeMaterial );
		planeMesh.position.y += y+planeHeight/2;
		planeMesh.castShadow = true;
		planeMesh.position.x = x;
		planeMesh.position.z = z;
		planeMesh.rotation.y = de2ra(angleY);

		scene.add(planeMesh);

		var Htmlelement	= document.createElement('iframe');
		Htmlelement.src	= link;
		Htmlelement.id = id;
		var elementWidth = pageWidth;//1200;
		var aspectRatio = planeHeight / planeWidth;
		var elementHeight = elementWidth * aspectRatio;
		Htmlelement.style.width  = elementWidth + "px";
		Htmlelement.style.height = elementHeight + "px";
		var cssObject = new THREE.CSS3DObject( Htmlelement );
		cssObject.position = planeMesh.position;
		cssObject.rotation.y = planeMesh.rotation.y;
		var percentBorder = 0.05;
		cssObject.scale.x /= (1 + percentBorder) * (elementWidth / planeWidth);
		cssObject.scale.y /= (1 + percentBorder) * (elementWidth / planeWidth);
		cssScene.add(cssObject);
	}


	function initHtml(){
		cssScene = new THREE.Scene();
		addIframeObject('/feri/webgl/oglasna',40,20,760,-90,182,33,16,'oglasna');
		addIframeObject('/feri/webgl/novice',40,20,760,-90,182,33,158,'novice');
		addIframeObject('/feri/forum/webgl',30,15,810,0,-262,35,126,'');
		addIframeObject('http://www.youtube.com/embed/DCBXp9O4rls',35,17,760,0,-224,25,125,'');
		addIframeObject('/feri/urnik/webgl',40,20,810,-90,182,33,283,'urniki');
		addIframeObject('/feri/webgl/getvse',32,19,605,90,-68,37,-87,'slike');
		addIframeObject('/feri/galerija/indexwebgl',30,25,820,90,-89,30,87,'galerija');
		addIframeObject('/feri/webgl/galerijadct',30,25,820,90,-89,30,-10,'galerijadct');
		addIframeObject('/vremenskapostaja/index.php#trenutnoStanje',23,25,520,90,-89,30,204);
		addIframeObject('/feri/webgl/cpu',45,27,900,-90,-84.5,32.5,-88);
	}

	function makeDrsnaVrata(){
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( '/feri/objekti/drsnavrata/vitrina vrata.obj', '/feri/objekti/drsnavrata/vitrina vrata.mtl', function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					//child.castShadow = true;
				}
			});
			object.scale = new THREE.Vector3(16,13.5,14);
			object.position.x = 47;
			object.position.z = -235;
			object.position.y = 0;
			object.rotation.y = de2ra(0);
			scene.add( object );
			drsnaVrata = object;
		} );
	}

	function makeVrataVhod(x, y, z, rotation,index){
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( '/feri/objekti/vrataZunaj/vrataZunaj.obj', '/feri/objekti/vrataZunaj/vrataZunaj.mtl', function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					//child.castShadow = true;
				}
			});
			object.scale = new THREE.Vector3(16,13.5,14);
			object.position.x = x;//84;
			object.position.z = z;//-364;
			object.position.y = y;//0;
			object.rotation.y = de2ra(rotation);
			scene.add( object );
			arrayVrata[index] = object;
			//arrayVrata.push(object);
			vrataInfo.push(true);
		} );
	}

	function makeVrata(x, y, z, rotation,index){
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( '/feri/objekti/vrata/vrata.obj', '/feri/objekti/vrata/vrata.mtl', function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					//child.castShadow = true;
				}
			});
			object.scale = new THREE.Vector3(0.3,0.3,0.3);
			object.position.x = x;//84;
			object.position.z = z;//-364;
			object.position.y = y;//0;
			object.rotation.y = de2ra(rotation);
			scene.add( object );
			arrayVrata[index] = object;
			//arrayVrata.push(object);
			vrataInfo.push(true);
		} );
	}

	//funkcija doda OBJ object z MTL datoteko v katero so shranjeni vsi pomembi podatki o materialih (teksturah)
	function addMTLObject(imeObj, imeMtl, x, y, z, scale, castShadow, receiveShadow, angleY){
		var loader_mtl = new THREE.OBJMTLLoader();
		loader_mtl.load( imeObj, imeMtl, function ( object ) {
			var object = object;
			object.traverse(function(child) {
				if ( child instanceof THREE.Mesh) {
					//child.castShadow = castShadow;
					//child.receiveShadow = receiveShadow;
				}
			});
			object.scale = new THREE.Vector3(scale,scale,scale);
			object.position.x = x;
			object.position.z = z;
			object.position.y = y;
			object.rotation.y = de2ra(angleY);
			scene.add( object );
			output = object;
		} );
	}

	//funkcija ki doda uporabnika v polje, če le ta še ne obstaja.
	function addPeople(x, y, z, uporabnisko, rotation){
		//izpisi(uporabnisko);
		var materialFront = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
		var materialSide = new THREE.MeshBasicMaterial( { color: 0x000088 } );
		var materialArray = [ materialFront, materialSide ];
		var textGeom = new THREE.TextGeometry(uporabnisko, 
		{
			size: 5, height: 2, curveSegments: 1,
			font: "helvetiker", weight: "normal", style: "normal",
			bevelThickness: 0.1, bevelSize: 2, bevelEnabled: false,
			material: 0, extrudeMaterial: 1
		});
		var textMaterial = new THREE.MeshFaceMaterial(materialArray);
		var textMesh = new THREE.Mesh(textGeom, textMaterial );
		
		textGeom.computeBoundingBox();
		var textWidth = textGeom.boundingBox.max.x - textGeom.boundingBox.min.x;
		textMesh.rotation.y = rotation;
		textMesh.position.set(x - textWidth / 2,y+4+4,z );
		if(model_cloveka != undefined){
			var mesh = model_cloveka.clone();
			//var mesh = new THREE.Mesh( model_cloveka.geometry, model_cloveka.material );
			mesh.position.set( x,y+4,z );
			mesh.castShadow = true;
			mesh.rotation.y = rotation;
		    scene.add(mesh);
			var najdeno = false;
			for(var i=0;i<uporabniki.length;i++){
				if(uporabniki[i].uporabnik == uporabnisko){
					najdeno = true;
					break;
				}
			}
			if(!najdeno){
				var objekt = {
					uporabnik:uporabnisko,
					model:mesh,
					napis:textMesh,
					dolzinaNapisa:textWidth
				}
				uporabniki.push(objekt);
			}
			scene.add(textMesh);
		}
	}

	function animate(){
		render();
	}

	function drsniVrata(){
		var position=0;
	  	var interval = setInterval(function(){
		    if (position >= 30) {
		        clearInterval(interval);
		        odprta = true;
		    } else {
			    drsnaVrata.position.x += 0.5;
			    position += 0.5;
		    }
	  	}, 10);
	}

	function zapriDrsna(){
		var position=30;
	  	var interval = setInterval(function(){
		    if (position <= 0) {
		        clearInterval(interval);
		    } else {
			    drsnaVrata.position.x -= 0.5;
			    position -= 0.5;
		    }
	  	}, 10);
	}

	function render() {
		var cordX = controls.getX();
		var cordZ = controls.getZ();
		var cordY = controls.getY();
		if(cordX > -215 && cordX < -99 && cordZ > 128 && cordZ < 160){
			if(prvicnapisano){
				$('#helper').addClass('ozadje-helper');
				$('#helper').html('Kliknite O, da se vam odprejo/zaprejo vrata.');
				prvicnapisano=false;
			}
		} else if(cordX > 28 && cordX < 63 && cordZ > -340 && cordZ < -220) {
			if(prvicnapisano){
				$('#helper').addClass('ozadje-helper');
				$('#helper').html('Kliknite O, da se vam odprejo/zaprejo vrata.');
				prvicnapisano=false;
			}
		} else if(cordX > 321 && cordX < 360 && cordZ > -181 && cordZ < -157){
			if(prvicnapisano){
				$('#helper').addClass('ozadje-helper');
				$('#helper').html('Kliknite 0, 1, 2 ali 3, da se peljete z dvigalom v izbrano nadstropje.');
				prvicnapisano=false;
			}
		} else {
			if(prvicnapisano == false){
				$('#helper').html('');
				$('#helper').removeClass('ozadje-helper');
				prvicnapisano = true;
			}
		}
//$("#test").html("X: "+controls.getX()+", Z: "+controls.getZ()+", Y: "+controls.getY());
		if(cordX > 28 && cordX < 63 && cordZ > -280 && cordZ < -160){
			if(prvic){
				drsniVrata();
				prvic = false;
			}
		} else {
			prvic = true;
			if(odprta == true){
				odprta = false;
				zapriDrsna();
			}
		}

		id_render = requestAnimationFrame(render);     
        controls.update( Date.now() - time );
		rendererCSS.render( cssScene, camera );
		renderer.render(scene, camera);
		time = Date.now();
	}

	//funkcija pretvori stopinje v radiane
	function de2ra(degree){
		return degree*(Math.PI/180);
	}

	//funkcija pretvori radiane v stopinje
	function ra2de(degree){
		return degree/(Math.PI/180);
	}

	function izpisi(msg){
		console.log(msg);
	}

	var uporabnik = "<?php echo $this->Session->check('uporabnik') == true ? $this->Session->read('uporabnik') : 'Gost'; ?>";

	//funkcija pošlje na server informacijo o tem, kje se uporabnik nahaja
	function updatePosition(){
		var output_array = new Array(uporabnik,controls.getX(),controls.getZ(),controls.getRotation(),controls.getY()-30);
		connection.send(JSON.stringify(output_array));
		setTimeout(updatePosition,50);
	}

	function sendMsg(){
		var sporocilo = $("#sporoc").val();
		$("#chat-room").append("<span style='font-style:italic;font-weight:bold;'>"+uporabnik+"</span>: "+sporocilo+"<br />");
		$("#chat-room").scrollTop($("#chat-room").height());
		var output_array = new Array(12,uporabnik,sporocilo);
		connection.send(JSON.stringify(output_array));
	}

	$(document).keydown(function (e) {
		if (e.keyCode == 87 || e.keyCode==83 || e.keyCode==65 || e.keyCode==68) {
			if(soundManager.getSoundById('koraki').playState==1){
				soundManager.resume('koraki');
			}
			else{
				if(soundManager.getSoundById('koraki').playState==0){
					soundManager.start('koraki');
				}
			}
		}
	});

	$(document).keyup(function (e) {
		if (e.keyCode == 87 || e.keyCode==83 || e.keyCode==65 || e.keyCode==68) {
			//console.log("STATE:: "+soundManager.getSoundById('koraki').playState + " PAVZA: "+pavza);
			if(soundManager.getSoundById('koraki').playState==1){
				soundManager.pause('koraki');
				pavza==1;
			} else {
				pavza==0;
			}
		}
	});

	soundManager.setup({
		url: '',
		onfinish: function() {
		//alert('The sound '+this.id+' finished playing.');
		},
		onready: function(){
			var mySound = soundManager.createSound({
				id: 'koraki',
				url: '/feri/objekti/korak.mp3'
			});
			var mySound2 = soundManager.createSound({
				id: 'prizgi',
				url: '/feri/objekti/prizgi.mp3'
			});
			var mySound3 = soundManager.createSound({
				id: 'ugasni',
				url: '/feri/objekti/ugasni.mp3'
			});
			var mySound4 = soundManager.createSound({
				id: 'dvigalo',
				url: '/feri/objekti/dvigalo2.mp3'
			});
			var mySound5 = soundManager.createSound({
				id: 'vrata',
				url: '/feri/objekti/vrata.mp3'
			});
		}
	});

	//funkcija odpre specifična vrata glede na stopinje in index vrat
	function odpriVrata(index, stopinje){
		console.log(arrayVrata[index]);
		var degree=0;
	  	var interval = setInterval(function(){
		    if (degree >= 90) {
		        clearInterval(interval);
		    } else {
			    arrayVrata[index].rotation.y += de2ra(stopinje);
			    degree += 1;
		    }
	  	}, 10);
	}

	//funkcija zapre specifična vrata glede na stopinje in index vrat
	function zapriVrata(index,stopinje){
		console.log(arrayVrata[index]);
		var degree=90;
	  	var interval = setInterval(function(){
		    if (degree <= 0) {
		    	soundManager.play('vrata');
		        clearInterval(interval);
		    } else {
			    arrayVrata[index].rotation.y += de2ra(stopinje);
			    degree -= 1;
		    }
	  	}, 10);
	}

	//funkcija premakno dvigalo navzdol glede na naslednjo pozicijo kam mora dvigalo iti (nadstropje)
	function elevatorDown(nextPosition){
		var i=elevatorPosition;
	  	var interval = setInterval(function(){
		    if (i <= nextPosition) {
		        clearInterval(interval);
		        elevatorPosition = i;
		        soundManager.stop('dvigalo');
		    } else {
		    	controls.getObject().position.y = i;
		    	dvigalo.position.y = i-30;
			    i-=0.5;
		    }
	  	}, 10);
		controls.isOnObject( true );
	}

	//funkcija premakne dvigalo navzgor glede na naslednjo pozicijo kam mora dvigalo iti (nadstropje)
	function elevatorUp(nextPosition) {
		var i = elevatorPosition;
	  	var interval = setInterval(function(){
		    if (i >= nextPosition) {
		        clearInterval(interval);
		        elevatorPosition = i;
		        soundManager.stop('dvigalo');
		    } else {
			    controls.getObject().position.y = i;
			    dvigalo.position.y = i-30;
			    i += 0.5;
		    }
	  	}, 10);
		controls.isOnObject( true );
	}

	window.addEventListener("keypress",function(e){
		//izpisi(e.which);
		if($("#sporoc").is(":focus") || $("#prijava_username").is(":focus") || $("#prijava_password").is(":focus") || $("#registracija_name").is(":focus") || $("#registracija_surname").is(":focus") || $("#registracija_username").is(":focus") || $("#pass1").is(":focus") || $("#pass2").is(":focus") || $("#mail").is(":focus")){
			pisanje = true;
		}else{
			pisanje = false;
		}

		var cordX = controls.getX();
		var cordZ = controls.getZ();
		var cordY = controls.getY();

		switch(e.which){
			//o -> vrata
			case 111:{
				if(pisanje == false){
					var index = 0;
					var vObmocju = false;
					if(cordX > -215 && cordX < -99 && cordZ > 128 && cordZ < 160){
						index = 0;
						vObmocju = true;
					} else if(cordX > 28 && cordX < 63 && cordZ > -340 && cordZ < -220) {
						index = 1;
						vObmocju = true;
					}

					if(vObmocju == true){
						if(vrataInfo[index] == true){
							vrataInfo[index] = false;
							soundManager.play('vrata');
							odpriVrata(index,-1);
						} else {
							vrataInfo[index] = true;
							zapriVrata(index,1);
						}	
					}
				} else {
					
				}
			}
			//pritličje
			case 48:{
				if(cordX > 321 && cordX < 360 && cordZ > -181 && cordZ < -157){
					if(pisanje == false){
						e.preventDefault();
						if(trenutnoNadstropje > 0){
							soundManager.play('dvigalo');
							elevatorDown(30); 
						} else {

						}
						trenutnoNadstropje = 0;
					}
				} 		
				break;
			}
			// prvo nadstropje
			case 49:{
				if(cordX > 321 && cordX < 360 && cordZ > -181 && cordZ < -157){
					if(pisanje == false){
						e.preventDefault();
						if(trenutnoNadstropje > 1){
							soundManager.play('dvigalo');
							elevatorDown(104);
						} else if (trenutnoNadstropje < 1){
							soundManager.play('dvigalo');
							elevatorUp(104);
						} else {

						}
						trenutnoNadstropje = 1;
					}
				} 		
				break;
			}
			//drugo nadstropje
			case 50:{
				if(cordX > 321 && cordX < 360 && cordZ > -181 && cordZ < -157){
					if(pisanje == false){
						e.preventDefault();
						if(trenutnoNadstropje > 2){
							soundManager.play('dvigalo');
							elevatorDown(177);
						} else if (trenutnoNadstropje < 2){
							soundManager.play('dvigalo');
							elevatorUp(177);
						} else {

						}
						trenutnoNadstropje = 2;
					}
				} 		
				break;
			}
			//tretje nadstropje
			case 51:{
				if(cordX > 321 && cordX < 360 && cordZ > -181 && cordZ < -157){
					if(pisanje == false){
						e.preventDefault();
						if(trenutnoNadstropje < 3){
							soundManager.play('dvigalo');
							elevatorUp(249);
						} else {

						}
						trenutnoNadstropje = 3;
					}
				} 		
				break;	
			}
			// enter -> za pisanje
			case 13:{
				e.preventDefault();
				if($("#sporoc").is(":focus")){
					sendMsg();
					$("#sporoc").val("");
					if(id_render != 0)
						requestAnimationFrame(render);
					$("#sporoc").blur();
				} else {
					if(id_render != 0)
						cancelAnimationFrame( id_render );
					$("#sporoc").focus();
				}
				break;
			}
			// p -> print screen
			case 112:{
				if(pisanje != true){
					var imgData = renderer.domElement.toDataURL();    
					izpisi(imgData);  
			        $.ajax({        
			        	type : "post",
				    	url: "webgl/shraniSliko",
				    	data: "image="+imgData
				    }).done(function(msg){
				    	console.log(msg);
				    	osveziDCT();
				   	}); 
				}
				break;
			}
			// l -> prižiganje in ugašanje luči
			case 108:{
				if(pisanje != true){
					if(prizgana){
						soundManager.play('ugasni');
						prizgana = false;
						for(var i=0;i<spotLights.length;i++){
							scene.remove(spotLights[i]);
						}
					}else{
						soundManager.play('prizgi');
						prizgana = true;
						for(var i=0;i<spotLights.length;i++){
							scene.add(spotLights[i]);
						}
					}
				}
				break;
			}
		}
	});
</script>
	</body>
</html>