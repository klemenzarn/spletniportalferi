<?php if($this->Session->check('uporabnik')){ ?>
<style>
	#fullview {

	}
	ul {
	}

	li {
		display: inline;
	}
	li img {
		width: 100px;
		cursor: pointer;
	}
	.izbrisislika{
		float:right;
	}
</style>

<?php
echo $this -> Html -> script('jquery-1.8.2');
$pot = 'img/Server/'.$this->Session->read('uporabnik').'/dct/';
$dir = new Folder($pot);

$files = $dir -> find('.*\.png');

?>
<script type="text/javascript">
	$(document).ready(function() {
		$('img').click(function() {
			if ($(this).attr('id') != 'izbrisi') {
				var src = $(this).attr('src');
				$('#fullview').attr('src', src);
			}
		});
		$('#izbrisi').click(function(){
			var slika = $('#fullview').attr('src').split('/');
			window.location='http://localhost/feri/webgl/brisi/'+slika[6];
		});
	}); 
</script>
<h1>Oglasna deska<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this -> Html -> image('izbrisi.png', array('id' => 'izbrisi', 'width' => '30', 'height' => '30', 'class'=>'izbrisislika')); ?></a></h1>
<hr />
<img id="fullview" width='720' src="<?php echo "/feri/".$pot.$files[0] ?>">
<ul>
	<?php
	foreach ($files as $file) {
		echo "<li>" . $this -> Html -> image('Server/'.$this->Session->read('uporabnik').'/dct/' . $file) . "</li>";
	}
	?>
</ul>
<?php
} else{
  echo "Za ogled lastne galerije morate biti prijavljeni!";
}
 ?>