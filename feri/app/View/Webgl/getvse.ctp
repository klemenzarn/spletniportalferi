<?php 
echo $this->Html->script('jquery-1.8.2');	
echo $this->Html->css('bjqs'); 
echo $this->Html->css('demo'); 
echo $this->Html->script('bjqs-1.3.min');
?>
<style type="text/css">
body,html{
	margin: 0;
	padding: 0;
	background-color:white;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#banner-fade').bjqs({
    animtype      : 'slide',
    height        : 280,
    width         : 550,
    nexttext : 'Naprej',
	prevtext : 'Nazaj'
  });
});
</script>
<h1 style='margin-bottom:0;'>Zaslonski posnetki</h1>
<?php
if(isset($slike)){
	if($slike == false){
		echo "Niste še naredili zaslonskih posnetkov.";
	}else {
		if(count($slike) == 1){
				echo "<img class='slikca' src='".$slike."' width='550' />";
			} else{ ?>
		 <div id="banner-fade">
	        <ul class="bjqs">
		<?php
			
		
			foreach ($slike as $key => $value) {
				echo "<li><img class='slikca' src='".$value."' width='550' /></li>";
			}
		}
	}
} else {
	echo "Za ogled vaših zaslonskih posnetkov se prosimo prijavite v sistem!";
}

?>
 </ul>
</div> 