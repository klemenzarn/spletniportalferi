<!DOCTYPE html>
<html>
<head>
	<title>Oglasna</title>
  <?php echo $this->Html->css('cake.generic'); ?>
	<?php echo $this->Html->script('jquery-1.8.2'); ?>
	<?php echo $this->Html->script('html2canvas'); ?>
	<?php echo $this->Html->script('jquery.plugin.html2canvas'); ?>
	<style type="text/css">
    body{
      background-color: white;
    }
    body,html{
      margin: 0;
      padding: 0;
    }
    #screenshot{
      background-color: white;
      height:100px;
    }
  </style>
</head>
<body>
  <div id='screenshot'>
<h1>Oglasna deska<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this->Html->image('save.jpg', array("width"=>"45")); ?></a></h1>
<hr />
    <div id='vsi_oglasi' style='float:left;'>
  <?php foreach ($deska as $oglas): ?>
    <div class='novica'>
    <div class='zacetek_novice'>
    <?php 
      echo $oglas['o']['naslov']." - ";
            echo $oglas['o']['avtor'];
      echo "<div class='plus'></div>";
      //echo $this->Html->image('plus.png', array('class' => 'plus'))
    ?>
    </div>
    <div class='vsebina_oglasna'>
      <?php 
        echo strip_tags($oglas['o']['vsebina']);
        if($datoteke != null){
          echo "<ul>";
                    //izpišemo fajle
          foreach ($datoteke as $file){
            if($file['o']['id'] == $oglas['o']['id']){
              if(!empty($file['d']['ime']))
                echo "<li><a href='./files/Oglasna/".$oglas['o']['id']."/".$file['d']['ime']."'>".$file['d']['ime']."</a></li>";

            }
          }
          echo "</ul>";
        }
      ?>
    </div>
    </div>
  <?php endforeach; ?>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $(".plus").click(function(){
        if($(this).hasClass("minus"))
        { 
                    //$(this).parent().parent().find(".zacetek_novice").css("font-weight","normal");
          $(this).parent().parent().find(".vsebina_oglasna").slideUp();         
        }
        else
        { 
                    //$(this).parent().parent().find(".zacetek_novice").css("font-weight","bold");
          $(this).parent().parent().find(".vsebina_oglasna").slideDown(); 
        }
        $(this).toggleClass("minus"); //classu slika dodamo oz "zamenjamo" z slika2
      });

  $("#save-picture").click(function(){
    html2canvas($("body"),{
      background:'#ff0000',
      onrendered: function (canvas) {
        
        var image = canvas.toDataURL("image/jpg");
        
        //$('body').append(canvas);
        console.log(image);
        $.ajax({        
              type : "post",
              url: "<?php echo $this->Html->url(array('controller'=>'webgl','action'=>'shraniSliko')); ?>",
              data: "image="+image
            }).done(function(msg){
              console.log(msg);
            }); 
      }
    });
  });
});
</script>
</body>
</html>


