<!DOCTYPE html>
<html>
<head>
	<title>Oglasna</title>
	<?php echo $this->Html->script('jquery-1.8.2'); ?>
    <?php echo $this->Html->script('jquery-ui-1.9.0.custom.min'); ?>
  <?php echo $this->Html->script('jquery-ui-tabs-rotate'); ?>
	<?php echo $this->Html->script('html2canvas'); ?>
	<?php echo $this->Html->script('jquery.plugin.html2canvas');

   ?>

	<style type="text/css">
    body{
      background-color: white;
    }
    body,html{
      margin: 0;
      padding: 0;
    }
    #featured{ 
  width:500px; 
  padding-right:250px; 
  position:relative; 
  height:250px;
  margin-top:35px;
  background:#fff;
}
#featured ul.ui-tabs-nav{ 
  position:absolute; 
  top:0; left:500px; 
  list-style:none; 
  padding:0; margin:0; 
  width:258px; 
}
#featured ul.ui-tabs-nav li{ 
  padding:1px 0; padding-left:13px;  
  font-size:12px; 
  color:#666; 
}
#featured ul.ui-tabs-nav li span{ 
  font-size:11px; font-family:Verdana; 
  line-height:18px; 
}
#featured .ui-tabs-panel{ 
  width:500px; height:250px; 
  background:#999; position:relative;
        overflow:hidden;
}
#featured .ui-tabs-hide{ 
  display:none; 
}
#featured li.ui-tabs-nav-item a{/*On Hover Style*/ 
  display:block; 
  height:60px; 
  color:#333;  background:#fff; 
  line-height:20px;
  outline:none;
}
#featured li.ui-tabs-nav-item a:hover{ 
  background:#f2f2f2; 
}
#featured li.ui-tabs-selected{ /*Selected tab style*/
  background:url('images/selected-item.gif') top left no-repeat;  
}
#featured ul.ui-tabs-nav li.ui-tabs-selected a{ 
  background:#ccc; 
}
#featured ul.ui-tabs-nav li img{ 
  float:left; margin:2px 5px; 
  background:#fff; 
  padding:2px; 
  border:1px solid #eee;
}
#featured .ui-tabs-panel .info{ 
  position:absolute; 
  top:190px; left:0; 
  height:60px; width: 500px;
  background: url('../images/transparent-bg.png'); 
}
#featured .info h2{ 
  font-size:18px; font-family:Georgia, serif; 
  color:#fff; padding:5px; margin:0;
  overflow:hidden; 
}
#featured .info p{ 
  margin:0 5px; 
  font-family:Verdana; font-size:11px; 
  line-height:15px; color:#f0f0f0;
}
#featured .info a{ 
  text-decoration:none; 
  color:#fff; 
}
#featured .info a:hover{ 
  text-decoration:underline; 
}
#featured li.ui-tabs-active{ 
  background:url('../images/selected-item.gif') top left no-repeat;  
}
#featured ul.ui-tabs-nav li.ui-tabs-active a{ 
  background:#ccc; 
}
/*novica*/
div.section div:nth-of-type(even)
{
    color: Green;
}
div.section div:nth-of-type(odd)
{
    color: Red;
}
.short_div:nth-of-type(odd){
    float:left;
    margin: 0 7px 15px 0;
    position:relative;    position:relative;    position:relative;
    width: 370px;
    border: 1px solid #888;
    box-shadow: 0 2px 10px  #888;
}

.short_div:nth-of-type(even){
    float:left;
    margin: 0 0 15px 7px;
    position:relative;    position:relative;    position:relative;
    width: 370px;
    border: 1px solid #888;
    box-shadow: 0 2px 10px  #888;
}
.slika_novice{
    overflow:hidden;
    height:220px;
    float:left;
}
.konec_texta{
    white-space:nowrap; 
    overflow:hidden; 
}
.short_div .info{ 
    position:absolute;
    top:196px;
    left:0;
    color:white;
  height:60px;
    width: 370px;
  background: url('../images/transparent-bg.png'); 
}
.short_div .info div{
    padding: 5px;   
}
    #screenshot{
      background-color: white;
    }
  </style>
</head>
<body>
  <div id='screenshot'>
<h1>Novice<a style="margin-left:10px" href="#" id='save-picture'><?php echo $this->Html->image('save.jpg', array("width"=>"45")); ?></a></h1>
<hr />
            <div id="featured" >
                <ul class="ui-tabs-nav">
                 <?php
                //sliko moramo spremeniti v 500px pri nalaganju slike!!!!!!!!!
                
                      //skrajšujem naslove novic za prikazovanje v slideshowu
                $naslov = array();
                for($i = 0; $i < 4; $i++){
                  if(strlen($novice[$i]['n']['naslov']) > 40){
                    $naslov[] = substr($novice[$i]['n']['naslov'],0,40)."...";
                  } else {
                    $naslov[] = $novice[$i]['n']['naslov'];
                  }
                }
                ?>  
                    <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[0];?></span></a></li>
                    <li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika']);    ?>"  width="80px" height="50px" alt="" /><span><?php echo $naslov[1];?></span></a></li>
                    <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[2];?></span></a></li>
                    <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><img src="<?php echo $this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika']);    ?>" width="80px" height="50px" alt="" /><span><?php echo $naslov[3];?></span></a></li>
                
                  </ul>
          
                <!-- First Content -->
                <div id="fragment-1" class="ui-tabs-panel" style="">
                <?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[0]['n']['id']."/".$novice[0]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id'])),array("escape"=>false)); ?>
                 <div class="info" >
                  <h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[0]['n']['id']))); ?>" ><?php echo $novice[0]['n']['naslov'];?></a></h2>
                  <p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[0]['n']['vsebina']);?></p>
                 </div>
                </div>
          
                <!-- Second Content -->
                <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">
                <?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[1]['n']['id']."/".$novice[1]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id'])),array("escape"=>false)); ?>
                 <div class="info" >
                  <h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[1]['n']['id']))); ?>" ><?php echo $novice[1]['n']['naslov'];?></a></h2>
                  <p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[1]['n']['vsebina']);?></p>
                 </div>
                </div>
          
                <!-- Third Content -->
                <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">
                <?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[2]['n']['id']."/".$novice[2]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id'])),array("escape"=>false)); ?>
                 <div class="info" >
                  <h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[2]['n']['id']))); ?>" ><?php echo $novice[2]['n']['naslov'];?></a></h2>
                  <p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[2]['n']['vsebina']);?></p>
                     </div>
                </div>
          
                <!-- Fourth Content -->
                <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">
                <?php echo $this->Html->link("<img src='".$this->Html->url('/files/Novice/'.$novice[3]['n']['id']."/".$novice[3]['n']['naslovna_slika'])."' width='500px' />",array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id'])),array("escape"=>false)); ?>
                 <div class="info" >
                  <h2 class='konec_texta' style="text-overflow:ellipsis;"><a href="<?php echo $this->Html->url(array("controller"=>"novice","action"=>"poglej","?"=>array("id"=>$novice[3]['n']['id']))); ?>" ><?php echo $novice[3]['n']['naslov'];?></a></h2>
                  <p class='konec_texta' style="text-overflow:ellipsis;"><?php echo strip_tags($novice[3]['n']['vsebina']);?></p>
                     </div>
                </div>
          
          </div>
<script type="text/javascript">
$(document).ready(function(){
  $("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);  
  $("#featured").hover(function() {  
      $("#featured").tabs("rotate",0,true);  
  },  
  function(){  
      $("#featured").tabs("rotate",5000,true);  
  }  
  );  


  $("#save-picture").click(function(){
    html2canvas($("#screenshot"),{
      background:'#ff0000',
      onrendered: function (canvas) {
        
        var image = canvas.toDataURL("image/jpg");
        //$('body').append(canvas);
        console.log(image);
        $.ajax({        
              type : "post",
              url: "<?php echo $this->Html->url(array('controller'=>'webgl','action'=>'shraniSliko')); ?>",
              data: "image="+image
            }).done(function(msg){
              console.log(msg);
            }); 
      }
    });
  });
});
</script>
</body>
</html>


