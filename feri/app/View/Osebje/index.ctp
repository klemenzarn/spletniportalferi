<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	
<div id="osebje-iskalnik">
<div id="naslov">Osebje</div>
<div id="podatki-iskanje-osebje">
<form method="get">
	<table>
		<tr>
			<td><b>Iskanje po:</b></td>
			<td>
			<select name="iskanje-po">
				<?php
				if (isset($this -> request -> query['iskanje-po'])) {
					$iskanjePo = $this -> request -> query['iskanje-po'];

					if ($iskanjePo == 'Ime') {
						echo '<option selected="selected">Ime</option>';
						echo '<option>Priimek</option>';
						echo '<option>Kabinet</option>';
					} else if ($iskanjePo == 'Priimek') {
						echo '<option>Ime</option>';
						echo '<option selected="selected">Priimek</option>';
						echo '<option>Kabinet</option>';
					} else if ($iskanjePo == 'Kabinet') {
						echo '<option>Ime</option>';
						echo '<option>Priimek</option>';
						echo '<option selected="selected">Kabinet</option>';
					} else if ($iskanjePo == 'Predmeti') {
						echo '<option>Ime</option>';
						echo '<option>Priimek</option>';
						echo '<option>Kabinet</option>';

					}
				} else {
					echo '<option selected="selected">Ime</option>';
					echo '<option>Priimek</option>';
					echo '<option>Kabinet</option>';
				}
				?>
			</select></td>
			<td>
			<input type="text" name="iskanje-opis" class="iskanje"/>
			</td>
			<td>
			<input type="submit" name="poisci" value="Išči" id="potrdi"/>
			</td>
		</tr>
	</table>
</form>
</div>
</div>
<div id="zaposleni">

<?php
if (isset($this -> request -> query['iskanje-opis'])) {
	$iskaniNiz = strtoupper($this -> request -> query['iskanje-opis']);
	$dolNiza = strlen($iskaniNiz);
	$najden = 0;


	switch($iskanjePo) {
		case 'Ime' :
			$pogoj = 'Ime';
			break;

		case 'Priimek' :
			$pogoj = 'Priimek';
			break;

		case 'Kabinet' :
			$pogoj = 'Kabinet';
			break;
	}

	foreach ($osebje as $oseba) {
        
		// BRUTE FORCE ISKANJE
			$original = $oseba[$pogoj];
			$dolOriginala = strlen($original);

			for ($j = 0; $j <= $dolOriginala - $dolNiza; $j++) {
				for ($i = 0; $i < $dolNiza && $iskaniNiz[$i] == $original[$i + $j]; $i++) {
					if ($i == $dolNiza - 1) {
						$index = $j;
					}
				}
			}		
		
		if (isset($index)) { // Preverimo, če smo našli enak vzorec v originalu
				if ($index != -1) {
                    ?>
                    <div id="zaposlen"> <!--//CSS: oglasna_deska -> zaposlen, vsi_oglasi->podatki-->
                    <div id="naslov"><?php echo $oseba['HabilitacijskiNazivKratki'] . " " . $oseba['AkademskiNaziv'] . " <b>" . $oseba['Ime'] . " " . $oseba['Priimek'] . "</b>, " . $oseba['SkrajsaniPoklic']; ?></div>
                    <div id="podatki_osebe">
                    <table style='width: 740px; border-collapse: collapse;'>
                    	<tr>
                    		<td><b>Kabinet:</b></td>
                    		<td>
                    			<?php
								if ($oseba['Kabinet'] != NULL) {
									echo $oseba['Kabinet'];
								} else {
									echo "/";
								}
                    			?>
                    			</td>
                    	</tr>
                    	<tr>
                    		<td><b>Govorilne ure:</b></td>
                    		<td>
                    			<?php
								if ($oseba['GovorilneUre'] != NULL) {
									echo $oseba['GovorilneUre'];
								} else {
									echo "/";
								}
                    			?>
                    		</td>
                    	</tr>
                    	<tr>
                    		<td><b>e-Pošta:</b></td>
                    		<td>
                    			<?php if ($oseba['Email'] != NULL) { ?>
								<a href="mailto:<?php echo $oseba['Email']; ?>"><?php echo $oseba['Email']; ?></a>
								<?php
								} else {
								echo "/";
								}
                    			?>      				
                    		</td>
                    	</tr>
                    	<tr>
                    		<td><b>Telefon:</b></td>
                    		<td>
                    			<?php
								if ($oseba['Telefon'] != NULL) {
									echo $oseba['Telefon'];
								} else {
									echo "/";
								}
                    			?>      				
                    		</td>
                    	</tr>
                    	<tr>
                    		<td><b>Predmeti:</b></td>
                    		<td>
                    			<?php
								$stPredmetov = count($oseba['Predmeti']);
								if ($stPredmetov != 0) {

									foreach ($oseba['Predmeti'] as $predmeti) {
										echo '<li>' . $predmeti['Naziv'] . '</li>';
									}
								} else {
									echo "/";
								}
                    			?>
                    		</td>
                    	</tr>
                    </table>
                    </div>
                    </div>
                    <?php

					$najden++;
					}
					}

					$index = -1;

					}

					if ($najden == 0) {
					echo 'Ni zadetkov!';
					} else {
					echo 'Število zadetkov: ' . $najden;
					}
					}
				?>
	</div>
</div>