<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
<?php echo $this->Session->flash(); ?>
<div id="oglasna_deska">
<div id="naslov">Dodaj Album</div>
<div id="podatki-dodaj-album">
<form method="post" action="../album/dodaj_album">
	<table class="dodaj-album">
		<tr>
			<td><b>Naslov:* </b></td><td><input type="text" name="naslov" /></td>
		</tr>
		<tr>
			<td><b>Avtor: </b></td><td><?php echo $avtor; ?></td>
		</tr>
		<tr>
			<td><b>Opis: </b></td><td><textarea name="opis" rows="4" cols="80"></textarea></td>
		</tr>
		<tr>
			<td></td><td><input type="submit" name="dodaj" value="Dodaj" id="potrdi" style="float: right;"/></td>
			
		</tr>
	</table>
</form>
</div>
</div>
</div>
