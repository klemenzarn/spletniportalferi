<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
<?php echo $this->Session->flash(); ?>
<div id="album-seznam">
<div id="naslov">Seznam že obstoječih albumov</div>
<div id="seznam-dodaj-album">
<form name="seznam-albumov" method="post" action="izbrisi_album">
<table class="seznam-albumov">
	<tr>
		<th>
			<input type="checkbox" name="izberi-vse" id="izberi-vse"/>
		</th>
		<th>
			<b>Naslov</b>
		</th>
		<th>
			<b>Avtor</b>
		</th>
		<th>
			<b>Opis</b>
		</th>
		<th>
			<b>Čas</b>
		</th>
		<th>
			<b>Število slik</b>
		</th>  
	</tr>	
	
<?php
foreach ($albumi as $album) {
?>
		<tr>
			<td><input type="checkbox" name="izbira-albuma[]" value="<?php echo $album['album']['id']; ?>"></td>
			<td><?php echo $album['album']['naslov']; ?></td>
			<td><?php echo $album['album']['avtor']; ?></td>
			<td><?php echo $album['album']['opis']; ?></td>
			<td><?php echo date('d.m.Y, H:i', strtotime($album['album']['datum'])); ?></td>
			<td><?php echo $album['album']['stevilo_slik']; ?></td>
		</tr>

<?php
	}
?>
</table>
<div><input type="submit" name="izbrisi-izbiro" value="Izbriši" id="potrdi" /></div>
</form>
</div>
</div>
</div>