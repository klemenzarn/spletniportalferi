<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
Dogodki v okolici Maribora
		<div id="dogodki_form">
			<form method='POST' name='webService' action="<?php echo $this->Html->url(array("controller"=>"Dogodki","action"=>"index"));?>">
			<div id="kategorija_dogodki">Kategorija</div>
			<select id="select" name='izbrana_kategorija'>
	    		<option value="" selected>- Vse kategorije -</option>
							                        <option  value="sport">Šport</option>
							                        <option  value="zabava">Zabava</option>
							                        <option  value="kultura">Kultura</option>
							                        <option  value="izobrazevanje">Izobraževanje</option>
							                        <option  value="studentski-svet">Dogodki študentskega sveta</option>
	  		</select>
                <br/>
                Iskalni niz:                <input type='text' name='iskalni_niz'/>
                <input type="submit" value="Prikaži dogodke" id="poisci"/>
                </form>
            <div id='dogodki'>
                <table id='tabela_forum'>
                <?php 
                    if($rezultat=="")
                    {
                     echo "<h3>Ni zadetkov</h3>";   
                    }
                    else
                    {
                    ?>
                    <th>Naslov dogodka</th><th>Datum</th><th>Kraj</th><th>Vsebina</th><th>Izvajalec</th><th>Povezava</th>
                    
                    <?php
                    for($i=0;$i<(sizeof($rezultat));$i=$i+6)
                    {
                        echo "<tr>";
                        echo "<td>".$rezultat[$i]."</td>";
                         echo "<td>".$rezultat[$i+1]."</td>";
                         echo "<td>".$rezultat[$i+2]."</td>";
                         echo "<td>".$rezultat[$i+3]."</td>";
                         echo "<td>".$rezultat[$i+4]."</td>";
                        echo "<td><a href='".$rezultat[$i+5]."' target='_blank'>Podrobnosti</a></td>";
                        echo "</tr>";
                    }
                    }
                    ?>
                </table>

            </div>
            </div>
            
</div>