﻿<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
<h1>Danes na TV, <?php echo date("d.m.Y"); ?></h1>
<div><a href="<?php echo $this -> Html ->url(array('controller' => 'TvSpored', 'action' => 'iskanje_oddaje')); ?>">Poišči oddajo ...</a></div>
<form method="post" action="<?php  $this -> Html -> url(array("controller" => "TvSpored", "action" => "index")); ?>">
<table>
	<tr>
		<td>
			<b>Programi: </b>
		</td>
		<td>
			<select name="program-naziv" id="program-naziv">
			<?php
			$stProgramov = count($programi); 
			for($i = 0; $i < $stProgramov; $i++){
				if($nazivPrograma != ""){
					if($nazivPrograma == $programi[$i]){	
						echo '<option selected="selected">' . $programi[$i] . '</option>';
					}
					else{
						echo '<option>' . $programi[$i] . '</option>';
					}
				}
				else{
					echo '<option>' . $programi[$i] . '</option>';
				}	
			}
			?>
			</select>
		</td>
		<td>
			<input type="submit" id="izberi-program" value="Izberi" />
		</td>
	</tr>
</table>

<?php
if (isset($oddaje)) {
?>
<h3><?php echo $nazivPrograma; ?></h3>
<table id="tv-spored">
<?php
$stOddaj = count($oddaje)/3;
if($stOddaj != null){
	for($i = 0; $i < $stOddaj; $i++){
		?>
			<tr>
				<td><?php echo $oddaje[$i*3+1]; ?></td>	
				<td>
					<div id="naziv-programa">
						<?php echo $oddaje[$i*3]; ?>
					</div>
					<div id="opis-oddaje" class="<?php echo $i; ?>" style="display: none;">
						<?php echo $oddaje[$i*3+2]; ?>
					</div>
				</td>
			</tr>
		<?php
	}
}
}
?>
	</tr>
</table>
</div>