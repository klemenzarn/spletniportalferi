<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
<?php
$vel = count($oddaje);
?>
<div id="short_div">
<div id="naslov_short">
	<div id="ime_naslova_short" class="sort">Trenutno na TV ...</div>
</div>
<div id="vse_obv">
		<?php 
		for($j = 0; $j < $vel; $j++){
			if(isset($oddaje[$j]['trenutnaOddajaResult']['string'])){
			$i = 0;
		?>
		<div id="podatki-tv-spored">
			<div id=program-logo>
				<img src="<?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 1]?>" />
			</div>
		 	<div id="dve-oddaje">
				<div><?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4]; ?></div>
				<div><b>Trenutno: <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 5]; ?></b> - <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 4]; ?></div>
				<div><b>Sledi: <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 3]; ?></b> - <?php echo $oddaje[$j]['trenutnaOddajaResult']['string'][$i * 4 + 2]; ?></div>
			</div>
		</div>
		<?php 
		$i++;
			}
else{
 ?>
 	<div id="podatki-tv-spored">
 		<div id=program-logo>
		</div>
		 <div id="dve-oddaje">
			<div></div>
			<div><b>Trenutno: -</b></div>
			<div><b>Sledi: -</b></div>
		</div>
 	</div>
 <?php
}
			} 
		?>
</div>
</div>
</div>
<?php ?>