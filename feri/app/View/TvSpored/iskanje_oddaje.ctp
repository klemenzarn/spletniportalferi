<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
<h1>
	Iskanje
</h1>
<div><a href="<?php echo $this -> Html ->url(array('controller' => 'TvSpored', 'action' => 'index')); ?>">Kaj je danes na sporedu ...</a></div>
<form method="post" action="<?php  $this -> Html -> url(array("controller" => "TvSpored", "action" => "iskanje_oddaje")); ?>">
<table id="iskanje-oddaje">
	<tr>
		<td>
			Naziv oddaje: 
		</td>
		<td>
			<input type="text" name="naziv-oddaje" />
		</td>
	</tr>
	<tr>
		<td>
			Začetek oddaje:
		</td>
		<td>
			<input type="text" maxlength="2" size="2" name="ura" id="ura"/>:<input type="text" maxlength="2" size="2" name="minute" id="minute"/>(ura:min)
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input id="potrdi" type="submit" value="Išči" style="float: right;"/>
		</td>
	</tr>
</table>


<?php if(isset($oddaje)){
//$programi[$i * 4] - naziv oddaje
//$programi[$i * 4 + 1] - čas začetka
//$programi[$i * 4 + 2] - opis oddaje
//$programi[$i * 4 + 3] - naziv programa
$programi = array();
	if(empty($oddaje)){
		echo "Oddaje danes ni na sporedu!";
	}
	else{
		$oddaje = $oddaje['string'];
		$stZadetkov = count($oddaje)/4;
		echo "Število zadetkov: " . $stZadetkov;
		for($i = 0; $i < $stZadetkov * 4; $i++){
			if($i % 4 == 3){
				$programi[] = $oddaje[$i]; // V polje damo samo nazive vseh programov
			}
		}
		$programi = array_unique($programi); // Dobimo polje vseh programov - pojavijo samo enkrat
		/*echo '<pre>';
		print_r($programi);
		echo '</pre>';*/
		
		for($i = 0; $i < $stZadetkov; $i++){
			if(isset($programi[$i])){
				?>
				
				<h2><?php echo $programi[$i]; //Izpišemo naziv programa samo 1x! ?></h2>
				<hr />
				<div>
					<h3><?php echo $oddaje[$i * 4 + 1] . " - " .  $oddaje[$i * 4]; ?></h3>
					<div><?php echo $oddaje[$i * 4 + 2]; ?></div>
				</div>
				<?php
			}
			else{
				?>
				<div>
					<h3><?php echo $oddaje[$i * 4 + 1] . " - " .  $oddaje[$i * 4]; ?></h3>
					<div><?php echo $oddaje[$i * 4 + 2]; ?></div>
				</div>
				<?php
			}
		}
	}
}
		?>
		
</div>
