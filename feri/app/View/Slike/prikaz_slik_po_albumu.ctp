<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'><?php echo $this->Session->flash(); ?>
<form name="pregled-slik" method="get" action="prikaz_slik_po_albumu">
	<table>
		<tr>
			<td><b>Izberite album: </b></td>
			<td>
			<select name="izbira-album">
				<?php
				foreach ($albumi as $album) {// vsak album dodamo kot možnost izbire
					if(isset($naslovAlbuma)){
						if($naslovAlbuma == $album['album']['naslov']){
							echo '<option selected="selected">' . $album['album']['naslov'] . '</option>';
					// Izpišemo naslov posameznega albuma
						}
						else{
							echo '<option>' . $album['album']['naslov'] . '</option>';
						}
					}
					else{
						echo '<option>' . $album['album']['naslov'] . '</option>';
					}
				}
				?>
			</select></td>
			<td>
			<input type="submit" name="prikazi" value="Prikaži"/>
			</td>
		</tr>
	</table>
</form>

<?php

		if (isset($stSlikVAlbumu)) {
			?>
<div id="album-seznam">
<div id="naslov">Seznam slik v albumu: <?php echo $naslovAlbuma; ?></div>
<div id="seznam-dodaj-album">
<form name="brisanje-slik" method="post" action="brisanje_slik">
	<table class="album-slike">
		<tr>
			<th><b><input type="checkbox" name="izberi-vse" id="izberi-vse"/></b></th>
			<th><b>Ime slike</b></th>
			<th><b>Velikost</b></th>
			<th><b>Možnosti</b></th>
		</tr>
<?php		

			for ($i = 0; $i < $stSlikVAlbumu; $i++) {
				$id = $slikeVAlbumu[$i]['slika']['id'];
				echo '<tr>';
				echo '<td><input type="checkbox" name="izbira-slike[]" value=' . $id . '></td>';
				echo '<td>' . $slikeVAlbumu[$i]['slika']['ime_slike'] . '</td>';
				echo '<td>' . $slikeVAlbumu[$i]['slika']['velikost_slike'] . 'KB</td>';
				//echo '<td><a href="'. $this->link . '"><img src="../img/uredi.png" /></a>  <a href=""><img src="../img/brisi.png" /></a></td>';
				//echo '<td>' . $this -> Html -> link(NULL, array("action" => "izbrisi", $id), NULL, 'Si prepričan, da želiš izbrisati sliko?') . '</td>';
				echo '<td>' . $this -> Html -> link($this -> Html -> image("brisi.png"), array("action" => "izbrisi", $id), array("escape" => false)) . '</td>';
			}
?>
	</table>
	<div><input type="hidden" name="naziv-albuma" value="<?php if(isset($naslovAlbuma)){echo $naslovAlbuma;}?>" /></div>
	<div><input type="submit" name="izbrisi-izbiro" value="Izbriši" id="potrdi"/></div>
</form>	
</div>
</div>

<?php
}
?>
</div>