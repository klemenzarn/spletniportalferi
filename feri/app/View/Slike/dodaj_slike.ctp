<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/novice/dodajnovico">Dodaj novico</a></li>
      <li id='neki'><a href="/feri/novice/uredi">Uredi novice</a></li>
      <li id='neki'><a href="/feri/oglasna/dodajoglasno">Dodaj prispevek na oglasno desko</a></li>
      <li id='neki'><a href="/feri/oglasna/uredi">Uredi oglasno desko</a></li>
      <li id='neki'><a href="/feri/album/dodaj_album">Dodaj album slik</a></li>
      <li id='neki'><a href="/feri/slike/dodaj_slike">Dodaj slike v album</a></li>
      <li id='neki'><a href="/feri/Album/index">Seznam albumov</a></li>
      <li id='neki'><a href="/feri/Slike/index">Uredi slike po albumu</a></li>
  </ul>
 </div> 
</div>
<div id='vse'>
<div id="album-seznam">
<div id="naslov">Dodaj slike</div>
<div id="seznam-dodaj-album">
<?php echo $this->Session->flash(); ?>
<form name="dodaj-sliko" method="post" action="../slike/dodaj_slike" enctype="multipart/form-data">
	
	<table style="padding: 10px;">
		<tr>
			<td><b>Izberite slike: </b>
			</td>
			<td>
				<input type="file" name="slika[]" multiple="multiple"/>
			</td>

		</tr>
		<tr>
			<td><b>Izberite album: </b></td>
			<td>
			<select name="izbira-album">
				<?php 
				
				foreach($albumi as $album){ // vsak album dodamo kot možnost izbire
				
				echo '<option>'.$album['album']['naslov'].'</option>'; // Izpišemo naslov posameznega albuma
				
				}
				?>
			</select>
			</td>
		</tr>
		<tr>
			<td><input type="submit" name="dodaj" value="Dodaj" id="potrdi" /></td>
			<td></td>
		</tr>
	</table>
</form>
</div>
</div>
</div>