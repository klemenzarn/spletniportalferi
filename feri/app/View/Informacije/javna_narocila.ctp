<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Javna narocila</div>
	<div id="vsebina">
	Informacije o javnih naročilih: Valerija Plazovnik, E-pošta: <a href="mailto:valerija.plazovnik@um.si">valerija.plazovnik@um.si</a>, T: 02 220 7036. <br /><br />
	<b>Zakoni, navodila in obrazci</b>
	<ul "povezave">
		<li>Zakon o javnem naročaju ZJN-2 (Ur. l. RS, št. 128/2006 s spremembami Ur. l. RS, št. <a href="http://www.uradni-list.si/1/objava.jsp?urlid=200816&stevilka=488" target="_blank">16/2008</a>, <a href="http://www.uradni-list.si/1/objava.jsp?urlid=201019&stevilka=805" target="_blank">19/2010</a>, <a href="http://www.uradni-list.si/1/objava.jsp?urlid=201118&stevilka=766" target="_blank">18/2011</a>, <a href="http://www.uradni-list.si/1/objava.jsp?urlid=201243&stevilka=1781" target="_blank">43/2012</a> Odl.US: U-I-211/11-26, <a href="http://www.uradni-list.si/1/objava.jsp?urlid=201290&stevilka=3526" target="_blank">90/2012</a>, <a href="http://www.uradni-list.si/1/objava.jsp?urlid=201312&stevilka=304" target="_blank">12/2013</a>)</li>
	    <li><a href="#">Navodilo o izvajanju naročil male vrednosti na UM št. N 16-2010-61PU</a>, veljalo do 30. 8. 2011</li>
	    <li><a href="#">Navodilo o izvajanju naročil na Univerzi v Mariboru št. N 40/2011-11 RSP</a>, velja od 31. 8. 2011</li>
	    <li><a href="#">Zahtevek za naročilo JN1</a>, velja od 31. 8. 2011</li>
	    <li><a href="#">Opis postopka do opreme (ARRS)</a></li>
	    <li><a href="#">Evidenca raziskovalne opreme s podatki o mesečni uporabi (ARRS)</a></li>
	</ul>
		<b>Javna naročila v teku</b>
	</div>
</div>
