<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Informacije za študente</div>
	<div id="vsebina">
	<b>Referat za študentske zadeve UM FERI</b>
	<ul id="povezave">
		<li><a href="#">Uradne ure in zaposleni</a></li>
	</ul>
	<b>Najpogostejša vprašanja in odgovori o študiju</b>
	<ul id="povezave">
		<li><a href="#">Prijava/odjava na izpit, komisijski izpiti</a></li>
	    <li><a href="#">Izguba študentske izkaznice</a></li>
	    <li><a href="#">Pogoji za napredovanje v višje letnike študija</a></li>
	    <li><a href="#">Priznavanje predmetov iztekajočih štud. programov v bolonjskih programih</a></li>
	    <li><a href="#">Izpitni roki</a></li>
	    <li><a href="#">Objava rezultatov in vpis ocen</a></li>
	    <li><a href="#">Diplomska naloga</a></li>
	    <li><a href="#">Napredovanje v višje letnike</a></li>
	    <li><a href="#">Priznavanje izpitov</a></li>
	    <li><a href="#">Opravljanje vaj </a></li>
	     <li><a href="#">Izračun povrečne ocene študija</a></li>
	    <li><a href="#">Zaključek študija po starih študijskih programih</a></li>
	    <li><a href="#">Menjava izbirnega predmeta</a></li>
	    <li><a href="#">Status študenta</a></li>
	    <li><a href="#">Dostop do študijske literature</a></li>
	    <li><a href="#">Športna vzgoja</a></li>
	</ul>
	<b>Urniki</b>
	<ul id="povezave">
		<li><a href="#">Urniki za tekoče študijsko leto</a></li>
	</ul>
	<b>Pravilniki in predpisi</b>
	<ul id="povezave">
		<li><a href="#">Komisija za študijske zadeve ter najpogostejši vzorci vlog oz. prošenj študentov</a></li>
	    <li><a href="#">STATUT Univerze v Mariboru</a></li>
	    <li><a href="#">Pravilnik o preverjanju in ocenjevanju znanja na Univerzi v Mariboru</a></li>
	    <li><a href="#">Pravilnik o ECTS kreditnem sistemu študija</a></li>
	    <li><a href="#">Pravilnik o priznavanju tujega izobraževanja na UM</a></li>
	    <li><a href="#">Pravilnik o priznavanju znanj in spretnosti, pridobljenih pred vpisom v študijski program</a></li>
	    <li><a href="#">Študijski koledar za študijsko leto 2012/2013</a></li>
	    <li><a href="#">Šolnine, vpisnine in drugi stroški</a></li>
	    <li><a href="#">Pravilnik o izvajanju interesnih dejavnosti študentov</a></li>
	    <li><a href="#">Pravilnik o izvajanju študentske ankete</a></li>
	    <li><a href="#">Obvestilo študentom o avtorskih pravicah</a></li>
	</ul>
	<b>Predmetniki za študijsko leto 2012/2013</b></br>
	Informacije o študijskih programih in predmetnikih so na voljo v področju <a href="#">Študijski programi</a>.</br></br>
	<b>Priprava in zagovor diplomskega dela na dodiplomskih študijskih programih</b>
	<ul id="povezave">
		<li><a href="#">Pravilnik o postopku priprave in zagovora diplomskega dela na dodiplomskem študiju</a></li>
	    <li><a href="#">Navodila za izdelavo diplomskega dela</a></li>
	    <li><a href="#">Pravilnik o izdelavi diplomskih del vseh stopenj študija v tujem jeziku na UM</a></li>
	    <li><a href="#">Postopek zagovora diplomskega dela</a></li>
	    <li><a href="#">Obrazci za prijavo teme diplomskega dela</a></li>
	    <li><a href="#">Vloga za podaljšanje teme diplomskega dela</a></li>
	    <li><a href="#">Potek oddaje izvodov diplomskega dela</a></li>
	    <li><a href="#">RAZPISANE TEME DIPLOMSKIH in MAGISTRSKIH DEL V ŠTUDIJSKEM LETU 2012/2013</a></li>
	</ul>
	<b>Priprava in zagovor diplomskega dela na podiplomskih (magistrskih) študijskih programih</b>
	<ul id="povezave">
		<li><a href="#">Pravilnik o postopku priprave in zagovora magistrskega dela na študijskem programu 2. stopnje</a></li>
	    <li><a href="#">Navodila za izdelavo magistrskega dela (2. stopnja)</a></li>
	    <li><a href="#">Spremembe in dopolnitve Pravilnika o postopku priprave in zagovora magistrskega dela na študijskem programu 2. stopnje</a></li>
	    <li><a href="#">Obrazci za prijavo teme magistrskega dela</a></li>
	    <li><a href="#">Vloga za podaljšanje teme magistrskega dela</a></li>
	</ul>
	<b>Tutorstvo</b>
	<ul id="povezave">
		<li><a href="#">Izhodišča za uvedbo tutorstva na UM</a></li>
	    <li><a href="#">Pravilnik o tutorskem sistemu na UM FERI</a></li>
	    <li><a href="#">Seznam tutorjev študentov</a></li>
	    <li><a href="#">Seznam tutorjev učiteljev in mentorjev letnikov</a></li>
	</ul>
	<b>Izvajanje izbirnosti med študijskimi programi FERI in članicami Univerze v Mariboru v okviru kreditnega študija za bolonjske študijske programe</b>
	<ul id="povezave">
		<li><a href="#">Navodilo o izvajanju izbirnosti med članicami Univerze v Mariboru</a></li>
	    <li><a href="#">Izvajanje izbirnosti znotraj UM FERI</a></li>
	    <li><a href="#">Izvajanje izbirnosti med članicami UM</a></li>
	</ul>
	<b>Praktično usposabljanje</b>
	<ul id="povezave">
		<li><a href="#">Praktično usposabljanje in diplomsko delo v prvostopenjskih bolonjskih študijskih programih </a></li>
	    <li><a href="#">Praktično izobraževanje na VS in praksa na UNI študiju (iztekajoči študijski programi)</a></li>
	</ul>
	<b>Študenti s posebnim statusom</b>
	<ul id="povezave">
		<li><a href="#">Pravilnik o študijskem procesu študentov invalidov Univerze v Mariboru </a></li>
	    <li><a href="#">Pravilnik o študentih s posebnim statusom na Univerzi v Mariboru </a></li>
	    <li><a href="#">Vloga za pridobitev posebnega statusa študenta na Univerzi v Mariboru </a></li>
	    <li><a href="#">Društvo študentov invalidov Slovenije</a></li>
	    <li><a href="#">Kontaktna oseba za študente s posebnim statusom </a></li>
	</ul>
	<b>Obštudijska dejavnost</b>
	<ul id="povezave">
		<li><a href="#">Tekmovanje z mini mobilnimi roboti </a></li>
	    <li><a href="#">IAESTE</a></li>
	    <li><a href="#">Društvo študentov FERI</a></li>
	    <li><a href="#">katedra-on.net - spletni časopis</a></li>
	    <li><a href="#">Študentski svet FERI</a></li>
	    <li><a href="#">Študentski svet UM</a></li>
	</ul>
	<b>Servisne informacije</b>
	<ul id="povezave">
		<li><a href="#">Pridobitev uporabniškega imena</a></li>
	    <li><a href="#">Sprememba gesla</a></li>
	    <li><a href="#">Knjižnica tehniških fakultet</a></li>
	    <li><a href="#">Podaljšanje veljavnosti uporabniškega imena</a></li>
	    <li><a href="#">Pridobitev elektronskega naslova</a></li>
	    <li><a href="#">FERI WebMail - spletni poštni strežnik</a></li>
	    <li><a href="#">Osebne spletne strani in FTP dostop</a></li>
	    <li><a href="#">EDUROAM - brezžično izobraževalno omrežje</a></li>
	    <li><a href="#">Program MSDN Academic Alliance (DreamSpark)</a></li>
	    <li><a href="#">Spletni dostop do e-pošte</a></li>
	    <li><a href="#">Omarice namenjene študentom</a></li>
	    <li><a href="#"> Zemljevid fakultete</a></li>
	</ul>
	</div>
</div>
