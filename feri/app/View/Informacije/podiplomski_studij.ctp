<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Podiplomski študij</div>
	<div id="vsebina">
	<b>Vpis v podiplomske študijske programe</b>
	<ul id="povezave">
	    <li><a href="#">Pomembni datumi za vpis na podiplomske študijske programe v študijskem letu 2013/2014</a></li>
	    <li><a href="#">Splošna določila Razpisa za vpis v podiplomske študijske programe v študijskem letu 2013/2014 - za vpis v študijske programe 2. stopnje</a></li>
	    <li><a href="#">Splošna določila Razpisa za vpis v podiplomske študijske programe v študijskem letu 2013/2014 - za vpis v študijske programe 3. stopnje</a></li>
	    <li><a href="#">Obvestilo vsem tujcem in Slovencem brez slovenskega državljanstva</a></li>
	    <li><a href="#">Podatki o vpisu v podiplomske študijske programe II. stopnje</a></li>
	    <li><a href="#">Podatki o vpisu v podiplomske študijske programe III. stopnje</a></li>
	    <li><a href="#">Študentje z že pridobljeno izobrazbo, ki po klasifikacijskem sistemu Klasius ustreza 2. bolonjski stopnji</a></li>
	</ul>
	<b>Študijski programi druge stopnje</b></br>
	Študij za pridobitev magistrskega naziva.
	<ul id="povezave">
	    <li><a href="#">Študijski program druge stopnje Elektrotehnika</a></li>
	    <li><a href="#">Študijski program druge stopnje Računalništvo in informacijske tehnologije</a></li>
	    <li><a href="#">Študijski program druge stopnje Informatika in tehnologije komuniciranja</a></li>
	    <li><a href="#">Študijski program druge stopnje Telekomunikacije</a></li>
	    <li><a href="#">Študijski program druge stopnje Medijske komunikacije</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Daljinsko vodenje (Remote Engineering)</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Gospodarsko inženirstvo</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Mehatronika</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Bioinformatika</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Izobraževalno računalništvo</a></li>
	</ul>
	<b>Študijski programi tretje stopnje</b></br>
	Študij za pridobitev doktorata znanosti.
	<ul id="povezave">
	    <li><a href="#">Splošne informacije o študiju v študijskem letu 2012/2013</a></li>
	    <li><a href="#">Pogoji za napredovanje v višje letnike 2012/2013 - programi 3. stopnje</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Elektrotehnika</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Računalništvo in informatika</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Medijske komunikacije</a></li>
	    <li><a href="#">Vloga za potrditev poteka študija na 3. stopnji</a></li>
	</ul>
	<b>Podiplomski študijski programi - študij za pridobitev magisterija in doktorata znanosti (sprejeti pred 11. 6. 2004)</b></br>
	Vpis v podiplomske študijske programe, sprejete pred 11. 6. 2004, ni več mogoč.
	<ul id="povezave">
		<li><a href="#">Podiplomski študijski programi (nebolonjski)</a></li>
	    <li><a href="#">Rok za dokončanje študija po študijskih programih, sprejetih pred 11. 6. 2004</a></li>
	</ul>
	<b>Sofinanciranje doktorskega študija</b>
	<ul id="povezave">
	    <li><a href="#">Inovativna shema za sofinanciranje doktorskega študija za spodbujanje sodelovanja z gospodarstvom in reševanje aktualnih družbenih izzivov</a></li>
	    <li><a href="#">Pravilnik o postopku dodelitve sredstev na javnem razpisu »Inovativna shema za sofinanciranje doktorskega študija za spodbujanje sodelovanja z gospodarstvom in reševanja aktualnih družbenih izzivov« št. A2/2011 – 41 AG</a></li>
	</ul>
	<b>Pravilniki in dokumenti za prijavo teme in izdelavo doktorske disertacije</b>
	<ul id="povezave">
	    <li><a href="#">Pravilnik o postopku priprave in zagovora doktorske disertacije na UM</a></li>
	    <li><a href="#">Navodila za prijavo teme doktorske disertacije ter izdelavo doktorske disertacije </a></li>
	    <li><a href="#">Navodila za študente podiplomskega študija, ki so študij prekinili po 1. letniku znanstvenega magisterija in želijo zaključiti študij po prekinitvi </a></li>
	    <li><a href="#">Predloga za predstavitev doktorske disertacije</a></li>
	    <li><a href="#">Protokol zagovora doktorske disertacjie</a></li>
	</ul>
	<b>Promocija doktorjev znanosti</b>
	<ul id="povezave">
	    <li><a href="#">Pogoji za promocijo</a></li>
	    <li><a href="#">Kriteriji in merila glede zahtevane znanstvene aktivnosti kandidatov na UM FERI</a></li>
	</ul>
	<b>Cenik šolnin podiplomskega študija</b>
	<ul id="povezave">
	    <li><a href="#">Cenik šolnin za podiplomski študij </a></li>
	</ul>
	</div>
</div>
