<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Raziskovalna dejavnost</div>
	<div id="vsebina">
	Ena izmed temeljnih nalog Fakultete za elektrotehniko, računalništvo in informatiko - FERI je, da se doma in v svetu čimbolj uveljavi kot znanstveno ustvarjalna institucija s kakovostnim izobraževalnim procesom. Zato v svojem poslanstvu daje prednost ustvarjanju novih znanj na področjih elektrotehnike, računalništva, informatike, telekomunikacij in medijskih komunikacij. Ta znanja nato uspešno prenaša v slovenski pa tudi širši mednarodni prostor. Znanstvenoraziskovalno delo na FERI dopolnjujemo tudi z aplikativnimi in razvojnimi raziskavami, s čimer temeljna znanja prehajajo v konkretno uporabo.</br></br>
	<b>Kontaktne osebe</b></br>
	Prodekan za raziskovalno dejavnost: prof. ddr. Denis ĐONLAGIĆ (ddonlagic@uni-mb.si) </br>
	Vodja službe za znanstveno in raziskovalno delo: Suzana PUŠAUER (suzana.pusauer@um.si) </br>
	Strokovna sodelavka: Anja INKRET (anja.inkret@um.si) </br>
	Strokovni sodelavec: Gregor HOHLER (gregor.hohler@um.si) </br>
	Administrativni referent: Jasmina VIŠIČ (jasmina.visic@um.si) </br>
	Skrbnik strani: Suzana Pušauer. Zadnja sprememba: 16.4. 2013. </br></br>
	<b>Raziskovalno sodelovanje</b>
	<ul id="povezave">
	    <li><a href="#">Raziskovalne skupine</a></li>
	    <li><a href="#">Programske skupine</a></li>
	    <li><a href="#">Mednarodno raziskovalno sodelovanje</a></li>
	    <li><a href="#">Intelektualna lastnina</a></li>
	    <li><a href="#">Cenik raziskovalne opreme</a></li>
	</ul>
	<b>Razpisi</b>
	<ul id="povezave">
	    <li><a href="#">Agencija za raziskovalno dejavnost</a></li>
	    <li><a href="#">Ministrstvo za izobraževanje, znanost in šport</a></li>
	    <li><a href="#">Ministrstvo za gospodarski razvoj in tehnologijo</a></li>
	    <li><a href="#">Tehnološka agencija Slovenije</a></li>
	    <li><a href="#">Aktualni razpisi</a></li>
	</ul>
	<b>Zakoni / merila / priporočila / kodeks</b>
	<ul id="povezave">
	    <li><a href="#">Zakoni in predpisi, ki se nanašajo na področje raziskovalne dejavnosti</a></li>
	    <li><a href="#">Habilitacije</a></li>
	    <li><a href="#">Priporočilo o objavah spoznanj iz doktorskih disertacij doktorandov na UM-FERI pred promocijo</a></li>
	    <li><a href="#">Kodeks etike znanstvenih in drugih objav</a></li>
	</ul>
	<b>Baze</b>
	<ul id="povezave">
		<li><a href="#">SICRIS</a></li>
	    <li><a href="#">COBISS</a></li>
	</ul>
	<b>Uporabne povezave</b>
	<ul id="povezave">
	    <li><a href="#">Javna agencija za raziskovalno dejavnost Republike Slovenije</a></li>
	    <li><a href="#">RTD - Raziskave in razvoj v Sloveniji</a></li>
	    <li><a href="#">TIA - Tehnološka agencija Slovenije </a></li>
	    <li><a href="#">Ministrstvo za izobraževanje, znanost kulturo in šport</a></li>
	    <li><a href="#">ARNES - Akademska in raziskovalna mreža Slovenije</a></li>
	    <li><a href="#">IZUM - Institut informacijskih znanosti</a></li>
	    <li><a href="#">SIST - Slovenski inštitut za standardizacijo</a></li>
	    <li><a href="#">SIPO - Urad Republike Slovenije za intelektualno lastnino</a></li>
	    <li><a href="#">Programi teritorialnega sodelovanja </a></li>
	    <li><a href="#">SBRA - Slovensko gospodarsko in raziskovalno združenje</a></li>
	    <li><a href="#">CMEPIUS - Center za mobilnost in evropske programe izobraževanja in usposabljanja</a></li>
	    <li><a href="#">Znanstvenoraziskovalni center Slovenske akademije znanosti in umetnosti</a></li>
	</ul>
	</div>
</div>
