<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Študijski programi</div>
	<div id="vsebina">
<b>Študijski programi prve stopnje</b></br>
	Študij za pridobitev univerzitetne oziroma visokošolske strokovne izobrazbe.
	<ul id="povezave">
	    <li><a href="#">Univerzitetni študijski program prve stopnje Elektrotehnika</a></li>
	    <li><a href="#">Univerzitetni študijski program prve stopnje Računalništvo in informacijske tehnologije</a></li>
	    <li><a href="#">Univerzitetni študijski program prve stopnje Informatika in tehnologije komuniciranja</a></li>
	    <li><a href="#">Univerzitetni študijski program prve stopnje Telekomunikacije</a></li>
	    <li><a href="#">Univerzitetni študijski program prve stopnje Medijske komunikacije</a></li>
	    <li><a href="#">Skupni univerzitetni študijski program prve stopnje Gospodarsko inženirstvo – smer Elektrotehnika</a></li>
	    <li><a href="#">Skupni univerzitetni študijski program prve stopnje Mehatronika</a></li>
	    <li><a href="#">Skupni univerzitetni študijski program prve stopnje Izobraževalno računalništvo</a></li>
	    <li><a href="#">Visokošolski strokovni študijski program prve stopnje Elektrotehnika</a></li>
	    <li><a href="#">Visokošolski strokovni študijski program prve stopnje Računalništvo in informacijske tehnologije</a></li>
	    <li><a href="#">Visokošolski strokovni študijski program prve stopnje Informatika in tehnologije komuniciranja</a></li>
	    <li><a href="#">Skupni visokošolski strokovni prve stopnje študijski program Mehatronika</a></li>
	    <li><a href="#">Skupni visokošolski strokovni študijski program prve stopnje Informacijska varnost</a></li>
	</ul>
	<b>Študijski programi druge stopnje</b></br>
	Študij za pridobitev magistrskega naziva.
	<ul id="povezave">
	    <li><a href="#">Študijski program druge stopnje Elektrotehnika</a></li>
	    <li><a href="#">Študijski program druge stopnje Računalništvo in informacijske tehnologije</a></li>
	    <li><a href="#">Študijski program druge stopnje Informatika in tehnologije komuniciranja</a></li>
	    <li><a href="#">Študijski program druge stopnje Telekomunikacije</a></li>
	    <li><a href="#">Študijski program druge stopnje Medijske komunikacije</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Daljinsko vodenje (Remote Engineering)</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Gospodarsko inženirstvo</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Mehatronika</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Bioinformatika</a></li>
	    <li><a href="#">Skupni študijski program druge stopnje Izobraževalno računalništvo</a></li>
	</ul>
	<b>Študijski programi tretje stopnje</b></br>
	Študij za pridobitev doktorata znanosti.
	<ul id="povezave">
	    <li><a href="#">Splošne informacije o študiju v študijskem letu 2012/2013</a></li>
	    <li><a href="#">Pogoji za napredovanje v višje letnike 2012/2013 - programi 3. stopnje</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Elektrotehnika</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Računalništvo in informatika</a></li>
	    <li><a href="#">Vsebine predmetov študijskega programa tretje stopnje Medijske komunikacije</a></li>
	    <li><a href="#">Vloga za potrditev poteka študija na 3. stopnji</a></li>
	</ul>
	</div>
</div>
