<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Predstavitev in splošne informacije</div>
	<div id="vsebina">
	<div id="slika_informacije">
	<p align="justify"><img width="276" hspace="10" height="240" border="1" align="right" src="http://www.feri.uni-mb.si/UserFiles/652/Image/G2-8_Small.jpg" alt="Objekt G2" title="Objekt G2" style="BORDER-BOTTOM: gray 2px outset; BORDER-LEFT: gray 2px outset; BORDER-TOP: #000 2px outset; MARGIN-RIGHT: 0px; BORDER-RIGHT: #000 2px outset"></img></p>
	</div>
	<b>Naslov:</b> <span id="podatki_informacije">UM FERI
	Smetanova ulica 17
	2000 Maribor </span></br>
	<b>Telefon:</b> <span id="podatki_informacije">+386 2 220 7000 </span></br>
	<b>Telefaks:</b> <span id="podatki_informacije">+386 2 220 7272, +386 2 220 7090 </span></br>
	<b>E-mail:</b> <span id="podatki_informacije">feri@um.si </span></br>
	<b>Matična številka:</b> <span id="podatki_informacije">5089638003 </span></br>
	<b>Identifikacijska številka za DDV:</b> <span id="podatki_informacije">SI71674705 </span></br>
	<b>IBAN:</b> <span id="podatki_informacije">SI56 01100 6090106039 </span></br>
	<b>SWIFT:</b> <span id="podatki_informacije">BSLJSI2X </span></br>
	<b>Šifra dejavnosti po SKD:</b> <span id="podatki_informacije">85.422 </span></br>
	<b>Dekan:</b> <span id="podatki_informacije">prof. dr. Borut Žalik </span></br>
	</br>
	</br>
	<ul id="povezave">
		<li><a href="#">Poslanstvo in vizija fakultete</a></li>
	    <li><a href="#">Strategija in vrednote fakultete</a></li>
	    <li><a href="#">Organiziranost in organi fakultete</a></li>
	    <li><a href="#">Informacije javnega značaja</a></li>
	    <li><a href="#">Vodstvo fakultete</a></li>
	    <li><a href="#">Imenik zaposlenih</a></li>
	    <li><a href="#">Zgodovina fakultete</a></li>
	    <li><a href="#">Lokacija fakultete</a></li>
	    <li><a href="#">Programi dela in letna poročila</a></li>
	    <li><a href="#">Akti, navodila, obrazci</a></li>
	</ul>
	</div>
</div>
