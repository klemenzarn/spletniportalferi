<div id='levo'>
<div id="cssmenu">
		<ul>
		    <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
			<li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
			<li id='neki'><a href="/feri/informacije/Informacije_za_studente#">Informacije za študente</a></li>
			<li id='neki'><a href="/feri/informacije/Studijski_programi#">Študijski programi</a></li>
			<li id='neki'><a href="/feri/informacije/Izredni_studij#">Izredni študij</a></li>
			<li id='neki'><a href="/feri/informacije/Podiplomski_studij#">Podiplomski študij</a></li>
			<li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
			<li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost#">Raziskovalna dejavnost</a></li>
			<li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
			<li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
			<li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
			<div id="meni_naslov">Inštituti</div>
			<li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
			<li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
			<li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
			<li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
			<li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
			<li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
			<li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
			<li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
			<div id="meni_naslov">Vprašaj Saro</div>
		</ul>
	</div>	
</div>
<div id='vse'>
	<div id="oglasi">Obvladovanje kakovosti</div>
	<div id="vsebina">
		Komisija za ocenjevanje kakovosti (KOK) FERI je pristojna za spremljanje, ugotavljanje in zagotavljanje kakovosti FERI, študijskih programov, znanstveno-raziskovalnega ter strokovnega dela in pripravo letnega poročila o kakovosti (samoevalvacijsko poročilo). <br />
		Na področju zagotavljanja kakovosti smo si zastavili naslednje cilje: <br />
		<ul>
			<li>vzpostavitev preglednega sistema zagotavljanja kakovosti,</li>
		    <li>sprotno spremljanje kakovosti (redne letne samoevalvacije, občasne zunanje evalvacije) na podlagi kazalcev in meril, ki bodo upoštevali mednarodne in nacionalne standarde,</li>
		    <li>zagotavljanje kakovosti na vseh temeljnih področjih delovanja FERI (izobraževanje, raziskovanje, upravljanje),</li>
		    <li>celovito spremljanje kakovosti, ki bo zajelo vhode (kakovost novih študentov, kakovost osebja, opreme, programov), procese (izobraževanje, raziskovanje, upravljanje) in izhode (kakovost študijskih rezultatov in dosežki diplomantov FERI, rezultati raziskovalnega dela),</li>
		    <li>uvajanje in izpopolnjevanje mehanizmov zagotavljanja kakovosti, kot stalna skrb vseh zaposlenih za kakovost,</li>
		    <li>jasna opredelitev vlog na področju spremljanja in zagotavljanja kakovosti (kdo izvaja aktivnosti, kdo je odgovoren, komu se poroča, kdo predlaga ukrepe),</li>
		    <li>stalno obveščanje javnosti.</li>
		</ul>
		Skrbnik strani: dr. Jože Pihler <br />
		Zadnja sprememba: 18. 3. 2013
		<ul class="povezave">
			<li><a href="#">Zunanja institucionalna evalvacija</a></li>
		    <li><a href="#">Samoevalvacijska poročila UM FERI</a></li>
		    <li><a href="#">Študentske ankete</a></li>
		    <li><a href="#">Ostala poročila KOK</a></li>
		    <li><a href="#">Letni načrti dela na področju kakovosti</a></li>
		    <li><a href="#">Zapisniki sestankov in sej komisije</a></li>
		    <li><a href="#">Pravilniki in navodila</a></li>
		    <li><a href="#">Člani komisije</a></li>
		    <li><a href="#">Povezave</a></li>
		</ul>
	</div>
</div>
