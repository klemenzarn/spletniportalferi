<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8" />
	<title>Feri</title>
	<link rel="icon" href="/feri/img/favicon.ico" type="image/x-icon">
	<?php
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('lightbox');
		echo $this->Html->script('jquery-1.8.2');
        echo $this->Html->script('lightbox');
		echo $this->Html->script('jquery-ui-1.9.0.custom.min');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<script type="text/javascript">
		$(document).ready(function(){
            $("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);  
            $("#featured").hover(function() {  
                $("#featured").tabs("rotate",0,true);  
            },  
            function(){  
                $("#featured").tabs("rotate",5000,true);  
            }  
            );  

		
			$("#menu2 li").each(function(){
				$(this).removeClass("izbrano");
			});
			var pathname = window.location.pathname;
			/*if (localStorage.izbrano)
			{
				$("#"+localStorage.izbrano).addClass("izbrano");
			} else {
				$("#index_b").addClass("izbrano");
			}*/
			var m = pathname.split('/');
			var id_name = m[2].toLowerCase();
			if(id_name == ""){
				$("#index_b").addClass("izbrano");
			} else {
				$("#"+id_name+"_b").addClass("izbrano");
			}
			
			$("#menu2 li").click(function(){
				localStorage.izbrano = this.id;
				
			});

			$("#napravapic").click(function(){
			//alert("KRNEKI");
			$.get('http://localhost/PICGESLO.txt', function(result) {
				if (result == 'ON') {
					alert('ON');
				} else if (result == 'OFF') {
					alert('OFF');
				} else {
					$( "#prijava_password" ).val(result);
					if($("#prijava_username").val()!=""){
						$( "#prijava_b1" ).click();
					}else{
						alert("Vpišite vaše uporabniško ime!");
					}
				}
			});
		});
			
			
			$(".plus").click(function(){
				if($(this).hasClass("minus"))
				{	
                    //$(this).parent().parent().find(".zacetek_novice").css("font-weight","normal");
					$(this).parent().parent().find(".vsebina_oglasna").slideUp();         
				}
				else
				{	
                    //$(this).parent().parent().find(".zacetek_novice").css("font-weight","bold");
					$(this).parent().parent().find(".vsebina_oglasna").slideDown(); 
				}
				$(this).toggleClass("minus"); //classu slika dodamo oz "zamenjamo" z slika2
			});
    $(".izbrisi_image").click(function(){
        $(this).parent().parent().hide();
    });

        $("#opac").click(function() {
			$("#novo_okno").css("display", "none");
			$("html").css("overflow", "auto");
			document.body.scroll = "yes";
		});
	
		$("#zapri_b1").click(function() {
				$("#novo_okno").css("display", "none");
				$("html").css("overflow", "auto");
				document.body.scroll = "yes";
			});
			
		$("#zapri_b2").click(function() {
				$("#novo_okno").css("display", "none");
				$("html").css("overflow", "auto");
				document.body.scroll = "yes";
			});
	
		$('#prijava_b').click(function(){

		$('#prijava').css("visibility","visible");
		$('#prijava').css("display","inline");
		$('#registracija').css("display","none");
		$('html').css("overflow", "hidden");
		
			//skrijemo scrollbar
			$('#novo_okno').css("display", "block");
			//pokažemo črno ozadje
			var vscroll = (document.all ? document.scrollTop : window.pageYOffset);
			$('#novo_okno').css("top", vscroll + "px");
			//na tej pa tej poziciji
			document.body.scroll = "no";
		});
		
		$('#register_b').click(function(){
			$("#registracija").css("visibility","visible");
			$("#prijava").css("display","none");
			$("#registracija").css("display","inline");
		});
		
				$('#prijava_b1').click(function(){
			var user = $('#prijava_username').val();
			var pas = $('#prijava_password').val();
						$.ajax({
			type : 'post',
			url : "/feri/index/prijava",
			data : "user="+user+"&pas="+pas,
			}).done(function(msg) {
				var test = msg.substr(0,4);
				if(test == "http"){
					window.location.href = msg;
				}else{
					$("#error1").empty();
					$("#error1").append(msg+"\n");
				}
			});
		});
		
		$(document).on({
     'keyup' : function(e) {
      if (e.keyCode == 13) {
       var user = $('#prijava_username').val();
       var pas = $('#prijava_password').val();
       $.ajax({
        type : 'post',
        url : "/feri/index/prijava",
        data : "user=" + user + "&pas=" + pas,
       }).done(function(msg) {
        var test = msg.substr(0, 4);
        if (test == "http") {
         window.location.href = msg;
        } else {
         $("#error1").empty();
         $("#error1").append(msg + "\n");
        }
       });

      } else if (e.keyCode == 27) {
       $("#novo_okno").css("display", "none");
       $("html").css("overflow", "auto");
       document.body.scroll = "yes";
      }
     }
    });
		
		$('#registracija_b').click(function(){
			var name = $('#registracija_name').val();
			var surname =$('#registracija_surname').val();
			var username =$('#registracija_username').val();
			var pas1 = $('#pass1').val();
			var pas2 = $('#pass2').val();
			var mail =$('#mail').val();
			
						$.ajax({
			type : 'post',
			url : "/feri/index/registracija",
			data : "name="+name+"&surname="+surname+"&username="+username+"&pas1="+pas1+"&pas2="+pas2+"&mail="+mail,
			}).done(function(msg) {
			if(msg=="OK")
			{
				window.location.href = "index";
			}
			else
			{
				$("#error2").empty();
				$("#error2").append(msg+"\n");
			}
			});
		});
		
		$("#sara").click(function(){
			winWidth = 345; 
			winHeight= 605; 	
			var openWin = window.open("http://sraka.uni-mb.si/esvetovalkaferiuporabnik/default.aspx","oknoSaraDruga","width="+winWidth+",height="+winHeight+",scrollbars=0,status=0"); 
			openWin.focus(); 
		});
		
		});
	</script>
</head>
  <body>
	<div id="glava">
		<?php echo $this->Html->image('logo_feri.png', array('alt' => 'logo', 'id' => 'logo_feri')); ?>
	</div>
	<div id="menu">
		<ul id="menu2">
		    <li id='index_b'><a href="/feri/index">Domov</a></li>
		    <li id='users_b'><a href="/feri/webgl">3D spletna stran</a></li>
			<li id='oglasna_b'><a href="/feri/oglasna">Oglasna deska</a></li>
			<li id='novice_b'><a href="/feri/novice">Novice</a></li>
			<li id='forum_b'><a href="/feri/forum">Forum</a></li>
            <li id='urnik_b'><a href="/feri/urnik">Urnik</a></li>
            <li id='koledar_b'><a href="/feri/koledar">Koledar</a></li>
            <li id='users_b'><a href="/feri/osebje">Osebje</a></li>
            <li id='users_b'><a href="/feri/galerija">Galerija</a></li>

            <?php
                if(!$this->Session->check("uporabnik"))
                {
					echo "<li id='prijava_b'><a>Prijava</a></li>";
			}else{
				if($this->Session->read('tip') == 2 || $this->Session->read('tip') == 3){
					echo "<li id='administracija_b'><a href='/feri/administracija'>Administracija</a></li>";
				}
			}
			?>
		</ul>
	</div>
		<div id="content">
<div id='change'><?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?> </div>
			<div id='desno'>
				<?php
                if($this->Session->check("uporabnik"))
                {
                    $uporabnik=$this->Session->read("uporabnik");
                    echo "<div id='prijavljen'>";
                    echo "<div style='margin:5px 0 5px 0;'>Prijavljen uporabnik</div>";
					$slika = $this->Session->read("slika");
                    echo $this->Html->image("Server/$uporabnik/$slika", array('alt' => 'ni slike','width'=>'140px'));
					//echo "<div style='margin:2px 0 0 0;'>".$uporabnik."</div>";
                    echo $this->Html->link($uporabnik,array('controller'=>'mojprofil','action'=>'index'),array("id"=>"ime_upo",'title'=>'Moj profil'));
                    echo $this->Form->create(null,array('url'=>array('controller'=>'Index','action'=>'odjava')));
                    echo "<input type='submit' value='Odjava' id='potrdi'/>";
					echo "</form>";
                    echo "</div>";
                }
                
                ?>
                <a class='zunanja_povezava' href="http://pridi-na-feri.si/" target="_blank">
                	<?php echo $this->Html->image('pridinaferi.jpg',array('width'=>'192px','title'=>'Pridi na FERI!'));?>
                </a>
                
                 <a class='zunanja_povezava' href="http://erasmus.feri.uni-mb.si/" target="_blank">
                	<?php echo $this->Html->image('erasmus.gif',array('width'=>'192px','title'=>'Erasmum na FERI'));?>
                </a>
                
                <a class='zunanja_povezava' href="http://www.magisterij-na-feri.si/" target="_blank">
                	<?php echo $this->Html->image('magisterij.jpg',array('width'=>'192px','title'=>'Magisterij na FERI'));?>
                </a>
                
                <a class='zunanja_povezava' href="http://ots.si//" target="_blank">
                	<?php echo $this->Html->image('ots2013.png',array('width'=>'192px','title'=>'OTS konterenca 2013'));?>
                </a>
                <?php $albumi=$this->requestAction('/Galerija/zadnji_trije');
				?>
                <div id="album2">
					<div id="naslov_the_shortest">Zadnji 3-je albumi</div>
					<div id="zadnji-trije">
						<?php 
						$i = 0;
						foreach($albumi as $album){
							$sumnik = array("Č","č","Š","š","Ž","ž","Ć","ć","Đ","đ"); 
							$sicnik = array("C","c","S","s","Z","z","C","c","Dj","dj"); 
							$naslov = str_replace($sumnik, $sicnik, $album['album']['naslov']);
							$idAlbuma = $album['album']['id'];
							$link = $this->Html->url(array("controller" => "galerija","action" => "album","?" => array("id" => $idAlbuma)));
						?>
						<div class="album2">
							<div id="naslov-albuma"><a href="<?php echo $link; ?>"><?php echo $album['album']['naslov']; ?></a></div>
						</div>
						<?php 
							$i++;
						} 
						?>
					</div>
					</div>
                
                
                </div>
<div style="clear:both;"></div>
		</div>

	<div id="noga">
		<?php echo $this->Html->image('logo_univerza.png', array('id' => 'logo_univerza')); ?>
	</div>
   <div id='novo_okno'>
        <div id='opac'></div>
        <div id='opravljanje'>
                <div id="prijava">
                <h2>Prijava</h2>
               	<label id="error1"></label><br/>
                   <label>*Uporabniško ime:</label><input type="text" id="prijava_username" name="uporabnisko"/><br/>
                   <label>*Geslo:</label><input type="password"  id="prijava_password" name="geslo"/><br/>
                <input type="submit" class="vpis" id="prijava_b1" value="Vpiši se"/>
                <input type="button" class="vpis" id="napravapic" value="Prijava z napravo" style='background-color:red;'/>
                <input type="button" class="vpis" id="register_b" value="Nimate še uporabniškega?"/>
                <a class="zapri" id=zapri_b1></a>
                </div>
                
                <div id='registracija' style="visibility:hidden">
                  <h2>Registracija</h2>
                  <label id="error2"></label><br/>
                <!--  <form method='POST' name='register' action="<?php echo $this->Html->url(array("controller"=>"index","action"=>"registracija"));?>"> -->
                  <label>*Ime:</label><input type="text" id="registracija_name" name="ime"/><br/>
                  <label>*Priimek:</label><input type="text" id="registracija_surname" name="priimek"/><br/>
                  <label>*Uporabniško ime:</label><input type="text" id="registracija_username" name="uporabnisko1"/><br/>
                  <label>*Geslo:</label><input type="password" id="pass1" name="geslo1"/><br/>
                  <label>*Ponovno geslo:</label><input type="password" id="pass2" name="ponovno_geslo"/><br/>
                  <label>*Mail:</label><input type="text" id="mail" name="mail"/><br/>
                <input type="submit" value="Potrdi" class='vpis' id="registracija_b"/> 
               <a class="zapri1" id=zapri_b2></a>
            <!--   </form> -->
                </div>
        </div>
    </div>
		<?php
echo $this->Js->writeBuffer();
echo $this->element('sql_dump'); ?>
  </body>



</html>
