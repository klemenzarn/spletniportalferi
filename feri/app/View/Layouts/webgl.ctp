<html>
	<head>
		<title>My first Three.js app</title>
		<style>canvas { width: 100%; height: 100% }</style>
	</head>
	<body>
		<script src="viewer/three.js"></script>
		<script src="viewer/TrackballControls.js"></script>
		<script src="viewer/OBJLoader.js"></script>
		<script src="viewer/Detector.js"></script>
		<script src="viewer/PointerLockControls.js"></script>
		<script src="viewer/stats.min.js"></script>
<div id="blocker">

                        <div id="instructions">
                                <span style="font-size:40px">Click to play</span>
                                <br />
                                (W, A, S, D = Move, SPACE = Jump, MOUSE = Look around)
                        </div>

                </div>

		<script>
			var cube;
			var scene, camera, renderer;
			var controls;
			var WIDTH = window.innerWidth;
			var HEIGHT = window.innerHeight;
			var clock = new THREE.Clock();
			var loader, texture;
			var time = Date.now();

			                        var blocker = document.getElementById( 'blocker' );
                        var instructions = document.getElementById( 'instructions' );

                        // http://www.html5rocks.com/en/tutorials/pointerlock/intro/

                        var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

                        if ( havePointerLock ) {

                                var element = document.body;

                                var pointerlockchange = function ( event ) {

                                        if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {

                                                controls.enabled = true;

                                                blocker.style.display = 'none';

                                        } else {

                                                controls.enabled = false;

                                                blocker.style.display = '-webkit-box';
                                                blocker.style.display = '-moz-box';
                                                blocker.style.display = 'box';

                                                instructions.style.display = '';

                                        }

                                }

                                var pointerlockerror = function ( event ) {

                                        instructions.style.display = '';

                                }

                                // Hook pointer lock state change events
                                document.addEventListener( 'pointerlockchange', pointerlockchange, false );
                                document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
                                document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );

                                document.addEventListener( 'pointerlockerror', pointerlockerror, false );
                                document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
                                document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );

                                instructions.addEventListener( 'click', function ( event ) {

                                        instructions.style.display = 'none';

                                        // Ask the browser to lock the pointer
                                        element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;

                                        if ( /Firefox/i.test( navigator.userAgent ) ) {

                                                var fullscreenchange = function ( event ) {

                                                        if ( document.fullscreenElement === element || document.mozFullscreenElement === element || document.mozFullScreenElement === element ) {

                                                                document.removeEventListener( 'fullscreenchange', fullscreenchange );
                                                                document.removeEventListener( 'mozfullscreenchange', fullscreenchange );

                                                                element.requestPointerLock();
                                                        }

                                                }

                                                document.addEventListener( 'fullscreenchange', fullscreenchange, false );
                                                document.addEventListener( 'mozfullscreenchange', fullscreenchange, false );

                                                element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;

                                                element.requestFullscreen();

                                        } else {

                                                element.requestPointerLock();

                                        }

                                }, false );

                        } else {

                                instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';

                        }


			init();
			animate();
			
			function init(){
				//naredimo sceno
				scene = new THREE.Scene();

				//kamero
				camera = new THREE.PerspectiveCamera(45, WIDTH/HEIGHT,0.1,1000);
				camera.position.z = 10;
				camera.position.y = 10;

				//naredimo kontrolle (w,a,s,d miška, arrows itd...)
				/*
				controls = new THREE.FirstPersonControls( camera );

				controls.movementSpeed = 50;
				controls.lookSpeed = 0.1;
*/

				controls = new THREE.PointerLockControls( camera );
                scene.add( controls.getObject() );
				//lights
				var ambient = new THREE.AmbientLight(0x444444);
				scene.add(ambient);

				/*var directionalLight = new THREE.DirectionalLight( 0xffffff, 2 );
				directionalLight.position.set( 0, 1, 0 );
				scene.add( directionalLight );*/

				//SpotLight(hex, intensity, distance, angle, exponent)
				var spotLight = new THREE.SpotLight( 0xffffff, 2, 1000,50,20);
				spotLight.position.set( 0, 100, 0 );


				scene.add( spotLight );

				//naredimo loading manager za texture, .obj 
				var manager = new THREE.LoadingManager();
				manager.onProgress = function(item, loaded, total) {
					console.log(item, loaded, total);
				};

				texture = new THREE.Texture();

				//naložimo texturo
				var loader1 = new THREE.ImageLoader(manager);
				loader1.load('texture.bmp', function(image) {
					texture.image = image;
					texture.needsUpdate = true;
					texture.magFilter = THREE.NearestFilter;
					texture.minFilter = THREE.NearestMipMapLinearFilter;
				});
				loader = new THREE.OBJLoader(manager);

				//naložimo objekte
				for(var i=0;i<10;i++) {
					for(var j=0;j<10;j++){
						addObject('suzanne.obj', i*7-30,0.5,j*7-30);
					}
					
				}

				

				//rišemo stvari
				var geometry = new THREE.CubeGeometry(1,1,1);

				var material = new THREE.MeshBasicMaterial({color: 0x00ff00});
				cube = new THREE.Mesh(geometry,material);
				scene.add(cube);

				/* Floor  */    
				var geometry1 = new THREE.PlaneGeometry( 1000, 1000, 1, 1 );
				var material1 = new THREE.MeshBasicMaterial( { color: 0xD0D0D0 } );
				var floor = new THREE.Mesh( geometry1, material1 );
				floor.material.side = THREE.DoubleSide;
				floor.rotation.x = de2ra(90);
				floor.position.y = -0.5;
				scene.add( floor );


				//naredimo renderer
				renderer = new THREE.WebGLRenderer();
				renderer.setSize(WIDTH,HEIGHT);

				//canvas pripnemo v body
				document.body.appendChild(renderer.domElement);
			}

			function addObject(ime, x,y,z){
				loader.load(ime, function(event) {
					var object = event;
					object.traverse(function(child) {
						if ( child instanceof THREE.Mesh) {
							child.material.map = texture;
						}
					});
						object.position.x = x;
						object.position.y = y;
						object.position.z = z;
						scene.add(object);
				});
			}

			function sleep(delay) {
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay);
      }	

			function animate(){
				render();
			}

			function render() {
				requestAnimationFrame(render);
				cube.rotation.y += 0.1;
				 controls.isOnObject( false );

                               // ray.ray.origin.copy( controls.getObject().position );
                               // ray.ray.origin.y -= 10;

//                                var intersections = ray.intersectObjects( objects );
/*
                                if ( intersections.length > 0 ) {

                                        var distance = intersections[ 0 ].distance;

                                        if ( distance > 0 && distance < 10 ) {

                                                controls.isOnObject( true );

                                        }

                                }*/

                                controls.update( Date.now() - time );
				//controls.update( clock.getDelta() );
				renderer.render(scene, camera);
				time = Date.now();
			}

			function de2ra(degree){
				return degree*(Math.PI/180);
			}
		</script>
	</body>
</html>