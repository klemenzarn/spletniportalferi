<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
    <!-- ime in priimek -->
<div class='short_div'>
	<div id='naslov_short'><div id='ime_naslova_short'>Osnovni podatki</div></div>
    <div id='podatki'>
        Ime in priimek: <?php echo $podatki[0]['users']['Ime']." ".$podatki[0]['users']['Priimek']; ?> <br />
        <?php 
            if($podatki[0]['users']['Mail'] == ""){
                echo "Email še ni nastavljen.";
            } else echo "Email: ".$podatki[0]['users']['Mail'];
        ?> <br />
        <?php 
            if($podatki[0]['users']['Datum_rojstva'] == ""){
                echo "Datum rojstva še ni nastavljen.";
            } else echo "Datum rojstva: ".$podatki[0]['users']['Datum_rojstva'];
        ?> <br />
        <?php 
            if($podatki[0]['users']['Vpisna'] == ""){
                echo "Če ste študent in nimate vpisne številke se obrnite na administratorja.";
            } else echo "Vpisna: ".$podatki[0]['users']['Vpisna'];
        ?> <br />
        Slika: <br />
        <?php echo "<div id='slika_mojp'>".$this->Html->image('Server/'.$podatki[0]['users']['Uporabnisko'].'/'.$podatki[0]['users']['Slika'], array('alt' => 'ni slike','width'=>'200px'))."</div>"; ?> <br />
    </div>
    </div>
    <div class='short_div'>
	<div id='naslov_short'><div id='ime_naslova_short'>Spremeni podatke</div></div>
        <div id='podatki'>
            <span class='spremeni_nas'>Spremeni osnovne podatke:</span>
            <?php 
                echo $this->Form->create(null,array('url'=>array("controller"=>"Mojprofil","action"=>"uredimail")));
            ?>
           <div class='podatki_sprememba'><div class='podatki_inp'><input placeholder="Vpiši e-poštni naslov" type="email" name="mail" value="<?php echo $podatki[0]['users']['Mail'] ?>" /> </div> </div>
              <?php 
                echo $this->Form->end("Spremeni email");
            ?>
             <?php 
                echo $this->Form->create(null,array('url'=>array("controller"=>"Mojprofil","action"=>"uredirojdan")));
            ?>
           <div class='podatki_sprememba'><div class='podatki_inp'><input placeholder="Vpiši datum rojstva" type="text" name="rojdan" value="<?php echo $podatki[0]['users']['Datum_rojstva'] ?>"/> </div></div>
            <?php 
                echo $this->Form->end("Spremeni datum");
            ?>
             <form method='POST' name='form' enctype="multipart/form-data" action="mojprofil/uredisliko">
            <div class='podatki_sprememba'> <div class='podatki_inp'><input type="file" placeholder="Naloži profilno sliko" name="datoteka" accept="image/*" /> </div></div>
           <?php 
                echo $this->Form->end("Spremeni sliko");
            ?>
            <br />
            <span class='spremeni_nas'>Spremeni geslo:</span>
            <?php 
                echo $this->Form->create(null,array('url'=>array("controller"=>"Mojprofil","action"=>"spremenigeslo")));
            ?>
            <div class='podatki_sprememba'><div class='podatki_inp1'><input placeholder="Vpiši trenutno geslo" type="password" name="prejpass" value=""/> </div> </div>
        
        <div class='podatki_sprememba'><div class='podatki_inp1'><input placeholder="Vpiši novo geslo" type="password" name="novoena" />            </div> 
        </div>
        <div class='podatki_sprememba'><div class='podatki_inp1'><input placeholder="Novo geslo vpiši še enkrat"  type="password" name="novodva" />         </div> 
            </div>
             <?php 
                echo $this->Form->end("Spremeni geslo");
            ?>
             <?php
              echo $this->Html->link("Uredi več podatkov",array("controller"=>"users","action"=>"shrani",$podatki[0]['users']['ID'])); 
               echo " ".$this->Html->link("Sodelujoči",array("controller"=>"users","action"=>"sodelujoci"));
              ?>
        </div>
       
    </div>

	<div class='short_div'>
	<div id='naslov_short'><div id='ime_naslova_short'>Prva stran</div></div>
        <div id='podatki3'>
        	<?php
        		echo $this->Form->create(null,array("url"=>array("controller"=>"mojprofil","action"=>"spremeniizgled")));
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "novice"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='novice' value='da' checked='checked' /> <label for='novice'>Novice</label> <br />";
				} else {
					echo "<input type='checkbox' name='novice' value='da' /> <label for='novice'>Novice</label> <br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "oglasna_deska"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='deska' value='da' checked='checked' /> <label for='deska'>Oglasna deska</label><br />";
				} else {
					echo "<input type='checkbox' name='deska' value='da' /> <label for='deska'>Oglasna deska</label><br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "urnik"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='urnik' value='da' checked='checked' /> <label for='urnik'>Urnik</label><br />";
				} else {
					echo "<input type='checkbox' name='urnik' value='da'/> <label for='urnik'>Urnik</label><br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "koledar"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='koledar' value='da' checked='checked' /> <label for='koledar'>Koledar</label><br />";
				} else {
					echo "<input type='checkbox' name='koledar' value='da' /> <label for='koledar'>Koledar</label><br />";
				}
				
				if($staticno == "novice2"){
					echo "<input type='checkbox' name='novice2' value='da' checked='checked' /> <label for='novice2'>Novice od drugod</label><br />";
				} else {
					echo "<input type='checkbox' name='novice2' value='da' /> <label for='novice2'>Novice od drugod</label><br />";
				}

				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "tvspored"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='tvspored' value='da' checked='checked' /> <label for='tvspored'>Tv spored</label><br />";
				} else {
					echo "<input type='checkbox' name='tvspored' value='da'/> <label for='tvspored'>Tv spored</label><br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "zapogl"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='zapogl' value='da' checked='checked' /> <label for='zapogl'>Zaposlitveni oglasi</label><br />";
				} else {
					echo "<input type='checkbox' name='zapogl' value='da'/> <label for='zapogl'>Zaposlitveni oglasi</label><br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "kino"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='kino' value='da' checked='checked' /> <label for='kino'>Kino</label><br />";
				} else {
					echo "<input type='checkbox' name='kino' value='da'/> <label for='kino'>Kino</label><br />";
				}
				
				$obstaja = false;
				foreach ($razporeditev as $raz) {
					if($raz == "dogodki"){
						$obstaja = true;
					}
				}
				if($obstaja == true){
					echo "<input type='checkbox' name='dogodki' value='da' checked='checked' /> <label for='dogodki'>Dogodki</label><br />";
				} else {
					echo "<input type='checkbox' name='dogodki' value='da'/> <label for='dogodki'>Dogodki</label><br />";
				}
			?>
			
			<input type='submit' value='Shrani' id='potrdi' />
			</form>
        </div>
    </div>

    <?php if($urniki != "ni" && !empty($urniki)){ ?>
    <div id='urniki'>  
        <div id='naslov'><div id='ime_naslova'>Moj urnik</div></div>
            <div id='urnik_div'>
            	<?php
             echo $this->Form->create(null,array('url'=>array("controller"=>"Mojprofil","action"=>"posodobismer")));
            ?>
            Posodobi smer: 
         <select name='branch_id'>
                  <?php foreach($vsi as $branch): ?>
                  <option value='<?php echo $branch[0]['Branch_Id']; ?>'><?php echo $branch[0]['Name']."  ".$branch[0]['Year'].". letnik"; ?></option>
                  <?php endforeach; ?>
              </select>
        <?php
            echo $this->Form->end("Posodobi smer");
			?>
                <table class='tabela_urniki'>
                    <tr>
                        <td>Pon</td>
                        <td>Tor</td>
                        <td>Sre</td>
                        <td>Čet</td>
                        <td>Pet</td>
                    </tr>
                    <?php 
                        $max = 0;
                        $velikost = array(0,0,0,0,0);
                        for($i = 0; $i < count($urniki); $i++){
                               $velikost[$urniki[$i]['dan'] - 1]++;
                        }
                        for($i = 0; $i < 5; $i++){
                            if($velikost[$i] > $max){
                                $max =  $velikost[$i]; 
                            }
                        }
                        for($k = 0; $k < $max; $k++){
                            echo "<tr>";
                            
                            for($l = 1; $l <= 5; $l++){
                                echo "<td>";
                                $izbris = 0;
                                foreach($urniki as $key => $urnik){
                                    if($urnik['dan'] == $l){
                                         echo "<span style='font-weight:bold;font-style:italic; font-size: 12px;'>".$urnik['zacetek']." - ". $urnik['konec']."</span></br>";
                                        echo "<span style='font-weight:bold; font-size: 12px;'>".$urnik['predmet']."</span></br>";
                                        echo $urnik['ucilnica'].", ".$urnik['vrsta']."</br>";
                                        echo $urnik['ime']." ".$urnik['priimek']."</br>";
                                        unset($urniki[$key]);
                                        break;
                                    }
                                }
                                echo "</td>";
                            }
                            
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        <div id='urnik_noga'>
        <?php echo $this->Html->link("Moje obveznosti",array("controller"=>"urnik","action"=>"obveznosti"),array("id"=>"link_obv")); ?>
         <?php echo $this->Html->link("Izvozi moj urnik",array("controller"=>"urnik","action"=>"izvoziMojUrnikVPDF"),array("id"=>"link_izvozi")); ?>
       </div>
        </div>
     <?php } else {
        if($tip == "1" || $tip == "2" || $tip == "3"){
            echo "<div id='urniki'>"; 
            echo "<div id='naslov'><div id='ime_naslova'>Moj urnik</div></div>";
            echo "<div id='urnik_div'>";
            echo "<span>Izberite si svojo smer:</span>"; 
            echo $this->Form->create(null,array('url'=>array("controller"=>"Mojprofil","action"=>"shranismer")));
            ?>
            
         <select name='branch_id'>
                  <?php foreach($vsi as $branch): ?>
                  <option value='<?php echo $branch[0]['Branch_Id']; ?>'><?php echo $branch[0]['Name']."  ".$branch[0]['Year'].". letnik"; ?></option>
                  <?php endforeach; ?>
              </select>
        <?php
            echo $this->Form->end("Shrani smer");
            echo "</div></div>";
        }
    }?>
</div>