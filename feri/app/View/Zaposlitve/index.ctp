<div id='levo'>
<div id="cssmenu">
  <ul>
      <li id='neki'><a href="/feri/Oglasi">Iskanje zaposlitve</a></li>
   <li id='neki'><a href="/feri/informacije/Predstavitev_splosne_informacije">Predstavitev in splošne informacije</a></li>
   <li id='neki'><a href="/feri/informacije/Informacije_za_studente">Informacije za študente</a></li>
   <li id='neki'><a href="/feri/informacije/Studijski_programi">Študijski programi</a></li>
   <li id='neki'><a href="/feri/informacije/Izredni_studij">Izredni študij</a></li>
   <li id='neki'><a href="/feri/informacije/Podiplomski_studij">Podiplomski študij</a></li>
   <li id='neki'><a href="/feri/informacije/Obladovanje_kakovosti">Obvladovanje kakovosti</a></li>
   <li id='neki'><a href="/feri/informacije/Raziskovalna_dejavnost">Raziskovalna dejavnost</a></li>
   <li id='neki'><a href="http://www.ktfmb.uni-mb.si/" target="_blank">Knjižnica tehniških fakultet</a></li>
   <li id='neki'><a href="/feri/informacije/Javna_narocila">Javna naročila</a></li>
   <li id='neki'><a href="http://alumni.feri.uni-mb.si/" target="_blank">Alumni</a></li>
   <div id="meni_naslov">Inštituti</div>
   <li id='neki'><a href="http://www.au.feri.uni-mb.si/">Inštitut za avtomatiko</a></li>
   <li id='neki'><a href="http://ietk.feri.um.si/" target="_blank">Inštitut za elektroniko in telekomunikacije</a></li>
   <li id='neki'><a href="http://ii.uni-mb.si/">Inštitut za informatiko</a></li>
   <li id='neki'><a href="http://www.pe.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za močnostno elektroniko</a></li>
   <li id='neki'><a href="http://www.cs.feri.uni-mb.si/podrocje.aspx" target="_blank">Inštitut za računalništvo</a></li>
   <li id='neki'><a href="http://www.ro.feri.uni-mb.si/portal/index.php" target="_blank">Inštitut za robotiko</a></li>
   <li id='neki'><a href="http://www.mp.feri.uni-mb.si/" target="_blank">Inštitut za matematiko in fiziko</a></li>
   <li id='neki'><a href="http://medijske.uni-mb.si/" target="_blank">Inštitut za medijske komunikacije</a></li>
   <div id="meni_naslov">Vprašaj Saro</div>
  </ul>
 </div> 
	
	<div id='sara'>
		<?php echo $this->Html->image('sara.jpg',array('width'=>"200px",'title'=>'Vprašaj Saro!','id'=>'sara')); ?>
	</div>
</div>
<div id='vse'>
	<div id="oglasi">Prosta delovna mesta</div>
		<div id="forma_oglasi">
			<form method='POST' name='webService' action="<?php echo $this->Html->url(array("controller"=>"Zaposlitve","action"=>"index"));?>">
			<div id="oglasi1">PROSTA DELOVNA MESTA, ISKANJE ZAPOSLITVE</div><br/>
			<select id="select5" name='select1'>
	    		<option value="" selected="selected">- Vsa področja -</option>
							                        <option  value="1">Administracija</option>
							                        <option  value="4">Elektrotehnika, Elektronika, Telekomunikacije</option>
							                        <option  value="7">Informatika, Programiranje</option>
							                        <option  value="11">Kreativa, Design</option>
							                        <option  value="12">Management, Poslovno svetovanje, Organizacija</option>
							                        <option  value="14">Novinarstvo, Mediji, Založništvo</option>
							                        <option  value="21">Strojništvo, Metalurgija, Rudarstvo</option>
							                        <option  value="28">Znanost, Tehnologija, Raziskave in razvoj</option>
	  		</select>
	  		<select id="select5" name='select2'>
	    		<option value="">- Vse regije -</option>
							                        <option  value="1">Gorenjska - KR</option>
							                        <option  value="2">Goriška - GO</option>
							                        <option  value="3">Jugovzhodna Slovenija - NM</option>
							                        <option  value="4">Koroška - SG</option>
							                        <option  value="5">Notranjsko - kraška - PO</option>
							                        <option  value="6">Obalno - kraška - KP</option>
							                        <option  value="7">Osrednjeslovenska - LJ</option>
							                        <option  value="8">Podravska - MB</option>
							                        <option  value="9">Pomurska - MS</option>
							                        <option  value="10">Savinjska - CE</option>
							                        <option  value="11">Spodnjeposavska - KK</option>
							                        <option  value="13">Tujina</option>
							                        <option  value="12">Zasavska - LJ</option>
	  		</select>
			<input type="submit" value="POIŠČI" id="poisci"/>
			<select id="select1" name='select3'>
	                            <option value="">- Vse stopnje izobrazbe -</option>
									                            <option  value="1">I.</option>
									                            <option  value="2">II.</option>
									                            <option  value="3">III.</option>
									                            <option  value="4">IV.</option>
									                            <option  value="5">V.</option>
									                            <option  value="6">VI/1.</option>
									                            <option  value="7">VI/2.</option>
									                            <option  value="8">VII.</option>
									                            <option  value="9">VIII/1.</option>
									                            <option  value="10">VIII/2.</option>
	  		</select>
			</form>
		</div>
		<br/>
		<br/>
		<div id='okvir'>
		<?php
				if(!empty($data)){
					if(!empty($data['DobiZaPodrocjeRegijoIzobrazboResult']['string']))
					{
						$st_oglasov=count($data['DobiZaPodrocjeRegijoIzobrazboResult']['string']);
						
						if($st_oglasov==1)
						{
								list($naslov, $datum, $opis,$kraji_dela,$delodajalec,$izobrazba,$naloge,$placilo,$povezava) = split('[*]', $data['DobiZaPodrocjeRegijoIzobrazboResult']['string']);
								echo "<a class='oglasi_naslov'  href=".$povezava.">".$naslov."<span id='datum'>".$datum."<span/></a>";
								echo "<br/>";
								echo "<br/>";
								echo "<b>".$delodajalec."</b>, ".$kraji_dela."<br/>";
								echo "<b>Opis:</b><br/>".$opis."<br/>";
								if($naloge!="")
								{
									echo "<b>Naloge in odgovornosti:</b>";
									echo "<br/>";
									echo $naloge;
									echo "<br/>";
								}
								if($izobrazba!="")
								{
								echo "<b>Izobrazba:</b>";
								echo "<br/>";
								echo $izobrazba;
								echo "<br/>";
								}
								if($placilo!="")
								{
								echo "<b>Placilo:</b>";
								echo "<br/>";
								echo $placilo;
								echo "<br/>";
								}
								echo "<br/>";
						}
						else
						{
							for($i=0;$i<$st_oglasov;$i++)
							{
								list($naslov, $datum, $opis,$kraji_dela,$delodajalec,$izobrazba,$naloge,$placilo,$povezava) = split('[*]', $data['DobiZaPodrocjeRegijoIzobrazboResult']['string'][$i]);
								echo "<a class='oglasi_naslov'  href=".$povezava.">".$naslov."<span id='datum'>".$datum."<span/></a>";
								echo "<br/>";
								echo "<br/>";
								echo "<b>".$delodajalec."</b>,".$kraji_dela."<br/>";
								echo "<b>Opis:</b><br/>".$opis."<br/>";
								if($naloge!="")
								{
									echo "<b>Naloge in odgovornosti:</b>";
									echo "<br/>";
									echo $naloge;
									echo "<br/>";
								}
								if($izobrazba!="")
								{
								echo "<b>Izobrazba:</b>";
								echo "<br/>";
								echo $izobrazba;
								echo "<br/>";
								}
								if($placilo!="")
								{
								echo "<b>Placilo:</b>";
								echo "<br/>";
								echo $placilo;
								echo "<br/>";
								}
								echo "<br/>";
							}
						}
					}
					else {
						echo "Za izbrane kriterije trenutno ni objavljenih aktualnih oglasov. Poskusite spremeniti iskalne kriterije.";
					}
				}else if(isset($message)){
					print_r($message);
					echo "Prišlo je do napake!";
				}
		?>
		</div>
</div>
